import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
plt.rcParams.update({'font.size': 4})




def plot_hist_from_dict_prob(dict, probabilities, n):
    fig, ax = plt.subplots(4,2,figsize=(4, 6))
    for index in range(len(probabilities)):
        prob = probabilities[index]
        i = index // 2
        j = index % 2
        imgs = dict[str(prob)]
        bir_tot = imgs[0]
        dea_tot = imgs[1]
        xedges = np.linspace(0, n, n+2)
        yedges = np.linspace(0, n, n+2)
        hist, xedges, yedges = np.histogram2d(bir_tot, dea_tot, bins = (xedges,yedges))
        loghist = np.log(hist+1)
        loghist = gaussian_filter(loghist, sigma=1)
        # heatmap = plt.pcolor(loghist.T, vmax = 10)
        im = ax[i,j].pcolor(loghist.T, vmax = 10)
        # if j == 1:
        #     ax[i,j].axes.get_yaxis().set_visible(False)
        # if i != 3:
        #     ax[i,j].axes.get_xaxis().set_visible(False)
        ax[i,j].set_title('p = '+str(prob), fontweight='bold')
    for ax in fig.get_axes():
        ax.label_outer()
    cbar_ax = fig.add_axes([0.91, 0.18, 0.03, 0.4])
    fig.colorbar(im, cax=cbar_ax)
    plt.savefig('plot_prob.pdf', format = 'pdf')


def plot_hist_from_dict_num_vertices(dict, probabilities, n):
    fig, ax = plt.subplots(4,2,figsize=(4, 6))
    for index in range(len(probabilities)):
        prob = probabilities[index]
        i = index // 2
        j = index % 2
        imgs = dict[str(prob)]
        bir_tot = imgs[0]
        dea_tot = imgs[1]
        xedges = np.linspace(0, n, n+2)
        yedges = np.linspace(0, n, n+2)
        hist, xedges, yedges = np.histogram2d(bir_tot, dea_tot, bins = (xedges,yedges))
        loghist = np.log(hist+1)
        loghist = gaussian_filter(loghist, sigma=1)
        # heatmap = plt.pcolor(loghist.T, vmax = 10)
        im = ax[i,j].pcolor(loghist.T, vmax = 10)
        # if j == 1:
        #     ax[i,j].axes.get_yaxis().set_visible(False)
        # if i != 3:
        #     ax[i,j].axes.get_xaxis().set_visible(False)
        ax[i,j].set_title('n = '+str(prob), fontweight='bold')
    for ax in fig.get_axes():
        ax.label_outer()
    cbar_ax = fig.add_axes([0.91, 0.18, 0.03, 0.4])
    fig.colorbar(im, cax=cbar_ax)
    plt.savefig('plot_num_vertices.pdf', format = 'pdf')





def plot_hist_from_dict_num_orientations(dict, probabilities, n):
    fig, ax = plt.subplots(7,2,figsize=(4, 6))
    for index in range(len(probabilities)):
        prob = probabilities[index]
        i = index // 2
        j = index % 2
        print('prob = '+str(prob)+',  i = '+str(i)+',  j = '+str(j))
        imgs = dict[str(prob)]
        bir_tot = imgs[0]
        dea_tot = imgs[1]
        xedges = np.linspace(0, n, n+2)
        yedges = np.linspace(0, n, n+2)
        hist, xedges, yedges = np.histogram2d(bir_tot, dea_tot, bins = (xedges,yedges))
        loghist = np.log(hist+1)
        loghist = gaussian_filter(loghist, sigma=1)
        # heatmap = plt.pcolor(loghist.T, vmax = 10)
        im = ax[i,j].pcolor(loghist.T)
        fig.colorbar(im, ax=ax[i,j])
        # if j == 1:
        #     ax[i,j].axes.get_yaxis().set_visible(False)
        # if i != 3:
        #     ax[i,j].axes.get_xaxis().set_visible(False)
        ax[i,j].set_title('n_orient = '+str(prob), fontweight='bold')
    for ax in fig.get_axes():
        ax.label_outer()
    # cbar_ax = fig.add_axes([0.91, 0.18, 0.03, 0.4])
    # fig.colorbar(im, cax=cbar_ax)
    plt.tight_layout()
    plt.savefig('plot_num_orient.pdf', format = 'pdf')



# probabilities = [0.10,0.15,0.20,0.25,0.30,0.35,0.40,0.45]
# per_tot = np.load('dictionary_prob.npy', allow_pickle = True)
# per_tot = per_tot.tolist()
# plot_hist_from_dict_prob(per_tot, probabilities, 55)
#
#
#
# num_vertices = [15,20,25,30,35,40,45,50]
# per_tot = np.load('dictionary_num_vertices.npy', allow_pickle = True)
# per_tot = per_tot.tolist()
# plot_hist_from_dict_num_vertices(per_tot, num_vertices, 55)



num_orientations = [100,200,500,1000,2000,5000,10000,20000,50000,100000,200000,500000,750000,1000000]
per_tot = np.load('dictionaries/dictionary_num_orientations.npy', allow_pickle = True)
per_tot = per_tot.tolist()
plot_hist_from_dict_num_orientations(per_tot, num_orientations, 55)
