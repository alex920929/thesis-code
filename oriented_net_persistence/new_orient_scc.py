from __future__ import absolute_import, division, print_function
import sys
from sys import platform
sys.path.append('../')
# if platform == "darwin":
#     import matplotlib
#     matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
from Dir_Wgraph import Dir_Wgraph
from net_persistence.Wgraph import Wgraph
import networkx as nx
import time
from net_persistence.persistence_diagram import PersistenceDiagram
from mpl_toolkits import mplot3d
from itertools import combinations
from scipy.ndimage import gaussian_filter
plt.rcParams.update({'font.size': 4})

def get_all_orientations(num_edges):
    # Returns all the possible orientation of a graph
    index = np.arange(2**num_edges)
    all_orientations = np.zeros((2**num_edges,num_edges))
    for i in range(num_edges):
        all_orientations[:,i] = np.mod(index,2)
        index = (index - np.mod(index,2))/2
    return all_orientations

def get_orientations_sample(num_edges, num_orient):
    # Returns a sample of the whole set of orientations of a graph
    # Note: Since the number of orientations is usually big, we do not check
    #       that orientations are unique. In this scenario it is statistically
    #       irrelevant.
    orientations_sample = (np.random.uniform(0,1,(num_orient,num_edges))<.5)
    return orientations_sample

def orient_graph(example, all_orientations, i):
    # Create the graph with the i-th orientation.
    orient = all_orientations[i]
    oriented_example = [[e1, e2, w] if orient[i] == 0 else [e2, e1, w]
						for i, (e1, e2, w) in enumerate(example)]
    graph = Dir_Wgraph(oriented_example)
    graph.build_graph()
    return graph

def get_random_graph(num_vertices, prob, seed = None, Starting_label=0):
    # Create a random graph G(n,p), with n = num_vertices, p=prob.
    example = []
    c = np.random.uniform(0,1,size = (num_vertices, num_vertices))
    w = np.random.uniform(0,50,size = (num_vertices, num_vertices))
    for i in range(Starting_label, Starting_label+num_vertices):
        for k in range(i+1, Starting_label+num_vertices):
            if c[i-Starting_label,k-Starting_label] < prob:
                val = int(w[i-Starting_label,k-Starting_label])
                example.append((i, k, val))
    return example

def plot_hist(bir_tot, dea_tot, n):
    # Plot the histogram
    xedges = np.linspace(0, n, n+2)
    yedges = np.linspace(0, n, n+2)
    hist, xedges, yedges = np.histogram2d(bir_tot, dea_tot, bins = (xedges,yedges))
    loghist = np.log(hist+1)
    loghist = gaussian_filter(loghist, sigma=.5)
    heatmap = plt.pcolor(loghist.T)
    plt.colorbar(heatmap)


def plot_hist_from_dict(dict, probabilities, n, num_vertices, proba):
    fig, ax = plt.subplots(7,2,figsize=(4, 6))
    for index in range(len(probabilities)):
        prob = probabilities[index]
        i = index // 2
        j = index % 2
        imgs = dict[str(prob)]
        bir_tot = imgs[0]
        dea_tot = imgs[1]
        xedges = np.linspace(0, n, n+2)
        yedges = np.linspace(0, n, n+2)
        hist, xedges, yedges = np.histogram2d(bir_tot, dea_tot, bins = (xedges,yedges))
        loghist = np.log(hist+1)
        loghist = gaussian_filter(loghist, sigma=.5)
        # heatmap = plt.pcolor(loghist.T, vmax = 10)
        im = ax[i,j].pcolor(loghist.T)
        fig.colorbar(im, ax=ax[i,j])
        # if j == 1:
        #     ax[i,j].axes.get_yaxis().set_visible(False)
        # if i != 3:
        #     ax[i,j].axes.get_xaxis().set_visible(False)
        ax[i,j].set_title('n_orient = '+str(prob), fontweight='bold')
    for ax in fig.get_axes():
        ax.label_outer()
    # cbar_ax = fig.add_axes([0.91, 0.18, 0.03, 0.4])
    # fig.colorbar(im, cax=cbar_ax)
    plt.savefig('plot/plot_'+str(num_vertices)+'_'+str(proba), format = 'pdf')


def draw_graph(example):
    graph = Wgraph(example)
    graph.build_graph()
    pos=nx.spring_layout(graph.G)
    nx.draw_networkx_nodes(graph.G, pos, node_size=30)
    nx.draw_networkx_edges(graph.G, pos)
    labels = nx.get_edge_attributes(graph.G,'weight')
    nx.draw_networkx_edge_labels(graph.G,pos,edge_labels=labels)

def eblocks_PD(graph):
    graph.build_filtered_subgraphs()
    graph.get_edge_blocks_filtration()
    graph.ranging_persistence()
    graph.plot_ranging_persistence_diagram()


def get_random_comm(N_c, size_comm, p_intra, p_inter):
    example = []
    for i in range(N_c):
        ex = get_random_graph(size_comm, p_intra, Starting_label = i*size_comm)
        for k in range(len(ex)):
            example.append(ex[k])
    x = range(N_c)
    y = range(size_comm)

    for xi in combinations(x,2):
        i = xi[0]
        j = xi[1]

        for yi in combinations(y,2):
            k = yi[0]
            l = yi[1]
            if np.random.uniform(0,1) < p_inter:
                example.append((i*size_comm + k, j*size_comm +l,
                                int(np.random.uniform(0,50))))

    return example




if __name__ == "__main__":
    ## To import the graph of the first version of beyond
    # from example_graphs import beyond as example
    probabilities = [0.10,0.15,0.20,0.25,0.30,0.35,0.40,0.45]
    num_vertices_tot = [15,20,25,30,35,40,45,50]
    num_orientations = [100,200,500,1000,2000,5000,10000,20000,50000,100000,200000,500000,750000,1000000]
    for prob in probabilities:
        for num_vertices in num_vertices_tot:

            per_tot = {}
            count = 0
            num = num_orientations[count]
            np.random.seed(5)
            example = get_random_graph(num_vertices, prob)
            num_edges = len(example)
            number_or_orientations_making_g_connetected = 0
            tot_pers = []
            tot_b_d =[]
            all_orientations = get_orientations_sample(num_edges, num_orientations[-1])

            # This loop is performed only once for the
            for i in range(all_orientations.shape[0]):
                graph = orient_graph(example, all_orientations, i)
                number_or_orientations_making_g_connetected += nx.is_strongly_connected(graph.G)
                graph.build_filtered_subgraphs()
                graph.get_scc_filtration()
                graph.ranging_persistence(equal_modulo_vertices = True)

                for r in graph.ranging_cornerpoints:
                    tot_pers.append(r)
                    tot_b_d.append([r.birth,r.death])

                if i == (num-1):
                    bir_tot = np.zeros((len(tot_b_d)))
                    dea_tot = np.zeros((len(tot_b_d)))
                    for i in range(len(tot_b_d)):
                        bir_tot[i] = tot_b_d[i][0]
                        if tot_b_d[i][1] == np.inf:
                            dea_tot[i] = 55
                        else:
                            dea_tot[i] = tot_b_d[i][1]
                    final_pers = list( dict.fromkeys(tot_pers))
                    per_tot.update({str(num): [bir_tot, dea_tot]})
                    if count< (len(num_orientations)-1):
                        count = count+1
                        num = num_orientations[count]
            plot_hist_from_dict(per_tot, num_orientations, 55, num_vertices, prob)
            np.save('dictionaries/dictionary_'+str(prob)+'_'+str(num_vertices_tot), per_tot, allow_pickle = True)
