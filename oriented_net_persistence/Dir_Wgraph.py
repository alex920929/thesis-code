import numpy as np
import sys
sys.path.append('../')
import networkx as nx
import platform
if platform.system() == "Darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from net_persistence.utils import get_powerset_, get_supersets
from net_persistence.persistence_diagram import CornerPoint, PersistenceDiagram
from itertools import chain

class Dir_Wgraph:
    """
    Attributes
    ----------

    weighted_edges : list
        list of tuples of the form [(v1, v2, w12), ...] where v1 and v2 are the
        labels associated to the vertices on the boundary of the edge and w12
        is its weight.
    """
    def __init__(self, weighted_edges):
        self.weighted_edges = weighted_edges

    def build_graph(self):
        """Builds the graph defined by the collection of weighted edges
        """
        self.G = nx.DiGraph()

        for v1, v2, w12 in self.weighted_edges:
            self.G.add_edge(v1, v2, weight = w12)

    def get_edge_filtration_values(self, subgraph = None,
                                    weight_transform = None):
        """Creates list of nodes and edges. Applies filtrating function to the
        weights of the edges and stores the values of the filtrating function
        in an array according to the ordering used to sort the edges in networkx
        edges dictionary
        """
        if subgraph is None:
            subgraph = self.G
        self.nodes = list(subgraph.nodes)
        self.edges = list(nx.get_edge_attributes(subgraph,'weight').keys())
        self.evaluate_weight_transform_and_set_on_edges(subgraph,
                                                            weight_transform)

    def evaluate_weight_transform_and_set_on_edges(self, subgraph,
                                                        weight_transform):
        """Creates a dictionary edge: value_of_the_weight_transform.

        Notes
        -----
        Does not use nx.set_edge_attributes. To be updated after networkx bug
        correction.
        See link_to_the_reported_issue
        """
        self.transformed_edges = self.get_filtration_values(subgraph,
                                                            weight_transform)
        self.transformed_edges_dict = {edge: value
                                            for edge, value in
                                            zip(self.edges, self.transformed_edges)}

    @staticmethod
    def identity(array):
        """Standard filtrating function: do nothing
        """
        return array

    def get_filtration_values(self, subgraph, func):
        """Evaluates func on the weights defined on the edges
        """
        return func(np.asarray(list(nx.get_edge_attributes(subgraph,'weight').values())))


    def get_subgraph_edges(self, value):
        """Returns the edges of self.G part of the sublevel set defined by value
        """
        return [edge + tuple([self.transformed_edges_dict[edge]])
                    for edge in self.transformed_edges_dict
                    if self.transformed_edges_dict[edge] <= value]

    @staticmethod
    def get_subgraph(edges):
        """Returns the subgraph defined by edges. Once the filtration is
        generated we are only interested in the 'hubbiness' of the nodes in the
        subgraph. Thus we do not set weights.
        """
        H = nx.DiGraph()
        [H.add_edge(v1, v2, weight =  np.round(w12, decimals = 2))
         for v1, v2, w12 in edges]
        return H

    def build_filtered_subgraphs(self, weight_transform = None):
        """Generates the filtration of G given the values of the filtrating
        function.
        """
        if weight_transform is None:
            weight_transform = self.identity

        self.weight_transform = weight_transform
        self.get_edge_filtration_values(weight_transform = weight_transform)
        self.filtration = []
        self.transformed_edges = np.unique(self.transformed_edges)
        self.transformed_edges.sort()

        for value in self.transformed_edges:
            edges = self.get_subgraph_edges(value)
            self.filtration.append(self.get_subgraph(edges))

    def get_eulerian_filtration(self, superset = False):
        if superset:
            self.eulerians = {sublevel:
                              get_supersets(self.get_eulerian_subgraphs(subgraph))
                              for sublevel, subgraph in enumerate(self.filtration)}
        else:
            self.eulerians = {sublevel:
                              self.get_eulerian_subgraphs(subgraph)
                              for sublevel, subgraph in enumerate(self.filtration)}
        cornerpoint_vertices_ = list(chain.from_iterable(self.eulerians.values()))
        self.cornerpoint_vertices = []
        print(self.eulerians)

        for s in cornerpoint_vertices_:
            if s not in self.cornerpoint_vertices:
                self.cornerpoint_vertices.append(s)
        print('self.cornerpoint_vertices')
        print(self.cornerpoint_vertices)

        self.persistence = {i: [k for k in self.eulerians
                            if cornerpoint_vertex in self.eulerians[k]]
                            for i, cornerpoint_vertex in enumerate(self.cornerpoint_vertices)}
        self.persistence = {key: value for key, value
                                in self.persistence.items()
                                if len(value) >= 1}
        print('PERSISTENCE')
        print(self.persistence)



    def get_scc_from_filt_1(self, subgraph):
        max_blocks = sorted(map(sorted,
        						nx.strongly_connected_components(subgraph)))

        # ~ print("max blocks ", max_blocks)
        newfilt=[]

        for k in max_blocks:
            if len(k)>1:
                newfilt.append(set(k))
        return newfilt

    def get_edge_blocks_from_filt_1(self, subgraph, edges_to_remove=1):
        max_blocks = sorted(map(sorted,
                            nx.k_edge_components(subgraph, k=edges_to_remove+1)))
        #print("max blocks ", max_blocks)
        newfilt=[]
        for k in max_blocks:
            if len(k)>1:
                newfilt.append(set(k))
        return newfilt


    def get_edge_blocks_filtration_1(self, edges_to_remove=1):
        self.edge_blocks={}
        self.cornerpoint_vertices = []
        for sublevel, subgraph in enumerate(self.filtration):
            self.edge_blocks[sublevel] = [set(component) for component in
										  self.get_edge_blocks_from_filt_1(subgraph,
										                                edges_to_remove=edges_to_remove)]
            print(sublevel)
            print(self.edge_blocks[sublevel], "-"*20)
        self.cornerpoint_vertices = self.get_list_of_unique_vertices(self.edge_blocks)


        self.persistencen = {i: [k for k in self.edge_blocks
								if cornerpoint_vertex in self.edge_blocks[k]]
								for i, cornerpoint_vertex in
								enumerate(self.cornerpoint_vertices)}
        print('HERE SELF.PERSISTENCE_before')
        print(self.persistencen)
        for key, value in self.persistencen.items():
            print('KEY, VALUE')
            print(key)
            print(value)
        self.persistence = {key: value for key, value
							 in self.persistencen.items()
							if len(value) >= 1}
        print('HERE SELF.PERSISTENCE')
        print(self.persistence)

    @staticmethod

    def get_list_of_unique_vertices(dict_of_vertices):
        cornerpoint_vertices_ = list(chain.from_iterable(dict_of_vertices.values()))

        cornerpoint_vertices = []
        for s in cornerpoint_vertices_:

            if s not in cornerpoint_vertices:
                cornerpoint_vertices.append(s)

        return cornerpoint_vertices


    def ranging_persistence_1(self):
        """Ranks ranging subgraphs according to their persistence. Recall that a
        temporary subgraph is said ranging if there exist Gm and Gn sublevel sets
        (m < n) in which the temporary subgraph is alive.
        """

        self.ranging_cornerpoints = []

        for vertices in self.persistence:
            # ~ print('THIS IS VERTICES')
            # ~ print(vertices)
            pers = self.persistence[vertices]
            # ~ print('THIS IS PERS')
            # ~ print(pers)
            # ~ print('end_pers')
            birth = min(pers)
            death = max(pers)
            if len(pers) == 1:
                death = birth + 1
            death = np.inf if death == len(self.filtration)-1 else self.transformed_edges[death]

            # ~ print('THESE BIRTH AND DEATH')
            # ~ print(birth,death)
            # ~ print(self.cornerpoint_vertices[vertices])
            c = CornerPoint(0,
							self.transformed_edges[birth],
							death,
							vertex = self.cornerpoint_vertices[vertices])
            self.ranging_cornerpoints.append(c)

        self.ranging_pd = PersistenceDiagram(cornerpoints = self.ranging_cornerpoints)


    def ranging_persistence(self, equal_modulo_vertices = False):
        """Ranks ranging hubs according to their persistence. Recall that a
        temporary hub is said ranging if there exist Gm and Gn sublevel sets
        (m < n) in which the temporary hub is alive.
        """
        self.ranging_cornerpoints = []

        for vertices in self.persistence:
            # ~ print('THIS IS VERTICES')
            # ~ print(vertices)
            pers = self.persistence[vertices]
            # ~ print('THIS IS PERS')
            # ~ print(pers)

            # if len(pers) >= 1:
            birth = min(pers)
            death = max(pers)
            # ~ print(death, len(self.filtration)-1)
            death = np.inf if death == len(self.filtration)-1 else self.transformed_edges[death]
            # ~ print('THESE BIRTH AND DEATH')
            # ~ print(birth,death)
            # ~ print(self.cornerpoint_vertices[vertices])
            c = CornerPoint(0,
				            self.transformed_edges[birth],
							death,
							vertices = self.cornerpoint_vertices[vertices],
							equal_modulo_vertices = equal_modulo_vertices)
            self.ranging_cornerpoints.append(c)

        self.ranging_pd = PersistenceDiagram(cornerpoints = self.ranging_cornerpoints)



    def get_scc_from_filt_new(self, subgraph, edges_to_remove=1):
        max_blocks = sorted(map(sorted,
				nx.strongly_connected_components(subgraph))
				)
        print("max blocks ", max_blocks)
        newfilt=[]

        for k in max_blocks:
            if len(k)>1:
                newfilt.append(set(k))

        return newfilt



    def get_scc_filtration_new(self, edges_to_remove=1):
        self.scc={}
        self.cornerpoint_vertices = []

        for sublevel, subgraph in enumerate(self.filtration):

            self.scc[sublevel] = [set(component) for component in
											self.get_scc_from_filt_new(subgraph)]
            print(sublevel)
            print(self.scc[sublevel], "-"*20)

        self.cornerpoint_vertices = self.get_list_of_unique_vertices(self.scc)
        self.persistence = {i: [k for k in self.scc
							if cornerpoint_vertex in self.scc[k]]
							for i, cornerpoint_vertex in enumerate(self.cornerpoint_vertices)}

        self.persistence = {key: value for key, value
								in self.persistence.items()
								if len(value) >= 1}


    def ranging_persistence_4(self):
        """Ranks ranging hubs according to their persistence. Recall that a
        temporary hub is said ranging if there exist Gm and Gn sublevel sets
        (m < n) in which the temporary hub is alive.
        """
        self.ranging_cornerpoints = []

        for vertices in self.persistence:
            # ~ print('THIS IS VERTICES')
            # ~ print(vertices)
            pers = self.persistence[vertices]
            # ~ print('THIS IS PERS')
            # ~ print(pers)
            # if len(pers) >= 1:
            birth = min(pers)
            death = max(pers)
            # ~ print(death, len(self.filtration)-1)
            # ~ death = np.inf if death == len(self.filtration)-1 else self.transformed_edges[death]
            # ~ print('THESE BIRTH AND DEATH')
            # ~ print(birth,death)
            # ~ print(self.cornerpoint_vertices[vertices])
            c = CornerPoint(0,
							self.transformed_edges[birth],
							death,
							vertices = self.cornerpoint_vertices[vertices])
            self.ranging_cornerpoints.append(c)

        self.ranging_pd = PersistenceDiagram(cornerpoints = self.ranging_cornerpoints)

    def get_scc_filtration(self, disp = False):

        # We find the blocks along the filtration
        self.eblocks={}		# contains the blocks
        for sublevel, subgraph in enumerate(self.filtration):
            self.eblocks[sublevel] = get_supersets(self.get_scc_from_filt_1(subgraph))
            # ~ print(sublevel)

        cornerpoint_vertices_ = list(chain.from_iterable(self.eblocks.values()))
        self.cornerpoint_vertices = []

        for s in cornerpoint_vertices_:
            if s not in self.cornerpoint_vertices:
                self.cornerpoint_vertices.append(s)
        persistence_supp = {} # list of blocks, composed by [birth, death, block]
        persistence_tot = {}
        count = 0

        for k in self.eblocks:
            # if the time is 0 we add every component to the set of blocks
            if k==0:
                for cor in self.eblocks[k]:
                    persistence_supp[count] = [k,k,cor] # [birth, death, block]
                    count = count+1
            else:
                for cor in self.eblocks[k]:
                    # if the block have already appeared previously we update the death value
                    if cor in self.eblocks[k-1]:
                        pers_temp = {}
                        for i in persistence_supp:

                            b,d,cor1 = persistence_supp[i]
                            if cor1==cor:
                                pers_temp[i] = [b,k,cor]
                            else:
                                pers_temp[i] = [b,d,cor1]
                        persistence_supp = pers_temp

                    else:
                        # if it is new it can be the merge of two or more blocks or it could be a new one
                        eblock_list = {}  # contains all the blocks contained in cor

                        # compute eblock_list
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            if list(cor1)[0] in list(cor):
                                if eblock_list=={}:
                                    eblock_list[1] = [i,b,d,cor1]
                                else:
                                    eblock_list[list(eblock_list.keys())[-1]+1] = [i,b,d,cor1]

                        if eblock_list == {}: # If this is empty, it is a new block, so we add it to the set
                            if len(persistence_supp)==0:
                                persistence_supp[1] = [k,k,cor]
                            else:
                                persistence_supp[list(persistence_supp.keys())[-1]+1] = [k,k,cor]

                        # otherwise we find the oldest component, and we update it, and the younger one die.
                        else:
                            m=[0,float('inf')]
                            pers_temp = []
                            # find the oldest component
                            for l in eblock_list:
                                i, b, _, _ = eblock_list[l]
                                if b < m[1]:
                                    m = [i,b]
                            for l in eblock_list:
                                i,b,d,cor1 = eblock_list[l]
                                if [i,b] == m:
                                    persistence_supp[i]=[b,k,cor] # update the oldest component
                                else:
                                    persistence_tot[len(persistence_tot)]=[b,k,cor1] # we add the dead component to the final set of cornerpoints
                                    del persistence_supp[i] # we delete the dead component.

        # add the component which are still alive
        for l in persistence_supp:
            b,d,cor = persistence_supp[l]
            persistence_tot[len(persistence_tot)] = [b,d,cor]

        # compute the set of persistence couple [birth, death]
        self.persistence={}
        self.persistence_tot = persistence_tot

        if disp == True:
            for l in persistence_tot:
                b,d,cor = persistence_tot[l]
                print([b,d,cor])

        for l in persistence_tot:
            b,d,_ = persistence_tot[l]
            self.persistence[len(self.persistence)] = [b,d]

        # ~ print('HERE SELF.PERSISTENCE')
        # ~ print(self.persistence)


    def get_edge_blocks_filtration(self, disp = False):

        # We find the blocks along the filtration
        self.eblocks={}		# contains the blocks
        for sublevel, subgraph in enumerate(self.filtration):
            self.eblocks[sublevel] = get_supersets(self.get_eblocks_from_filt(subgraph))
            print(sublevel)

        cornerpoint_vertices_ = list(chain.from_iterable(self.eblocks.values()))
        self.cornerpoint_vertices = []

        for s in cornerpoint_vertices_:
            if s not in self.cornerpoint_vertices:
                self.cornerpoint_vertices.append(s)
        persistence_supp = {} # list of blocks, composed by [birth, death, block]
        persistence_tot = {}
        count = 0

        for k in self.eblocks:
            # if the time is 0 we add every component to the set of blocks
            if k==0:
                for cor in self.eblocks[k]:
                    persistence_supp[count] = [k,k,cor] # [birth, death, block]
                    count = count+1
            else:
                for cor in self.eblocks[k]:
                    # if the block have already appeared previously we update the death value
                    if cor in self.eblocks[k-1]:
                        pers_temp = {}
                        for i in persistence_supp:

                            b,d,cor1 = persistence_supp[i]
                            if cor1==cor:
                                pers_temp[i] = [b,k,cor]
                            else:
                                pers_temp[i] = [b,d,cor1]
                            persistence_supp = pers_temp

                    else:
                        # if it is new it can be the merge of two or more blocks or it could be a new one
                        eblock_list = {}  # contains all the blocks contained in cor

                        # compute eblock_list
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            if list(cor1)[0] in list(cor):
                                if eblock_list=={}:
                                    eblock_list[1] = [i,b,d,cor1]
                                else:
                                    eblock_list[eblock_list.keys()[-1]+1] = [i,b,d,cor1]

                        if eblock_list == {}: # If this is empty, it is a new block, so we add it to the set
                            if len(persistence_supp)==0:
                                persistence_supp[1] = [k,k,cor]
                            else:
                                persistence_supp[persistence_supp.keys()[-1]+1] = [k,k,cor]

                        # otherwise we find the oldest component, and we update it, and the younger one die.
                        else:
                            m=[0,float('inf')]
                            pers_temp = []
                            # find the oldest component
                            for l in eblock_list:
                                i, b, _, _ = eblock_list[l]
                                if b < m[1]:
                                    m = [i,b]
                            for l in eblock_list:
                                i,b,d,cor1 = eblock_list[l]
                                if [i,b] == m:
                                    persistence_supp[i]=[b,k,cor] # update the oldest component
                                else:
                                    persistence_tot[len(persistence_tot)]=[b,k,cor1] # we add the dead component to the final set of cornerpoints
                                    del persistence_supp[i] # we delete the dead component.



        # add the component which are still alive
        for l in persistence_supp:
            b,d,cor = persistence_supp[l]
            persistence_tot[len(persistence_tot)] = [b,d,cor]

        # compute the set of persistence couple [birth, death]
        self.persistence={}
        self.persistence_tot = persistence_tot

        if disp == True:
            for l in persistence_tot:
                b,d,cor = persistence_tot[l]
                print([b,d,cor])

        for l in persistence_tot:
            b,d,_ = persistence_tot[l]
            self.persistence[len(self.persistence)] = [b,d]

        print('HERE SELF.PERSISTENCE')
        print(self.persistence)


    def get_blocks_filtration(self, disp = False):

        # We find the blocks along the filtration
        self.blocks={}		# contains the blocks
        for sublevel, subgraph in enumerate(self.filtration):
            self.blocks[sublevel] = get_supersets(self.get_blocks_from_filt(subgraph))
            print(sublevel)
        # ~ print(self.blocks)
        cornerpoint_vertices_ = list(chain.from_iterable(self.blocks.values()))
        self.cornerpoint_vertices = []

        for s in cornerpoint_vertices_:
            if s not in self.cornerpoint_vertices:
                self.cornerpoint_vertices.append(s)

        persistence_supp = {} # list of blocks, composed by [birth, death, block]
        persistence_tot = {}
        count = 0

        for k in self.blocks:
            print(k)
            # if the time is 0 we add every component to the set of blocks
            if k==0:
                for cor in self.blocks[k]:
                    persistence_supp[count] = [k,k,cor] # [birth, death, block]
                    count = count+1
            else:

                for cor in self.blocks[k]:
                    # if the block have already appeared previously we update the death value
                    if cor in self.blocks[k-1]:
                        pers_temp = {}
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            if cor1==cor:
                                pers_temp[i] = [b,k,cor] # update with the new value
                            else:
                                pers_temp[i] = [b,d,cor1] # keep it unchanged
                        persistence_supp = pers_temp

                    else:
                        # if it is new it can be the merge of two or more blocks or it could be a new one
                        block_list = {}  # contains all the blocks contained in cor

                        # compute eblock_list
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            if cor1<=cor: #list(cor1) in list(cor):
                                if block_list=={}:
                                    block_list[1] = [i,b,d,cor1]
                                else:
                                    block_list[block_list.keys()[-1]+1] = [i,b,d,cor1]

                        if block_list == {}: # If this is empty, it is a new block, so we add it to the set
                            persistence_supp[persistence_supp.keys()[-1]+1] = [k,k,cor]

                        # otherwise we find the oldest component, and we update it, and the younger one die.
                        else:
                            m=[0,float('inf')]
                            pers_temp = []
                            # find the oldest component
                            for l in block_list:
                                i, b, _, _ = block_list[l]
                                if b < m[1]:
                                    m = [i,b]
                            for l in block_list:
                                i,b,d,cor1 = block_list[l]
                                if [i,b] == m:
                                    persistence_supp[i]=[b,k,cor] # update the oldest component
                                else:
                                    persistence_tot[len(persistence_tot)]=[b,k,cor1] # we add the dead component to the final set of cornerpoints
                                    del persistence_supp[i] # we delete the dead component.
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            persistence_supp[i] = [b,k,cor1]



        # add the component which are still alive
        for l in persistence_supp:
            b,d,cor = persistence_supp[l]
            persistence_tot[len(persistence_tot)] = [b,d,cor]

        if disp == True:
            for l in persistence_tot:
                b,d,cor = persistence_tot[l]
                print([b,d,cor])
        # compute the set of persistence couple [birth, death]
        self.persistence={}
        self.persistence_tot = persistence_tot
        for l in persistence_tot:
            b,d,_ = persistence_tot[l]
            self.persistence[len(self.persistence)] = [b,d]


    def get_cliques_comm_filtration(self, k_size, disp = False):

        # We find the blocks along the filtration
        self.clcomm={}		# contains the blocks
        for sublevel, subgraph in enumerate(self.filtration):
            self.clcomm[sublevel] = get_supersets(self.get_cliques_comm_from_filt(subgraph,k_size))
            print(sublevel)
            # ~ print(self.blocks)
        cornerpoint_vertices_ = list(chain.from_iterable(self.clcomm.values()))
        self.cornerpoint_vertices = []

        for s in cornerpoint_vertices_:
            if s not in self.cornerpoint_vertices:
                self.cornerpoint_vertices.append(s)

        persistence_supp = {} # list of blocks, composed by [birth, death, block]
        persistence_tot = {}
        count = 0

        for k in self.clcomm:
            print(k)
            # if the time is 0 we add every component to the set of blocks
            if k==0:
                for cor in self.clcomm[k]:
                    persistence_supp[count] = [k,k,cor] # [birth, death, block]
                    count = count+1
            else:

                for cor in self.clcomm[k]:
                    # if the block have already appeared previously we update the death value
                    if cor in self.clcomm[k-1]:
                        pers_temp = {}
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            if cor1==cor:
                                pers_temp[i] = [b,k,cor] # update with the new value
                            else:
                                pers_temp[i] = [b,d,cor1] # keep it unchanged
                        persistence_supp = pers_temp

                    else:
                        # if it is new it can be the merge of two or more blocks or it could be a new one
                        clcomm_list = {}  # contains all the blocks contained in cor

                        # compute eblock_list
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            if cor1<=cor: #list(cor1) in list(cor):
                                if clcomm_list=={}:
                                    clcomm_list[1] = [i,b,d,cor1]
                                else:
                                    clcomm_list[clcomm_list.keys()[-1]+1] = [i,b,d,cor1]

                        if clcomm_list == {}: # If this is empty, it is a new block, so we add it to the set
                            if persistence_supp=={}:
                                persistence_supp[1] = [k,k,cor]
                            else:
                                persistence_supp[persistence_supp.keys()[-1]+1] = [k,k,cor]

                        # otherwise we find the oldest component, and we update it, and the younger one die.
                        else:
                            m=[0,float('inf')]
                            pers_temp = []
                            # find the oldest component
                            for l in clcomm_list:
                                i, b, _, _ = clcomm_list[l]
                                if b < m[1]:
                                    m = [i,b]
                            for l in clcomm_list:
                                i,b,d,cor1 = clcomm_list[l]
                                if [i,b] == m:
                                    persistence_supp[i]=[b,k,cor] # update the oldest component
                                else:
                                    persistence_tot[len(persistence_tot)]=[b,k,cor1] # we add the dead component to the final set of cornerpoints
                                    del persistence_supp[i] # we delete the dead component.
                        for i in persistence_supp:
                            b,d,cor1 = persistence_supp[i]
                            persistence_supp[i] = [b,k,cor1]



        # add the component which are still alive
        for l in persistence_supp:
            b,d,cor = persistence_supp[l]
            persistence_tot[len(persistence_tot)] = [b,d,cor]

        if disp == True:
            for l in persistence_tot:
                b,d,cor = persistence_tot[l]
                print([b,d,cor])
        # compute the set of persistence couple [birth, death]
        self.persistence={}
        self.persistence_tot = persistence_tot
        for l in persistence_tot:
            b,d,_ = persistence_tot[l]
            self.persistence[len(self.persistence)] = [b,d]



    @staticmethod
    def get_maximum_steady_persistence(array):
        """Return list of consecutive lists of numbers from vals (number list)."""
        sub_persistence = []
        sub_persistences = [sub_persistence]
        consecutive = None

        for value in array:
            if (value == consecutive) or (consecutive is None):
                sub_persistence.append(value)
            else:
                sub_persistence = [value]
                sub_persistences.append(sub_persistence)
            consecutive = value + 1

        sub_persistences.sort(key=len)

        return sub_persistences

    def steady_persistence(self):
        """Ranks steady hubs according to their persistence. Recall that a
        temporary hub is steady if it lives through consecutive sublevel sets of
        the filtration induced by the weights of the graph.
        """
        self.steady_cornerpoints = []

        for vertices in self.persistence:
            pers = self.persistence[vertices]
            # if len(pers) > 1:
            max_steady_pers = self.get_maximum_steady_persistence(pers)
            births = [min(c) for c in max_steady_pers]
            deaths = [max(c) for c in max_steady_pers]
            deaths = [np.inf if d == len(self.filtration)-1
                      else self.transformed_edges[d + 1] for d in deaths]
            # death = np.inf if death == len(self.filtration)-1 else self.transformed_edges[death + 1]
            for birth,death in zip(births, deaths):
                c = CornerPoint(0,
                                self.transformed_edges[birth],
                                death,
                                vertices = self.cornerpoint_vertices[vertices])
                self.steady_cornerpoints.append(c)

        self.steady_pd = PersistenceDiagram(cornerpoints = self.steady_cornerpoints)

    # ~ def ranging_persistence(self):
        # ~ """Ranks ranging hubs according to their persistence. Recall that a
        # ~ temporary hub is said ranging if there exist Gm and Gn sublevel sets
        # ~ (m < n) in which the temporary hub is alive.
        # ~ """
        # ~ self.ranging_cornerpoints = []

        # ~ for vertices in self.persistence:
            # ~ pers = self.persistence[vertices]
            # ~ # if len(pers) >= 1:
            # ~ birth = min(pers)
            # ~ death = max(pers)
            # ~ print(death, len(self.filtration)-1)
            # ~ death = np.inf if death == len(self.filtration)-1 else self.transformed_edges[death]
            # ~ c = CornerPoint(0,
                            # ~ self.transformed_edges[birth],
                            # ~ death,
                            # ~ vertices = self.cornerpoint_vertices[vertices])
            # ~ self.ranging_cornerpoints.append(c)

        # ~ self.ranging_pd = PersistenceDiagram(cornerpoints = self.ranging_cornerpoints)

    @staticmethod
    def get_eulerian_subgraphs(graph):
        return [set(comb) for comb in get_powerset_(list(graph.nodes))
                if (nx.is_k_edge_connected(graph.subgraph(comb),2) & nx.is_k_edge_connected(graph.subgraph(comb),1))]


    def get_eblocks_from_filt(self, subgraph):

        max_blocks = sorted(map(sorted, nx.k_edge_components(subgraph, k=2)))
        newfilt=[]
        for k in max_blocks:
            if len(k)>1:
                newfilt.append(set(k))
        return newfilt


    def get_blocks_from_filt(self, subgraph):

        max_blocks = nx.k_components(subgraph)
        # ~ print(max_blocks)
        newfilt=[]
        if len(max_blocks)>1:
            for kel in max_blocks[2]:
                # ~ print(kel)
                if kel in newfilt:
                    a=1
                else:
                    newfilt.append(set(kel))
        for k in subgraph.edges:
            newfilt.append(set(k))
        return newfilt


    def get_cliques_comm_from_filt(self, subgraph, k):
        # k is the size of cliques we are looking for.
        newfilt = list(nx.algorithms.community.k_clique_communities(subgraph, k))
        return newfilt

    @staticmethod
    def get_eblocks_subgraphs(graph):
        return [set(comb) for comb in get_powerset_(list(graph.nodes))
                if (nx.is_k_edge_connected(graph.subgraph(comb),2) & nx.is_k_edge_connected(graph.subgraph(comb),1))]


    def plot_filtration(self):
        """Plots all the subgraphs of self.G given by considering the sublevel
        sets of the function defined on the weighted edges
        """
        fig, self.ax_arr = plt.subplots(int(np.ceil(len(self.filtration) / 3)),3)
        self.ax_arr = self.ax_arr.ravel()
        ordinals = ['st', 'nd', 'rd']
        for i, h in enumerate(self.filtration):
            title = str(i + 1) + ordinals[i] if i < 3 else str(i + 1) + 'th'
            self._draw(graph = h, plot_weights = True, ax = self.ax_arr[i],
                        title = title + " sublevel set")


    def _draw(self, graph = None, plot_weights = True, ax = None, title = None):
        """Plots a graph using networkx wrappers

        Parameters
        ----------
        graph : <networkx graph>
            A graph instance of networkx.Graph()
        plot_weights : bool
            If True weights are plotted on the edges of the graph
        ax : <matplotlib.axis._subplot>
            matplotlib axes handle
        title : string
            title to be attributed to ax
        """
        if graph is None:
            graph = self.G
        pos = nx.shell_layout(graph)
        if title is not None:
            ax.set_title(title)
        nx.draw(graph, pos = pos, ax = ax, with_labels = True)
        if plot_weights:
            labels = nx.get_edge_attributes(graph,'weight')
            labels = {k : np.round(v, decimals = 2) for k,v in labels.items()}
            nx.draw_networkx_edge_labels(graph, pos, edge_labels = labels,
                                        ax = ax)

    def plot_steady_persistence_diagram(self, title = None):
        """Uses gudhi and networkx wrappers to plot the persistence diagram and
        the hubs obtained through the steady-hubs analysis, respectively
        """
        fig, ax = plt.subplots()
        self.steady_pd.plot_gudhi(ax,
                persistence_to_plot = self.steady_pd.persistence_to_plot)
        if title is not None:
            plt.suptitle(title)

    def plot_ranging_persistence_diagram(self, title = None):
        """Uses gudhi and networkx wrappers to plot the persistence diagram and
        the hubs obtained through the ranging-hubs analysis, respectively
        """
        fig, ax = plt.subplots()
        self.ranging_pd.plot_gudhi(ax,
                persistence_to_plot = self.ranging_pd.persistence_to_plot)
        if title is not None:
            plt.suptitle(title)


    def reduce_persistence(self, tot_pers, tot_b_d):
        self.final_pers = list( dict.fromkeys(tot_pers) )
        bd_array = np.asarray(tot_b_d)
        self.final_b_d, c = np.unique(bd_array, return_counts = True, axis =0)

        b = np.zeros((len(self.final_b_d),1))
        d = np.zeros((len(self.final_b_d),1))
        for i in range(len(self.final_b_d)):
            el = self.final_b_d[i,:]
            b[i] = el[0]
            if el[1] == np.inf:
                d[i] = 14
            else:
                d[i] = el[1]

        self.fin_b = b
        self.fin_d = d
        self.fin_mult = c






if __name__ == "__main__":
    example = [('a', 'b', 14),
               ('b', 'c',  4),
               ('c', 'd',  6),
               ('d', 'e',  5),
               ('e', 'f', 12),
               ('f', 'g',  8),
               ('g', 'h',  9),
               ('i', 'f', 11),
               ('i', 'a', 13),
               ('i', 'b',  2),
               ('i', 'c',  3),
               ('c', 'f', 15),
               ('c', 'e',  7),
               ('h', 'f', 10)]

    graph = Dir_WGraph(example)
    graph.build_graph()
    graph.build_filtered_subgraphs()
    graph.get_eulerian_filtration()
    graph.steady_persistence()
    graph.plot_steady_persistence_diagram()
