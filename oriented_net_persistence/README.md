# Oriented graph persistence

In this folder you can find the code to replicate the experiments on the distribution of cornerpoints.

# Code

The script 

```
python3 new_orient_scc.py
```

generates the dictionaries containing the information about the distribution of the cornerpoints and saves them in the directory "dictionaries".
Running the script

```
python3 generate_images.py
```
you will obtain a pdf file containing the cornerpoint distributions for the chosen dictionary. 
