import numpy as np


def binomial(n, k):
    """
    A fast way to calculate binomial coefficients by Andrew Dalke.
    See http://stackoverflow.com/questions/3025162/statistics-combinations-in-python
    """
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in range(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0



def p_ik(i,k,p):

    return binomial(k,k-i)*np.power(p,k-i)*np.power((1-p),i)



def compute_prob(n,p):
    prob = np.zeros(n+1)
    prob[0] = p
    prob[1] = 0
    prob[2] = 1-p
    print(prob)
    for i in range(3,n+1): #Aggiungo un vertice alla volta
        print(i)
        r = prob
        for j in range(i): # aggiorno la j-esima componente di p
            print('j = '+str(j))
            if j!=0:
                prob[j] = r[j-1]*np.power(p,i-1)
            for k in range(j,i): #
                prob[j] = prob[j]+r[j]*p_ik(j,k,p)
        prob[i] = np.power((1-p), i*(i-1)/2)
        print(prob)
    return prob



n = 3
p = 1/3

prob = compute_prob(n,p)

t = sum(prob)-prob[0]

print(t)
print(prob)
