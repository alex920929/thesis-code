from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
import time

def opp(array):
	return -array

def identity(array):
	return array

def max_(array):
	return np.max(array) - array


example = [('aa', 'ab', 5),
			('aa', 'ae', 3),
			('aa', 'ac', 2),
			('ab', 'ac', 4),
			('ab', 'ad', 3),
			('ab', 'ae', 5),
			('ac', 'ad', 1),
			('ad', 'ae', 6),			
			('ad', 'bb', 0),			
			('ba', 'bb', 3),
			('ba', 'be', 5),
			('ba', 'bc', 6),
			('bb', 'bc', 2),
			('bb', 'bd', 4),
			('bb', 'be', 3),
			('bc', 'bd', 4),
			('bd', 'be', 5),			
			('be', 'cb', 0),
			('ca', 'cb', 2),
			('ca', 'ce', 3),
			('ca', 'cc', 3),
			('cb', 'cc', 5),
			('cb', 'cd', 6),
			('cb', 'ce', 3),
			('cc', 'cd', 4),
			('cd', 'ce', 7),			
			('ca', 'dc', 0),			
			('da', 'db', 5),
			('da', 'de', 3),
			('da', 'dc', 2),
			('db', 'dc', 6),
			('db', 'dd', 4),
			('db', 'de', 7),
			('dc', 'dd', 4),
			('dd', 'de', 3),
			# ~ ('aa', 'db', 0)
			]
			
			

start = time. time()
print('here')
graph = Wgraph.Wgraph(example)
graph.build_graph()


graph.build_filtered_subgraphs()
print('here1')

graph.get_edge_blocks_filtration(disp=True)
# ~ graph.get_cliques_comm_filtration(k_size = 3, disp=True)
end = time. time()
print(end - start)
graph.ranging_persistence()
print('here')
# ~ graph._draw()
graph.plot_ranging_persistence_diagram()
plt.show()


# ~ pos = nx.spring_layout(graph.G)

pos = {'aa': [ .5, 1],
		'ab': [0 , .6], 
		'ac': [.25, 0],
		'ad': [.75 , 0], 
		'ae': [1, .6], 
		
		'ba': [ 2.5, 1],
		'bb': [2 , .6], 
		'bc': [2.25, 0],
		'bd': [2.75 , 0], 
		'be': [3, .6], 
		
		'ca': [ 4.5, 1],
		'cb': [4 , .6], 
		'cc': [4.25, 0],
		'cd': [4.75 , 0], 
		'ce': [5, .6], 
		
		'da': [ 4.5, 3],
		'db': [4 , 2.6], 
		'dc': [4.25, 2],
		'dd': [4.75 , 2], 
		'de': [5, 2.6], 
		}
		
		
nx.draw_networkx_nodes(graph.G, pos, node_size=1)
nx.draw_networkx_edges(graph.G, pos)
labels = nx.get_edge_attributes(graph.G,'weight')
nx.draw_networkx_edge_labels(graph.G,pos,edge_labels=labels)
labels=nx.draw_networkx_labels(graph.G,pos)
plt.show()
