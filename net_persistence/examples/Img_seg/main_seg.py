#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
import numpy as np
from skimage.transform import (hough_line, hough_line_peaks,
                               probabilistic_hough_line)
from skimage.feature import canny
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
from matplotlib import cm
import pandas as pd





def opp(array):
	return -array

def identity(array):
	return array

def max_(array):
	return np.max(array) - array


def grad(I):
	# Compute the gradient magnitude of the input image I
	
    I = I/np.max(I)
    gx = np.zeros(I.shape)
    gx[1:,:] = I[1:,:] - I[0:-1,:]

    gy = np.zeros(I.shape)
    gy[:,1:] = I[:,1:] - I[:,0:-1]

    G=np.sqrt((np.power(gx,2)+np.power(gy,2)))
    return G




# ~ img = cv2.imread('ws.png')
# ~ img = cv2.imread('lena.png')
# ~ img = cv2.imread('index.jpeg')
# ~ img = cv2.imread('images.jpeg')
# ~ img = cv2.imread('bird_1.png')
# ~ img = cv2.imread('color_grid.jpeg')
img = cv2.imread('plane.jpg')
# ~ img = cv2.imread('plane.jpg')

# ~ print(np.max(img), np.min(img))
# ~ N = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
# ~ plt.imshow(N, cmap = 'gray')
# ~ plt.show()


print(np.max(img), np.min(img))
I = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
plt.imshow(I, cmap = 'gray')
plt.show()
N = np.zeros([int(I.shape[0]/2+1),int(I.shape[1]/2)+1])
print(np.max(img), np.min(img))
for i in range(N.shape[0]):
	for j in range(N.shape[1]):
		N[i,j] = np.sum(I[2*i:(2*i+1),2*j:(2*j+1)])/4
 
M = np.zeros(N.shape)
for i in range(N.shape[0]):
	for j in range(N.shape[1]):
		M[i,j] = float(N[i,j])
# ~ M = np.zeros([80,80])

# ~ for i in range(M.shape[0]):
	# ~ for j in range(M.shape[1]):
		# ~ if ((((i//10)+(j//10))%2)==0):
			# ~ M[i,j]=256

# ~ img=M
# ~ M = M + np.random.randn(M.shape[0], M.shape[1])*15
plt.imshow(M, cmap = 'gray')
plt.show()
print(type(M[1,1]))

k = M.shape

supp_grid = np.reshape(range(M.size),M.shape)

x, y = np.mgrid[0:k[0],0:k[1]]
graph_structure = []
Stepsize = 2
for i in range(k[0]-1):
	for j in range(k[1]-1):
		# ~ print([supp_grid[i,j], supp_grid[i+1,j], img[x[i+1,j],y[i+1,j]]-img[x[i,j],y[i,j]]])
		if i>0:
			graph_structure.append(tuple([supp_grid[i,j], supp_grid[i+1,j], ((abs(M[x[i+1,j],y[i+1,j]]-M[x[i,j],y[i,j]]))//Stepsize) ]))
			graph_structure.append(tuple([supp_grid[i,j], supp_grid[i,j+1], ((abs(M[x[i,j+1],y[i,j+1]]-M[x[i,j],y[i,j]]))//Stepsize) ]))
			graph_structure.append(tuple([supp_grid[i,j], supp_grid[i+1,j+1], ((abs(M[x[i+1,j+1],y[i+1,j+1]]-M[x[i,j],y[i,j]]))//Stepsize) ]))
			graph_structure.append(tuple([supp_grid[i,j], supp_grid[i-1,j+1], ((abs(M[x[i-1,j+1],y[i-1,j+1]]-M[x[i,j],y[i,j]]))//Stepsize) ]))
		else:
			graph_structure.append(tuple([supp_grid[i,j], supp_grid[i+1,j], ((abs(M[x[i+1,j],y[i+1,j]]-M[x[i,j],y[i,j]]))//Stepsize) ]))
			graph_structure.append(tuple([supp_grid[i,j], supp_grid[i,j+1], ((abs(M[x[i,j+1],y[i,j+1]]-M[x[i,j],y[i,j]]))//Stepsize) ]))
			graph_structure.append(tuple([supp_grid[i,j], supp_grid[i+1,j+1], ((abs(M[x[i+1,j+1],y[i+1,j+1]]-M[x[i,j],y[i,j]]))//Stepsize) ]))
			



graph = Wgraph.Wgraph(graph_structure)
print('here')
graph.build_graph()
print('here')

graph.build_filtered_subgraphs(weight_transform = identity)

print('here')
graph.get_cliques_comm_filtration(k_size = 3)
print('here')
graph.ranging_persistence()

segm = np.zeros([M.size,1])

# ~ for i in graph.persistence_tot:
	# ~ segm_part = np.zeros([M.size,1])
	# ~ b, d, cor = graph.persistence_tot[i]
	# ~ #print(graph.persistence_tot[i])
	# ~ k = 0
	# ~ for j in cor:
		# ~ segm[j] = segm[j]+2 #np.power(2,i)
		# ~ segm_part[j] = segm_part[j]+2 #np.power(2,i)
		# ~ k = k+1
	# ~ if (k>200):
		# ~ segm_part = np.reshape(segm_part, M.shape)
		# ~ plt.imshow(segm_part, cmap = 'gray')
		# ~ plt.show()
	# ~ print(k)
	# ~ if (k>200):
		# ~ segm_part = np.reshape(segm_part, M.shape)
		# ~ plt.imshow(segm_part, cmap = 'gray')
		# ~ plt.show()
		
# ~ for i in range(25):
	# ~ comm_all = graph.clcomm[i]
	# ~ segm_inter= np.zeros([M.size,1])
	# ~ l = 0
	# ~ for j in comm_all:
		# ~ cor = set(j)
		# ~ for k in cor:
			# ~ segm_inter[k] = segm_inter[k] + l*20
		# ~ l=l+1
	# ~ segm_inter = np.reshape(segm_inter, M.shape)
	# ~ plt.imshow(segm_inter, cmap = 'gray')
	# ~ plt.show()

i = 1
comm_all = graph.clcomm[i]
segm_inter= np.zeros([M.size,1])
# ~ print(comm_all)
l = 0
for j in comm_all:
	cor = set(j)
	for k in cor:
		segm_inter[k] = segm_inter[k] + l*20
	l=l+1
segm_inter = np.reshape(segm_inter, M.shape)
plt.imshow(segm_inter, cmap = 'gray')
plt.show()

segm = np.reshape(segm, M.shape)
print(segm)


plt.imshow(segm, cmap = 'gray')
plt.show()
