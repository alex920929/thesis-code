# import the necessary packages
from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
import matplotlib.pyplot as plt
import argparse
from skimage.future import graph
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
import numpy as np

# # construct the argument parser and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--image", required = True, help = "Path to the image")
# args = vars(ap.parse_args())
# # load the image and convert it to a floating point data type
# image = img_as_float(io.imread(args["image"]))


image = img_as_float(io.imread('struz_col.jpg'))
# loop over the number of segments
numSegments = 1000
# apply SLIC and extract (approximately) the supplied number
# of segments
segments = slic(image, n_segments = numSegments, sigma = 5)
g = graph.rag_mean_color(image, segments)
# print(segments.shape)
print(g)
# show the output of SLIC
fig = plt.figure("Superpixels -- %d segments" % (numSegments))
ax = fig.add_subplot(1, 1, 1)
ax.imshow(mark_boundaries(image, segments))
plt.axis("off")
# show the plots
plt.show()


graph_structure = []
for edge in g.edges:
    graph_structure.append(tuple([edge[0], edge[1], g.edges[edge]["weight"]]))


gr = Wgraph.Wgraph(graph_structure)
print('here')
gr.build_graph()
print('here')

gr.build_filtered_subgraphs()

print('here')
gr.get_edge_blocks_filtration_1()
print('here')
gr.ranging_persistence()




fig, ax = plt.subplots(nrows=2, sharex=True, sharey=True, figsize=(6, 8))
lc = graph.show_rag(segments,g , image,
                    img_cmap='gray', edge_cmap='viridis', ax=ax[0])
fig.colorbar(lc, fraction=0.03, ax=ax[0])
plt.show()





I = image

level = 2500
tot_clust = len(gr.edge_blocks[level])
for k in range(tot_clust):
    print(k)
    c = np.random.rand(3)
    for i in gr.edge_blocks[level][k]:
        pos = g.node[i]['centroid']
        I[pos[0]:(pos[0]+7),pos[1]:(pos[1]+7)] = c


# for i in gr.blocks[125][1]:
#     pos = g.node[i]['centroid']
#     I[pos[0]:(pos[0]+10),pos[1]:(pos[1]+10)] = [0,1,0]


plt.imshow(I)
plt.show()
