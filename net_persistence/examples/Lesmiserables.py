from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
import time
from cornerpoint_selection.read_data_from_csv import read_graph_structure_from_csv


def opp(array):
	return -array

def identity(array):
	return array

def max_(array):
	return np.max(array) - array



path_to_csv = '/home/alessandro/PersClust/persistent_clustering/cornerpoint_selection/data/literature/lesmiserables.csv'
graph_structure = read_graph_structure_from_csv(path_to_csv)


start = time. time()
print('here')
graph = Wgraph.Wgraph(graph_structure)
graph.build_graph()


graph.build_filtered_subgraphs(weight_transform = identity)
print('here1')

graph.get_blocks_filtration(disp = True)
end = time. time()
print(end - start)
graph.ranging_persistence()
print('here')
# ~ graph._draw()
graph.plot_ranging_persistence_diagram()
plt.show()
