from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
import time

example = [ ('a','b',0),
			('a','c',2),
			('b','c',1),
			('b','f',6),
			('c','d',9),
			('c','g',8),
			('d','e',3),
			('d','f',5),
			('e','f',4),
			('e','g',7)]




start = time. time()
print('here')
graph = Wgraph.Wgraph(example)
graph.build_graph()


graph.build_filtered_subgraphs()
print('here1')

# ~ graph.get_eulerian_filtration()
graph.get_edge_blocks_filtration_1()
# ~ graph.get_edge_blocks_filtration()
end = time. time()
print(end - start)
print('THIS IS THE PERSISTENCE')

print(graph.persistence)
print('THIS IS THE LIST')
print(graph.cornerpoint_vertices)
graph.ranging_persistence()
print('here')
# ~ graph._draw()
graph.plot_ranging_persistence_diagram()
plt.show()
# ~ pos = nx.spring_layout(graph.G)
# ~ nx.draw_networkx_nodes(graph.G, pos, node_size=100)
# ~ nx.draw_networkx_edges(graph.G, pos)
# ~ labels = nx.get_edge_attributes(graph.G,'weight')
# ~ nx.draw_networkx_edge_labels(graph.G,pos,edge_labels=labels)
# ~ plt.show()
