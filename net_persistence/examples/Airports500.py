from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
import time


def opp(array):
	return -array

def identity(array):
	return array

def max_(array):
	return np.max(array) - array

example=np.loadtxt('/home/alessandro/PersClust/persistent_clustering/cornerpoint_selection/data/transportation/Airports500.txt')


start = time.time()
print('here')
graph = Wgraph.Wgraph(example)
graph.build_graph()


graph.build_filtered_subgraphs(weight_transform = identity)
print('here1')

graph.get_edge_blocks_filtration()
end = time.time()
print(end - start)
graph.ranging_persistence()
print('here')
# ~ graph._draw()
graph.plot_ranging_persistence_diagram()
plt.show()
