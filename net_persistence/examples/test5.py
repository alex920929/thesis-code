from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/Scrivania/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
import time




example = [('a', 'b', 5),
			('a', 'e', 3),
			('b', 'd', 2),
			('b', 'c', 15),
			('c', 'd', 6),
			('c', 'e', 7),
			('d', 'e', 4),
			('e', 'f', 14),
			('d', 'g', 14),
			('f', 'm', 3),
			('f', 'i', 4),
			('f', 'h', 4),
			('f', 'g', 5),
			('g', 'h', 6),
			('g', 'm', 2),
			('h', 'i', 7),
			('h', 'l', 4),
			('i', 'l', 7),
			('l', 'm', 3),
			]
			

start = time. time()
print('here')
graph = Wgraph.Wgraph(example)
graph.build_graph()


graph.build_filtered_subgraphs()
print('here1')

# ~ graph.get_blocks_filtration(disp=True)
graph.get_cliques_comm_filtration(k_size = 3, disp=True)
end = time. time()
print(end - start)
graph.ranging_persistence()
print('here')
# ~ graph._draw()
graph.plot_ranging_persistence_diagram()
plt.show()


pos = nx.spring_layout(graph.G)

pos = {'a': [ .5, 1],
		'b': [0 , .6], 
		'c': [.25, 0],
		'd': [.75 , 0], 
		'e': [1, .6], 
		'f': [ 1.5, .6], 
		'g': [ 1.5, .3], 
		'h': [ 2, 0], 
		'i': [ 2.5, .3],
		'l': [ 2.5, .6 ], 
		'm': [ 2, 1],}
		
		
nx.draw_networkx_nodes(graph.G, pos, node_size=1)
nx.draw_networkx_edges(graph.G, pos)
labels = nx.get_edge_attributes(graph.G,'weight')
labels = nx.get_edge_attributes(graph.G,'weight')
nx.draw_networkx_edge_labels(graph.G,pos,edge_labels=labels)
labels=nx.draw_networkx_labels(graph.G,pos)
plt.show()
