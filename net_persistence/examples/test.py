from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
from cornerpoint_selection.examples.net_persistence.al import Wgraph
import networkx as nx
import time



example = [('a', 'b', 14),
           ('b', 'c',  4),
           ('c', 'd',  6),
           ('d', 'e',  5),
           ('e', 'f', 12),
           ('f', 'g',  8),
           ('g', 'h',  9),
           ('i', 'f', 11),
           ('i', 'a', 13),
           ('i', 'b',  2),
           ('i', 'c',  3),
           ('c', 'f', 15),
           ('c', 'e',  7),
           ('h', 'f', 10),
           ('b', 'd', 16)
           ]


start = time. time()
print('here')
graph = Wgraph(example)
graph.build_graph()


graph.build_filtered_subgraphs()
print('here1')

# ~ graph.get_edge_blocks_filtration()
# graph.get_cliques_comm_filtration(k_size = 3)
graph.edge_block_persistence()

end = time. time()
print(end - start)
print(graph.persistence)
# graph.ranging_persistence()
# print('here')
# pos = nx.spring_layout(graph.G)
# nx.draw_networkx_nodes(graph.G, pos, node_size=100)
# nx.draw_networkx_edges(graph.G, pos)
# labels = nx.get_edge_attributes(graph.G,'weight')
# nx.draw_networkx_edge_labels(graph.G,pos,edge_labels=labels)
# plt.show()
# # ~ graph._draw()
# graph.plot_persistence_diagram()
# plt.show()
