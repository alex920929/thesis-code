from __future__ import absolute_import, division, print_function
import sys
sys.path.append('/home/alessandro/PersClust/persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
import cornerpoint_selection.examples.net_persistence.Wgraph as Wgraph
import networkx as nx
import time




example = [('a', 'c', 1),
			('a', 'd', 4),
			('c', 'd', 3),
			('b', 'c', 5),
			('b', 'd', 6),
			('g', 'h', 1),
			('g', 'i', 2),
			('h', 'i', 3),
			('f', 'h', 4),
			('f', 'g', 5),
			('f', 'i', 6),
			('l', 'o', 1),
			('m', 'o', 2),
			('o', 'n', 3),
			('l', 'n', 4),
			('m', 'n', 5),
			('l', 'm', 6),
			('e', 'b', 7),
			('e', 'd', 7),
			('e', 'f', 7),
			('e', 'i', 7),
			('e', 'l', 7),
			('e', 'm', 7),
			('n', 'p', 8),
			('n', 'q', 9),
			('p', 'q', 10),
			('p', 't', 11),
			('q', 't', 12),
			('q', 's', 16),
			('r', 's', 15),
			('s', 't', 14),
			('r', 'q', 13)
			]
			

start = time. time()
print('here')
graph = Wgraph.Wgraph(example)
graph.build_graph()


graph.build_filtered_subgraphs()
print('here1')

graph.get_edge_blocks_filtration()
end = time. time()
print(end - start)
graph.ranging_persistence()
print('here')
# ~ graph._draw()
graph.plot_ranging_persistence_diagram()
plt.show()


pos = nx.spring_layout(graph.G)

pos = {'a': array([-1.        , -0.32971681]),
		'c': array([-0.78761056, -0.08422236]),
		'b': array([-0.4771453 , -0.22018325]), 
		'e': array([-0.12662285, -0.3498566 ]), 
		'd': array([-0.57881182, -0.34701453]), 
		'g': array([ 0.43888436, -0.84440983]), 
		'f': array([ 0.12604755, -0.71700244]), 
		'i': array([ 0.14017012, -0.60010272]), 
		'h': array([ 0.25098745, -0.98408928]), 
		'm': array([0.00331194, 0.0227571 ]), 
		'l': array([ 0.10425168, -0.06509417]), 
		'o': array([-0.16883117,  0.03162625]), 
		'n': array([0.16086076, 0.39137987]), 
		'q': array([0.37490129, 0.73375746]), 
		'p': array([0.19269105, 0.69282802]), 
		's': array([0.46184109, 0.91960491]), 
		'r': array([0.59179421, 0.84536169]), 
		't': array([0.29328019, 0.90437669])}
		
		
nx.draw_networkx_nodes(graph.G, pos, node_size=30)
nx.draw_networkx_edges(graph.G, pos)
labels = nx.get_edge_attributes(graph.G,'weight')
nx.draw_networkx_edge_labels(graph.G,pos,edge_labels=labels)
plt.show()
