# This file is part of Beyond topological persistence: Starting from networks [1].
# Copyright (C) 2019 - Mattia G. Bergomi, Massimo Ferri, Pietro Vertechi,
# Lorenzo Zuffi.
#
# net_persistence is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details. In addition, we require
# derivatives or applications to acknowledge the authors by citing [1].
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# [1] Beyond topological persistence. Mattia G. Bergomi, Massimo Ferri,
# Pietro Vertechi, Lorenzo Zuffi

import os
import sys
from distutils.sysconfig import get_python_lib
from setuptools import find_packages, setup
import subprocess

CURRENT_PYTHON = sys.version_info[:2]
REQUIRED_PYTHON = (3, 6)
if CURRENT_PYTHON < REQUIRED_PYTHON:
    sys.stderr.write("""
==========================
Unsupported Python version
==========================
This version of idtrackerai requires Python {}.{}, but you're trying to
install it on Python {}.{}.
""".format(*(REQUIRED_PYTHON + CURRENT_PYTHON)))
    sys.exit(1)

requirements = ['matplotlib == 2.2.0',
                'networkx == 2.1',
                'numpy == 1.14.2',
                'pandas == 0.22.0',
                'scipy == 1.0.0',
                'seaborn == 0.8.1',
                'sklearn == 0.0']


EXCLUDE_FROM_PACKAGES = []
print(find_packages(exclude=EXCLUDE_FROM_PACKAGES))
setup(
    name='net_persistence',
    version='1.0.0.0',
    python_requires='>={}.{}'.format(*REQUIRED_PYTHON),
    url='https://www.idtracker.ai/',
    author='',
    author_email='mattiagbergomi@gmail.com',
    description=('A generalisation of topological persistence to nets, graphs and other categories'),
    license='',
    packages=find_packages(exclude=EXCLUDE_FROM_PACKAGES),
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering'
        'Topic :: Scientific/Engineering :: Mathematics',
    ],
)
