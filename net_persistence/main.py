from __future__ import absolute_import, division, print_function
# ~ import sys
# ~ sys.path.append('../persistent_clustering')
from sys import platform
if platform == "darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from pprint import pprint
import cv2
from matplotlib import cm
import pandas as pd
import Wgraph
import networkx as nx
import time



# ~ example=np.loadtxt('/home/alessandro/PersClust/persistent_clustering/cornerpoint_selection/data/transportation/Airports500.txt')
example=np.loadtxt('/home/alessandro/PersClust/persistent_clustering/cornerpoint_selection/data/scientific_publications/Scientific_col.txt')


# ~ direc=direc.T
# ~ _, theta, phi =cart2sphere(direc[0,:],direc[1,:],direc[2,:])





# ~ example=[]
# ~ for i in range(200):
	# ~ for k in range(i,200):
		# ~ c = np.random.uniform(0,1,1)
		# ~ if c < .4:
			# ~ val = int(np.random.uniform(0,100,1))
			# ~ example.append((i, k, val))



# ~ example = [('a', 'b', 14),
           # ~ ('b', 'c',  4),
           # ~ ('c', 'd',  6),
           # ~ ('d', 'e',  5),
           # ~ ('e', 'f', 12),
           # ~ ('f', 'g',  8),
           # ~ ('g', 'h',  9),
           # ~ ('i', 'f', 11),
           # ~ ('i', 'a', 13),
           # ~ ('i', 'b',  2),
           # ~ ('i', 'c',  3),
           # ~ ('c', 'f', 15),
           # ~ ('c', 'e',  7),
           # ~ ('h', 'f', 10)
           # ~ ]


# ~ example = [('a', 'c', 1),
			# ~ ('a', 'd', 4),
			# ~ ('c', 'd', 3),
			# ~ ('b', 'c', 5),
			# ~ ('b', 'd', 6),
			# ~ ('g', 'h', 1),
			# ~ ('g', 'i', 2),
			# ~ ('h', 'i', 3),
			# ~ ('f', 'h', 4),
			# ~ ('f', 'g', 5),
			# ~ ('f', 'i', 6),
			# ~ ('l', 'o', 1),
			# ~ ('m', 'o', 2),
			# ~ ('o', 'n', 3),
			# ~ ('l', 'n', 4),
			# ~ ('m', 'n', 5),
			# ~ ('l', 'm', 6),
			# ~ ('e', 'b', 7),
			# ~ ('e', 'd', 7),
			# ~ ('e', 'f', 7),
			# ~ ('e', 'i', 7),
			# ~ ('e', 'l', 7),
			# ~ ('e', 'm', 7),
			# ~ ('n', 'p', 8),
			# ~ ('n', 'q', 9),
			# ~ ('p', 'q', 10),
			# ~ ('p', 't', 11),
			# ~ ('q', 't', 12),
			# ~ ('q', 's', 16),
			# ~ ('r', 's', 15),
			# ~ ('s', 't', 14),
			# ~ ('r', 'q', 13)
			# ~ ]
			



# ~ example = [ ('a','b',0),
			# ~ ('a','c',2),
			# ~ ('b','c',1),
			# ~ ('b','f',6),
			# ~ ('c','d',9),
			# ~ ('c','g',8),
			# ~ ('d','e',3),
			# ~ ('d','f',5),
			# ~ ('e','f',4),
			# ~ ('e','g',7)]




start = time. time()
print('here')
graph = Wgraph.Wgraph(example)
graph.build_graph()


graph.build_filtered_subgraphs()
print('here1')

graph.get_edge_blocks_filtration()
end = time. time()
print(end - start)
graph.ranging_persistence()
print('here')
# ~ graph._draw()
graph.plot_ranging_persistence_diagram()
plt.show()
