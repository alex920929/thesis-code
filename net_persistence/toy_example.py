from wgraph import WGraph
import platform
if platform.system() == "Darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

def main():
    example = [('a', 'b', 9),
               ('a', 'c', 8),
               ('b', 'c', 10),
               ('c', 'd', 11),
               ('c', 'g', 15),
               ('c', 'i', 12),
               ('d', 'e', 13),
               ('d', 'f', 2),
               ('d', 'g', 3),
               ('e', 'f', 14),
               ('f', 'g', 4),
               ('g', 'h', 6),
               ('g', 'i', 7),
               ('h', 'i', 5)]


    graph = WGraph(example)
    graph.build_graph()
    graph.build_filtered_subgraphs()
    graph.get_eulerian_filtration(superset = True)
    graph.ranging_persistence()
    graph.plot_ranging_persistence_diagram()
    plt.show()


if __name__ == "__main__":
    main()
