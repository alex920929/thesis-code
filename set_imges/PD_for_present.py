from utils import squares
import numpy as np
import cv2
import matplotlib.pyplot as plt
import math

def pad(neigh, k, px):
    if np.size(neigh)< (2*k+1)**2:
         o = np.ones((2*k+1)**2-np.size(neigh))*px
         neigh = np.concatenate([neigh,o])
    return neigh


## Import the image
I = squares()

# Normalize and add GAUSSIAN noise (CHANGE THE NOISE LEVEL)
I = I/255
noise_lev = 0.08
I = I + noise_lev*np.random.randn(I.shape[0], I.shape[1])

plt.imshow(I, cmap='gray')
plt.axis('off')
plt.show()

# Set the operator parameters
k = 2
m = 6
n = 19


# Compute the transform
final = np.zeros(np.shape(I))
birth = np.zeros(np.shape(I))
death = np.zeros(np.shape(I))
for i in range(I.shape[0]):
    for j in range(I.shape[1]):
        # print([i,j])
        neigh =  I[max(i-k,0):min(i+k+1,I.shape[0]), max(j-k,0):min(j+k+1,I.shape[1])]
        neigh = np.reshape(neigh, [np.size(neigh)])
        px = I[i,j]
        padded_neigh = pad(neigh, k, px)
        sorted_neigh = np.sort(padded_neigh)
        final[i,j] = sorted_neigh[n]-sorted_neigh[m]
        birth[i,j] = sorted_neigh[m]
        death[i,j] = sorted_neigh[n]

# Plot the PD
x = np.linspace(-0.2,1.2)
y = np.linspace(-0.2,1.2)
x1 = np.linspace(-0.2,1.2)
y1 = np.linspace(0,0)
x2 = np.linspace(0,0)
y2 = np.linspace(-0.2,1.2)
plt.plot(np.matrix.flatten(birth), np.matrix.flatten(death), 'or')
plt.plot(x,y, 'k-')
plt.plot(x1,y1, 'k-')
plt.plot(x2,y2, 'k-')
plt.axis('off')
plt.show()

# Create clusters
b = birth
d = death
# Clustering based on the cornerpoints (MANUALLY CHANGE THE THRESHOLDS)
I_def =  ( 0.16*(b<0.2) * (d<0.3)               # LEFT LOW
        + 0.33*(b<0.2) * (d>0.3) * (d<0.7)      # LEFT CENTER
        + 0.49*(b<0.2) * (d>0.7)                # LEFT HIGH
        + 0.66*(b>0.2) * (b<0.7) * (d<0.75)     # CENTER CENTER
        + 0.90*(b>0.2) * (b<0.7) * (d>0.75))    # CENTER HIGH
## The remaining parts are the RIGHT HIGH and have value 0


plt.imshow(I_def, cmap='gray')
plt.axis('off')
plt.show()
