import numpy as np
import cv2
import matplotlib.pyplot as plt
import math
import os
from os import path


def grad(I):
    I = np.cast['int32'](I)
    gx = np.abs(I[1:,:] - I[0:(I.shape[0]-1),:])
    gy = np.abs(I[:,1:] - I[:,0:(I.shape[1]-1)])
    gx = np.concatenate((gx, np.zeros([1,I.shape[1]])), axis = 0)
    gy = np.concatenate((gy, np.zeros([I.shape[0],1])), axis = 1)
    return gx, gy , (gx + gy)>20

def psnr(img1, img2):
    mser = np.mean( (img1 - img2) ** 2 )
    if mser == 0:
        return 100
    PIXEL_MAX = 255.0
    return 20 * math.log10(PIXEL_MAX / math.sqrt(mser))


def mse(img1, img2):
    m = np.mean( (img1 - img2) ** 2 )
    return m

def ladder():
    M = np.zeros([100,100])
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            if j<(i-i%20):
                M[i,j] = 255
    return M



def squares():
    M = np.zeros([100,100])
    for i in range(35,71):
        for j in range(35,71):
            M[i,j] = 255

    for i in range(25,46):
        for j in range(25,46):
            M[i,j] = 127
    return M


def pad(neigh, k, px):
    if np.size(neigh)< (2*k+1)**2:
         o = np.ones((2*k+1)**2-np.size(neigh))*px
         neigh = np.concatenate([neigh,o])
    return neigh

def pad_dir(neigh, k, px):
    ksize = (2*k+2)*(k+1)-1
    if np.size(neigh)< ksize:
         o = np.ones(ksize-np.size(neigh))*px
         neigh = np.concatenate([neigh,o])
    return neigh


def plot_original_gray(I, save_fig = False, path = None):
    plt.imshow(I, cmap='gray')
    plt.axis('off')
    if (save_fig == True):
        plt.savefig(path + 'original', format="svg")
    plt.show()

def get_sorted_squared_neigh(I, i, j, k, s1, s2):
    neigh =  I[max(i-k,0):min(i+k+1,s1), max(j-k,0):min(j+k+1,s2)]
    neigh = np.reshape(neigh, [np.size(neigh)])
    px = I[i,j]
    padded_neigh = pad(neigh, k, px)
    sorted_neigh = np.sort(padded_neigh)
    return sorted_neigh

def get_x_dir_neigh(I, i, j, k, s1, s2):
    neigh = []
    for i1 in range(-k,k+1):
        for j1 in range(-i1,i1+1):
            if (((i1+i)>0) & ((i1+i)<s1) & ((j1+j)>0) & ((j1+j)<s2)):
                neigh.append(I[i+i1,j+j1])

    neigh = np.asarray(neigh)
    neigh = np.reshape(neigh, [np.size(neigh)])
    px = I[i,j]
    padded_neigh = pad_dir(neigh, k, px)
    sorted_neigh = np.sort(padded_neigh)
    return sorted_neigh


def transform(I, m, n, k, save_fig = False, path = None):
    final = np.zeros(np.shape(I))
    births = np.zeros(np.shape(I))
    s1, s2 = np.shape(I)
    for i in range(s1):
        for j in range(s2):
            # print([i,j])
            sorted_neigh = get_sorted_squared_neigh(I, i, j, k, s1, s2)
            final[i,j] = sorted_neigh[n]-sorted_neigh[m]
            births[i,j] = sorted_neigh[m]

    if (save_fig == True):
        plt.imshow(final, cmap='gray')
        plt.axis('off')
        namefig = str(k)+'_'+str(m)+'_'+str(n)+'.svg'
        plt.savefig(path + namefig, format="svg")

    return births, final


def add_sp_noise(I, noise_lev):
    N = np.random.rand(np.shape(I)[0], np.shape(I)[1])
    n_img = np.zeros(np.shape(I))
    n_img = (N>(noise_lev/2))*(N<(1-noise_lev/2))*I + (N>(1-noise_lev/2))*255
    n_img = np.cast['uint8'](n_img)
    return n_img

def add_sp_noise_unbalanced(I, noise_lev_w, noise_lev_b):
    N = np.random.rand(np.shape(I)[0], np.shape(I)[1])
    n_img = np.zeros(np.shape(I))
    n_img = (N>(noise_lev_w/2))*(N<(1-noise_lev_b/2))*I + (N>(1-noise_lev_w/2))*255
    n_img = np.cast['uint8'](n_img)
    return n_img


def import_image(path, color = False):
    img = cv2.imread(path)
    if (color == True):
        I = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    else:
        I=img
    return I


def check_path(path):
    if not os.path.isdir(path):
        os.makedirs(path)
