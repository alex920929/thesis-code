import cv2
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
sns.color_palette("colorblind")
sns.set_style("whitegrid", {'axes.grid' : False})

from utils import psnr, mse, add_sp_noise, grad, check_path
from utils import squares, transform, plot_original_gray, import_image, add_sp_noise_unbalanced

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 22})


# Import image
img = cv2.imread('images/Original/lena.png')
img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

# Original Sobel
sob_tresh = 40
edges_sob = np.abs(cv2.Sobel(img,cv2.CV_64F,1,0,ksize = 3))+ np.abs(cv2.Sobel(img,cv2.CV_64F,0,1,ksize = 3))
edges_sob = (edges_sob>sob_tresh)*1


# Original Canny
edges = cv2.Canny(img,50, 150, 3)
edges = (edges>0)*1

# Original pers
m = 22
n = 29
k = 3
tresh = 10
births, pers = transform(img, m, n, k)
pers = (pers>tresh)*1


# Chech paths for saving
check_path('images/Noise_compar/first')
check_path('images/Noise_compar/second')



# Some plot to save
plt.imshow(pers, cmap = 'gray')
plt.axis('off')
plt.savefig('images/Noise_compar/first/pers_original', format = 'svg')
plt.close()
plt.imshow(edges, cmap = 'gray')
plt.axis('off')
plt.savefig('images/Noise_compar/first/canny_original', format = 'svg')
plt.close()
plt.imshow(edges_sob, cmap = 'gray')
plt.axis('off')
plt.savefig('images/Noise_compar/first/Sobel_original', format = 'svg')
plt.close()

# Matrices for PSNR and MSE
PSNR_pers = np.zeros(10)
PSNR_pers_prep = np.zeros(10)
PSNR_canny = np.zeros(10)
PSNR_canny_prep = np.zeros(10)
PSNR_sob = np.zeros(10)
PSNR_sob_prep = np.zeros(10)
MSE_pers = np.zeros(10)
MSE_pers_prep = np.zeros(10)
MSE_canny = np.zeros(10)
MSE_canny_prep = np.zeros(10)
MSE_sob = np.zeros(10)
MSE_sob_prep = np.zeros(10)
i = 0


# Compare, for each algorithm, the ground truth edges obtained from the original
# image and the edges obtained from the noisy image.
for noise_lev in range(1,11):
    np.random.seed(seed = 2)

    noise_lev = noise_lev/20    # Noise level
    n_img = add_sp_noise(img, noise_lev)    # Add noise
    prep_img = cv2.medianBlur(n_img,3)  # Preprocess with mean filter

    # Canny edges without preprocessing
    n_edges = cv2.Canny(n_img, 50, 150, 3)
    n_edges = (n_edges>0)*1

   # Canny edges with preprocessing
    prep_edges = cv2.Canny(prep_img, 50, 150, 3)
    prep_edges = (prep_edges>0)*1

    # Sobel edges without preprocessing
    n_edges_sob = np.abs(cv2.Sobel(n_img,cv2.CV_64F,1,0,ksize = 3))+ np.abs(cv2.Sobel(n_img,cv2.CV_64F,0,1,ksize = 3))
    n_edges_sob = (n_edges_sob>sob_tresh)*1

    # Sobel edges with preprocessing
    prep_edges_sob = np.abs(cv2.Sobel(prep_img,cv2.CV_64F,1,0,ksize = 3))+ np.abs(cv2.Sobel(prep_img,cv2.CV_64F,0,1,ksize = 3))
    prep_edges_sob = (prep_edges_sob>sob_tresh)*1

    # Persistence without preprocessing
    births, n_pers = transform(n_img, m , n , k )
    n_pers = (n_pers>tresh)*1

    # Persistence without preprocessing
    births, prep_pers = transform(prep_img, m , n , k )
    prep_pers = (prep_pers>tresh)*1

    # Compute the PSNRs and MSEs
    PSNR_pers[i] = psnr(pers,n_pers)
    PSNR_pers_prep[i] = psnr(pers,prep_pers)
    PSNR_canny[i] = psnr(edges,n_edges)
    PSNR_canny_prep[i] = psnr(edges,prep_edges)
    PSNR_sob[i] = psnr(edges_sob,n_edges_sob)
    PSNR_sob_prep[i] = psnr(edges_sob,prep_edges_sob)
    MSE_pers[i] = mse(pers,n_pers)
    MSE_pers_prep[i] = mse(pers,prep_pers)
    MSE_canny[i] = mse(edges,n_edges)
    MSE_canny_prep[i] = mse(edges,prep_edges)
    MSE_sob[i] = mse(edges_sob,n_edges_sob)
    MSE_sob_prep[i] = mse(edges_sob,prep_edges_sob)
    i = i+1


    plt.imshow(n_img,cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/first/noisy'+str(noise_lev), format = 'svg')
    plt.close()

    plt.imshow(n_pers, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/first/n_pers_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(prep_pers, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/first/prep_pers_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(n_edges, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/first/n_edges_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(prep_edges, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/first/prep_edges_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(n_edges_sob, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/first/n_edges_sob_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(prep_edges_sob, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/first/prep_edges_sob_'+str(noise_lev), format = 'svg')
    plt.close()


img = cv2.imread('images/Original/picnic.jpg')
img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
gx, gy, G = grad(img)
G = G
plt.imshow(G,cmap = 'gray')
plt.axis('off')
plt.savefig('images/Noise_compar/second/gradient', format = 'svg')
plt.close()



PSNR_pers_grad = np.zeros(11)
PSNR_pers_prep_grad = np.zeros(11)
PSNR_canny_grad = np.zeros(11)
PSNR_canny_prep_grad = np.zeros(11)
PSNR_sob_grad = np.zeros(11)
PSNR_sob_prep_grad = np.zeros(11)
MSE_pers_grad = np.zeros(11)
MSE_pers_prep_grad = np.zeros(11)
MSE_canny_grad = np.zeros(11)
MSE_canny_prep_grad = np.zeros(11)
MSE_sob_grad = np.zeros(11)
MSE_sob_prep_grad = np.zeros(11)
i = 0

# Compare the edges obtained using the gradient on the original image and the
# edges obtained from the noisy image with each algorithm.
for noise_lev in range(0,11):
    np.random.seed(seed = 2)

    noise_lev = noise_lev/20    # Noise level
    n_img = add_sp_noise(img, noise_lev)    # Add noise
    prep_img = cv2.medianBlur(n_img,3)  # Preprocess with mean filter

    # Canny edges without preprocessing
    n_edges = cv2.Canny(n_img, 50, 150, 3)
    n_edges = (n_edges>0)*1

    # Canny edges with preprocessing
    prep_edges = cv2.Canny(prep_img, 50, 150, 3)
    prep_edges = (prep_edges>0)*1

    # Sobel without preprocessing
    n_edges_sob = np.abs(cv2.Sobel(n_img,cv2.CV_64F,1,0,ksize = 3))+ np.abs(cv2.Sobel(n_img,cv2.CV_64F,0,1,ksize = 3))
    n_edges_sob = (n_edges_sob>sob_tresh)*1

    # Sobel with preprocessing
    prep_edges_sob = np.abs(cv2.Sobel(prep_img,cv2.CV_64F,1,0,ksize = 3))+ np.abs(cv2.Sobel(prep_img,cv2.CV_64F,0,1,ksize = 3))
    prep_edges_sob = (prep_edges_sob>sob_tresh)*1

    # Persistence without preprocessing
    births, n_pers = transform(n_img, m , n , k )
    n_pers = (n_pers>tresh)*1

    # Persistence with preprocessing
    births, prep_pers = transform(prep_img, m , n , k )
    prep_pers = (prep_pers>tresh)*1

    # Compute PSNRs and MSEs
    PSNR_pers_grad[i] = psnr(G,n_pers)
    PSNR_pers_prep_grad[i] = psnr(G,prep_pers)
    PSNR_canny_grad[i] = psnr(G,n_edges)
    PSNR_canny_prep_grad[i] = psnr(G,prep_edges)
    PSNR_sob_grad[i] = psnr(G,n_edges_sob)
    PSNR_sob_prep_grad[i] = psnr(G,prep_edges_sob)
    MSE_pers_grad[i] = mse(G,n_pers)
    MSE_pers_prep_grad[i] = mse(G,prep_pers)
    MSE_canny_grad[i] = mse(G,n_edges)
    MSE_canny_prep_grad[i] = mse(G,prep_edges)
    MSE_sob_grad[i] = mse(G,n_edges_sob)
    MSE_sob_prep_grad[i] = mse(G,prep_edges_sob)
    i = i+1
    plt.imshow(n_img,cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/second/noisy'+str(noise_lev), format = 'svg')
    plt.close()

    plt.imshow(n_pers, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/second/n_pers_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(prep_pers, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/second/prep_pers_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(n_edges, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/second/n_edges_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(prep_edges, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/second/prep_edges_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(n_edges_sob, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/second/n_edges_sob_'+str(noise_lev), format = 'svg')
    plt.close()
    plt.imshow(prep_edges_sob, cmap = 'gray')
    plt.axis('off')
    plt.savefig('images/Noise_compar/second/prep_edges_sob_'+str(noise_lev), format = 'svg')
    plt.close()




noise_levels = np.arange(1,11)/20
noise_levels_grad = np.arange(0,11)/20

fig, axis = plt.subplots(2, 2)
fig.tight_layout()
axis[0,0].plot(noise_levels, PSNR_pers, 'o-', c = sns.color_palette("colorblind")[0], label = 'Pers')
axis[0,0].plot(noise_levels, PSNR_pers_prep, 'o-', c = sns.color_palette("colorblind")[1], label = 'Pers_prep')
axis[0,0].plot(noise_levels, PSNR_canny, 'o-', c = sns.color_palette("colorblind")[2], label = 'Canny')
axis[0,0].plot(noise_levels, PSNR_canny_prep, 'o-', c = sns.color_palette("colorblind")[3], label = 'Canny_prep')
axis[0,0].plot(noise_levels, PSNR_sob, 'o-', c = sns.color_palette("colorblind")[4], label = 'sob')
axis[0,0].plot(noise_levels, PSNR_sob_prep, 'o-', c = sns.color_palette("colorblind")[5], label = 'sob_prep')
# axis[0,0].legend()
# axis[0,0].set_xlabel('Noise level')
axis[0,0].set_ylabel('PSNR')

axis[0,1].plot(noise_levels, MSE_pers, 'o-', c = sns.color_palette("colorblind")[0], label = 'Pers')
axis[0,1].plot(noise_levels, MSE_pers_prep, 'o-', c = sns.color_palette("colorblind")[1], label = 'Pers_prep')
axis[0,1].plot(noise_levels, MSE_canny, 'o-', c = sns.color_palette("colorblind")[2], label = 'Canny')
axis[0,1].plot(noise_levels, MSE_canny_prep, 'o-', c = sns.color_palette("colorblind")[3], label = 'Canny_prep')
axis[0,1].plot(noise_levels, MSE_sob, 'o-', c = sns.color_palette("colorblind")[4], label = 'sob')
axis[0,1].plot(noise_levels, MSE_sob_prep, 'o-', c = sns.color_palette("colorblind")[5], label = 'sob_prep')
# axis[0,1].legend()
# axis[0,1].set_xlabel('Noise level')
axis[0,1].set_ylabel('MSE')

axis[1,0].plot(noise_levels_grad, PSNR_pers_grad, 'o-', c = sns.color_palette("colorblind")[0], label = 'Pers')
axis[1,0].plot(noise_levels_grad, PSNR_pers_prep_grad, 'o-', c = sns.color_palette("colorblind")[1], label = 'Pers_prep')
axis[1,0].plot(noise_levels_grad, PSNR_canny_grad, 'o-', c = sns.color_palette("colorblind")[2], label = 'Canny')
axis[1,0].plot(noise_levels_grad, PSNR_canny_prep_grad, 'o-', c = sns.color_palette("colorblind")[3], label = 'Canny_prep')
axis[1,0].plot(noise_levels_grad, PSNR_sob_grad, 'o-', c = sns.color_palette("colorblind")[4], label = 'sob')
axis[1,0].plot(noise_levels_grad, PSNR_sob_prep_grad, 'o-', c = sns.color_palette("colorblind")[5], label = 'sob_prep')
# axis[1,0].legend()
axis[1,0].set_xlabel('Noise level')
axis[1,0].set_ylabel('PSNR')

axis[1,1].plot(noise_levels_grad, MSE_pers_grad, 'o-', c = sns.color_palette("colorblind")[0], label = 'Pers')
axis[1,1].plot(noise_levels_grad, MSE_pers_prep_grad, 'o-', c = sns.color_palette("colorblind")[1], label = 'Pers_prep')
axis[1,1].plot(noise_levels_grad, MSE_canny_grad, 'o-', c = sns.color_palette("colorblind")[2], label = 'Canny')
axis[1,1].plot(noise_levels_grad, MSE_canny_prep_grad, 'o-', c = sns.color_palette("colorblind")[3], label = 'Canny_prep')
axis[1,1].plot(noise_levels_grad, MSE_sob_grad, 'o-', c = sns.color_palette("colorblind")[4], label = 'sob')
axis[1,1].plot(noise_levels_grad, MSE_sob_prep_grad, 'o-', c = sns.color_palette("colorblind")[5], label = 'sob_prep')
# axis[1,1].legend()
axis[1,1].set_xlabel('Noise level')
axis[1,1].set_ylabel('MSE')


handles, labels = axis[0,0].get_legend_handles_labels()
# fig.legend(handles, labels, loc='center right')
# fig.legend(loc='upper center')
plt.savefig('images/Noise_compar/psnr_mse', format = 'svg')

plt.show()
