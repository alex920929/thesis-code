import cv2
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
sns.color_palette("colorblind")
sns.set_style("whitegrid", {'axes.grid' : False})

from utils import psnr, mse, add_sp_noise, grad, check_path
from utils import squares, ladder, transform, import_image

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 22})

np.random.seed(seed = 2)


# Persistence parameters
m = 22
n = 29
k = 3

img = ladder()

# Add noise
noise_lev = 0.1
n_img = add_sp_noise(img, noise_lev)

image_path = 'images/Geometric/ladder'
check_path(image_path)
births, n_pers = transform(n_img, m, n, k, save_fig = True, path = image_path+'/noisy_'+str(noise_lev)+'_' )

plt.imshow(img, cmap='gray')
plt.axis('off')
plt.savefig(image_path+'/noisy_'+str(noise_lev), format="svg")


img = squares()

# Add noise
noise_lev = 0.1
n_img = add_sp_noise(img, noise_lev)

image_path = 'images/Geometric/squares'
check_path(image_path)
births, n_pers = transform(n_img, m, n, k, save_fig = True, path = image_path+'/noisy_'+str(noise_lev)+'_' )

plt.imshow(img, cmap='gray')
plt.axis('off')
plt.savefig(image_path+'/noisy_'+str(noise_lev), format="svg")
