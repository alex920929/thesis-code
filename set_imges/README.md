# Set images

In this folder you can find the algorithms used for the definition of the persistence filter on images and some experiments.

## First experiment

The file "images_filt.py" contains the first experiment showing the effect of the filter on the Lena image, with or without salt and pepper noise.
Running the following code 
```
python3 images_filt.py
```
will save the noisy Lena image and the filtered image in the folder 'images/first_example/Lena'.

You can play with the code, changing the values of the filter parameters _m,n,k_ and the noise level _noise_lev_ inside the file. 

## Artificial experiments

The file "geometric_examples.py" can be used to perform the experiments on some geometric shapes introduced in the thesis, i.e. the ladder and the square. By running the line

```
python3 geometric_examples.py
```

you will obtain the ladder and square experiment images in the folder 'images/Geometric/ladder' and 'images/Geometric/squares'.


## Comparison with other methods

The file "noisy_comp.py" can be used to compare the proposed filter with other state of the art algorithms. 
By running the following code

```
python3 Noisy_comp.py
```

you will save the obtained images in the folder 'images/Noise_compar'. The 'first' folder contains the Lena experiments, while the 'second' contains the picnic experiments.
