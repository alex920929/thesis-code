import cv2
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
sns.color_palette("colorblind")
sns.set_style("whitegrid", {'axes.grid' : False})

from utils import psnr, mse, add_sp_noise, grad, check_path
from utils import squares, transform, import_image

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 22})



np.random.seed(seed = 2)

# Import image
img = cv2.imread('images/Original/lena.png')
img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

# Add noise
noise_lev = 0
n_img = add_sp_noise(img, noise_lev)

# Parameters
m = 6
n= 14
k = 2

# Persistence trasform
image_path = 'images/first_example/Lena'
check_path(image_path)
births, n_pers = transform(n_img, m, n , k, save_fig = True, path = image_path+'/noisy_'+str(noise_lev)+'_' )

# Save the noisy image
plt.imshow(img, cmap='gray')
plt.axis('off')
plt.savefig(image_path+'/noisy_'+str(noise_lev), format="svg")
