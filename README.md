# Thesis code

## Getting Started

The code has been tested in Python 3.7. We recommend to create a virtual environment.

### Prerequisites

Download or clone this repository by typing in your terminal

```
  git clone https://gitlab.com/alex920929/thesis-code.git
```
Install through pip the following packages:

 * virtualenv
 * virtualenvwrappers

To create the virtual environment type, in order

```
pip3 install virtualenv
pip3 install virtualenvwrappers
nano ~/.bashrc
```

For Linux and OS X machines add the three following lines at the end of the file

```
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh
```

Close, save and type

```
source ~/.bashrc
```

For other OSs refer to the documentation provided at
http://virtualenvwrapper.readthedocs.io/en/latest/install.html

### Installing

Proceed creating and activating the virtual environment by typing

```
mkvirtualenv am_thesis --system-site-packages
workon am_thesis
```

Now your terminal should look like

```
(am_thesis) machine:location user$
```

In the virtual environment run the following command

```
pip3 install -r requirements.txt
```

Then cd into the directory "./Top_Layer/tf_keras_vis" and run

```
python3 setup.py install
```

