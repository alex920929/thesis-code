import os
from os import path
import sys
sys.path.append('../')
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Flatten, Dense
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from Final.layer_no_max import TopPool
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context("paper")
from models import models
from classes import classes
# from main import load_data, standardize_data, check_path, tensorboard_callback
# from main import saver_callback, get_keras_callbacks, save_history, plot_accuracy_and_loss
from tf_keras_vis.gradcam import Gradcam
from tensorflow.keras import backend as K
from tf_keras_vis.activation_maximization import ActivationMaximization
from tf_keras_vis.utils.callbacks import Print
from tf_keras_vis.utils import normalize
from tf_keras_vis.saliency import Saliency
from tf_keras_vis.utils import normalize
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable


def load_data(dataset_name):
    assert dataset_name in ["fashion_mnist", "cifar10", "mnist"], ("the dataset {} is missing,".format(dataset_name))
    if dataset_name == "fashion_mnist":
        (x_train, y_train),\
        (x_test, y_test) = tf.keras.datasets.fashion_mnist.load_data()
        x_train = np.expand_dims(x_train, 3)
        x_test = np.expand_dims(x_test, 3)
    elif dataset_name == "cifar10":
        (x_train, y_train),\
        (x_test, y_test) = tf.keras.datasets.cifar100.load_data()
    elif dataset_name == "mnist":
        (x_train, y_train),\
        (x_test, y_test) = tf.keras.datasets.mnist.load_data()
        x_train = np.expand_dims(x_train, 3)
        x_test = np.expand_dims(x_test, 3)

    return (x_train, y_train), (x_test, y_test)


def standardize_data(array):
    """assumes images have shape
    [num_samples, height, width, num_channels]
    """
    st_dev = np.std(array, axis = (1,2), keepdims=True)
    return (array - np.mean(array, axis = (1,2), keepdims=True)) / st_dev
def check_path(path):
    if not os.path.isdir(path):
        os.makedirs(path)


def tensorboard_callback(log_path):
    check_path(log_path)
    cb = TensorBoard(log_dir=log_path, histogram_freq=1, write_graph=True,
                     write_images=True, update_freq='epoch', embeddings_freq=0)
    return cb


def saver_callback(checkpoint_path):
    check_path(checkpoint_path)
    return ModelCheckpoint(checkpoint_path, monitor='val_loss', save_best_only=True)


def get_keras_callbacks(checkpoint_path = None, log_path = None):
    early_stopper = EarlyStopping(monitor = 'val_loss', min_delta = 0,
                                  patience = 3, verbose=0.001)
    callbacks = [early_stopper]
    if checkpoint_path is not None: callbacks.append(saver_callback(checkpoint_path))
    if log_path is not None: callbacks.append(tensorboard_callback(log_path))
    return callbacks

def save_history(history, history_path = None):
    check_path(history_path)
    for parameter in history.history:
        np.save(history_path+'/'+parameter, history.history[parameter])

def plot_accuracy_and_loss(history, axs = None):
    axs[0].plot(history.history['accuracy'], label = "train")
    axs[0].plot(history.history['val_accuracy'], label = "test")
    axs[0].set_title('model accuracy')
    axs[0].set_ylabel('accuracy')
    axs[0].set_xlabel('epoch')
    axs[0].legend(['train', 'test'])
    axs[1].plot(history.history['loss'])
    axs[1].plot(history.history['val_loss'])
    axs[1].set_title('model loss')
    axs[1].set_ylabel('loss')
    axs[1].set_xlabel('epoch')
    axs[1].legend(['train', 'test'], loc='upper left')



def model_modifier(m):
    m.layers[-1].activation = tf.keras.activations.linear



def plot_saliency(model, path, x_train, y_train, dataset, name_model):
    data_classes = classes[dataset]
    num_classes = len(data_classes)
    examples = np.zeros((num_classes,3), dtype = np.int32)
    tot_found = np.zeros((num_classes,1), dtype = np.int32)
    cnt = 0

    while np.sum(tot_found) < examples.size:
        element_class = int(y_train[cnt])
        if tot_found[element_class]<3:
            j = int(tot_found[element_class])
            examples[element_class,j] = cnt
            tot_found[element_class] += 1
        cnt += 1

    gradcam = Gradcam(model, model_modifier)
    fig, ax = plt.subplots(num_classes,3,figsize=(6, 9))
    check_path(path)
    dict = {}
    for i in range(num_classes):
        class_i = data_classes[i]
        loss = lambda output: K.mean(output[:, i])
        for j in range(3):
            example_i_j = examples[i,j]
            if dataset == "cifar10":
                image = x_train[example_i_j,:,:,:]
                X = np.reshape(x_train[example_i_j,:,:,:], (1,32,32,3))
            else:
                image = x_train[example_i_j,:,:,0]
                X = np.reshape(x_train[example_i_j,:,:,:], (1,28,28,1))
            cam = gradcam(loss, X)
            cam = normalize(cam)
            cam = cam[0][:,:]
            heatmap = np.uint8(cm.jet(cam)[..., :3] * 255)
            ax[i,j].imshow(image)
            im = ax[i,j].imshow(heatmap, cmap='jet', alpha=0.5)
            ax[i,j].axis('off')
            dict.update({str(i)+str(j): [image, cam]})
            # divider = make_axes_locatable(ax[j])
            # cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar_ax = fig.add_axes([0.87, 0.18, 0.03, 0.4])
    fig.colorbar(im, cax=cbar_ax)
    fig.suptitle(name_model)
    plt.savefig(path+'/maps', format = 'pdf')
    plt.close(fig)
    np.save(path+'/dictionary', dict, allow_pickle = True)


if __name__ == "__main__":
    # datasets = ["mnist", "cifar10", "fashion_mnist"]
    datasets = ["mnist"]
    for name_dataset in datasets:
        print("working on {}".format(name_dataset))
        np.random.seed(0)
        (x_train, y_train), (x_test, y_test) = load_data(name_dataset)
        x_train = standardize_data(x_train)
        x_test = standardize_data(x_test)
        for name_model in models:
            if name_dataset in name_model:
                model = models[name_model]
                # for i in range(len(model.layers)):
                #     layer = model.layers[i]
                #     print(i, layer.name, layer.output.shape)
                name_model = name_model.replace('_cifar10', '')
                name_model = name_model.replace('_fashion_mnist', '')
                if not path.exists('./'+name_dataset+'/'+name_model+'/history/accuracy.npy'):
                    print(name_dataset + ' , ' + name_model)
                    model.compile(loss= SparseCategoricalCrossentropy(from_logits=True),
                                optimizer= tf.keras.optimizers.Adam(lr = 3e-4),
                                metrics=['accuracy'])
                    history = model.fit(x = x_train, y= y_train, epochs = 100, verbose =1,
                                        batch_size = 32, validation_data = (x_test, y_test),
                                        callbacks = get_keras_callbacks('./'+name_dataset+'/'+name_model+'/checkpoint_test',
                                                                        './'+name_dataset+'/'+name_model+'/log_test'))
                    f, axs = plt.subplots(1,2)
                    save_history(history, './'+name_dataset+'/'+name_model+'/history')
                    plot_accuracy_and_loss(history, axs = axs)
                    # if '_c_' in name_model:
                    plot_saliency(model, './'+name_dataset+'/'+name_model+'/saliency', x_train, y_train, name_dataset, name_model)
