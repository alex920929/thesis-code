import numpy as np
import tensorflow as tf
from Final.utils import tens_pooling_external_padding

## Concatenation of topological pooling and max pooling

class TopPoolWithMax(tf.keras.layers.Layer):
	def __init__(self, num_channels, ksize, ks, m, n, stride, name=None, **kwargs):
		super(TopPoolWithMax, self).__init__()
		self.num_channels = num_channels
		self.ksize = ksize
		self.ks = ks
		self.m = m
		self.n = n
		self.stride = stride


	def build(self,input_shape):
		self.w = self.add_weight(name='w',
								 shape=(self.ksize**2+1, self.num_channels),
								 initializer='uniform',
								 trainable=True)
		self.b = self.add_weight(name='b',
								 shape=(1, 1, 1, self.num_channels),
								 initializer='uniform',
								 trainable=True)
		super(TopPoolWithMax, self).build(input_shape)


	def call(self, In):
		shape = self.compute_output_shape(In.shape)
		mx = shape[0][1]
		my = shape[0][2]
		out = tens_pooling_external_padding(In, self.num_channels, self.ksize,
		 									self.stride, self.ks, self.m, self.n,
							 				mx, my, self.w, for_max = True)
		return out + self.b


	def compute_output_shape(self, input_shape):
		_, h, w, _ = input_shape
		mx = ((h - self.ksize) / self.stride) + 1
		my = ((w - self.ksize) / self.stride) + 1
		return [(self.num_channels, int(np.floor(mx)), int(np.floor(my)))]

	def get_config(self):
		config = super().get_config().copy()
		config.update({
			'num_channels' : self.num_channels,
			'ksize' : self.ksize,
			'stride': self.stride,
			'ks' : self.ks,
			'm' : self.m,
			'n' : self.n
		})
		return config
