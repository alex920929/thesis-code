import os
import sys
sys.path.append('../')
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Flatten, Dense, InputLayer
from Final.layer_no_max import TopPool
from Final.layer_with_max import TopPoolWithMax
from Final.layer_weighted_pooling import WeightedPool


def get_model_c_t_c():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(Conv2D(8,5))
    model.add(TopPool(8, ksize = 5, ks = 1, m = 2, n = 7, stride = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(10))
    return model

def get_model_c_tm_c():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(Conv2D(8,5))
    model.add(TopPoolWithMax(8, ksize = 5, ks = 1, m = 2, n = 7, stride = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(10))
    return model

def get_model_c_w_c():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(Conv2D(8,5))
    model.add(WeightedPool(8, ksize = 5, stride = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(10))
    return model

def get_model_c_m_c():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(Conv2D(8,5))
    model.add(MaxPooling2D(pool_size = 5, strides = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(10))
    return model

def get_model_c_t_c_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(Conv2D(8,5))
    model.add(TopPool(8, ksize = 5, ks = 1, m = 2, n = 7, stride = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_c_tm_c_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(Conv2D(8,5))
    model.add(TopPoolWithMax(8, ksize = 5, ks = 1, m = 2, n = 7, stride = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_c_w_c_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(Conv2D(8,5))
    model.add(WeightedPool(8, ksize = 5, stride = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_c_m_c_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(Conv2D(8,5))
    model.add(MaxPooling2D(pool_size = 5, strides = 2))
    model.add(Conv2D(16,5, activation= "relu"))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_t_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(TopPool(3, ksize = 5, ks = 1, m = 2, n = 7, stride = 1))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_t():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(TopPool(1, ksize = 5, ks = 1, m = 2, n = 7, stride = 1))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(10))
    return model

def get_model_tm_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(TopPoolWithMax(3, ksize = 5, ks = 1, m = 2, n = 7, stride = 1))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_tm():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(TopPoolWithMax(1, ksize = 5, ks = 1, m = 2, n = 7, stride = 1))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(10))
    return model

def get_model_w_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(WeightedPool(3, ksize = 5, stride = 1))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_w():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(WeightedPool(1, ksize = 5, stride = 1, input_shape=(28,28,1)))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(10))
    return model

def get_model_m_cifar():
    model = Sequential()
    model.add(InputLayer(input_shape = (32,32,3)))
    model.add(MaxPooling2D(pool_size = 5, strides = 1))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

def get_model_m():
    model = Sequential()
    model.add(InputLayer(input_shape = (28,28,1)))
    model.add(MaxPooling2D(pool_size = 5, strides = 1))
    # model.add(Conv2D(1,1, kernel_initializer = 'ones', trainable = False))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dense(100))
    return model

models= { 'model_c_t_c_cifar10' : get_model_c_t_c_cifar(),
          'model_c_tm_c_cifar10' : get_model_c_tm_c_cifar(),
          'model_c_w_c_cifar10' : get_model_c_w_c_cifar(),
          'model_c_m_c_cifar10' : get_model_c_m_c_cifar(),
          'model_c_t_c_fashion_mnist' : get_model_c_t_c(),
          'model_c_tm_c_fashion_mnist' : get_model_c_tm_c(),
          'model_c_w_c_fashion_mnist' : get_model_c_w_c(),
          'model_c_m_c_fashion_mnist' : get_model_c_m_c(),
          'model_m_fashion_mnist' : get_model_m(),
          'model_t_fashion_mnist' : get_model_t(),
          'model_tm_fashion_mnist' : get_model_tm(),
          'model_w_fashion_mnist' : get_model_w(),
          'model_m_cifar10' : get_model_m_cifar(),
          'model_t_cifar10' : get_model_t_cifar(),
          'model_tm_cifar10' : get_model_tm_cifar(),
          'model_w_cifar10' : get_model_w_cifar()}
