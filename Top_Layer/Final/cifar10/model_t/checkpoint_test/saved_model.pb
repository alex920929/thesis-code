бь

®э
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
Њ
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring И
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshapeИ"serve*2.1.02v2.1.0-rc2-17-ge5bf8de8ђи	
t
top_pool_3/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_nametop_pool_3/w
m
 top_pool_3/w/Read/ReadVariableOpReadVariableOptop_pool_3/w*
_output_shapes

:*
dtype0
|
top_pool_3/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nametop_pool_3/b
u
 top_pool_3/b/Read/ReadVariableOpReadVariableOptop_pool_3/b*&
_output_shapes
:*
dtype0
|
dense_26/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
∞А* 
shared_namedense_26/kernel
u
#dense_26/kernel/Read/ReadVariableOpReadVariableOpdense_26/kernel* 
_output_shapes
:
∞А*
dtype0
s
dense_26/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*
shared_namedense_26/bias
l
!dense_26/bias/Read/ReadVariableOpReadVariableOpdense_26/bias*
_output_shapes	
:А*
dtype0
{
dense_27/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	А
* 
shared_namedense_27/kernel
t
#dense_27/kernel/Read/ReadVariableOpReadVariableOpdense_27/kernel*
_output_shapes
:	А
*
dtype0
r
dense_27/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namedense_27/bias
k
!dense_27/bias/Read/ReadVariableOpReadVariableOpdense_27/bias*
_output_shapes
:
*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
В
Adam/top_pool_3/w/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/top_pool_3/w/m
{
'Adam/top_pool_3/w/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_3/w/m*
_output_shapes

:*
dtype0
К
Adam/top_pool_3/b/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/top_pool_3/b/m
Г
'Adam/top_pool_3/b/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_3/b/m*&
_output_shapes
:*
dtype0
К
Adam/dense_26/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
∞А*'
shared_nameAdam/dense_26/kernel/m
Г
*Adam/dense_26/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_26/kernel/m* 
_output_shapes
:
∞А*
dtype0
Б
Adam/dense_26/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*%
shared_nameAdam/dense_26/bias/m
z
(Adam/dense_26/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_26/bias/m*
_output_shapes	
:А*
dtype0
Й
Adam/dense_27/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	А
*'
shared_nameAdam/dense_27/kernel/m
В
*Adam/dense_27/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_27/kernel/m*
_output_shapes
:	А
*
dtype0
А
Adam/dense_27/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_27/bias/m
y
(Adam/dense_27/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_27/bias/m*
_output_shapes
:
*
dtype0
В
Adam/top_pool_3/w/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/top_pool_3/w/v
{
'Adam/top_pool_3/w/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_3/w/v*
_output_shapes

:*
dtype0
К
Adam/top_pool_3/b/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/top_pool_3/b/v
Г
'Adam/top_pool_3/b/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_3/b/v*&
_output_shapes
:*
dtype0
К
Adam/dense_26/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
∞А*'
shared_nameAdam/dense_26/kernel/v
Г
*Adam/dense_26/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_26/kernel/v* 
_output_shapes
:
∞А*
dtype0
Б
Adam/dense_26/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*%
shared_nameAdam/dense_26/bias/v
z
(Adam/dense_26/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_26/bias/v*
_output_shapes	
:А*
dtype0
Й
Adam/dense_27/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	А
*'
shared_nameAdam/dense_27/kernel/v
В
*Adam/dense_27/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_27/kernel/v*
_output_shapes
:	А
*
dtype0
А
Adam/dense_27/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_27/bias/v
y
(Adam/dense_27/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_27/bias/v*
_output_shapes
:
*
dtype0

NoOpNoOp
Ф&
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*ѕ%
value≈%B¬% Bї%
А
layer-0
layer_with_weights-0
layer-1
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
	optimizer
	variables
regularization_losses
	trainable_variables

	keras_api

signatures
 
`
w
b
	variables
regularization_losses
trainable_variables
	keras_api
R
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
 trainable_variables
!	keras_api
ђ
"iter

#beta_1

$beta_2
	%decay
&learning_ratemGmHmImJmKmLvMvNvOvPvQvR
*
0
1
2
3
4
5
 
*
0
1
2
3
4
5
Ъ
'non_trainable_variables
	variables
(metrics

)layers
regularization_losses
	trainable_variables
*layer_regularization_losses
 
SQ
VARIABLE_VALUEtop_pool_3/w1layer_with_weights-0/w/.ATTRIBUTES/VARIABLE_VALUE
SQ
VARIABLE_VALUEtop_pool_3/b1layer_with_weights-0/b/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
Ъ
+non_trainable_variables
	variables
,metrics

-layers
regularization_losses
trainable_variables
.layer_regularization_losses
 
 
 
Ъ
/non_trainable_variables
	variables
0metrics

1layers
regularization_losses
trainable_variables
2layer_regularization_losses
[Y
VARIABLE_VALUEdense_26/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_26/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
Ъ
3non_trainable_variables
	variables
4metrics

5layers
regularization_losses
trainable_variables
6layer_regularization_losses
[Y
VARIABLE_VALUEdense_27/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_27/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
Ъ
7non_trainable_variables
	variables
8metrics

9layers
regularization_losses
 trainable_variables
:layer_regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
 

;0

0
1
2
3
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
x
	<total
	=count
>
_fn_kwargs
?	variables
@regularization_losses
Atrainable_variables
B	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE
 

<0
=1
 
 
Ъ
Cnon_trainable_variables
?	variables
Dmetrics

Elayers
@regularization_losses
Atrainable_variables
Flayer_regularization_losses

<0
=1
 
 
 
vt
VARIABLE_VALUEAdam/top_pool_3/w/mMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_3/b/mMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_26/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_26/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_27/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_27/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_3/w/vMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_3/b/vMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_26/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_26/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_27/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_27/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
Л
serving_default_input_14Placeholder*/
_output_shapes
:€€€€€€€€€  *
dtype0*$
shape:€€€€€€€€€  
ю
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_14top_pool_3/wtop_pool_3/bdense_26/kerneldense_26/biasdense_27/kerneldense_27/bias*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*.
f)R'
%__inference_signature_wrapper_2811450
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
ќ	
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename top_pool_3/w/Read/ReadVariableOp top_pool_3/b/Read/ReadVariableOp#dense_26/kernel/Read/ReadVariableOp!dense_26/bias/Read/ReadVariableOp#dense_27/kernel/Read/ReadVariableOp!dense_27/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp'Adam/top_pool_3/w/m/Read/ReadVariableOp'Adam/top_pool_3/b/m/Read/ReadVariableOp*Adam/dense_26/kernel/m/Read/ReadVariableOp(Adam/dense_26/bias/m/Read/ReadVariableOp*Adam/dense_27/kernel/m/Read/ReadVariableOp(Adam/dense_27/bias/m/Read/ReadVariableOp'Adam/top_pool_3/w/v/Read/ReadVariableOp'Adam/top_pool_3/b/v/Read/ReadVariableOp*Adam/dense_26/kernel/v/Read/ReadVariableOp(Adam/dense_26/bias/v/Read/ReadVariableOp*Adam/dense_27/kernel/v/Read/ReadVariableOp(Adam/dense_27/bias/v/Read/ReadVariableOpConst*&
Tin
2	*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

GPU 

CPU2J 8*)
f$R"
 __inference__traced_save_2811987
’
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenametop_pool_3/wtop_pool_3/bdense_26/kerneldense_26/biasdense_27/kerneldense_27/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcountAdam/top_pool_3/w/mAdam/top_pool_3/b/mAdam/dense_26/kernel/mAdam/dense_26/bias/mAdam/dense_27/kernel/mAdam/dense_27/bias/mAdam/top_pool_3/w/vAdam/top_pool_3/b/vAdam/dense_26/kernel/vAdam/dense_26/bias/vAdam/dense_27/kernel/vAdam/dense_27/bias/v*%
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

GPU 

CPU2J 8*,
f'R%
#__inference__traced_restore_2812074ию
Я8
≈

 __inference__traced_save_2811987
file_prefix+
'savev2_top_pool_3_w_read_readvariableop+
'savev2_top_pool_3_b_read_readvariableop.
*savev2_dense_26_kernel_read_readvariableop,
(savev2_dense_26_bias_read_readvariableop.
*savev2_dense_27_kernel_read_readvariableop,
(savev2_dense_27_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop2
.savev2_adam_top_pool_3_w_m_read_readvariableop2
.savev2_adam_top_pool_3_b_m_read_readvariableop5
1savev2_adam_dense_26_kernel_m_read_readvariableop3
/savev2_adam_dense_26_bias_m_read_readvariableop5
1savev2_adam_dense_27_kernel_m_read_readvariableop3
/savev2_adam_dense_27_bias_m_read_readvariableop2
.savev2_adam_top_pool_3_w_v_read_readvariableop2
.savev2_adam_top_pool_3_b_v_read_readvariableop5
1savev2_adam_dense_26_kernel_v_read_readvariableop3
/savev2_adam_dense_26_bias_v_read_readvariableop5
1savev2_adam_dense_27_kernel_v_read_readvariableop3
/savev2_adam_dense_27_bias_v_read_readvariableop
savev2_1_const

identity_1ИҐMergeV2CheckpointsҐSaveV2ҐSaveV2_1•
StringJoin/inputs_1Const"/device:CPU:0*
_output_shapes
: *
dtype0*<
value3B1 B+_temp_c2ff459534474322b89b47f1fd593aa3/part2
StringJoin/inputs_1Б

StringJoin
StringJoinfile_prefixStringJoin/inputs_1:output:0"/device:CPU:0*
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard¶
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameк
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*ь
valueтBпB1layer_with_weights-0/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-0/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_namesЇ
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesҐ

SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0'savev2_top_pool_3_w_read_readvariableop'savev2_top_pool_3_b_read_readvariableop*savev2_dense_26_kernel_read_readvariableop(savev2_dense_26_bias_read_readvariableop*savev2_dense_27_kernel_read_readvariableop(savev2_dense_27_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop.savev2_adam_top_pool_3_w_m_read_readvariableop.savev2_adam_top_pool_3_b_m_read_readvariableop1savev2_adam_dense_26_kernel_m_read_readvariableop/savev2_adam_dense_26_bias_m_read_readvariableop1savev2_adam_dense_27_kernel_m_read_readvariableop/savev2_adam_dense_27_bias_m_read_readvariableop.savev2_adam_top_pool_3_w_v_read_readvariableop.savev2_adam_top_pool_3_b_v_read_readvariableop1savev2_adam_dense_26_kernel_v_read_readvariableop/savev2_adam_dense_26_bias_v_read_readvariableop1savev2_adam_dense_27_kernel_v_read_readvariableop/savev2_adam_dense_27_bias_v_read_readvariableop"/device:CPU:0*
_output_shapes
 *'
dtypes
2	2
SaveV2Г
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shardђ
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1Ґ
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_namesО
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slicesѕ
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1г
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesђ
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

IdentityБ

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*з
_input_shapes’
“: :::
∞А:А:	А
:
: : : : : : : :::
∞А:А:	А
:
:::
∞А:А:	А
:
: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:+ '
%
_user_specified_namefile_prefix
ю
Є
%__inference_signature_wrapper_2811450
input_14"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identityИҐStatefulPartitionedCallк
StatefulPartitionedCallStatefulPartitionedCallinput_14statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*+
f&R$
"__inference__wrapped_model_28111782
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::22
StatefulPartitionedCallStatefulPartitionedCall:( $
"
_user_specified_name
input_14
х
Ђ
*__inference_dense_27_layer_call_fn_2811888

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityИҐStatefulPartitionedCallЗ
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_27_layer_call_and_return_conditional_losses_28113522
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*/
_input_shapes
:€€€€€€€€€А::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
с
ё
E__inference_dense_26_layer_call_and_return_conditional_losses_2811330

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐMatMul/ReadVariableOpП
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
∞А*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2
MatMulН
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
BiasAdd/ReadVariableOpВ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2	
BiasAddЦ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:€€€€€€€€€А2

Identity"
identityIdentity:output:0*/
_input_shapes
:€€€€€€€€€∞::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
ч
Ђ
*__inference_dense_26_layer_call_fn_2811871

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityИҐStatefulPartitionedCallИ
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€А**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_26_layer_call_and_return_conditional_losses_28113302
StatefulPartitionedCallП
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:€€€€€€€€€А2

Identity"
identityIdentity:output:0*/
_input_shapes
:€€€€€€€€€∞::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
Д
©
,__inference_top_pool_3_layer_call_fn_2811843
in"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityИҐStatefulPartitionedCallН
StatefulPartitionedCallStatefulPartitionedCallinstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:€€€€€€€€€**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_top_pool_3_layer_call_and_return_conditional_losses_28112942
StatefulPartitionedCallЦ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:€€€€€€€€€  ::22
StatefulPartitionedCallStatefulPartitionedCall:" 

_user_specified_nameIn
єІ
Ћ
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811702

inputs1
-top_pool_3_expanddims_readvariableop_resource*
&top_pool_3_add_readvariableop_resource+
'dense_26_matmul_readvariableop_resource,
(dense_26_biasadd_readvariableop_resource+
'dense_27_matmul_readvariableop_resource,
(dense_27_biasadd_readvariableop_resource
identityИҐdense_26/BiasAdd/ReadVariableOpҐdense_26/MatMul/ReadVariableOpҐdense_27/BiasAdd/ReadVariableOpҐdense_27/MatMul/ReadVariableOpҐ$top_pool_3/ExpandDims/ReadVariableOpҐtop_pool_3/add/ReadVariableOpй
top_pool_3/ExtractImagePatchesExtractImagePatchesinputs*
T0*/
_output_shapes
:€€€€€€€€€K*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool_3/ExtractImagePatchesZ
top_pool_3/ShapeShapeinputs*
T0*
_output_shapes
:2
top_pool_3/ShapeА
top_pool_3/Shape_1Shape(top_pool_3/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_3/Shape_1К
top_pool_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool_3/strided_slice/stackО
 top_pool_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice/stack_1О
 top_pool_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice/stack_2§
top_pool_3/strided_sliceStridedSlicetop_pool_3/Shape:output:0'top_pool_3/strided_slice/stack:output:0)top_pool_3/strided_slice/stack_1:output:0)top_pool_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slicef
top_pool_3/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/mul/yЖ
top_pool_3/mulMul!top_pool_3/strided_slice:output:0top_pool_3/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_3/mulj
top_pool_3/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/mul_1/y}
top_pool_3/mul_1Multop_pool_3/mul:z:0top_pool_3/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_3/mul_1z
top_pool_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape/shape/1z
top_pool_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape/shape/2z
top_pool_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape/shape/3п
top_pool_3/Reshape/shapePacktop_pool_3/mul_1:z:0#top_pool_3/Reshape/shape/1:output:0#top_pool_3/Reshape/shape/2:output:0#top_pool_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape/shapeЇ
top_pool_3/ReshapeReshape(top_pool_3/ExtractImagePatches:patches:0!top_pool_3/Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
top_pool_3/Reshapeѓ
top_pool_3/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool_3/MirrorPad/paddingsЌ
top_pool_3/MirrorPad	MirrorPadtop_pool_3/Reshape:output:0&top_pool_3/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
mode	SYMMETRIC2
top_pool_3/MirrorPadД
 top_pool_3/ExtractImagePatches_1ExtractImagePatchestop_pool_3/MirrorPad:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
ksizes
*
paddingVALID*
rates
*
strides
2"
 top_pool_3/ExtractImagePatches_1В
top_pool_3/Shape_2Shape*top_pool_3/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_3/Shape_2u
top_pool_3/Shape_3Shapetop_pool_3/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool_3/Shape_3О
 top_pool_3/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_3/strided_slice_1/stackТ
"top_pool_3/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_1/stack_1Т
"top_pool_3/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_1/stack_2∞
top_pool_3/strided_slice_1StridedSlicetop_pool_3/Shape_3:output:0)top_pool_3/strided_slice_1/stack:output:0+top_pool_3/strided_slice_1/stack_1:output:0+top_pool_3/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_1О
 top_pool_3/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_2/stackТ
"top_pool_3/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_2/stack_1Т
"top_pool_3/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_2/stack_2∞
top_pool_3/strided_slice_2StridedSlicetop_pool_3/Shape_2:output:0)top_pool_3/strided_slice_2/stack:output:0+top_pool_3/strided_slice_2/stack_1:output:0+top_pool_3/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_2О
 top_pool_3/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_3/stackТ
"top_pool_3/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_3/stack_1Т
"top_pool_3/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_3/stack_2∞
top_pool_3/strided_slice_3StridedSlicetop_pool_3/Shape_2:output:0)top_pool_3/strided_slice_3/stack:output:0+top_pool_3/strided_slice_3/stack_1:output:0+top_pool_3/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_3~
top_pool_3/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_1/shape/3~
top_pool_3/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_1/shape/4~
top_pool_3/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_1/shape/5“
top_pool_3/Reshape_1/shapePack#top_pool_3/strided_slice_1:output:0#top_pool_3/strided_slice_2:output:0#top_pool_3/strided_slice_3:output:0%top_pool_3/Reshape_1/shape/3:output:0%top_pool_3/Reshape_1/shape/4:output:0%top_pool_3/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape_1/shape№
top_pool_3/Reshape_1Reshape*top_pool_3/ExtractImagePatches_1:patches:0#top_pool_3/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/Reshape_1u
top_pool_3/Shape_4Shapetop_pool_3/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool_3/Shape_4О
 top_pool_3/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_3/strided_slice_4/stackТ
"top_pool_3/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_4/stack_1Т
"top_pool_3/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_4/stack_2∞
top_pool_3/strided_slice_4StridedSlicetop_pool_3/Shape_4:output:0)top_pool_3/strided_slice_4/stack:output:0+top_pool_3/strided_slice_4/stack_1:output:0+top_pool_3/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_4О
 top_pool_3/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_5/stackТ
"top_pool_3/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_5/stack_1Т
"top_pool_3/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_5/stack_2∞
top_pool_3/strided_slice_5StridedSlicetop_pool_3/Shape_4:output:0)top_pool_3/strided_slice_5/stack:output:0+top_pool_3/strided_slice_5/stack_1:output:0+top_pool_3/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_5О
 top_pool_3/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_6/stackТ
"top_pool_3/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_6/stack_1Т
"top_pool_3/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_6/stack_2∞
top_pool_3/strided_slice_6StridedSlicetop_pool_3/Shape_4:output:0)top_pool_3/strided_slice_6/stack:output:0+top_pool_3/strided_slice_6/stack_1:output:0+top_pool_3/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_6~
top_pool_3/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool_3/Reshape_2/shape/3~
top_pool_3/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_2/shape/4Ђ
top_pool_3/Reshape_2/shapePack#top_pool_3/strided_slice_4:output:0#top_pool_3/strided_slice_5:output:0#top_pool_3/strided_slice_6:output:0%top_pool_3/Reshape_2/shape/3:output:0%top_pool_3/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape_2/shapeЋ
top_pool_3/Reshape_2Reshapetop_pool_3/Reshape_1:output:0#top_pool_3/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
top_pool_3/Reshape_2n
top_pool_3/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/sort/axis†
top_pool_3/sort/NegNegtop_pool_3/Reshape_2:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
top_pool_3/sort/Negu
top_pool_3/sort/ShapeShapetop_pool_3/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool_3/sort/ShapeФ
#top_pool_3/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool_3/sort/strided_slice/stackШ
%top_pool_3/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_3/sort/strided_slice/stack_1Ш
%top_pool_3/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_3/sort/strided_slice/stack_2¬
top_pool_3/sort/strided_sliceStridedSlicetop_pool_3/sort/Shape:output:0,top_pool_3/sort/strided_slice/stack:output:0.top_pool_3/sort/strided_slice/stack_1:output:0.top_pool_3/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/sort/strided_slicen
top_pool_3/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/sort/Rankѓ
top_pool_3/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool_3/sort/transpositionб
top_pool_3/sort/transpose	Transposetop_pool_3/sort/Neg:y:0&top_pool_3/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
top_pool_3/sort/transposeХ
top_pool_3/sort/TopKV2TopKV2top_pool_3/sort/transpose:y:0&top_pool_3/sort/strided_slice:output:0*
T0*И
_output_shapesv
t:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/sort/TopKV2ц
top_pool_3/sort/transpose_1	Transposetop_pool_3/sort/TopKV2:values:0&top_pool_3/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/sort/transpose_1ѓ
top_pool_3/sort/Neg_1Negtop_pool_3/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/sort/Neg_1°
 top_pool_3/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_3/strided_slice_7/stack•
"top_pool_3/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_3/strided_slice_7/stack_1•
"top_pool_3/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_3/strided_slice_7/stack_2ы
top_pool_3/strided_slice_7StridedSlicetop_pool_3/sort/Neg_1:y:0)top_pool_3/strided_slice_7/stack:output:0+top_pool_3/strided_slice_7/stack_1:output:0+top_pool_3/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_3/strided_slice_7°
 top_pool_3/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_3/strided_slice_8/stack•
"top_pool_3/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_3/strided_slice_8/stack_1•
"top_pool_3/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_3/strided_slice_8/stack_2ы
top_pool_3/strided_slice_8StridedSlicetop_pool_3/sort/Neg_1:y:0)top_pool_3/strided_slice_8/stack:output:0+top_pool_3/strided_slice_8/stack_1:output:0+top_pool_3/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_3/strided_slice_8љ
top_pool_3/subSub#top_pool_3/strided_slice_8:output:0#top_pool_3/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/subm
top_pool_3/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2
top_pool_3/Floor/xk
top_pool_3/FloorFloortop_pool_3/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_3/Floorp
top_pool_3/CastCasttop_pool_3/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_3/Castq
top_pool_3/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2
top_pool_3/Floor_1/xq
top_pool_3/Floor_1Floortop_pool_3/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_3/Floor_1v
top_pool_3/Cast_1Casttop_pool_3/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_3/Cast_1j
top_pool_3/Shape_5Shapetop_pool_3/sub:z:0*
T0*
_output_shapes
:2
top_pool_3/Shape_5^
top_pool_3/Shape_6Shapeinputs*
T0*
_output_shapes
:2
top_pool_3/Shape_6О
 top_pool_3/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_3/strided_slice_9/stackТ
"top_pool_3/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_9/stack_1Т
"top_pool_3/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_9/stack_2∞
top_pool_3/strided_slice_9StridedSlicetop_pool_3/Shape_6:output:0)top_pool_3/strided_slice_9/stack:output:0+top_pool_3/strided_slice_9/stack_1:output:0+top_pool_3/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_9~
top_pool_3/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_3/shape/3~
top_pool_3/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_3/shape/4Н
top_pool_3/Reshape_3/shapePack#top_pool_3/strided_slice_9:output:0top_pool_3/Cast:y:0top_pool_3/Cast_1:y:0%top_pool_3/Reshape_3/shape/3:output:0%top_pool_3/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape_3/shapeЃ
top_pool_3/Reshape_3Reshapetop_pool_3/sub:z:0#top_pool_3/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
top_pool_3/Reshape_3Ї
$top_pool_3/ExpandDims/ReadVariableOpReadVariableOp-top_pool_3_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02&
$top_pool_3/ExpandDims/ReadVariableOpx
top_pool_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool_3/ExpandDims/dimї
top_pool_3/ExpandDims
ExpandDims,top_pool_3/ExpandDims/ReadVariableOp:value:0"top_pool_3/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool_3/ExpandDims|
top_pool_3/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/ExpandDims_1/dimЈ
top_pool_3/ExpandDims_1
ExpandDimstop_pool_3/ExpandDims:output:0$top_pool_3/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool_3/ExpandDims_1|
top_pool_3/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/ExpandDims_2/dimљ
top_pool_3/ExpandDims_2
ExpandDims top_pool_3/ExpandDims_1:output:0$top_pool_3/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool_3/ExpandDims_2™
top_pool_3/mul_2Multop_pool_3/Reshape_3:output:0 top_pool_3/ExpandDims_2:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
top_pool_3/mul_2Ж
 top_pool_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_3/Sum/reduction_indicesҐ
top_pool_3/SumSumtop_pool_3/mul_2:z:0)top_pool_3/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
top_pool_3/Sum≠
top_pool_3/add/ReadVariableOpReadVariableOp&top_pool_3_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool_3/add/ReadVariableOp£
top_pool_3/addAddV2top_pool_3/Sum:output:0%top_pool_3/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€2
top_pool_3/addu
flatten_13/ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€0	  2
flatten_13/ConstХ
flatten_13/ReshapeReshapetop_pool_3/add:z:0flatten_13/Const:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2
flatten_13/Reshape™
dense_26/MatMul/ReadVariableOpReadVariableOp'dense_26_matmul_readvariableop_resource* 
_output_shapes
:
∞А*
dtype02 
dense_26/MatMul/ReadVariableOp§
dense_26/MatMulMatMulflatten_13/Reshape:output:0&dense_26/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2
dense_26/MatMul®
dense_26/BiasAdd/ReadVariableOpReadVariableOp(dense_26_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02!
dense_26/BiasAdd/ReadVariableOp¶
dense_26/BiasAddBiasAdddense_26/MatMul:product:0'dense_26/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2
dense_26/BiasAdd©
dense_27/MatMul/ReadVariableOpReadVariableOp'dense_27_matmul_readvariableop_resource*
_output_shapes
:	А
*
dtype02 
dense_27/MatMul/ReadVariableOp°
dense_27/MatMulMatMuldense_26/BiasAdd:output:0&dense_27/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2
dense_27/MatMulІ
dense_27/BiasAdd/ReadVariableOpReadVariableOp(dense_27_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_27/BiasAdd/ReadVariableOp•
dense_27/BiasAddBiasAdddense_27/MatMul:product:0'dense_27/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2
dense_27/BiasAddЇ
IdentityIdentitydense_27/BiasAdd:output:0 ^dense_26/BiasAdd/ReadVariableOp^dense_26/MatMul/ReadVariableOp ^dense_27/BiasAdd/ReadVariableOp^dense_27/MatMul/ReadVariableOp%^top_pool_3/ExpandDims/ReadVariableOp^top_pool_3/add/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::2B
dense_26/BiasAdd/ReadVariableOpdense_26/BiasAdd/ReadVariableOp2@
dense_26/MatMul/ReadVariableOpdense_26/MatMul/ReadVariableOp2B
dense_27/BiasAdd/ReadVariableOpdense_27/BiasAdd/ReadVariableOp2@
dense_27/MatMul/ReadVariableOpdense_27/MatMul/ReadVariableOp2L
$top_pool_3/ExpandDims/ReadVariableOp$top_pool_3/ExpandDims/ReadVariableOp2>
top_pool_3/add/ReadVariableOptop_pool_3/add/ReadVariableOp:& "
 
_user_specified_nameinputs
сw
№
G__inference_top_pool_3_layer_call_and_return_conditional_losses_2811294
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identityИҐExpandDims/ReadVariableOpҐadd/ReadVariableOpѕ
ExtractImagePatchesExtractImagePatchesin*
T0*/
_output_shapes
:€€€€€€€€€K*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2в
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3≠
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapeО
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2	
ReshapeЩ
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddings°
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
mode	SYMMETRIC2
	MirrorPadг
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2о
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2о
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2о
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5ъ
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shape∞
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3€€€€€€€€€€€€€€€€€€€€€€€€€€€2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2о
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2о
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2о
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4ё
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shapeЯ
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stackВ
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1В
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2А
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/RankЩ
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transpositionµ
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
sort/transposeй
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*И
_output_shapesv
t:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
sort/TopKV2 
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
sort/transpose_1О

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2

sort/Neg_1Л
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stackП
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1П
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2є
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7Л
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stackП
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1П
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2є
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8С
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2о
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4ј
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shapeВ
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
	Reshape_3Щ
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dimП

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dimЛ
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dimС
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
SumМ
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€2
addФ
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:€€€€€€€€€  ::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
м
ё
E__inference_dense_27_layer_call_and_return_conditional_losses_2811352

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐMatMul/ReadVariableOpО
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	А
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2	
BiasAddХ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*/
_input_shapes
:€€€€€€€€€А::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
О
c
G__inference_flatten_13_layer_call_and_return_conditional_losses_2811849

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€0	  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€:& "
 
_user_specified_nameinputs
р—
Ќ
"__inference__wrapped_model_2811178
input_14?
;sequential_13_top_pool_3_expanddims_readvariableop_resource8
4sequential_13_top_pool_3_add_readvariableop_resource9
5sequential_13_dense_26_matmul_readvariableop_resource:
6sequential_13_dense_26_biasadd_readvariableop_resource9
5sequential_13_dense_27_matmul_readvariableop_resource:
6sequential_13_dense_27_biasadd_readvariableop_resource
identityИҐ-sequential_13/dense_26/BiasAdd/ReadVariableOpҐ,sequential_13/dense_26/MatMul/ReadVariableOpҐ-sequential_13/dense_27/BiasAdd/ReadVariableOpҐ,sequential_13/dense_27/MatMul/ReadVariableOpҐ2sequential_13/top_pool_3/ExpandDims/ReadVariableOpҐ+sequential_13/top_pool_3/add/ReadVariableOpЗ
,sequential_13/top_pool_3/ExtractImagePatchesExtractImagePatchesinput_14*
T0*/
_output_shapes
:€€€€€€€€€K*
ksizes
*
paddingVALID*
rates
*
strides
2.
,sequential_13/top_pool_3/ExtractImagePatchesx
sequential_13/top_pool_3/ShapeShapeinput_14*
T0*
_output_shapes
:2 
sequential_13/top_pool_3/Shape™
 sequential_13/top_pool_3/Shape_1Shape6sequential_13/top_pool_3/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2"
 sequential_13/top_pool_3/Shape_1¶
,sequential_13/top_pool_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2.
,sequential_13/top_pool_3/strided_slice/stack™
.sequential_13/top_pool_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.sequential_13/top_pool_3/strided_slice/stack_1™
.sequential_13/top_pool_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.sequential_13/top_pool_3/strided_slice/stack_2ш
&sequential_13/top_pool_3/strided_sliceStridedSlice'sequential_13/top_pool_3/Shape:output:05sequential_13/top_pool_3/strided_slice/stack:output:07sequential_13/top_pool_3/strided_slice/stack_1:output:07sequential_13/top_pool_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&sequential_13/top_pool_3/strided_sliceВ
sequential_13/top_pool_3/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2 
sequential_13/top_pool_3/mul/yЊ
sequential_13/top_pool_3/mulMul/sequential_13/top_pool_3/strided_slice:output:0'sequential_13/top_pool_3/mul/y:output:0*
T0*
_output_shapes
: 2
sequential_13/top_pool_3/mulЖ
 sequential_13/top_pool_3/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2"
 sequential_13/top_pool_3/mul_1/yµ
sequential_13/top_pool_3/mul_1Mul sequential_13/top_pool_3/mul:z:0)sequential_13/top_pool_3/mul_1/y:output:0*
T0*
_output_shapes
: 2 
sequential_13/top_pool_3/mul_1Ц
(sequential_13/top_pool_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2*
(sequential_13/top_pool_3/Reshape/shape/1Ц
(sequential_13/top_pool_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2*
(sequential_13/top_pool_3/Reshape/shape/2Ц
(sequential_13/top_pool_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2*
(sequential_13/top_pool_3/Reshape/shape/3√
&sequential_13/top_pool_3/Reshape/shapePack"sequential_13/top_pool_3/mul_1:z:01sequential_13/top_pool_3/Reshape/shape/1:output:01sequential_13/top_pool_3/Reshape/shape/2:output:01sequential_13/top_pool_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2(
&sequential_13/top_pool_3/Reshape/shapeт
 sequential_13/top_pool_3/ReshapeReshape6sequential_13/top_pool_3/ExtractImagePatches:patches:0/sequential_13/top_pool_3/Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2"
 sequential_13/top_pool_3/ReshapeЋ
+sequential_13/top_pool_3/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2-
+sequential_13/top_pool_3/MirrorPad/paddingsЕ
"sequential_13/top_pool_3/MirrorPad	MirrorPad)sequential_13/top_pool_3/Reshape:output:04sequential_13/top_pool_3/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
mode	SYMMETRIC2$
"sequential_13/top_pool_3/MirrorPadЃ
.sequential_13/top_pool_3/ExtractImagePatches_1ExtractImagePatches+sequential_13/top_pool_3/MirrorPad:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
ksizes
*
paddingVALID*
rates
*
strides
20
.sequential_13/top_pool_3/ExtractImagePatches_1ђ
 sequential_13/top_pool_3/Shape_2Shape8sequential_13/top_pool_3/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2"
 sequential_13/top_pool_3/Shape_2Я
 sequential_13/top_pool_3/Shape_3Shape+sequential_13/top_pool_3/MirrorPad:output:0*
T0*
_output_shapes
:2"
 sequential_13/top_pool_3/Shape_3™
.sequential_13/top_pool_3/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 20
.sequential_13/top_pool_3/strided_slice_1/stackЃ
0sequential_13/top_pool_3/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_1/stack_1Ѓ
0sequential_13/top_pool_3/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_1/stack_2Д
(sequential_13/top_pool_3/strided_slice_1StridedSlice)sequential_13/top_pool_3/Shape_3:output:07sequential_13/top_pool_3/strided_slice_1/stack:output:09sequential_13/top_pool_3/strided_slice_1/stack_1:output:09sequential_13/top_pool_3/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_1™
.sequential_13/top_pool_3/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.sequential_13/top_pool_3/strided_slice_2/stackЃ
0sequential_13/top_pool_3/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_2/stack_1Ѓ
0sequential_13/top_pool_3/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_2/stack_2Д
(sequential_13/top_pool_3/strided_slice_2StridedSlice)sequential_13/top_pool_3/Shape_2:output:07sequential_13/top_pool_3/strided_slice_2/stack:output:09sequential_13/top_pool_3/strided_slice_2/stack_1:output:09sequential_13/top_pool_3/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_2™
.sequential_13/top_pool_3/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.sequential_13/top_pool_3/strided_slice_3/stackЃ
0sequential_13/top_pool_3/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_3/stack_1Ѓ
0sequential_13/top_pool_3/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_3/stack_2Д
(sequential_13/top_pool_3/strided_slice_3StridedSlice)sequential_13/top_pool_3/Shape_2:output:07sequential_13/top_pool_3/strided_slice_3/stack:output:09sequential_13/top_pool_3/strided_slice_3/stack_1:output:09sequential_13/top_pool_3/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_3Ъ
*sequential_13/top_pool_3/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_13/top_pool_3/Reshape_1/shape/3Ъ
*sequential_13/top_pool_3/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_13/top_pool_3/Reshape_1/shape/4Ъ
*sequential_13/top_pool_3/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_13/top_pool_3/Reshape_1/shape/5¬
(sequential_13/top_pool_3/Reshape_1/shapePack1sequential_13/top_pool_3/strided_slice_1:output:01sequential_13/top_pool_3/strided_slice_2:output:01sequential_13/top_pool_3/strided_slice_3:output:03sequential_13/top_pool_3/Reshape_1/shape/3:output:03sequential_13/top_pool_3/Reshape_1/shape/4:output:03sequential_13/top_pool_3/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2*
(sequential_13/top_pool_3/Reshape_1/shapeФ
"sequential_13/top_pool_3/Reshape_1Reshape8sequential_13/top_pool_3/ExtractImagePatches_1:patches:01sequential_13/top_pool_3/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3€€€€€€€€€€€€€€€€€€€€€€€€€€€2$
"sequential_13/top_pool_3/Reshape_1Я
 sequential_13/top_pool_3/Shape_4Shape+sequential_13/top_pool_3/Reshape_1:output:0*
T0*
_output_shapes
:2"
 sequential_13/top_pool_3/Shape_4™
.sequential_13/top_pool_3/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 20
.sequential_13/top_pool_3/strided_slice_4/stackЃ
0sequential_13/top_pool_3/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_4/stack_1Ѓ
0sequential_13/top_pool_3/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_4/stack_2Д
(sequential_13/top_pool_3/strided_slice_4StridedSlice)sequential_13/top_pool_3/Shape_4:output:07sequential_13/top_pool_3/strided_slice_4/stack:output:09sequential_13/top_pool_3/strided_slice_4/stack_1:output:09sequential_13/top_pool_3/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_4™
.sequential_13/top_pool_3/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.sequential_13/top_pool_3/strided_slice_5/stackЃ
0sequential_13/top_pool_3/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_5/stack_1Ѓ
0sequential_13/top_pool_3/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_5/stack_2Д
(sequential_13/top_pool_3/strided_slice_5StridedSlice)sequential_13/top_pool_3/Shape_4:output:07sequential_13/top_pool_3/strided_slice_5/stack:output:09sequential_13/top_pool_3/strided_slice_5/stack_1:output:09sequential_13/top_pool_3/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_5™
.sequential_13/top_pool_3/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:20
.sequential_13/top_pool_3/strided_slice_6/stackЃ
0sequential_13/top_pool_3/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_6/stack_1Ѓ
0sequential_13/top_pool_3/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_6/stack_2Д
(sequential_13/top_pool_3/strided_slice_6StridedSlice)sequential_13/top_pool_3/Shape_4:output:07sequential_13/top_pool_3/strided_slice_6/stack:output:09sequential_13/top_pool_3/strided_slice_6/stack_1:output:09sequential_13/top_pool_3/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_6Ъ
*sequential_13/top_pool_3/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2,
*sequential_13/top_pool_3/Reshape_2/shape/3Ъ
*sequential_13/top_pool_3/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_13/top_pool_3/Reshape_2/shape/4Н
(sequential_13/top_pool_3/Reshape_2/shapePack1sequential_13/top_pool_3/strided_slice_4:output:01sequential_13/top_pool_3/strided_slice_5:output:01sequential_13/top_pool_3/strided_slice_6:output:03sequential_13/top_pool_3/Reshape_2/shape/3:output:03sequential_13/top_pool_3/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2*
(sequential_13/top_pool_3/Reshape_2/shapeГ
"sequential_13/top_pool_3/Reshape_2Reshape+sequential_13/top_pool_3/Reshape_1:output:01sequential_13/top_pool_3/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2$
"sequential_13/top_pool_3/Reshape_2К
"sequential_13/top_pool_3/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2$
"sequential_13/top_pool_3/sort/axis 
!sequential_13/top_pool_3/sort/NegNeg+sequential_13/top_pool_3/Reshape_2:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2#
!sequential_13/top_pool_3/sort/NegЯ
#sequential_13/top_pool_3/sort/ShapeShape%sequential_13/top_pool_3/sort/Neg:y:0*
T0*
_output_shapes
:2%
#sequential_13/top_pool_3/sort/Shape∞
1sequential_13/top_pool_3/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:23
1sequential_13/top_pool_3/sort/strided_slice/stackі
3sequential_13/top_pool_3/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:25
3sequential_13/top_pool_3/sort/strided_slice/stack_1і
3sequential_13/top_pool_3/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:25
3sequential_13/top_pool_3/sort/strided_slice/stack_2Ц
+sequential_13/top_pool_3/sort/strided_sliceStridedSlice,sequential_13/top_pool_3/sort/Shape:output:0:sequential_13/top_pool_3/sort/strided_slice/stack:output:0<sequential_13/top_pool_3/sort/strided_slice/stack_1:output:0<sequential_13/top_pool_3/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2-
+sequential_13/top_pool_3/sort/strided_sliceК
"sequential_13/top_pool_3/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2$
"sequential_13/top_pool_3/sort/RankЋ
+sequential_13/top_pool_3/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2-
+sequential_13/top_pool_3/sort/transpositionЩ
'sequential_13/top_pool_3/sort/transpose	Transpose%sequential_13/top_pool_3/sort/Neg:y:04sequential_13/top_pool_3/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2)
'sequential_13/top_pool_3/sort/transposeЌ
$sequential_13/top_pool_3/sort/TopKV2TopKV2+sequential_13/top_pool_3/sort/transpose:y:04sequential_13/top_pool_3/sort/strided_slice:output:0*
T0*И
_output_shapesv
t:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2&
$sequential_13/top_pool_3/sort/TopKV2Ѓ
)sequential_13/top_pool_3/sort/transpose_1	Transpose-sequential_13/top_pool_3/sort/TopKV2:values:04sequential_13/top_pool_3/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2+
)sequential_13/top_pool_3/sort/transpose_1ў
#sequential_13/top_pool_3/sort/Neg_1Neg-sequential_13/top_pool_3/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2%
#sequential_13/top_pool_3/sort/Neg_1љ
.sequential_13/top_pool_3/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   20
.sequential_13/top_pool_3/strided_slice_7/stackЅ
0sequential_13/top_pool_3/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   22
0sequential_13/top_pool_3/strided_slice_7/stack_1Ѕ
0sequential_13/top_pool_3/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               22
0sequential_13/top_pool_3/strided_slice_7/stack_2ѕ
(sequential_13/top_pool_3/strided_slice_7StridedSlice'sequential_13/top_pool_3/sort/Neg_1:y:07sequential_13/top_pool_3/strided_slice_7/stack:output:09sequential_13/top_pool_3/strided_slice_7/stack_1:output:09sequential_13/top_pool_3/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_7љ
.sequential_13/top_pool_3/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   20
.sequential_13/top_pool_3/strided_slice_8/stackЅ
0sequential_13/top_pool_3/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   22
0sequential_13/top_pool_3/strided_slice_8/stack_1Ѕ
0sequential_13/top_pool_3/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               22
0sequential_13/top_pool_3/strided_slice_8/stack_2ѕ
(sequential_13/top_pool_3/strided_slice_8StridedSlice'sequential_13/top_pool_3/sort/Neg_1:y:07sequential_13/top_pool_3/strided_slice_8/stack:output:09sequential_13/top_pool_3/strided_slice_8/stack_1:output:09sequential_13/top_pool_3/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_8х
sequential_13/top_pool_3/subSub1sequential_13/top_pool_3/strided_slice_8:output:01sequential_13/top_pool_3/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€2
sequential_13/top_pool_3/subЙ
 sequential_13/top_pool_3/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2"
 sequential_13/top_pool_3/Floor/xХ
sequential_13/top_pool_3/FloorFloor)sequential_13/top_pool_3/Floor/x:output:0*
T0*
_output_shapes
: 2 
sequential_13/top_pool_3/FloorЪ
sequential_13/top_pool_3/CastCast"sequential_13/top_pool_3/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
sequential_13/top_pool_3/CastН
"sequential_13/top_pool_3/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2$
"sequential_13/top_pool_3/Floor_1/xЫ
 sequential_13/top_pool_3/Floor_1Floor+sequential_13/top_pool_3/Floor_1/x:output:0*
T0*
_output_shapes
: 2"
 sequential_13/top_pool_3/Floor_1†
sequential_13/top_pool_3/Cast_1Cast$sequential_13/top_pool_3/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2!
sequential_13/top_pool_3/Cast_1Ф
 sequential_13/top_pool_3/Shape_5Shape sequential_13/top_pool_3/sub:z:0*
T0*
_output_shapes
:2"
 sequential_13/top_pool_3/Shape_5|
 sequential_13/top_pool_3/Shape_6Shapeinput_14*
T0*
_output_shapes
:2"
 sequential_13/top_pool_3/Shape_6™
.sequential_13/top_pool_3/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 20
.sequential_13/top_pool_3/strided_slice_9/stackЃ
0sequential_13/top_pool_3/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_9/stack_1Ѓ
0sequential_13/top_pool_3/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:22
0sequential_13/top_pool_3/strided_slice_9/stack_2Д
(sequential_13/top_pool_3/strided_slice_9StridedSlice)sequential_13/top_pool_3/Shape_6:output:07sequential_13/top_pool_3/strided_slice_9/stack:output:09sequential_13/top_pool_3/strided_slice_9/stack_1:output:09sequential_13/top_pool_3/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2*
(sequential_13/top_pool_3/strided_slice_9Ъ
*sequential_13/top_pool_3/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_13/top_pool_3/Reshape_3/shape/3Ъ
*sequential_13/top_pool_3/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_13/top_pool_3/Reshape_3/shape/4п
(sequential_13/top_pool_3/Reshape_3/shapePack1sequential_13/top_pool_3/strided_slice_9:output:0!sequential_13/top_pool_3/Cast:y:0#sequential_13/top_pool_3/Cast_1:y:03sequential_13/top_pool_3/Reshape_3/shape/3:output:03sequential_13/top_pool_3/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2*
(sequential_13/top_pool_3/Reshape_3/shapeж
"sequential_13/top_pool_3/Reshape_3Reshape sequential_13/top_pool_3/sub:z:01sequential_13/top_pool_3/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2$
"sequential_13/top_pool_3/Reshape_3д
2sequential_13/top_pool_3/ExpandDims/ReadVariableOpReadVariableOp;sequential_13_top_pool_3_expanddims_readvariableop_resource*
_output_shapes

:*
dtype024
2sequential_13/top_pool_3/ExpandDims/ReadVariableOpФ
'sequential_13/top_pool_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2)
'sequential_13/top_pool_3/ExpandDims/dimу
#sequential_13/top_pool_3/ExpandDims
ExpandDims:sequential_13/top_pool_3/ExpandDims/ReadVariableOp:value:00sequential_13/top_pool_3/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2%
#sequential_13/top_pool_3/ExpandDimsШ
)sequential_13/top_pool_3/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_13/top_pool_3/ExpandDims_1/dimп
%sequential_13/top_pool_3/ExpandDims_1
ExpandDims,sequential_13/top_pool_3/ExpandDims:output:02sequential_13/top_pool_3/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2'
%sequential_13/top_pool_3/ExpandDims_1Ш
)sequential_13/top_pool_3/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_13/top_pool_3/ExpandDims_2/dimх
%sequential_13/top_pool_3/ExpandDims_2
ExpandDims.sequential_13/top_pool_3/ExpandDims_1:output:02sequential_13/top_pool_3/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2'
%sequential_13/top_pool_3/ExpandDims_2в
sequential_13/top_pool_3/mul_2Mul+sequential_13/top_pool_3/Reshape_3:output:0.sequential_13/top_pool_3/ExpandDims_2:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2 
sequential_13/top_pool_3/mul_2Ґ
.sequential_13/top_pool_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :20
.sequential_13/top_pool_3/Sum/reduction_indicesЏ
sequential_13/top_pool_3/SumSum"sequential_13/top_pool_3/mul_2:z:07sequential_13/top_pool_3/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
sequential_13/top_pool_3/Sum„
+sequential_13/top_pool_3/add/ReadVariableOpReadVariableOp4sequential_13_top_pool_3_add_readvariableop_resource*&
_output_shapes
:*
dtype02-
+sequential_13/top_pool_3/add/ReadVariableOpџ
sequential_13/top_pool_3/addAddV2%sequential_13/top_pool_3/Sum:output:03sequential_13/top_pool_3/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€2
sequential_13/top_pool_3/addС
sequential_13/flatten_13/ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€0	  2 
sequential_13/flatten_13/ConstЌ
 sequential_13/flatten_13/ReshapeReshape sequential_13/top_pool_3/add:z:0'sequential_13/flatten_13/Const:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2"
 sequential_13/flatten_13/Reshape‘
,sequential_13/dense_26/MatMul/ReadVariableOpReadVariableOp5sequential_13_dense_26_matmul_readvariableop_resource* 
_output_shapes
:
∞А*
dtype02.
,sequential_13/dense_26/MatMul/ReadVariableOp№
sequential_13/dense_26/MatMulMatMul)sequential_13/flatten_13/Reshape:output:04sequential_13/dense_26/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2
sequential_13/dense_26/MatMul“
-sequential_13/dense_26/BiasAdd/ReadVariableOpReadVariableOp6sequential_13_dense_26_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02/
-sequential_13/dense_26/BiasAdd/ReadVariableOpё
sequential_13/dense_26/BiasAddBiasAdd'sequential_13/dense_26/MatMul:product:05sequential_13/dense_26/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2 
sequential_13/dense_26/BiasAdd”
,sequential_13/dense_27/MatMul/ReadVariableOpReadVariableOp5sequential_13_dense_27_matmul_readvariableop_resource*
_output_shapes
:	А
*
dtype02.
,sequential_13/dense_27/MatMul/ReadVariableOpў
sequential_13/dense_27/MatMulMatMul'sequential_13/dense_26/BiasAdd:output:04sequential_13/dense_27/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2
sequential_13/dense_27/MatMul—
-sequential_13/dense_27/BiasAdd/ReadVariableOpReadVariableOp6sequential_13_dense_27_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02/
-sequential_13/dense_27/BiasAdd/ReadVariableOpЁ
sequential_13/dense_27/BiasAddBiasAdd'sequential_13/dense_27/MatMul:product:05sequential_13/dense_27/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2 
sequential_13/dense_27/BiasAddЬ
IdentityIdentity'sequential_13/dense_27/BiasAdd:output:0.^sequential_13/dense_26/BiasAdd/ReadVariableOp-^sequential_13/dense_26/MatMul/ReadVariableOp.^sequential_13/dense_27/BiasAdd/ReadVariableOp-^sequential_13/dense_27/MatMul/ReadVariableOp3^sequential_13/top_pool_3/ExpandDims/ReadVariableOp,^sequential_13/top_pool_3/add/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::2^
-sequential_13/dense_26/BiasAdd/ReadVariableOp-sequential_13/dense_26/BiasAdd/ReadVariableOp2\
,sequential_13/dense_26/MatMul/ReadVariableOp,sequential_13/dense_26/MatMul/ReadVariableOp2^
-sequential_13/dense_27/BiasAdd/ReadVariableOp-sequential_13/dense_27/BiasAdd/ReadVariableOp2\
,sequential_13/dense_27/MatMul/ReadVariableOp,sequential_13/dense_27/MatMul/ReadVariableOp2h
2sequential_13/top_pool_3/ExpandDims/ReadVariableOp2sequential_13/top_pool_3/ExpandDims/ReadVariableOp2Z
+sequential_13/top_pool_3/add/ReadVariableOp+sequential_13/top_pool_3/add/ReadVariableOp:( $
"
_user_specified_name
input_14
∞	
¬
/__inference_sequential_13_layer_call_fn_2811430
input_14"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identityИҐStatefulPartitionedCallТ
StatefulPartitionedCallStatefulPartitionedCallinput_14statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*S
fNRL
J__inference_sequential_13_layer_call_and_return_conditional_losses_28114212
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::22
StatefulPartitionedCallStatefulPartitionedCall:( $
"
_user_specified_name
input_14
™	
ј
/__inference_sequential_13_layer_call_fn_2811724

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identityИҐStatefulPartitionedCallР
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*S
fNRL
J__inference_sequential_13_layer_call_and_return_conditional_losses_28114212
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
К
и
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811365
input_14-
)top_pool_3_statefulpartitionedcall_args_1-
)top_pool_3_statefulpartitionedcall_args_2+
'dense_26_statefulpartitionedcall_args_1+
'dense_26_statefulpartitionedcall_args_2+
'dense_27_statefulpartitionedcall_args_1+
'dense_27_statefulpartitionedcall_args_2
identityИҐ dense_26/StatefulPartitionedCallҐ dense_27/StatefulPartitionedCallҐ"top_pool_3/StatefulPartitionedCallњ
"top_pool_3/StatefulPartitionedCallStatefulPartitionedCallinput_14)top_pool_3_statefulpartitionedcall_args_1)top_pool_3_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:€€€€€€€€€**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_top_pool_3_layer_call_and_return_conditional_losses_28112942$
"top_pool_3/StatefulPartitionedCallл
flatten_13/PartitionedCallPartitionedCall+top_pool_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€∞**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_flatten_13_layer_call_and_return_conditional_losses_28113122
flatten_13/PartitionedCall…
 dense_26/StatefulPartitionedCallStatefulPartitionedCall#flatten_13/PartitionedCall:output:0'dense_26_statefulpartitionedcall_args_1'dense_26_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€А**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_26_layer_call_and_return_conditional_losses_28113302"
 dense_26/StatefulPartitionedCallќ
 dense_27/StatefulPartitionedCallStatefulPartitionedCall)dense_26/StatefulPartitionedCall:output:0'dense_27_statefulpartitionedcall_args_1'dense_27_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_27_layer_call_and_return_conditional_losses_28113522"
 dense_27/StatefulPartitionedCallи
IdentityIdentity)dense_27/StatefulPartitionedCall:output:0!^dense_26/StatefulPartitionedCall!^dense_27/StatefulPartitionedCall#^top_pool_3/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::2D
 dense_26/StatefulPartitionedCall dense_26/StatefulPartitionedCall2D
 dense_27/StatefulPartitionedCall dense_27/StatefulPartitionedCall2H
"top_pool_3/StatefulPartitionedCall"top_pool_3/StatefulPartitionedCall:( $
"
_user_specified_name
input_14
Д
ж
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811396

inputs-
)top_pool_3_statefulpartitionedcall_args_1-
)top_pool_3_statefulpartitionedcall_args_2+
'dense_26_statefulpartitionedcall_args_1+
'dense_26_statefulpartitionedcall_args_2+
'dense_27_statefulpartitionedcall_args_1+
'dense_27_statefulpartitionedcall_args_2
identityИҐ dense_26/StatefulPartitionedCallҐ dense_27/StatefulPartitionedCallҐ"top_pool_3/StatefulPartitionedCallљ
"top_pool_3/StatefulPartitionedCallStatefulPartitionedCallinputs)top_pool_3_statefulpartitionedcall_args_1)top_pool_3_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:€€€€€€€€€**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_top_pool_3_layer_call_and_return_conditional_losses_28112942$
"top_pool_3/StatefulPartitionedCallл
flatten_13/PartitionedCallPartitionedCall+top_pool_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€∞**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_flatten_13_layer_call_and_return_conditional_losses_28113122
flatten_13/PartitionedCall…
 dense_26/StatefulPartitionedCallStatefulPartitionedCall#flatten_13/PartitionedCall:output:0'dense_26_statefulpartitionedcall_args_1'dense_26_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€А**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_26_layer_call_and_return_conditional_losses_28113302"
 dense_26/StatefulPartitionedCallќ
 dense_27/StatefulPartitionedCallStatefulPartitionedCall)dense_26/StatefulPartitionedCall:output:0'dense_27_statefulpartitionedcall_args_1'dense_27_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_27_layer_call_and_return_conditional_losses_28113522"
 dense_27/StatefulPartitionedCallи
IdentityIdentity)dense_27/StatefulPartitionedCall:output:0!^dense_26/StatefulPartitionedCall!^dense_27/StatefulPartitionedCall#^top_pool_3/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::2D
 dense_26/StatefulPartitionedCall dense_26/StatefulPartitionedCall2D
 dense_27/StatefulPartitionedCall dense_27/StatefulPartitionedCall2H
"top_pool_3/StatefulPartitionedCall"top_pool_3/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
∞	
¬
/__inference_sequential_13_layer_call_fn_2811405
input_14"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identityИҐStatefulPartitionedCallТ
StatefulPartitionedCallStatefulPartitionedCallinput_14statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*S
fNRL
J__inference_sequential_13_layer_call_and_return_conditional_losses_28113962
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::22
StatefulPartitionedCallStatefulPartitionedCall:( $
"
_user_specified_name
input_14
О
c
G__inference_flatten_13_layer_call_and_return_conditional_losses_2811312

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€0	  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€:& "
 
_user_specified_nameinputs
г
H
,__inference_flatten_13_layer_call_fn_2811854

inputs
identity∞
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€∞**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_flatten_13_layer_call_and_return_conditional_losses_28113122
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€:& "
 
_user_specified_nameinputs
сw
№
G__inference_top_pool_3_layer_call_and_return_conditional_losses_2811836
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identityИҐExpandDims/ReadVariableOpҐadd/ReadVariableOpѕ
ExtractImagePatchesExtractImagePatchesin*
T0*/
_output_shapes
:€€€€€€€€€K*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2в
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3≠
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapeО
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2	
ReshapeЩ
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddings°
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
mode	SYMMETRIC2
	MirrorPadг
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2о
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2о
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2о
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5ъ
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shape∞
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3€€€€€€€€€€€€€€€€€€€€€€€€€€€2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2о
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2о
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2о
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4ё
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shapeЯ
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stackВ
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1В
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2А
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/RankЩ
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transpositionµ
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
sort/transposeй
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*И
_output_shapesv
t:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
sort/TopKV2 
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
sort/transpose_1О

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2

sort/Neg_1Л
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stackП
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1П
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2є
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7Л
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stackП
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1П
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2є
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8С
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2о
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4ј
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shapeВ
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
	Reshape_3Щ
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dimП

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dimЛ
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dimС
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
SumМ
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€2
addФ
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:€€€€€€€€€  ::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
љi
Е
#__inference__traced_restore_2812074
file_prefix!
assignvariableop_top_pool_3_w#
assignvariableop_1_top_pool_3_b&
"assignvariableop_2_dense_26_kernel$
 assignvariableop_3_dense_26_bias&
"assignvariableop_4_dense_27_kernel$
 assignvariableop_5_dense_27_bias 
assignvariableop_6_adam_iter"
assignvariableop_7_adam_beta_1"
assignvariableop_8_adam_beta_2!
assignvariableop_9_adam_decay*
&assignvariableop_10_adam_learning_rate
assignvariableop_11_total
assignvariableop_12_count+
'assignvariableop_13_adam_top_pool_3_w_m+
'assignvariableop_14_adam_top_pool_3_b_m.
*assignvariableop_15_adam_dense_26_kernel_m,
(assignvariableop_16_adam_dense_26_bias_m.
*assignvariableop_17_adam_dense_27_kernel_m,
(assignvariableop_18_adam_dense_27_bias_m+
'assignvariableop_19_adam_top_pool_3_w_v+
'assignvariableop_20_adam_top_pool_3_b_v.
*assignvariableop_21_adam_dense_26_kernel_v,
(assignvariableop_22_adam_dense_26_bias_v.
*assignvariableop_23_adam_dense_27_kernel_v,
(assignvariableop_24_adam_dense_27_bias_v
identity_26ИҐAssignVariableOpҐAssignVariableOp_1ҐAssignVariableOp_10ҐAssignVariableOp_11ҐAssignVariableOp_12ҐAssignVariableOp_13ҐAssignVariableOp_14ҐAssignVariableOp_15ҐAssignVariableOp_16ҐAssignVariableOp_17ҐAssignVariableOp_18ҐAssignVariableOp_19ҐAssignVariableOp_2ҐAssignVariableOp_20ҐAssignVariableOp_21ҐAssignVariableOp_22ҐAssignVariableOp_23ҐAssignVariableOp_24ҐAssignVariableOp_3ҐAssignVariableOp_4ҐAssignVariableOp_5ҐAssignVariableOp_6ҐAssignVariableOp_7ҐAssignVariableOp_8ҐAssignVariableOp_9Ґ	RestoreV2ҐRestoreV2_1р
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*ь
valueтBпB1layer_with_weights-0/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-0/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_namesј
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices®
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*x
_output_shapesf
d:::::::::::::::::::::::::*'
dtypes
2	2
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

IdentityН
AssignVariableOpAssignVariableOpassignvariableop_top_pool_3_wIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1Х
AssignVariableOp_1AssignVariableOpassignvariableop_1_top_pool_3_bIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2Ш
AssignVariableOp_2AssignVariableOp"assignvariableop_2_dense_26_kernelIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3Ц
AssignVariableOp_3AssignVariableOp assignvariableop_3_dense_26_biasIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4Ш
AssignVariableOp_4AssignVariableOp"assignvariableop_4_dense_27_kernelIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5Ц
AssignVariableOp_5AssignVariableOp assignvariableop_5_dense_27_biasIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0	*
_output_shapes
:2

Identity_6Т
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_iterIdentity_6:output:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7Ф
AssignVariableOp_7AssignVariableOpassignvariableop_7_adam_beta_1Identity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8Ф
AssignVariableOp_8AssignVariableOpassignvariableop_8_adam_beta_2Identity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9У
AssignVariableOp_9AssignVariableOpassignvariableop_9_adam_decayIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0*
_output_shapes
:2
Identity_10Я
AssignVariableOp_10AssignVariableOp&assignvariableop_10_adam_learning_rateIdentity_10:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11Т
AssignVariableOp_11AssignVariableOpassignvariableop_11_totalIdentity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12Т
AssignVariableOp_12AssignVariableOpassignvariableop_12_countIdentity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13†
AssignVariableOp_13AssignVariableOp'assignvariableop_13_adam_top_pool_3_w_mIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14†
AssignVariableOp_14AssignVariableOp'assignvariableop_14_adam_top_pool_3_b_mIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15£
AssignVariableOp_15AssignVariableOp*assignvariableop_15_adam_dense_26_kernel_mIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16°
AssignVariableOp_16AssignVariableOp(assignvariableop_16_adam_dense_26_bias_mIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17£
AssignVariableOp_17AssignVariableOp*assignvariableop_17_adam_dense_27_kernel_mIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0*
_output_shapes
:2
Identity_18°
AssignVariableOp_18AssignVariableOp(assignvariableop_18_adam_dense_27_bias_mIdentity_18:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19†
AssignVariableOp_19AssignVariableOp'assignvariableop_19_adam_top_pool_3_w_vIdentity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20†
AssignVariableOp_20AssignVariableOp'assignvariableop_20_adam_top_pool_3_b_vIdentity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21£
AssignVariableOp_21AssignVariableOp*assignvariableop_21_adam_dense_26_kernel_vIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21_
Identity_22IdentityRestoreV2:tensors:22*
T0*
_output_shapes
:2
Identity_22°
AssignVariableOp_22AssignVariableOp(assignvariableop_22_adam_dense_26_bias_vIdentity_22:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_22_
Identity_23IdentityRestoreV2:tensors:23*
T0*
_output_shapes
:2
Identity_23£
AssignVariableOp_23AssignVariableOp*assignvariableop_23_adam_dense_27_kernel_vIdentity_23:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_23_
Identity_24IdentityRestoreV2:tensors:24*
T0*
_output_shapes
:2
Identity_24°
AssignVariableOp_24AssignVariableOp(assignvariableop_24_adam_dense_27_bias_vIdentity_24:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_24®
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_namesФ
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slicesƒ
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpД
Identity_25Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_25С
Identity_26IdentityIdentity_25:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_26"#
identity_26Identity_26:output:0*y
_input_shapesh
f: :::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:+ '
%
_user_specified_namefile_prefix
Д
ж
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811421

inputs-
)top_pool_3_statefulpartitionedcall_args_1-
)top_pool_3_statefulpartitionedcall_args_2+
'dense_26_statefulpartitionedcall_args_1+
'dense_26_statefulpartitionedcall_args_2+
'dense_27_statefulpartitionedcall_args_1+
'dense_27_statefulpartitionedcall_args_2
identityИҐ dense_26/StatefulPartitionedCallҐ dense_27/StatefulPartitionedCallҐ"top_pool_3/StatefulPartitionedCallљ
"top_pool_3/StatefulPartitionedCallStatefulPartitionedCallinputs)top_pool_3_statefulpartitionedcall_args_1)top_pool_3_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:€€€€€€€€€**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_top_pool_3_layer_call_and_return_conditional_losses_28112942$
"top_pool_3/StatefulPartitionedCallл
flatten_13/PartitionedCallPartitionedCall+top_pool_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€∞**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_flatten_13_layer_call_and_return_conditional_losses_28113122
flatten_13/PartitionedCall…
 dense_26/StatefulPartitionedCallStatefulPartitionedCall#flatten_13/PartitionedCall:output:0'dense_26_statefulpartitionedcall_args_1'dense_26_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€А**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_26_layer_call_and_return_conditional_losses_28113302"
 dense_26/StatefulPartitionedCallќ
 dense_27/StatefulPartitionedCallStatefulPartitionedCall)dense_26/StatefulPartitionedCall:output:0'dense_27_statefulpartitionedcall_args_1'dense_27_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_27_layer_call_and_return_conditional_losses_28113522"
 dense_27/StatefulPartitionedCallи
IdentityIdentity)dense_27/StatefulPartitionedCall:output:0!^dense_26/StatefulPartitionedCall!^dense_27/StatefulPartitionedCall#^top_pool_3/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::2D
 dense_26/StatefulPartitionedCall dense_26/StatefulPartitionedCall2D
 dense_27/StatefulPartitionedCall dense_27/StatefulPartitionedCall2H
"top_pool_3/StatefulPartitionedCall"top_pool_3/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
с
ё
E__inference_dense_26_layer_call_and_return_conditional_losses_2811864

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐMatMul/ReadVariableOpП
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
∞А*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2
MatMulН
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02
BiasAdd/ReadVariableOpВ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2	
BiasAddЦ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:€€€€€€€€€А2

Identity"
identityIdentity:output:0*/
_input_shapes
:€€€€€€€€€∞::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
К
и
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811379
input_14-
)top_pool_3_statefulpartitionedcall_args_1-
)top_pool_3_statefulpartitionedcall_args_2+
'dense_26_statefulpartitionedcall_args_1+
'dense_26_statefulpartitionedcall_args_2+
'dense_27_statefulpartitionedcall_args_1+
'dense_27_statefulpartitionedcall_args_2
identityИҐ dense_26/StatefulPartitionedCallҐ dense_27/StatefulPartitionedCallҐ"top_pool_3/StatefulPartitionedCallњ
"top_pool_3/StatefulPartitionedCallStatefulPartitionedCallinput_14)top_pool_3_statefulpartitionedcall_args_1)top_pool_3_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:€€€€€€€€€**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_top_pool_3_layer_call_and_return_conditional_losses_28112942$
"top_pool_3/StatefulPartitionedCallл
flatten_13/PartitionedCallPartitionedCall+top_pool_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€∞**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_flatten_13_layer_call_and_return_conditional_losses_28113122
flatten_13/PartitionedCall…
 dense_26/StatefulPartitionedCallStatefulPartitionedCall#flatten_13/PartitionedCall:output:0'dense_26_statefulpartitionedcall_args_1'dense_26_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:€€€€€€€€€А**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_26_layer_call_and_return_conditional_losses_28113302"
 dense_26/StatefulPartitionedCallќ
 dense_27/StatefulPartitionedCallStatefulPartitionedCall)dense_26/StatefulPartitionedCall:output:0'dense_27_statefulpartitionedcall_args_1'dense_27_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_dense_27_layer_call_and_return_conditional_losses_28113522"
 dense_27/StatefulPartitionedCallи
IdentityIdentity)dense_27/StatefulPartitionedCall:output:0!^dense_26/StatefulPartitionedCall!^dense_27/StatefulPartitionedCall#^top_pool_3/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::2D
 dense_26/StatefulPartitionedCall dense_26/StatefulPartitionedCall2D
 dense_27/StatefulPartitionedCall dense_27/StatefulPartitionedCall2H
"top_pool_3/StatefulPartitionedCall"top_pool_3/StatefulPartitionedCall:( $
"
_user_specified_name
input_14
м
ё
E__inference_dense_27_layer_call_and_return_conditional_losses_2811881

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityИҐBiasAdd/ReadVariableOpҐMatMul/ReadVariableOpО
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	А
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2
MatMulМ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOpБ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2	
BiasAddХ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*/
_input_shapes
:€€€€€€€€€А::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
™	
ј
/__inference_sequential_13_layer_call_fn_2811713

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identityИҐStatefulPartitionedCallР
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:€€€€€€€€€
**
config_proto

GPU 

CPU2J 8*S
fNRL
J__inference_sequential_13_layer_call_and_return_conditional_losses_28113962
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
єІ
Ћ
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811576

inputs1
-top_pool_3_expanddims_readvariableop_resource*
&top_pool_3_add_readvariableop_resource+
'dense_26_matmul_readvariableop_resource,
(dense_26_biasadd_readvariableop_resource+
'dense_27_matmul_readvariableop_resource,
(dense_27_biasadd_readvariableop_resource
identityИҐdense_26/BiasAdd/ReadVariableOpҐdense_26/MatMul/ReadVariableOpҐdense_27/BiasAdd/ReadVariableOpҐdense_27/MatMul/ReadVariableOpҐ$top_pool_3/ExpandDims/ReadVariableOpҐtop_pool_3/add/ReadVariableOpй
top_pool_3/ExtractImagePatchesExtractImagePatchesinputs*
T0*/
_output_shapes
:€€€€€€€€€K*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool_3/ExtractImagePatchesZ
top_pool_3/ShapeShapeinputs*
T0*
_output_shapes
:2
top_pool_3/ShapeА
top_pool_3/Shape_1Shape(top_pool_3/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_3/Shape_1К
top_pool_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool_3/strided_slice/stackО
 top_pool_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice/stack_1О
 top_pool_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice/stack_2§
top_pool_3/strided_sliceStridedSlicetop_pool_3/Shape:output:0'top_pool_3/strided_slice/stack:output:0)top_pool_3/strided_slice/stack_1:output:0)top_pool_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slicef
top_pool_3/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/mul/yЖ
top_pool_3/mulMul!top_pool_3/strided_slice:output:0top_pool_3/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_3/mulj
top_pool_3/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/mul_1/y}
top_pool_3/mul_1Multop_pool_3/mul:z:0top_pool_3/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_3/mul_1z
top_pool_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape/shape/1z
top_pool_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape/shape/2z
top_pool_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape/shape/3п
top_pool_3/Reshape/shapePacktop_pool_3/mul_1:z:0#top_pool_3/Reshape/shape/1:output:0#top_pool_3/Reshape/shape/2:output:0#top_pool_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape/shapeЇ
top_pool_3/ReshapeReshape(top_pool_3/ExtractImagePatches:patches:0!top_pool_3/Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
top_pool_3/Reshapeѓ
top_pool_3/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool_3/MirrorPad/paddingsЌ
top_pool_3/MirrorPad	MirrorPadtop_pool_3/Reshape:output:0&top_pool_3/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
mode	SYMMETRIC2
top_pool_3/MirrorPadД
 top_pool_3/ExtractImagePatches_1ExtractImagePatchestop_pool_3/MirrorPad:output:0*
T0*/
_output_shapes
:€€€€€€€€€*
ksizes
*
paddingVALID*
rates
*
strides
2"
 top_pool_3/ExtractImagePatches_1В
top_pool_3/Shape_2Shape*top_pool_3/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_3/Shape_2u
top_pool_3/Shape_3Shapetop_pool_3/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool_3/Shape_3О
 top_pool_3/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_3/strided_slice_1/stackТ
"top_pool_3/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_1/stack_1Т
"top_pool_3/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_1/stack_2∞
top_pool_3/strided_slice_1StridedSlicetop_pool_3/Shape_3:output:0)top_pool_3/strided_slice_1/stack:output:0+top_pool_3/strided_slice_1/stack_1:output:0+top_pool_3/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_1О
 top_pool_3/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_2/stackТ
"top_pool_3/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_2/stack_1Т
"top_pool_3/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_2/stack_2∞
top_pool_3/strided_slice_2StridedSlicetop_pool_3/Shape_2:output:0)top_pool_3/strided_slice_2/stack:output:0+top_pool_3/strided_slice_2/stack_1:output:0+top_pool_3/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_2О
 top_pool_3/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_3/stackТ
"top_pool_3/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_3/stack_1Т
"top_pool_3/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_3/stack_2∞
top_pool_3/strided_slice_3StridedSlicetop_pool_3/Shape_2:output:0)top_pool_3/strided_slice_3/stack:output:0+top_pool_3/strided_slice_3/stack_1:output:0+top_pool_3/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_3~
top_pool_3/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_1/shape/3~
top_pool_3/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_1/shape/4~
top_pool_3/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_1/shape/5“
top_pool_3/Reshape_1/shapePack#top_pool_3/strided_slice_1:output:0#top_pool_3/strided_slice_2:output:0#top_pool_3/strided_slice_3:output:0%top_pool_3/Reshape_1/shape/3:output:0%top_pool_3/Reshape_1/shape/4:output:0%top_pool_3/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape_1/shape№
top_pool_3/Reshape_1Reshape*top_pool_3/ExtractImagePatches_1:patches:0#top_pool_3/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/Reshape_1u
top_pool_3/Shape_4Shapetop_pool_3/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool_3/Shape_4О
 top_pool_3/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_3/strided_slice_4/stackТ
"top_pool_3/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_4/stack_1Т
"top_pool_3/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_4/stack_2∞
top_pool_3/strided_slice_4StridedSlicetop_pool_3/Shape_4:output:0)top_pool_3/strided_slice_4/stack:output:0+top_pool_3/strided_slice_4/stack_1:output:0+top_pool_3/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_4О
 top_pool_3/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_5/stackТ
"top_pool_3/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_5/stack_1Т
"top_pool_3/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_5/stack_2∞
top_pool_3/strided_slice_5StridedSlicetop_pool_3/Shape_4:output:0)top_pool_3/strided_slice_5/stack:output:0+top_pool_3/strided_slice_5/stack_1:output:0+top_pool_3/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_5О
 top_pool_3/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_3/strided_slice_6/stackТ
"top_pool_3/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_6/stack_1Т
"top_pool_3/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_6/stack_2∞
top_pool_3/strided_slice_6StridedSlicetop_pool_3/Shape_4:output:0)top_pool_3/strided_slice_6/stack:output:0+top_pool_3/strided_slice_6/stack_1:output:0+top_pool_3/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_6~
top_pool_3/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool_3/Reshape_2/shape/3~
top_pool_3/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_2/shape/4Ђ
top_pool_3/Reshape_2/shapePack#top_pool_3/strided_slice_4:output:0#top_pool_3/strided_slice_5:output:0#top_pool_3/strided_slice_6:output:0%top_pool_3/Reshape_2/shape/3:output:0%top_pool_3/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape_2/shapeЋ
top_pool_3/Reshape_2Reshapetop_pool_3/Reshape_1:output:0#top_pool_3/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
top_pool_3/Reshape_2n
top_pool_3/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/sort/axis†
top_pool_3/sort/NegNegtop_pool_3/Reshape_2:output:0*
T0*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
top_pool_3/sort/Negu
top_pool_3/sort/ShapeShapetop_pool_3/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool_3/sort/ShapeФ
#top_pool_3/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool_3/sort/strided_slice/stackШ
%top_pool_3/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_3/sort/strided_slice/stack_1Ш
%top_pool_3/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_3/sort/strided_slice/stack_2¬
top_pool_3/sort/strided_sliceStridedSlicetop_pool_3/sort/Shape:output:0,top_pool_3/sort/strided_slice/stack:output:0.top_pool_3/sort/strided_slice/stack_1:output:0.top_pool_3/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/sort/strided_slicen
top_pool_3/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/sort/Rankѓ
top_pool_3/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool_3/sort/transpositionб
top_pool_3/sort/transpose	Transposetop_pool_3/sort/Neg:y:0&top_pool_3/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/€€€€€€€€€€€€€€€€€€€€€€€€€€€	2
top_pool_3/sort/transposeХ
top_pool_3/sort/TopKV2TopKV2top_pool_3/sort/transpose:y:0&top_pool_3/sort/strided_slice:output:0*
T0*И
_output_shapesv
t:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€:8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/sort/TopKV2ц
top_pool_3/sort/transpose_1	Transposetop_pool_3/sort/TopKV2:values:0&top_pool_3/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/sort/transpose_1ѓ
top_pool_3/sort/Neg_1Negtop_pool_3/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/sort/Neg_1°
 top_pool_3/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_3/strided_slice_7/stack•
"top_pool_3/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_3/strided_slice_7/stack_1•
"top_pool_3/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_3/strided_slice_7/stack_2ы
top_pool_3/strided_slice_7StridedSlicetop_pool_3/sort/Neg_1:y:0)top_pool_3/strided_slice_7/stack:output:0+top_pool_3/strided_slice_7/stack_1:output:0+top_pool_3/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_3/strided_slice_7°
 top_pool_3/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_3/strided_slice_8/stack•
"top_pool_3/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_3/strided_slice_8/stack_1•
"top_pool_3/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_3/strided_slice_8/stack_2ы
top_pool_3/strided_slice_8StridedSlicetop_pool_3/sort/Neg_1:y:0)top_pool_3/strided_slice_8/stack:output:0+top_pool_3/strided_slice_8/stack_1:output:0+top_pool_3/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_3/strided_slice_8љ
top_pool_3/subSub#top_pool_3/strided_slice_8:output:0#top_pool_3/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+€€€€€€€€€€€€€€€€€€€€€€€€€€€2
top_pool_3/subm
top_pool_3/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2
top_pool_3/Floor/xk
top_pool_3/FloorFloortop_pool_3/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_3/Floorp
top_pool_3/CastCasttop_pool_3/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_3/Castq
top_pool_3/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  аA2
top_pool_3/Floor_1/xq
top_pool_3/Floor_1Floortop_pool_3/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_3/Floor_1v
top_pool_3/Cast_1Casttop_pool_3/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_3/Cast_1j
top_pool_3/Shape_5Shapetop_pool_3/sub:z:0*
T0*
_output_shapes
:2
top_pool_3/Shape_5^
top_pool_3/Shape_6Shapeinputs*
T0*
_output_shapes
:2
top_pool_3/Shape_6О
 top_pool_3/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_3/strided_slice_9/stackТ
"top_pool_3/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_9/stack_1Т
"top_pool_3/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_3/strided_slice_9/stack_2∞
top_pool_3/strided_slice_9StridedSlicetop_pool_3/Shape_6:output:0)top_pool_3/strided_slice_9/stack:output:0+top_pool_3/strided_slice_9/stack_1:output:0+top_pool_3/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_3/strided_slice_9~
top_pool_3/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_3/shape/3~
top_pool_3/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/Reshape_3/shape/4Н
top_pool_3/Reshape_3/shapePack#top_pool_3/strided_slice_9:output:0top_pool_3/Cast:y:0top_pool_3/Cast_1:y:0%top_pool_3/Reshape_3/shape/3:output:0%top_pool_3/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_3/Reshape_3/shapeЃ
top_pool_3/Reshape_3Reshapetop_pool_3/sub:z:0#top_pool_3/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
top_pool_3/Reshape_3Ї
$top_pool_3/ExpandDims/ReadVariableOpReadVariableOp-top_pool_3_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02&
$top_pool_3/ExpandDims/ReadVariableOpx
top_pool_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool_3/ExpandDims/dimї
top_pool_3/ExpandDims
ExpandDims,top_pool_3/ExpandDims/ReadVariableOp:value:0"top_pool_3/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool_3/ExpandDims|
top_pool_3/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/ExpandDims_1/dimЈ
top_pool_3/ExpandDims_1
ExpandDimstop_pool_3/ExpandDims:output:0$top_pool_3/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool_3/ExpandDims_1|
top_pool_3/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_3/ExpandDims_2/dimљ
top_pool_3/ExpandDims_2
ExpandDims top_pool_3/ExpandDims_1:output:0$top_pool_3/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool_3/ExpandDims_2™
top_pool_3/mul_2Multop_pool_3/Reshape_3:output:0 top_pool_3/ExpandDims_2:output:0*
T0*3
_output_shapes!
:€€€€€€€€€2
top_pool_3/mul_2Ж
 top_pool_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_3/Sum/reduction_indicesҐ
top_pool_3/SumSumtop_pool_3/mul_2:z:0)top_pool_3/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
top_pool_3/Sum≠
top_pool_3/add/ReadVariableOpReadVariableOp&top_pool_3_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool_3/add/ReadVariableOp£
top_pool_3/addAddV2top_pool_3/Sum:output:0%top_pool_3/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€2
top_pool_3/addu
flatten_13/ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€0	  2
flatten_13/ConstХ
flatten_13/ReshapeReshapetop_pool_3/add:z:0flatten_13/Const:output:0*
T0*(
_output_shapes
:€€€€€€€€€∞2
flatten_13/Reshape™
dense_26/MatMul/ReadVariableOpReadVariableOp'dense_26_matmul_readvariableop_resource* 
_output_shapes
:
∞А*
dtype02 
dense_26/MatMul/ReadVariableOp§
dense_26/MatMulMatMulflatten_13/Reshape:output:0&dense_26/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2
dense_26/MatMul®
dense_26/BiasAdd/ReadVariableOpReadVariableOp(dense_26_biasadd_readvariableop_resource*
_output_shapes	
:А*
dtype02!
dense_26/BiasAdd/ReadVariableOp¶
dense_26/BiasAddBiasAdddense_26/MatMul:product:0'dense_26/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:€€€€€€€€€А2
dense_26/BiasAdd©
dense_27/MatMul/ReadVariableOpReadVariableOp'dense_27_matmul_readvariableop_resource*
_output_shapes
:	А
*
dtype02 
dense_27/MatMul/ReadVariableOp°
dense_27/MatMulMatMuldense_26/BiasAdd:output:0&dense_27/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2
dense_27/MatMulІ
dense_27/BiasAdd/ReadVariableOpReadVariableOp(dense_27_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_27/BiasAdd/ReadVariableOp•
dense_27/BiasAddBiasAdddense_27/MatMul:product:0'dense_27/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€
2
dense_27/BiasAddЇ
IdentityIdentitydense_27/BiasAdd:output:0 ^dense_26/BiasAdd/ReadVariableOp^dense_26/MatMul/ReadVariableOp ^dense_27/BiasAdd/ReadVariableOp^dense_27/MatMul/ReadVariableOp%^top_pool_3/ExpandDims/ReadVariableOp^top_pool_3/add/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:€€€€€€€€€  ::::::2B
dense_26/BiasAdd/ReadVariableOpdense_26/BiasAdd/ReadVariableOp2@
dense_26/MatMul/ReadVariableOpdense_26/MatMul/ReadVariableOp2B
dense_27/BiasAdd/ReadVariableOpdense_27/BiasAdd/ReadVariableOp2@
dense_27/MatMul/ReadVariableOpdense_27/MatMul/ReadVariableOp2L
$top_pool_3/ExpandDims/ReadVariableOp$top_pool_3/ExpandDims/ReadVariableOp2>
top_pool_3/add/ReadVariableOptop_pool_3/add/ReadVariableOp:& "
 
_user_specified_nameinputs"ѓL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*µ
serving_default°
E
input_149
serving_default_input_14:0€€€€€€€€€  <
dense_270
StatefulPartitionedCall:0€€€€€€€€€
tensorflow/serving/predict:ЫН
Є
layer-0
layer_with_weights-0
layer-1
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
	optimizer
	variables
regularization_losses
	trainable_variables

	keras_api

signatures
S_default_save_signature
T__call__
*U&call_and_return_all_conditional_losses"ё
_tf_keras_sequentialњ{"class_name": "Sequential", "name": "sequential_13", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "sequential_13", "layers": [{"class_name": "TopPool", "config": {"name": "top_pool_3", "trainable": true, "dtype": "float32", "num_channels": 3, "ksize": 5, "stride": 1, "ks": 1, "m": 2, "n": 7, "batch_input_shape": [null, 32, 32, 3]}}, {"class_name": "Flatten", "config": {"name": "flatten_13", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_26", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_27", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}, "is_graph_network": true, "keras_version": "2.2.4-tf", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential_13", "layers": [{"class_name": "TopPool", "config": {"name": "top_pool_3", "trainable": true, "dtype": "float32", "num_channels": 3, "ksize": 5, "stride": 1, "ks": 1, "m": 2, "n": 7, "batch_input_shape": [null, 32, 32, 3]}}, {"class_name": "Flatten", "config": {"name": "flatten_13", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_26", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_27", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}}, "training_config": {"loss": {"class_name": "SparseCategoricalCrossentropy", "config": {"reduction": "auto", "name": "sparse_categorical_crossentropy", "from_logits": true}}, "metrics": ["accuracy"], "weighted_metrics": null, "sample_weight_mode": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0003000000142492354, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
ѓ"ђ
_tf_keras_input_layerМ{"class_name": "InputLayer", "name": "input_14", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": [null, 32, 32, 3], "config": {"batch_input_shape": [null, 32, 32, 3], "dtype": "float32", "sparse": false, "ragged": false, "name": "input_14"}}
’
w
b
	variables
regularization_losses
trainable_variables
	keras_api
V__call__
*W&call_and_return_all_conditional_losses"Є
_tf_keras_layerЮ{"class_name": "TopPool", "name": "top_pool_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "top_pool_3", "trainable": true, "dtype": "float32", "num_channels": 3, "ksize": 5, "stride": 1, "ks": 1, "m": 2, "n": 7}}
≤
	variables
regularization_losses
trainable_variables
	keras_api
X__call__
*Y&call_and_return_all_conditional_losses"£
_tf_keras_layerЙ{"class_name": "Flatten", "name": "flatten_13", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "flatten_13", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 1, "axes": {}}}}
ш

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
Z__call__
*[&call_and_return_all_conditional_losses"”
_tf_keras_layerє{"class_name": "Dense", "name": "dense_26", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_26", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 2352}}}}
ц

kernel
bias
	variables
regularization_losses
 trainable_variables
!	keras_api
\__call__
*]&call_and_return_all_conditional_losses"—
_tf_keras_layerЈ{"class_name": "Dense", "name": "dense_27", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_27", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}}
њ
"iter

#beta_1

$beta_2
	%decay
&learning_ratemGmHmImJmKmLvMvNvOvPvQvR"
	optimizer
J
0
1
2
3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
Ј
'non_trainable_variables
	variables
(metrics

)layers
regularization_losses
	trainable_variables
*layer_regularization_losses
T__call__
S_default_save_signature
*U&call_and_return_all_conditional_losses
&U"call_and_return_conditional_losses"
_generic_user_object
,
^serving_default"
signature_map
:2top_pool_3/w
&:$2top_pool_3/b
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
Ъ
+non_trainable_variables
	variables
,metrics

-layers
regularization_losses
trainable_variables
.layer_regularization_losses
V__call__
*W&call_and_return_all_conditional_losses
&W"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Ъ
/non_trainable_variables
	variables
0metrics

1layers
regularization_losses
trainable_variables
2layer_regularization_losses
X__call__
*Y&call_and_return_all_conditional_losses
&Y"call_and_return_conditional_losses"
_generic_user_object
#:!
∞А2dense_26/kernel
:А2dense_26/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
Ъ
3non_trainable_variables
	variables
4metrics

5layers
regularization_losses
trainable_variables
6layer_regularization_losses
Z__call__
*[&call_and_return_all_conditional_losses
&["call_and_return_conditional_losses"
_generic_user_object
": 	А
2dense_27/kernel
:
2dense_27/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
Ъ
7non_trainable_variables
	variables
8metrics

9layers
regularization_losses
 trainable_variables
:layer_regularization_losses
\__call__
*]&call_and_return_all_conditional_losses
&]"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
'
;0"
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Ъ
	<total
	=count
>
_fn_kwargs
?	variables
@regularization_losses
Atrainable_variables
B	keras_api
___call__
*`&call_and_return_all_conditional_losses"е
_tf_keras_layerЋ{"class_name": "MeanMetricWrapper", "name": "accuracy", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "accuracy", "dtype": "float32"}}
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
<0
=1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Ъ
Cnon_trainable_variables
?	variables
Dmetrics

Elayers
@regularization_losses
Atrainable_variables
Flayer_regularization_losses
___call__
*`&call_and_return_all_conditional_losses
&`"call_and_return_conditional_losses"
_generic_user_object
.
<0
=1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
#:!2Adam/top_pool_3/w/m
+:)2Adam/top_pool_3/b/m
(:&
∞А2Adam/dense_26/kernel/m
!:А2Adam/dense_26/bias/m
':%	А
2Adam/dense_27/kernel/m
 :
2Adam/dense_27/bias/m
#:!2Adam/top_pool_3/w/v
+:)2Adam/top_pool_3/b/v
(:&
∞А2Adam/dense_26/kernel/v
!:А2Adam/dense_26/bias/v
':%	А
2Adam/dense_27/kernel/v
 :
2Adam/dense_27/bias/v
й2ж
"__inference__wrapped_model_2811178њ
Л≤З
FullArgSpec
argsЪ 
varargsjargs
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ */Ґ,
*К'
input_14€€€€€€€€€  
К2З
/__inference_sequential_13_layer_call_fn_2811430
/__inference_sequential_13_layer_call_fn_2811405
/__inference_sequential_13_layer_call_fn_2811724
/__inference_sequential_13_layer_call_fn_2811713ј
Ј≤≥
FullArgSpec1
args)Ъ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsЪ
p 

 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 
ц2у
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811702
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811379
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811576
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811365ј
Ј≤≥
FullArgSpec1
args)Ъ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsЪ
p 

 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 
“2ѕ
,__inference_top_pool_3_layer_call_fn_2811843Ю
Х≤С
FullArgSpec
argsЪ
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
н2к
G__inference_top_pool_3_layer_call_and_return_conditional_losses_2811836Ю
Х≤С
FullArgSpec
argsЪ
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
÷2”
,__inference_flatten_13_layer_call_fn_2811854Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
с2о
G__inference_flatten_13_layer_call_and_return_conditional_losses_2811849Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
‘2—
*__inference_dense_26_layer_call_fn_2811871Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
п2м
E__inference_dense_26_layer_call_and_return_conditional_losses_2811864Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
‘2—
*__inference_dense_27_layer_call_fn_2811888Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
п2м
E__inference_dense_27_layer_call_and_return_conditional_losses_2811881Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
5B3
%__inference_signature_wrapper_2811450input_14
ћ2…∆
љ≤є
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkwjkwargs
defaultsЪ 

kwonlyargsЪ

jtraining%
kwonlydefaults™

trainingp 
annotations™ *
 
ћ2…∆
љ≤є
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkwjkwargs
defaultsЪ 

kwonlyargsЪ

jtraining%
kwonlydefaults™

trainingp 
annotations™ *
 Ю
"__inference__wrapped_model_2811178x9Ґ6
/Ґ,
*К'
input_14€€€€€€€€€  
™ "3™0
.
dense_27"К
dense_27€€€€€€€€€
І
E__inference_dense_26_layer_call_and_return_conditional_losses_2811864^0Ґ-
&Ґ#
!К
inputs€€€€€€€€€∞
™ "&Ґ#
К
0€€€€€€€€€А
Ъ 
*__inference_dense_26_layer_call_fn_2811871Q0Ґ-
&Ґ#
!К
inputs€€€€€€€€€∞
™ "К€€€€€€€€€А¶
E__inference_dense_27_layer_call_and_return_conditional_losses_2811881]0Ґ-
&Ґ#
!К
inputs€€€€€€€€€А
™ "%Ґ"
К
0€€€€€€€€€

Ъ ~
*__inference_dense_27_layer_call_fn_2811888P0Ґ-
&Ґ#
!К
inputs€€€€€€€€€А
™ "К€€€€€€€€€
ђ
G__inference_flatten_13_layer_call_and_return_conditional_losses_2811849a7Ґ4
-Ґ*
(К%
inputs€€€€€€€€€
™ "&Ґ#
К
0€€€€€€€€€∞
Ъ Д
,__inference_flatten_13_layer_call_fn_2811854T7Ґ4
-Ґ*
(К%
inputs€€€€€€€€€
™ "К€€€€€€€€€∞ј
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811365rAҐ>
7Ґ4
*К'
input_14€€€€€€€€€  
p

 
™ "%Ґ"
К
0€€€€€€€€€

Ъ ј
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811379rAҐ>
7Ґ4
*К'
input_14€€€€€€€€€  
p 

 
™ "%Ґ"
К
0€€€€€€€€€

Ъ Њ
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811576p?Ґ<
5Ґ2
(К%
inputs€€€€€€€€€  
p

 
™ "%Ґ"
К
0€€€€€€€€€

Ъ Њ
J__inference_sequential_13_layer_call_and_return_conditional_losses_2811702p?Ґ<
5Ґ2
(К%
inputs€€€€€€€€€  
p 

 
™ "%Ґ"
К
0€€€€€€€€€

Ъ Ш
/__inference_sequential_13_layer_call_fn_2811405eAҐ>
7Ґ4
*К'
input_14€€€€€€€€€  
p

 
™ "К€€€€€€€€€
Ш
/__inference_sequential_13_layer_call_fn_2811430eAҐ>
7Ґ4
*К'
input_14€€€€€€€€€  
p 

 
™ "К€€€€€€€€€
Ц
/__inference_sequential_13_layer_call_fn_2811713c?Ґ<
5Ґ2
(К%
inputs€€€€€€€€€  
p

 
™ "К€€€€€€€€€
Ц
/__inference_sequential_13_layer_call_fn_2811724c?Ґ<
5Ґ2
(К%
inputs€€€€€€€€€  
p 

 
™ "К€€€€€€€€€
Ѓ
%__inference_signature_wrapper_2811450ДEҐB
Ґ 
;™8
6
input_14*К'
input_14€€€€€€€€€  "3™0
.
dense_27"К
dense_27€€€€€€€€€
≥
G__inference_top_pool_3_layer_call_and_return_conditional_losses_2811836h3Ґ0
)Ґ&
$К!
In€€€€€€€€€  
™ "-Ґ*
#К 
0€€€€€€€€€
Ъ Л
,__inference_top_pool_3_layer_call_fn_2811843[3Ґ0
)Ґ&
$К!
In€€€€€€€€€  
™ " К€€€€€€€€€