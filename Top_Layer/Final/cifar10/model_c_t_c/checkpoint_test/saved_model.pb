��
��
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape�"serve*2.1.02v2.1.0-rc2-17-ge5bf8de8��
~
conv2d/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d/kernel
w
!conv2d/kernel/Read/ReadVariableOpReadVariableOpconv2d/kernel*&
_output_shapes
:*
dtype0
n
conv2d/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d/bias
g
conv2d/bias/Read/ReadVariableOpReadVariableOpconv2d/bias*
_output_shapes
:*
dtype0
p

top_pool/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_name
top_pool/w
i
top_pool/w/Read/ReadVariableOpReadVariableOp
top_pool/w*
_output_shapes

:*
dtype0
x

top_pool/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_name
top_pool/b
q
top_pool/b/Read/ReadVariableOpReadVariableOp
top_pool/b*&
_output_shapes
:*
dtype0
�
conv2d_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_nameconv2d_1/kernel
{
#conv2d_1/kernel/Read/ReadVariableOpReadVariableOpconv2d_1/kernel*&
_output_shapes
:*
dtype0
r
conv2d_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d_1/bias
k
!conv2d_1/bias/Read/ReadVariableOpReadVariableOpconv2d_1/bias*
_output_shapes
:*
dtype0
v
dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*
shared_namedense/kernel
o
 dense/kernel/Read/ReadVariableOpReadVariableOpdense/kernel* 
_output_shapes
:
��*
dtype0
m

dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_name
dense/bias
f
dense/bias/Read/ReadVariableOpReadVariableOp
dense/bias*
_output_shapes	
:�*
dtype0
y
dense_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*
shared_namedense_1/kernel
r
"dense_1/kernel/Read/ReadVariableOpReadVariableOpdense_1/kernel*
_output_shapes
:	�
*
dtype0
p
dense_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namedense_1/bias
i
 dense_1/bias/Read/ReadVariableOpReadVariableOpdense_1/bias*
_output_shapes
:
*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
�
Adam/conv2d/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d/kernel/m
�
(Adam/conv2d/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d/kernel/m*&
_output_shapes
:*
dtype0
|
Adam/conv2d/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/conv2d/bias/m
u
&Adam/conv2d/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d/bias/m*
_output_shapes
:*
dtype0
~
Adam/top_pool/w/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*"
shared_nameAdam/top_pool/w/m
w
%Adam/top_pool/w/m/Read/ReadVariableOpReadVariableOpAdam/top_pool/w/m*
_output_shapes

:*
dtype0
�
Adam/top_pool/b/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameAdam/top_pool/b/m

%Adam/top_pool/b/m/Read/ReadVariableOpReadVariableOpAdam/top_pool/b/m*&
_output_shapes
:*
dtype0
�
Adam/conv2d_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*'
shared_nameAdam/conv2d_1/kernel/m
�
*Adam/conv2d_1/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_1/kernel/m*&
_output_shapes
:*
dtype0
�
Adam/conv2d_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d_1/bias/m
y
(Adam/conv2d_1/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_1/bias/m*
_output_shapes
:*
dtype0
�
Adam/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*$
shared_nameAdam/dense/kernel/m
}
'Adam/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/m* 
_output_shapes
:
��*
dtype0
{
Adam/dense/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*"
shared_nameAdam/dense/bias/m
t
%Adam/dense/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/dense_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*&
shared_nameAdam/dense_1/kernel/m
�
)Adam/dense_1/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/m*
_output_shapes
:	�
*
dtype0
~
Adam/dense_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*$
shared_nameAdam/dense_1/bias/m
w
'Adam/dense_1/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/m*
_output_shapes
:
*
dtype0
�
Adam/conv2d/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d/kernel/v
�
(Adam/conv2d/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d/kernel/v*&
_output_shapes
:*
dtype0
|
Adam/conv2d/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*#
shared_nameAdam/conv2d/bias/v
u
&Adam/conv2d/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d/bias/v*
_output_shapes
:*
dtype0
~
Adam/top_pool/w/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*"
shared_nameAdam/top_pool/w/v
w
%Adam/top_pool/w/v/Read/ReadVariableOpReadVariableOpAdam/top_pool/w/v*
_output_shapes

:*
dtype0
�
Adam/top_pool/b/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameAdam/top_pool/b/v

%Adam/top_pool/b/v/Read/ReadVariableOpReadVariableOpAdam/top_pool/b/v*&
_output_shapes
:*
dtype0
�
Adam/conv2d_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*'
shared_nameAdam/conv2d_1/kernel/v
�
*Adam/conv2d_1/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_1/kernel/v*&
_output_shapes
:*
dtype0
�
Adam/conv2d_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d_1/bias/v
y
(Adam/conv2d_1/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_1/bias/v*
_output_shapes
:*
dtype0
�
Adam/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*$
shared_nameAdam/dense/kernel/v
}
'Adam/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/v* 
_output_shapes
:
��*
dtype0
{
Adam/dense/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*"
shared_nameAdam/dense/bias/v
t
%Adam/dense/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense/bias/v*
_output_shapes	
:�*
dtype0
�
Adam/dense_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*&
shared_nameAdam/dense_1/kernel/v
�
)Adam/dense_1/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/v*
_output_shapes
:	�
*
dtype0
~
Adam/dense_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*$
shared_nameAdam/dense_1/bias/v
w
'Adam/dense_1/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/v*
_output_shapes
:
*
dtype0

NoOpNoOp
�6
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�6
value�6B�6 B�6
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
	optimizer
		variables

regularization_losses
trainable_variables
	keras_api

signatures
 
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
`
w
b
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
R
 	variables
!regularization_losses
"trainable_variables
#	keras_api
h

$kernel
%bias
&	variables
'regularization_losses
(trainable_variables
)	keras_api
h

*kernel
+bias
,	variables
-regularization_losses
.trainable_variables
/	keras_api
�
0iter

1beta_1

2beta_2
	3decay
4learning_ratem]m^m_m`mamb$mc%md*me+mfvgvhvivjvkvl$vm%vn*vo+vp
F
0
1
2
3
4
5
$6
%7
*8
+9
 
F
0
1
2
3
4
5
$6
%7
*8
+9
�
5non_trainable_variables
		variables
6metrics

7layers

regularization_losses
trainable_variables
8layer_regularization_losses
 
YW
VARIABLE_VALUEconv2d/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEconv2d/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
9non_trainable_variables
	variables
:metrics

;layers
regularization_losses
trainable_variables
<layer_regularization_losses
QO
VARIABLE_VALUE
top_pool/w1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUE
top_pool/b1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
=non_trainable_variables
	variables
>metrics

?layers
regularization_losses
trainable_variables
@layer_regularization_losses
[Y
VARIABLE_VALUEconv2d_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEconv2d_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
Anon_trainable_variables
	variables
Bmetrics

Clayers
regularization_losses
trainable_variables
Dlayer_regularization_losses
 
 
 
�
Enon_trainable_variables
 	variables
Fmetrics

Glayers
!regularization_losses
"trainable_variables
Hlayer_regularization_losses
XV
VARIABLE_VALUEdense/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
TR
VARIABLE_VALUE
dense/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

$0
%1
 

$0
%1
�
Inon_trainable_variables
&	variables
Jmetrics

Klayers
'regularization_losses
(trainable_variables
Llayer_regularization_losses
ZX
VARIABLE_VALUEdense_1/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_1/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE

*0
+1
 

*0
+1
�
Mnon_trainable_variables
,	variables
Nmetrics

Olayers
-regularization_losses
.trainable_variables
Player_regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
 

Q0
*
0
1
2
3
4
5
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
x
	Rtotal
	Scount
T
_fn_kwargs
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE
 

R0
S1
 
 
�
Ynon_trainable_variables
U	variables
Zmetrics

[layers
Vregularization_losses
Wtrainable_variables
\layer_regularization_losses

R0
S1
 
 
 
|z
VARIABLE_VALUEAdam/conv2d/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/conv2d/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
tr
VARIABLE_VALUEAdam/top_pool/w/mMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
tr
VARIABLE_VALUEAdam/top_pool/b/mMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/conv2d_1/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/conv2d_1/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/mRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/mPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/conv2d/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/conv2d/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
tr
VARIABLE_VALUEAdam/top_pool/w/vMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
tr
VARIABLE_VALUEAdam/top_pool/b/vMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/conv2d_1/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/conv2d_1/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/vRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/vPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
serving_default_input_1Placeholder*/
_output_shapes
:���������  *
dtype0*$
shape:���������  
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1conv2d/kernelconv2d/bias
top_pool/w
top_pool/bconv2d_1/kernelconv2d_1/biasdense/kernel
dense/biasdense_1/kerneldense_1/bias*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*.
f)R'
%__inference_signature_wrapper_1734617
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename!conv2d/kernel/Read/ReadVariableOpconv2d/bias/Read/ReadVariableOptop_pool/w/Read/ReadVariableOptop_pool/b/Read/ReadVariableOp#conv2d_1/kernel/Read/ReadVariableOp!conv2d_1/bias/Read/ReadVariableOp dense/kernel/Read/ReadVariableOpdense/bias/Read/ReadVariableOp"dense_1/kernel/Read/ReadVariableOp dense_1/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp(Adam/conv2d/kernel/m/Read/ReadVariableOp&Adam/conv2d/bias/m/Read/ReadVariableOp%Adam/top_pool/w/m/Read/ReadVariableOp%Adam/top_pool/b/m/Read/ReadVariableOp*Adam/conv2d_1/kernel/m/Read/ReadVariableOp(Adam/conv2d_1/bias/m/Read/ReadVariableOp'Adam/dense/kernel/m/Read/ReadVariableOp%Adam/dense/bias/m/Read/ReadVariableOp)Adam/dense_1/kernel/m/Read/ReadVariableOp'Adam/dense_1/bias/m/Read/ReadVariableOp(Adam/conv2d/kernel/v/Read/ReadVariableOp&Adam/conv2d/bias/v/Read/ReadVariableOp%Adam/top_pool/w/v/Read/ReadVariableOp%Adam/top_pool/b/v/Read/ReadVariableOp*Adam/conv2d_1/kernel/v/Read/ReadVariableOp(Adam/conv2d_1/bias/v/Read/ReadVariableOp'Adam/dense/kernel/v/Read/ReadVariableOp%Adam/dense/bias/v/Read/ReadVariableOp)Adam/dense_1/kernel/v/Read/ReadVariableOp'Adam/dense_1/bias/v/Read/ReadVariableOpConst*2
Tin+
)2'	*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

GPU 

CPU2J 8*)
f$R"
 __inference__traced_save_1735224
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameconv2d/kernelconv2d/bias
top_pool/w
top_pool/bconv2d_1/kernelconv2d_1/biasdense/kernel
dense/biasdense_1/kerneldense_1/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcountAdam/conv2d/kernel/mAdam/conv2d/bias/mAdam/top_pool/w/mAdam/top_pool/b/mAdam/conv2d_1/kernel/mAdam/conv2d_1/bias/mAdam/dense/kernel/mAdam/dense/bias/mAdam/dense_1/kernel/mAdam/dense_1/bias/mAdam/conv2d/kernel/vAdam/conv2d/bias/vAdam/top_pool/w/vAdam/top_pool/b/vAdam/conv2d_1/kernel/vAdam/conv2d_1/bias/vAdam/dense/kernel/vAdam/dense/bias/vAdam/dense_1/kernel/vAdam/dense_1/bias/v*1
Tin*
(2&*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

GPU 

CPU2J 8*,
f'R%
#__inference__traced_restore_1735347�

�
�
(__inference_conv2d_layer_call_fn_1734288

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*A
_output_shapes/
-:+���������������������������**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_conv2d_layer_call_and_return_conditional_losses_17342802
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
'__inference_dense_layer_call_fn_1735072

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_17344672
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
,__inference_sequential_layer_call_fn_1734910

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_17345452
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
��
�
G__inference_sequential_layer_call_and_return_conditional_losses_1734895

inputs)
%conv2d_conv2d_readvariableop_resource*
&conv2d_biasadd_readvariableop_resource/
+top_pool_expanddims_readvariableop_resource(
$top_pool_add_readvariableop_resource+
'conv2d_1_conv2d_readvariableop_resource,
(conv2d_1_biasadd_readvariableop_resource(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource
identity��conv2d/BiasAdd/ReadVariableOp�conv2d/Conv2D/ReadVariableOp�conv2d_1/BiasAdd/ReadVariableOp�conv2d_1/Conv2D/ReadVariableOp�dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�dense_1/BiasAdd/ReadVariableOp�dense_1/MatMul/ReadVariableOp�"top_pool/ExpandDims/ReadVariableOp�top_pool/add/ReadVariableOp�
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
conv2d/Conv2D/ReadVariableOp�
conv2d/Conv2DConv2Dinputs$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d/Conv2D�
conv2d/BiasAdd/ReadVariableOpReadVariableOp&conv2d_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
conv2d/BiasAdd/ReadVariableOp�
conv2d/BiasAddBiasAddconv2d/Conv2D:output:0%conv2d/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
conv2d/BiasAdd�
top_pool/ExtractImagePatchesExtractImagePatchesconv2d/BiasAdd:output:0*
T0*0
_output_shapes
:����������*
ksizes
*
paddingVALID*
rates
*
strides
2
top_pool/ExtractImagePatchesg
top_pool/ShapeShapeconv2d/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool/Shapez
top_pool/Shape_1Shape&top_pool/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool/Shape_1�
top_pool/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
top_pool/strided_slice/stack�
top_pool/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice/stack_1�
top_pool/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice/stack_2�
top_pool/strided_sliceStridedSlicetop_pool/Shape:output:0%top_pool/strided_slice/stack:output:0'top_pool/strided_slice/stack_1:output:0'top_pool/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_sliceb
top_pool/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/mul/y~
top_pool/mulMultop_pool/strided_slice:output:0top_pool/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool/mulf
top_pool/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/mul_1/yu
top_pool/mul_1Multop_pool/mul:z:0top_pool/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool/mul_1v
top_pool/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape/shape/1v
top_pool/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape/shape/2v
top_pool/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape/shape/3�
top_pool/Reshape/shapePacktop_pool/mul_1:z:0!top_pool/Reshape/shape/1:output:0!top_pool/Reshape/shape/2:output:0!top_pool/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape/shape�
top_pool/ReshapeReshape&top_pool/ExtractImagePatches:patches:0top_pool/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2
top_pool/Reshape�
top_pool/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool/MirrorPad/paddings�
top_pool/MirrorPad	MirrorPadtop_pool/Reshape:output:0$top_pool/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
top_pool/MirrorPad�
top_pool/ExtractImagePatches_1ExtractImagePatchestop_pool/MirrorPad:output:0*
T0*/
_output_shapes
:���������H*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool/ExtractImagePatches_1|
top_pool/Shape_2Shape(top_pool/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool/Shape_2o
top_pool/Shape_3Shapetop_pool/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool/Shape_3�
top_pool/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool/strided_slice_1/stack�
 top_pool/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_1/stack_1�
 top_pool/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_1/stack_2�
top_pool/strided_slice_1StridedSlicetop_pool/Shape_3:output:0'top_pool/strided_slice_1/stack:output:0)top_pool/strided_slice_1/stack_1:output:0)top_pool/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_1�
top_pool/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_2/stack�
 top_pool/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_2/stack_1�
 top_pool/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_2/stack_2�
top_pool/strided_slice_2StridedSlicetop_pool/Shape_2:output:0'top_pool/strided_slice_2/stack:output:0)top_pool/strided_slice_2/stack_1:output:0)top_pool/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_2�
top_pool/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_3/stack�
 top_pool/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_3/stack_1�
 top_pool/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_3/stack_2�
top_pool/strided_slice_3StridedSlicetop_pool/Shape_2:output:0'top_pool/strided_slice_3/stack:output:0)top_pool/strided_slice_3/stack_1:output:0)top_pool/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_3z
top_pool/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_1/shape/3z
top_pool/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_1/shape/4z
top_pool/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_1/shape/5�
top_pool/Reshape_1/shapePack!top_pool/strided_slice_1:output:0!top_pool/strided_slice_2:output:0!top_pool/strided_slice_3:output:0#top_pool/Reshape_1/shape/3:output:0#top_pool/Reshape_1/shape/4:output:0#top_pool/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape_1/shape�
top_pool/Reshape_1Reshape(top_pool/ExtractImagePatches_1:patches:0!top_pool/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
top_pool/Reshape_1o
top_pool/Shape_4Shapetop_pool/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool/Shape_4�
top_pool/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool/strided_slice_4/stack�
 top_pool/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_4/stack_1�
 top_pool/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_4/stack_2�
top_pool/strided_slice_4StridedSlicetop_pool/Shape_4:output:0'top_pool/strided_slice_4/stack:output:0)top_pool/strided_slice_4/stack_1:output:0)top_pool/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_4�
top_pool/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_5/stack�
 top_pool/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_5/stack_1�
 top_pool/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_5/stack_2�
top_pool/strided_slice_5StridedSlicetop_pool/Shape_4:output:0'top_pool/strided_slice_5/stack:output:0)top_pool/strided_slice_5/stack_1:output:0)top_pool/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_5�
top_pool/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_6/stack�
 top_pool/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_6/stack_1�
 top_pool/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_6/stack_2�
top_pool/strided_slice_6StridedSlicetop_pool/Shape_4:output:0'top_pool/strided_slice_6/stack:output:0)top_pool/strided_slice_6/stack_1:output:0)top_pool/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_6z
top_pool/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool/Reshape_2/shape/3z
top_pool/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_2/shape/4�
top_pool/Reshape_2/shapePack!top_pool/strided_slice_4:output:0!top_pool/strided_slice_5:output:0!top_pool/strided_slice_6:output:0#top_pool/Reshape_2/shape/3:output:0#top_pool/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape_2/shape�
top_pool/Reshape_2Reshapetop_pool/Reshape_1:output:0!top_pool/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool/Reshape_2j
top_pool/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/sort/axis�
top_pool/sort/NegNegtop_pool/Reshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool/sort/Nego
top_pool/sort/ShapeShapetop_pool/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool/sort/Shape�
!top_pool/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2#
!top_pool/sort/strided_slice/stack�
#top_pool/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool/sort/strided_slice/stack_1�
#top_pool/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool/sort/strided_slice/stack_2�
top_pool/sort/strided_sliceStridedSlicetop_pool/sort/Shape:output:0*top_pool/sort/strided_slice/stack:output:0,top_pool/sort/strided_slice/stack_1:output:0,top_pool/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/sort/strided_slicej
top_pool/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/sort/Rank�
top_pool/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool/sort/transposition�
top_pool/sort/transpose	Transposetop_pool/sort/Neg:y:0$top_pool/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
top_pool/sort/transpose�
top_pool/sort/TopKV2TopKV2top_pool/sort/transpose:y:0$top_pool/sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
top_pool/sort/TopKV2�
top_pool/sort/transpose_1	Transposetop_pool/sort/TopKV2:values:0$top_pool/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
top_pool/sort/transpose_1�
top_pool/sort/Neg_1Negtop_pool/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2
top_pool/sort/Neg_1�
top_pool/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2 
top_pool/strided_slice_7/stack�
 top_pool/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool/strided_slice_7/stack_1�
 top_pool/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2"
 top_pool/strided_slice_7/stack_2�
top_pool/strided_slice_7StridedSlicetop_pool/sort/Neg_1:y:0'top_pool/strided_slice_7/stack:output:0)top_pool/strided_slice_7/stack_1:output:0)top_pool/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool/strided_slice_7�
top_pool/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2 
top_pool/strided_slice_8/stack�
 top_pool/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool/strided_slice_8/stack_1�
 top_pool/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2"
 top_pool/strided_slice_8/stack_2�
top_pool/strided_slice_8StridedSlicetop_pool/sort/Neg_1:y:0'top_pool/strided_slice_8/stack:output:0)top_pool/strided_slice_8/stack_1:output:0)top_pool/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool/strided_slice_8�
top_pool/subSub!top_pool/strided_slice_8:output:0!top_pool/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
top_pool/subi
top_pool/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
top_pool/Floor/xe
top_pool/FloorFloortop_pool/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool/Floorj
top_pool/CastCasttop_pool/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool/Castm
top_pool/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
top_pool/Floor_1/xk
top_pool/Floor_1Floortop_pool/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool/Floor_1p
top_pool/Cast_1Casttop_pool/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool/Cast_1d
top_pool/Shape_5Shapetop_pool/sub:z:0*
T0*
_output_shapes
:2
top_pool/Shape_5k
top_pool/Shape_6Shapeconv2d/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool/Shape_6�
top_pool/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool/strided_slice_9/stack�
 top_pool/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_9/stack_1�
 top_pool/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_9/stack_2�
top_pool/strided_slice_9StridedSlicetop_pool/Shape_6:output:0'top_pool/strided_slice_9/stack:output:0)top_pool/strided_slice_9/stack_1:output:0)top_pool/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_9z
top_pool/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_3/shape/3z
top_pool/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_3/shape/4�
top_pool/Reshape_3/shapePack!top_pool/strided_slice_9:output:0top_pool/Cast:y:0top_pool/Cast_1:y:0#top_pool/Reshape_3/shape/3:output:0#top_pool/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape_3/shape�
top_pool/Reshape_3Reshapetop_pool/sub:z:0!top_pool/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
top_pool/Reshape_3�
"top_pool/ExpandDims/ReadVariableOpReadVariableOp+top_pool_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02$
"top_pool/ExpandDims/ReadVariableOpt
top_pool/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool/ExpandDims/dim�
top_pool/ExpandDims
ExpandDims*top_pool/ExpandDims/ReadVariableOp:value:0 top_pool/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool/ExpandDimsx
top_pool/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/ExpandDims_1/dim�
top_pool/ExpandDims_1
ExpandDimstop_pool/ExpandDims:output:0"top_pool/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool/ExpandDims_1x
top_pool/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/ExpandDims_2/dim�
top_pool/ExpandDims_2
ExpandDimstop_pool/ExpandDims_1:output:0"top_pool/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool/ExpandDims_2�
top_pool/mul_2Multop_pool/Reshape_3:output:0top_pool/ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
top_pool/mul_2�
top_pool/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2 
top_pool/Sum/reduction_indices�
top_pool/SumSumtop_pool/mul_2:z:0'top_pool/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
top_pool/Sum�
top_pool/add/ReadVariableOpReadVariableOp$top_pool_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool/add/ReadVariableOp�
top_pool/addAddV2top_pool/Sum:output:0#top_pool/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
top_pool/add�
conv2d_1/Conv2D/ReadVariableOpReadVariableOp'conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02 
conv2d_1/Conv2D/ReadVariableOp�
conv2d_1/Conv2DConv2Dtop_pool/add:z:0&conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d_1/Conv2D�
conv2d_1/BiasAdd/ReadVariableOpReadVariableOp(conv2d_1_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
conv2d_1/BiasAdd/ReadVariableOp�
conv2d_1/BiasAddBiasAddconv2d_1/Conv2D:output:0'conv2d_1/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
conv2d_1/BiasAdd{
conv2d_1/ReluReluconv2d_1/BiasAdd:output:0*
T0*/
_output_shapes
:���������2
conv2d_1/Reluo
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"����   2
flatten/Const�
flatten/ReshapeReshapeconv2d_1/Relu:activations:0flatten/Const:output:0*
T0*(
_output_shapes
:����������2
flatten/Reshape�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulflatten/Reshape:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense/MatMul�
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
dense/BiasAdd/ReadVariableOp�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense/BiasAdd�
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
dense_1/MatMul/ReadVariableOp�
dense_1/MatMulMatMuldense/BiasAdd:output:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_1/MatMul�
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02 
dense_1/BiasAdd/ReadVariableOp�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_1/BiasAdd�
IdentityIdentitydense_1/BiasAdd:output:0^conv2d/BiasAdd/ReadVariableOp^conv2d/Conv2D/ReadVariableOp ^conv2d_1/BiasAdd/ReadVariableOp^conv2d_1/Conv2D/ReadVariableOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^dense_1/BiasAdd/ReadVariableOp^dense_1/MatMul/ReadVariableOp#^top_pool/ExpandDims/ReadVariableOp^top_pool/add/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::2>
conv2d/BiasAdd/ReadVariableOpconv2d/BiasAdd/ReadVariableOp2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp2B
conv2d_1/BiasAdd/ReadVariableOpconv2d_1/BiasAdd/ReadVariableOp2@
conv2d_1/Conv2D/ReadVariableOpconv2d_1/Conv2D/ReadVariableOp2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp2@
dense_1/BiasAdd/ReadVariableOpdense_1/BiasAdd/ReadVariableOp2>
dense_1/MatMul/ReadVariableOpdense_1/MatMul/ReadVariableOp2H
"top_pool/ExpandDims/ReadVariableOp"top_pool/ExpandDims/ReadVariableOp2:
top_pool/add/ReadVariableOptop_pool/add/ReadVariableOp:& "
 
_user_specified_nameinputs
�
�
B__inference_dense_layer_call_and_return_conditional_losses_1735065

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
� 
�
G__inference_sequential_layer_call_and_return_conditional_losses_1734545

inputs)
%conv2d_statefulpartitionedcall_args_1)
%conv2d_statefulpartitionedcall_args_2+
'top_pool_statefulpartitionedcall_args_1+
'top_pool_statefulpartitionedcall_args_2+
'conv2d_1_statefulpartitionedcall_args_1+
'conv2d_1_statefulpartitionedcall_args_2(
$dense_statefulpartitionedcall_args_1(
$dense_statefulpartitionedcall_args_2*
&dense_1_statefulpartitionedcall_args_1*
&dense_1_statefulpartitionedcall_args_2
identity��conv2d/StatefulPartitionedCall� conv2d_1/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall� top_pool/StatefulPartitionedCall�
conv2d/StatefulPartitionedCallStatefulPartitionedCallinputs%conv2d_statefulpartitionedcall_args_1%conv2d_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_conv2d_layer_call_and_return_conditional_losses_17342802 
conv2d/StatefulPartitionedCall�
 top_pool/StatefulPartitionedCallStatefulPartitionedCall'conv2d/StatefulPartitionedCall:output:0'top_pool_statefulpartitionedcall_args_1'top_pool_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_top_pool_layer_call_and_return_conditional_losses_17344282"
 top_pool/StatefulPartitionedCall�
 conv2d_1/StatefulPartitionedCallStatefulPartitionedCall)top_pool/StatefulPartitionedCall:output:0'conv2d_1_statefulpartitionedcall_args_1'conv2d_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_conv2d_1_layer_call_and_return_conditional_losses_17343012"
 conv2d_1/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall)conv2d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_17344492
flatten/PartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0$dense_statefulpartitionedcall_args_1$dense_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_17344672
dense/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0&dense_1_statefulpartitionedcall_args_1&dense_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_17344892!
dense_1/StatefulPartitionedCall�
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^conv2d/StatefulPartitionedCall!^conv2d_1/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^top_pool/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::2@
conv2d/StatefulPartitionedCallconv2d/StatefulPartitionedCall2D
 conv2d_1/StatefulPartitionedCall conv2d_1/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 top_pool/StatefulPartitionedCall top_pool/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
Ǘ
�
#__inference__traced_restore_1735347
file_prefix"
assignvariableop_conv2d_kernel"
assignvariableop_1_conv2d_bias!
assignvariableop_2_top_pool_w!
assignvariableop_3_top_pool_b&
"assignvariableop_4_conv2d_1_kernel$
 assignvariableop_5_conv2d_1_bias#
assignvariableop_6_dense_kernel!
assignvariableop_7_dense_bias%
!assignvariableop_8_dense_1_kernel#
assignvariableop_9_dense_1_bias!
assignvariableop_10_adam_iter#
assignvariableop_11_adam_beta_1#
assignvariableop_12_adam_beta_2"
assignvariableop_13_adam_decay*
&assignvariableop_14_adam_learning_rate
assignvariableop_15_total
assignvariableop_16_count,
(assignvariableop_17_adam_conv2d_kernel_m*
&assignvariableop_18_adam_conv2d_bias_m)
%assignvariableop_19_adam_top_pool_w_m)
%assignvariableop_20_adam_top_pool_b_m.
*assignvariableop_21_adam_conv2d_1_kernel_m,
(assignvariableop_22_adam_conv2d_1_bias_m+
'assignvariableop_23_adam_dense_kernel_m)
%assignvariableop_24_adam_dense_bias_m-
)assignvariableop_25_adam_dense_1_kernel_m+
'assignvariableop_26_adam_dense_1_bias_m,
(assignvariableop_27_adam_conv2d_kernel_v*
&assignvariableop_28_adam_conv2d_bias_v)
%assignvariableop_29_adam_top_pool_w_v)
%assignvariableop_30_adam_top_pool_b_v.
*assignvariableop_31_adam_conv2d_1_kernel_v,
(assignvariableop_32_adam_conv2d_1_bias_v+
'assignvariableop_33_adam_dense_kernel_v)
%assignvariableop_34_adam_dense_bias_v-
)assignvariableop_35_adam_dense_1_kernel_v+
'assignvariableop_36_adam_dense_1_bias_v
identity_38��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�	RestoreV2�RestoreV2_1�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*�
value�B�%B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*]
valueTBR%B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�:::::::::::::::::::::::::::::::::::::*3
dtypes)
'2%	2
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOpassignvariableop_conv2d_kernelIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOpassignvariableop_1_conv2d_biasIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOpassignvariableop_2_top_pool_wIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOpassignvariableop_3_top_pool_bIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp"assignvariableop_4_conv2d_1_kernelIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp assignvariableop_5_conv2d_1_biasIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOpassignvariableop_6_dense_kernelIdentity_6:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOpassignvariableop_7_dense_biasIdentity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp!assignvariableop_8_dense_1_kernelIdentity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOpassignvariableop_9_dense_1_biasIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0	*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOpassignvariableop_10_adam_iterIdentity_10:output:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpassignvariableop_11_adam_beta_1Identity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOpassignvariableop_12_adam_beta_2Identity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOpassignvariableop_13_adam_decayIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp&assignvariableop_14_adam_learning_rateIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOpassignvariableop_15_totalIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOpassignvariableop_16_countIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp(assignvariableop_17_adam_conv2d_kernel_mIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp&assignvariableop_18_adam_conv2d_bias_mIdentity_18:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp%assignvariableop_19_adam_top_pool_w_mIdentity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp%assignvariableop_20_adam_top_pool_b_mIdentity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp*assignvariableop_21_adam_conv2d_1_kernel_mIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21_
Identity_22IdentityRestoreV2:tensors:22*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp(assignvariableop_22_adam_conv2d_1_bias_mIdentity_22:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_22_
Identity_23IdentityRestoreV2:tensors:23*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp'assignvariableop_23_adam_dense_kernel_mIdentity_23:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_23_
Identity_24IdentityRestoreV2:tensors:24*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp%assignvariableop_24_adam_dense_bias_mIdentity_24:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_24_
Identity_25IdentityRestoreV2:tensors:25*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOp)assignvariableop_25_adam_dense_1_kernel_mIdentity_25:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_25_
Identity_26IdentityRestoreV2:tensors:26*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOp'assignvariableop_26_adam_dense_1_bias_mIdentity_26:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_26_
Identity_27IdentityRestoreV2:tensors:27*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp(assignvariableop_27_adam_conv2d_kernel_vIdentity_27:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_27_
Identity_28IdentityRestoreV2:tensors:28*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp&assignvariableop_28_adam_conv2d_bias_vIdentity_28:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_28_
Identity_29IdentityRestoreV2:tensors:29*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp%assignvariableop_29_adam_top_pool_w_vIdentity_29:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_29_
Identity_30IdentityRestoreV2:tensors:30*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp%assignvariableop_30_adam_top_pool_b_vIdentity_30:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_30_
Identity_31IdentityRestoreV2:tensors:31*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp*assignvariableop_31_adam_conv2d_1_kernel_vIdentity_31:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_31_
Identity_32IdentityRestoreV2:tensors:32*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp(assignvariableop_32_adam_conv2d_1_bias_vIdentity_32:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_32_
Identity_33IdentityRestoreV2:tensors:33*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOp'assignvariableop_33_adam_dense_kernel_vIdentity_33:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_33_
Identity_34IdentityRestoreV2:tensors:34*
T0*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOp%assignvariableop_34_adam_dense_bias_vIdentity_34:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_34_
Identity_35IdentityRestoreV2:tensors:35*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOp)assignvariableop_35_adam_dense_1_kernel_vIdentity_35:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_35_
Identity_36IdentityRestoreV2:tensors:36*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOp'assignvariableop_36_adam_dense_1_bias_vIdentity_36:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_36�
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_names�
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slices�
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_37Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_37�
Identity_38IdentityIdentity_37:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_38"#
identity_38Identity_38:output:0*�
_input_shapes�
�: :::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:+ '
%
_user_specified_namefile_prefix
�
�
%__inference_signature_wrapper_1734617
input_1"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*+
f&R$
"__inference__wrapped_model_17342682
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_1
�

�
C__inference_conv2d_layer_call_and_return_conditional_losses_1734280

inputs"
conv2d_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOpo
dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      2
dilation_rate�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������*
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:& "
 
_user_specified_nameinputs
� 
�
G__inference_sequential_layer_call_and_return_conditional_losses_1734580

inputs)
%conv2d_statefulpartitionedcall_args_1)
%conv2d_statefulpartitionedcall_args_2+
'top_pool_statefulpartitionedcall_args_1+
'top_pool_statefulpartitionedcall_args_2+
'conv2d_1_statefulpartitionedcall_args_1+
'conv2d_1_statefulpartitionedcall_args_2(
$dense_statefulpartitionedcall_args_1(
$dense_statefulpartitionedcall_args_2*
&dense_1_statefulpartitionedcall_args_1*
&dense_1_statefulpartitionedcall_args_2
identity��conv2d/StatefulPartitionedCall� conv2d_1/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall� top_pool/StatefulPartitionedCall�
conv2d/StatefulPartitionedCallStatefulPartitionedCallinputs%conv2d_statefulpartitionedcall_args_1%conv2d_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_conv2d_layer_call_and_return_conditional_losses_17342802 
conv2d/StatefulPartitionedCall�
 top_pool/StatefulPartitionedCallStatefulPartitionedCall'conv2d/StatefulPartitionedCall:output:0'top_pool_statefulpartitionedcall_args_1'top_pool_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_top_pool_layer_call_and_return_conditional_losses_17344282"
 top_pool/StatefulPartitionedCall�
 conv2d_1/StatefulPartitionedCallStatefulPartitionedCall)top_pool/StatefulPartitionedCall:output:0'conv2d_1_statefulpartitionedcall_args_1'conv2d_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_conv2d_1_layer_call_and_return_conditional_losses_17343012"
 conv2d_1/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall)conv2d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_17344492
flatten/PartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0$dense_statefulpartitionedcall_args_1$dense_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_17344672
dense/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0&dense_1_statefulpartitionedcall_args_1&dense_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_17344892!
dense_1/StatefulPartitionedCall�
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^conv2d/StatefulPartitionedCall!^conv2d_1/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^top_pool/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::2@
conv2d/StatefulPartitionedCallconv2d/StatefulPartitionedCall2D
 conv2d_1/StatefulPartitionedCall conv2d_1/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 top_pool/StatefulPartitionedCall top_pool/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
� 
�
G__inference_sequential_layer_call_and_return_conditional_losses_1734502
input_1)
%conv2d_statefulpartitionedcall_args_1)
%conv2d_statefulpartitionedcall_args_2+
'top_pool_statefulpartitionedcall_args_1+
'top_pool_statefulpartitionedcall_args_2+
'conv2d_1_statefulpartitionedcall_args_1+
'conv2d_1_statefulpartitionedcall_args_2(
$dense_statefulpartitionedcall_args_1(
$dense_statefulpartitionedcall_args_2*
&dense_1_statefulpartitionedcall_args_1*
&dense_1_statefulpartitionedcall_args_2
identity��conv2d/StatefulPartitionedCall� conv2d_1/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall� top_pool/StatefulPartitionedCall�
conv2d/StatefulPartitionedCallStatefulPartitionedCallinput_1%conv2d_statefulpartitionedcall_args_1%conv2d_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_conv2d_layer_call_and_return_conditional_losses_17342802 
conv2d/StatefulPartitionedCall�
 top_pool/StatefulPartitionedCallStatefulPartitionedCall'conv2d/StatefulPartitionedCall:output:0'top_pool_statefulpartitionedcall_args_1'top_pool_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_top_pool_layer_call_and_return_conditional_losses_17344282"
 top_pool/StatefulPartitionedCall�
 conv2d_1/StatefulPartitionedCallStatefulPartitionedCall)top_pool/StatefulPartitionedCall:output:0'conv2d_1_statefulpartitionedcall_args_1'conv2d_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_conv2d_1_layer_call_and_return_conditional_losses_17343012"
 conv2d_1/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall)conv2d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_17344492
flatten/PartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0$dense_statefulpartitionedcall_args_1$dense_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_17344672
dense/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0&dense_1_statefulpartitionedcall_args_1&dense_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_17344892!
dense_1/StatefulPartitionedCall�
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^conv2d/StatefulPartitionedCall!^conv2d_1/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^top_pool/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::2@
conv2d/StatefulPartitionedCallconv2d/StatefulPartitionedCall2D
 conv2d_1/StatefulPartitionedCall conv2d_1/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 top_pool/StatefulPartitionedCall top_pool/StatefulPartitionedCall:' #
!
_user_specified_name	input_1
�
E
)__inference_flatten_layer_call_fn_1735055

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_17344492
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:& "
 
_user_specified_nameinputs
�
�
*__inference_top_pool_layer_call_fn_1735044
in"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_top_pool_layer_call_and_return_conditional_losses_17344282
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������::22
StatefulPartitionedCallStatefulPartitionedCall:" 

_user_specified_nameIn
�w
�
E__inference_top_pool_layer_call_and_return_conditional_losses_1734428
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identity��ExpandDims/ReadVariableOp�add/ReadVariableOp�
ExtractImagePatchesExtractImagePatchesin*
T0*0
_output_shapes
:����������*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3�
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape�
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:���������2	
Reshape�
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddings�
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
	MirrorPad�
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:���������H*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5�
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shape�
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2�
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2�
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2�
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4�
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape�
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack�
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1�
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2�
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/Rank�
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transposition�
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
sort/transpose�
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
sort/TopKV2�
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
sort/transpose_1�

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2

sort/Neg_1�
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack�
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1�
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2�
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7�
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack�
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1�
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2�
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8�
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2�
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4�
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shape�
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
	Reshape_3�
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dim�

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dim�
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dim�
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
Sum�
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
add�
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
�
�
,__inference_sequential_layer_call_fn_1734558
input_1"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_17345452
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_1
�
�
*__inference_conv2d_1_layer_call_fn_1734309

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*A
_output_shapes/
-:+���������������������������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_conv2d_1_layer_call_and_return_conditional_losses_17343012
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
)__inference_dense_1_layer_call_fn_1735089

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_17344892
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
��
�
"__inference__wrapped_model_1734268
input_14
0sequential_conv2d_conv2d_readvariableop_resource5
1sequential_conv2d_biasadd_readvariableop_resource:
6sequential_top_pool_expanddims_readvariableop_resource3
/sequential_top_pool_add_readvariableop_resource6
2sequential_conv2d_1_conv2d_readvariableop_resource7
3sequential_conv2d_1_biasadd_readvariableop_resource3
/sequential_dense_matmul_readvariableop_resource4
0sequential_dense_biasadd_readvariableop_resource5
1sequential_dense_1_matmul_readvariableop_resource6
2sequential_dense_1_biasadd_readvariableop_resource
identity��(sequential/conv2d/BiasAdd/ReadVariableOp�'sequential/conv2d/Conv2D/ReadVariableOp�*sequential/conv2d_1/BiasAdd/ReadVariableOp�)sequential/conv2d_1/Conv2D/ReadVariableOp�'sequential/dense/BiasAdd/ReadVariableOp�&sequential/dense/MatMul/ReadVariableOp�)sequential/dense_1/BiasAdd/ReadVariableOp�(sequential/dense_1/MatMul/ReadVariableOp�-sequential/top_pool/ExpandDims/ReadVariableOp�&sequential/top_pool/add/ReadVariableOp�
'sequential/conv2d/Conv2D/ReadVariableOpReadVariableOp0sequential_conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02)
'sequential/conv2d/Conv2D/ReadVariableOp�
sequential/conv2d/Conv2DConv2Dinput_1/sequential/conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
sequential/conv2d/Conv2D�
(sequential/conv2d/BiasAdd/ReadVariableOpReadVariableOp1sequential_conv2d_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02*
(sequential/conv2d/BiasAdd/ReadVariableOp�
sequential/conv2d/BiasAddBiasAdd!sequential/conv2d/Conv2D:output:00sequential/conv2d/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
sequential/conv2d/BiasAdd�
'sequential/top_pool/ExtractImagePatchesExtractImagePatches"sequential/conv2d/BiasAdd:output:0*
T0*0
_output_shapes
:����������*
ksizes
*
paddingVALID*
rates
*
strides
2)
'sequential/top_pool/ExtractImagePatches�
sequential/top_pool/ShapeShape"sequential/conv2d/BiasAdd:output:0*
T0*
_output_shapes
:2
sequential/top_pool/Shape�
sequential/top_pool/Shape_1Shape1sequential/top_pool/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
sequential/top_pool/Shape_1�
'sequential/top_pool/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2)
'sequential/top_pool/strided_slice/stack�
)sequential/top_pool/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2+
)sequential/top_pool/strided_slice/stack_1�
)sequential/top_pool/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2+
)sequential/top_pool/strided_slice/stack_2�
!sequential/top_pool/strided_sliceStridedSlice"sequential/top_pool/Shape:output:00sequential/top_pool/strided_slice/stack:output:02sequential/top_pool/strided_slice/stack_1:output:02sequential/top_pool/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2#
!sequential/top_pool/strided_slicex
sequential/top_pool/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
sequential/top_pool/mul/y�
sequential/top_pool/mulMul*sequential/top_pool/strided_slice:output:0"sequential/top_pool/mul/y:output:0*
T0*
_output_shapes
: 2
sequential/top_pool/mul|
sequential/top_pool/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
sequential/top_pool/mul_1/y�
sequential/top_pool/mul_1Mulsequential/top_pool/mul:z:0$sequential/top_pool/mul_1/y:output:0*
T0*
_output_shapes
: 2
sequential/top_pool/mul_1�
#sequential/top_pool/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2%
#sequential/top_pool/Reshape/shape/1�
#sequential/top_pool/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2%
#sequential/top_pool/Reshape/shape/2�
#sequential/top_pool/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2%
#sequential/top_pool/Reshape/shape/3�
!sequential/top_pool/Reshape/shapePacksequential/top_pool/mul_1:z:0,sequential/top_pool/Reshape/shape/1:output:0,sequential/top_pool/Reshape/shape/2:output:0,sequential/top_pool/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2#
!sequential/top_pool/Reshape/shape�
sequential/top_pool/ReshapeReshape1sequential/top_pool/ExtractImagePatches:patches:0*sequential/top_pool/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2
sequential/top_pool/Reshape�
&sequential/top_pool/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2(
&sequential/top_pool/MirrorPad/paddings�
sequential/top_pool/MirrorPad	MirrorPad$sequential/top_pool/Reshape:output:0/sequential/top_pool/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
sequential/top_pool/MirrorPad�
)sequential/top_pool/ExtractImagePatches_1ExtractImagePatches&sequential/top_pool/MirrorPad:output:0*
T0*/
_output_shapes
:���������H*
ksizes
*
paddingVALID*
rates
*
strides
2+
)sequential/top_pool/ExtractImagePatches_1�
sequential/top_pool/Shape_2Shape3sequential/top_pool/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
sequential/top_pool/Shape_2�
sequential/top_pool/Shape_3Shape&sequential/top_pool/MirrorPad:output:0*
T0*
_output_shapes
:2
sequential/top_pool/Shape_3�
)sequential/top_pool/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)sequential/top_pool/strided_slice_1/stack�
+sequential/top_pool/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_1/stack_1�
+sequential/top_pool/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_1/stack_2�
#sequential/top_pool/strided_slice_1StridedSlice$sequential/top_pool/Shape_3:output:02sequential/top_pool/strided_slice_1/stack:output:04sequential/top_pool/strided_slice_1/stack_1:output:04sequential/top_pool/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential/top_pool/strided_slice_1�
)sequential/top_pool/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)sequential/top_pool/strided_slice_2/stack�
+sequential/top_pool/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_2/stack_1�
+sequential/top_pool/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_2/stack_2�
#sequential/top_pool/strided_slice_2StridedSlice$sequential/top_pool/Shape_2:output:02sequential/top_pool/strided_slice_2/stack:output:04sequential/top_pool/strided_slice_2/stack_1:output:04sequential/top_pool/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential/top_pool/strided_slice_2�
)sequential/top_pool/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)sequential/top_pool/strided_slice_3/stack�
+sequential/top_pool/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_3/stack_1�
+sequential/top_pool/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_3/stack_2�
#sequential/top_pool/strided_slice_3StridedSlice$sequential/top_pool/Shape_2:output:02sequential/top_pool/strided_slice_3/stack:output:04sequential/top_pool/strided_slice_3/stack_1:output:04sequential/top_pool/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential/top_pool/strided_slice_3�
%sequential/top_pool/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%sequential/top_pool/Reshape_1/shape/3�
%sequential/top_pool/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%sequential/top_pool/Reshape_1/shape/4�
%sequential/top_pool/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2'
%sequential/top_pool/Reshape_1/shape/5�
#sequential/top_pool/Reshape_1/shapePack,sequential/top_pool/strided_slice_1:output:0,sequential/top_pool/strided_slice_2:output:0,sequential/top_pool/strided_slice_3:output:0.sequential/top_pool/Reshape_1/shape/3:output:0.sequential/top_pool/Reshape_1/shape/4:output:0.sequential/top_pool/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2%
#sequential/top_pool/Reshape_1/shape�
sequential/top_pool/Reshape_1Reshape3sequential/top_pool/ExtractImagePatches_1:patches:0,sequential/top_pool/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
sequential/top_pool/Reshape_1�
sequential/top_pool/Shape_4Shape&sequential/top_pool/Reshape_1:output:0*
T0*
_output_shapes
:2
sequential/top_pool/Shape_4�
)sequential/top_pool/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)sequential/top_pool/strided_slice_4/stack�
+sequential/top_pool/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_4/stack_1�
+sequential/top_pool/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_4/stack_2�
#sequential/top_pool/strided_slice_4StridedSlice$sequential/top_pool/Shape_4:output:02sequential/top_pool/strided_slice_4/stack:output:04sequential/top_pool/strided_slice_4/stack_1:output:04sequential/top_pool/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential/top_pool/strided_slice_4�
)sequential/top_pool/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)sequential/top_pool/strided_slice_5/stack�
+sequential/top_pool/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_5/stack_1�
+sequential/top_pool/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_5/stack_2�
#sequential/top_pool/strided_slice_5StridedSlice$sequential/top_pool/Shape_4:output:02sequential/top_pool/strided_slice_5/stack:output:04sequential/top_pool/strided_slice_5/stack_1:output:04sequential/top_pool/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential/top_pool/strided_slice_5�
)sequential/top_pool/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)sequential/top_pool/strided_slice_6/stack�
+sequential/top_pool/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_6/stack_1�
+sequential/top_pool/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_6/stack_2�
#sequential/top_pool/strided_slice_6StridedSlice$sequential/top_pool/Shape_4:output:02sequential/top_pool/strided_slice_6/stack:output:04sequential/top_pool/strided_slice_6/stack_1:output:04sequential/top_pool/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential/top_pool/strided_slice_6�
%sequential/top_pool/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2'
%sequential/top_pool/Reshape_2/shape/3�
%sequential/top_pool/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%sequential/top_pool/Reshape_2/shape/4�
#sequential/top_pool/Reshape_2/shapePack,sequential/top_pool/strided_slice_4:output:0,sequential/top_pool/strided_slice_5:output:0,sequential/top_pool/strided_slice_6:output:0.sequential/top_pool/Reshape_2/shape/3:output:0.sequential/top_pool/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#sequential/top_pool/Reshape_2/shape�
sequential/top_pool/Reshape_2Reshape&sequential/top_pool/Reshape_1:output:0,sequential/top_pool/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
sequential/top_pool/Reshape_2�
sequential/top_pool/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
sequential/top_pool/sort/axis�
sequential/top_pool/sort/NegNeg&sequential/top_pool/Reshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
sequential/top_pool/sort/Neg�
sequential/top_pool/sort/ShapeShape sequential/top_pool/sort/Neg:y:0*
T0*
_output_shapes
:2 
sequential/top_pool/sort/Shape�
,sequential/top_pool/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2.
,sequential/top_pool/sort/strided_slice/stack�
.sequential/top_pool/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.sequential/top_pool/sort/strided_slice/stack_1�
.sequential/top_pool/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.sequential/top_pool/sort/strided_slice/stack_2�
&sequential/top_pool/sort/strided_sliceStridedSlice'sequential/top_pool/sort/Shape:output:05sequential/top_pool/sort/strided_slice/stack:output:07sequential/top_pool/sort/strided_slice/stack_1:output:07sequential/top_pool/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&sequential/top_pool/sort/strided_slice�
sequential/top_pool/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
sequential/top_pool/sort/Rank�
&sequential/top_pool/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2(
&sequential/top_pool/sort/transposition�
"sequential/top_pool/sort/transpose	Transpose sequential/top_pool/sort/Neg:y:0/sequential/top_pool/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2$
"sequential/top_pool/sort/transpose�
sequential/top_pool/sort/TopKV2TopKV2&sequential/top_pool/sort/transpose:y:0/sequential/top_pool/sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2!
sequential/top_pool/sort/TopKV2�
$sequential/top_pool/sort/transpose_1	Transpose(sequential/top_pool/sort/TopKV2:values:0/sequential/top_pool/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2&
$sequential/top_pool/sort/transpose_1�
sequential/top_pool/sort/Neg_1Neg(sequential/top_pool/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2 
sequential/top_pool/sort/Neg_1�
)sequential/top_pool/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2+
)sequential/top_pool/strided_slice_7/stack�
+sequential/top_pool/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2-
+sequential/top_pool/strided_slice_7/stack_1�
+sequential/top_pool/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2-
+sequential/top_pool/strided_slice_7/stack_2�
#sequential/top_pool/strided_slice_7StridedSlice"sequential/top_pool/sort/Neg_1:y:02sequential/top_pool/strided_slice_7/stack:output:04sequential/top_pool/strided_slice_7/stack_1:output:04sequential/top_pool/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2%
#sequential/top_pool/strided_slice_7�
)sequential/top_pool/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2+
)sequential/top_pool/strided_slice_8/stack�
+sequential/top_pool/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2-
+sequential/top_pool/strided_slice_8/stack_1�
+sequential/top_pool/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2-
+sequential/top_pool/strided_slice_8/stack_2�
#sequential/top_pool/strided_slice_8StridedSlice"sequential/top_pool/sort/Neg_1:y:02sequential/top_pool/strided_slice_8/stack:output:04sequential/top_pool/strided_slice_8/stack_1:output:04sequential/top_pool/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2%
#sequential/top_pool/strided_slice_8�
sequential/top_pool/subSub,sequential/top_pool/strided_slice_8:output:0,sequential/top_pool/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
sequential/top_pool/sub
sequential/top_pool/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
sequential/top_pool/Floor/x�
sequential/top_pool/FloorFloor$sequential/top_pool/Floor/x:output:0*
T0*
_output_shapes
: 2
sequential/top_pool/Floor�
sequential/top_pool/CastCastsequential/top_pool/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
sequential/top_pool/Cast�
sequential/top_pool/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
sequential/top_pool/Floor_1/x�
sequential/top_pool/Floor_1Floor&sequential/top_pool/Floor_1/x:output:0*
T0*
_output_shapes
: 2
sequential/top_pool/Floor_1�
sequential/top_pool/Cast_1Castsequential/top_pool/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
sequential/top_pool/Cast_1�
sequential/top_pool/Shape_5Shapesequential/top_pool/sub:z:0*
T0*
_output_shapes
:2
sequential/top_pool/Shape_5�
sequential/top_pool/Shape_6Shape"sequential/conv2d/BiasAdd:output:0*
T0*
_output_shapes
:2
sequential/top_pool/Shape_6�
)sequential/top_pool/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)sequential/top_pool/strided_slice_9/stack�
+sequential/top_pool/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_9/stack_1�
+sequential/top_pool/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+sequential/top_pool/strided_slice_9/stack_2�
#sequential/top_pool/strided_slice_9StridedSlice$sequential/top_pool/Shape_6:output:02sequential/top_pool/strided_slice_9/stack:output:04sequential/top_pool/strided_slice_9/stack_1:output:04sequential/top_pool/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#sequential/top_pool/strided_slice_9�
%sequential/top_pool/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%sequential/top_pool/Reshape_3/shape/3�
%sequential/top_pool/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%sequential/top_pool/Reshape_3/shape/4�
#sequential/top_pool/Reshape_3/shapePack,sequential/top_pool/strided_slice_9:output:0sequential/top_pool/Cast:y:0sequential/top_pool/Cast_1:y:0.sequential/top_pool/Reshape_3/shape/3:output:0.sequential/top_pool/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#sequential/top_pool/Reshape_3/shape�
sequential/top_pool/Reshape_3Reshapesequential/top_pool/sub:z:0,sequential/top_pool/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
sequential/top_pool/Reshape_3�
-sequential/top_pool/ExpandDims/ReadVariableOpReadVariableOp6sequential_top_pool_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02/
-sequential/top_pool/ExpandDims/ReadVariableOp�
"sequential/top_pool/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2$
"sequential/top_pool/ExpandDims/dim�
sequential/top_pool/ExpandDims
ExpandDims5sequential/top_pool/ExpandDims/ReadVariableOp:value:0+sequential/top_pool/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2 
sequential/top_pool/ExpandDims�
$sequential/top_pool/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$sequential/top_pool/ExpandDims_1/dim�
 sequential/top_pool/ExpandDims_1
ExpandDims'sequential/top_pool/ExpandDims:output:0-sequential/top_pool/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2"
 sequential/top_pool/ExpandDims_1�
$sequential/top_pool/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$sequential/top_pool/ExpandDims_2/dim�
 sequential/top_pool/ExpandDims_2
ExpandDims)sequential/top_pool/ExpandDims_1:output:0-sequential/top_pool/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2"
 sequential/top_pool/ExpandDims_2�
sequential/top_pool/mul_2Mul&sequential/top_pool/Reshape_3:output:0)sequential/top_pool/ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
sequential/top_pool/mul_2�
)sequential/top_pool/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2+
)sequential/top_pool/Sum/reduction_indices�
sequential/top_pool/SumSumsequential/top_pool/mul_2:z:02sequential/top_pool/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
sequential/top_pool/Sum�
&sequential/top_pool/add/ReadVariableOpReadVariableOp/sequential_top_pool_add_readvariableop_resource*&
_output_shapes
:*
dtype02(
&sequential/top_pool/add/ReadVariableOp�
sequential/top_pool/addAddV2 sequential/top_pool/Sum:output:0.sequential/top_pool/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
sequential/top_pool/add�
)sequential/conv2d_1/Conv2D/ReadVariableOpReadVariableOp2sequential_conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02+
)sequential/conv2d_1/Conv2D/ReadVariableOp�
sequential/conv2d_1/Conv2DConv2Dsequential/top_pool/add:z:01sequential/conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
sequential/conv2d_1/Conv2D�
*sequential/conv2d_1/BiasAdd/ReadVariableOpReadVariableOp3sequential_conv2d_1_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02,
*sequential/conv2d_1/BiasAdd/ReadVariableOp�
sequential/conv2d_1/BiasAddBiasAdd#sequential/conv2d_1/Conv2D:output:02sequential/conv2d_1/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
sequential/conv2d_1/BiasAdd�
sequential/conv2d_1/ReluRelu$sequential/conv2d_1/BiasAdd:output:0*
T0*/
_output_shapes
:���������2
sequential/conv2d_1/Relu�
sequential/flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"����   2
sequential/flatten/Const�
sequential/flatten/ReshapeReshape&sequential/conv2d_1/Relu:activations:0!sequential/flatten/Const:output:0*
T0*(
_output_shapes
:����������2
sequential/flatten/Reshape�
&sequential/dense/MatMul/ReadVariableOpReadVariableOp/sequential_dense_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02(
&sequential/dense/MatMul/ReadVariableOp�
sequential/dense/MatMulMatMul#sequential/flatten/Reshape:output:0.sequential/dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
sequential/dense/MatMul�
'sequential/dense/BiasAdd/ReadVariableOpReadVariableOp0sequential_dense_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02)
'sequential/dense/BiasAdd/ReadVariableOp�
sequential/dense/BiasAddBiasAdd!sequential/dense/MatMul:product:0/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
sequential/dense/BiasAdd�
(sequential/dense_1/MatMul/ReadVariableOpReadVariableOp1sequential_dense_1_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02*
(sequential/dense_1/MatMul/ReadVariableOp�
sequential/dense_1/MatMulMatMul!sequential/dense/BiasAdd:output:00sequential/dense_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
sequential/dense_1/MatMul�
)sequential/dense_1/BiasAdd/ReadVariableOpReadVariableOp2sequential_dense_1_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02+
)sequential/dense_1/BiasAdd/ReadVariableOp�
sequential/dense_1/BiasAddBiasAdd#sequential/dense_1/MatMul:product:01sequential/dense_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
sequential/dense_1/BiasAdd�
IdentityIdentity#sequential/dense_1/BiasAdd:output:0)^sequential/conv2d/BiasAdd/ReadVariableOp(^sequential/conv2d/Conv2D/ReadVariableOp+^sequential/conv2d_1/BiasAdd/ReadVariableOp*^sequential/conv2d_1/Conv2D/ReadVariableOp(^sequential/dense/BiasAdd/ReadVariableOp'^sequential/dense/MatMul/ReadVariableOp*^sequential/dense_1/BiasAdd/ReadVariableOp)^sequential/dense_1/MatMul/ReadVariableOp.^sequential/top_pool/ExpandDims/ReadVariableOp'^sequential/top_pool/add/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::2T
(sequential/conv2d/BiasAdd/ReadVariableOp(sequential/conv2d/BiasAdd/ReadVariableOp2R
'sequential/conv2d/Conv2D/ReadVariableOp'sequential/conv2d/Conv2D/ReadVariableOp2X
*sequential/conv2d_1/BiasAdd/ReadVariableOp*sequential/conv2d_1/BiasAdd/ReadVariableOp2V
)sequential/conv2d_1/Conv2D/ReadVariableOp)sequential/conv2d_1/Conv2D/ReadVariableOp2R
'sequential/dense/BiasAdd/ReadVariableOp'sequential/dense/BiasAdd/ReadVariableOp2P
&sequential/dense/MatMul/ReadVariableOp&sequential/dense/MatMul/ReadVariableOp2V
)sequential/dense_1/BiasAdd/ReadVariableOp)sequential/dense_1/BiasAdd/ReadVariableOp2T
(sequential/dense_1/MatMul/ReadVariableOp(sequential/dense_1/MatMul/ReadVariableOp2^
-sequential/top_pool/ExpandDims/ReadVariableOp-sequential/top_pool/ExpandDims/ReadVariableOp2P
&sequential/top_pool/add/ReadVariableOp&sequential/top_pool/add/ReadVariableOp:' #
!
_user_specified_name	input_1
�
`
D__inference_flatten_layer_call_and_return_conditional_losses_1734449

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"����   2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:����������2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:& "
 
_user_specified_nameinputs
�
�
,__inference_sequential_layer_call_fn_1734925

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_17345802
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
E__inference_conv2d_1_layer_call_and_return_conditional_losses_1734301

inputs"
conv2d_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOpo
dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      2
dilation_rate�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������*
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������2	
BiasAddr
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������2
Relu�
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*A
_output_shapes/
-:+���������������������������2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+���������������������������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:& "
 
_user_specified_nameinputs
�
�
,__inference_sequential_layer_call_fn_1734593
input_1"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_17345802
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_1
� 
�
G__inference_sequential_layer_call_and_return_conditional_losses_1734522
input_1)
%conv2d_statefulpartitionedcall_args_1)
%conv2d_statefulpartitionedcall_args_2+
'top_pool_statefulpartitionedcall_args_1+
'top_pool_statefulpartitionedcall_args_2+
'conv2d_1_statefulpartitionedcall_args_1+
'conv2d_1_statefulpartitionedcall_args_2(
$dense_statefulpartitionedcall_args_1(
$dense_statefulpartitionedcall_args_2*
&dense_1_statefulpartitionedcall_args_1*
&dense_1_statefulpartitionedcall_args_2
identity��conv2d/StatefulPartitionedCall� conv2d_1/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall� top_pool/StatefulPartitionedCall�
conv2d/StatefulPartitionedCallStatefulPartitionedCallinput_1%conv2d_statefulpartitionedcall_args_1%conv2d_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_conv2d_layer_call_and_return_conditional_losses_17342802 
conv2d/StatefulPartitionedCall�
 top_pool/StatefulPartitionedCallStatefulPartitionedCall'conv2d/StatefulPartitionedCall:output:0'top_pool_statefulpartitionedcall_args_1'top_pool_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_top_pool_layer_call_and_return_conditional_losses_17344282"
 top_pool/StatefulPartitionedCall�
 conv2d_1/StatefulPartitionedCallStatefulPartitionedCall)top_pool/StatefulPartitionedCall:output:0'conv2d_1_statefulpartitionedcall_args_1'conv2d_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_conv2d_1_layer_call_and_return_conditional_losses_17343012"
 conv2d_1/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall)conv2d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_17344492
flatten/PartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0$dense_statefulpartitionedcall_args_1$dense_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

GPU 

CPU2J 8*K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_17344672
dense/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0&dense_1_statefulpartitionedcall_args_1&dense_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_17344892!
dense_1/StatefulPartitionedCall�
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^conv2d/StatefulPartitionedCall!^conv2d_1/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall!^top_pool/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::2@
conv2d/StatefulPartitionedCallconv2d/StatefulPartitionedCall2D
 conv2d_1/StatefulPartitionedCall conv2d_1/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2D
 top_pool/StatefulPartitionedCall top_pool/StatefulPartitionedCall:' #
!
_user_specified_name	input_1
�w
�
E__inference_top_pool_layer_call_and_return_conditional_losses_1735037
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identity��ExpandDims/ReadVariableOp�add/ReadVariableOp�
ExtractImagePatchesExtractImagePatchesin*
T0*0
_output_shapes
:����������*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3�
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape�
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:���������2	
Reshape�
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddings�
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
	MirrorPad�
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:���������H*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5�
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shape�
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2�
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2�
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2�
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4�
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape�
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack�
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1�
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2�
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/Rank�
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transposition�
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
sort/transpose�
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
sort/TopKV2�
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
sort/transpose_1�

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2

sort/Neg_1�
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack�
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1�
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2�
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7�
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack�
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1�
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2�
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8�
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2�
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4�
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shape�
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
	Reshape_3�
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dim�

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dim�
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dim�
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
Sum�
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
add�
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
�
`
D__inference_flatten_layer_call_and_return_conditional_losses_1735050

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"����   2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:����������2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:& "
 
_user_specified_nameinputs
�
�
B__inference_dense_layer_call_and_return_conditional_losses_1734467

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
��
�
G__inference_sequential_layer_call_and_return_conditional_losses_1734756

inputs)
%conv2d_conv2d_readvariableop_resource*
&conv2d_biasadd_readvariableop_resource/
+top_pool_expanddims_readvariableop_resource(
$top_pool_add_readvariableop_resource+
'conv2d_1_conv2d_readvariableop_resource,
(conv2d_1_biasadd_readvariableop_resource(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource*
&dense_1_matmul_readvariableop_resource+
'dense_1_biasadd_readvariableop_resource
identity��conv2d/BiasAdd/ReadVariableOp�conv2d/Conv2D/ReadVariableOp�conv2d_1/BiasAdd/ReadVariableOp�conv2d_1/Conv2D/ReadVariableOp�dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�dense_1/BiasAdd/ReadVariableOp�dense_1/MatMul/ReadVariableOp�"top_pool/ExpandDims/ReadVariableOp�top_pool/add/ReadVariableOp�
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
conv2d/Conv2D/ReadVariableOp�
conv2d/Conv2DConv2Dinputs$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d/Conv2D�
conv2d/BiasAdd/ReadVariableOpReadVariableOp&conv2d_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
conv2d/BiasAdd/ReadVariableOp�
conv2d/BiasAddBiasAddconv2d/Conv2D:output:0%conv2d/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
conv2d/BiasAdd�
top_pool/ExtractImagePatchesExtractImagePatchesconv2d/BiasAdd:output:0*
T0*0
_output_shapes
:����������*
ksizes
*
paddingVALID*
rates
*
strides
2
top_pool/ExtractImagePatchesg
top_pool/ShapeShapeconv2d/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool/Shapez
top_pool/Shape_1Shape&top_pool/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool/Shape_1�
top_pool/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
top_pool/strided_slice/stack�
top_pool/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice/stack_1�
top_pool/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice/stack_2�
top_pool/strided_sliceStridedSlicetop_pool/Shape:output:0%top_pool/strided_slice/stack:output:0'top_pool/strided_slice/stack_1:output:0'top_pool/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_sliceb
top_pool/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/mul/y~
top_pool/mulMultop_pool/strided_slice:output:0top_pool/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool/mulf
top_pool/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/mul_1/yu
top_pool/mul_1Multop_pool/mul:z:0top_pool/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool/mul_1v
top_pool/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape/shape/1v
top_pool/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape/shape/2v
top_pool/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape/shape/3�
top_pool/Reshape/shapePacktop_pool/mul_1:z:0!top_pool/Reshape/shape/1:output:0!top_pool/Reshape/shape/2:output:0!top_pool/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape/shape�
top_pool/ReshapeReshape&top_pool/ExtractImagePatches:patches:0top_pool/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2
top_pool/Reshape�
top_pool/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool/MirrorPad/paddings�
top_pool/MirrorPad	MirrorPadtop_pool/Reshape:output:0$top_pool/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
top_pool/MirrorPad�
top_pool/ExtractImagePatches_1ExtractImagePatchestop_pool/MirrorPad:output:0*
T0*/
_output_shapes
:���������H*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool/ExtractImagePatches_1|
top_pool/Shape_2Shape(top_pool/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool/Shape_2o
top_pool/Shape_3Shapetop_pool/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool/Shape_3�
top_pool/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool/strided_slice_1/stack�
 top_pool/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_1/stack_1�
 top_pool/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_1/stack_2�
top_pool/strided_slice_1StridedSlicetop_pool/Shape_3:output:0'top_pool/strided_slice_1/stack:output:0)top_pool/strided_slice_1/stack_1:output:0)top_pool/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_1�
top_pool/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_2/stack�
 top_pool/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_2/stack_1�
 top_pool/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_2/stack_2�
top_pool/strided_slice_2StridedSlicetop_pool/Shape_2:output:0'top_pool/strided_slice_2/stack:output:0)top_pool/strided_slice_2/stack_1:output:0)top_pool/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_2�
top_pool/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_3/stack�
 top_pool/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_3/stack_1�
 top_pool/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_3/stack_2�
top_pool/strided_slice_3StridedSlicetop_pool/Shape_2:output:0'top_pool/strided_slice_3/stack:output:0)top_pool/strided_slice_3/stack_1:output:0)top_pool/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_3z
top_pool/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_1/shape/3z
top_pool/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_1/shape/4z
top_pool/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_1/shape/5�
top_pool/Reshape_1/shapePack!top_pool/strided_slice_1:output:0!top_pool/strided_slice_2:output:0!top_pool/strided_slice_3:output:0#top_pool/Reshape_1/shape/3:output:0#top_pool/Reshape_1/shape/4:output:0#top_pool/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape_1/shape�
top_pool/Reshape_1Reshape(top_pool/ExtractImagePatches_1:patches:0!top_pool/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
top_pool/Reshape_1o
top_pool/Shape_4Shapetop_pool/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool/Shape_4�
top_pool/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool/strided_slice_4/stack�
 top_pool/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_4/stack_1�
 top_pool/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_4/stack_2�
top_pool/strided_slice_4StridedSlicetop_pool/Shape_4:output:0'top_pool/strided_slice_4/stack:output:0)top_pool/strided_slice_4/stack_1:output:0)top_pool/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_4�
top_pool/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_5/stack�
 top_pool/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_5/stack_1�
 top_pool/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_5/stack_2�
top_pool/strided_slice_5StridedSlicetop_pool/Shape_4:output:0'top_pool/strided_slice_5/stack:output:0)top_pool/strided_slice_5/stack_1:output:0)top_pool/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_5�
top_pool/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2 
top_pool/strided_slice_6/stack�
 top_pool/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_6/stack_1�
 top_pool/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_6/stack_2�
top_pool/strided_slice_6StridedSlicetop_pool/Shape_4:output:0'top_pool/strided_slice_6/stack:output:0)top_pool/strided_slice_6/stack_1:output:0)top_pool/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_6z
top_pool/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool/Reshape_2/shape/3z
top_pool/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_2/shape/4�
top_pool/Reshape_2/shapePack!top_pool/strided_slice_4:output:0!top_pool/strided_slice_5:output:0!top_pool/strided_slice_6:output:0#top_pool/Reshape_2/shape/3:output:0#top_pool/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape_2/shape�
top_pool/Reshape_2Reshapetop_pool/Reshape_1:output:0!top_pool/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool/Reshape_2j
top_pool/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/sort/axis�
top_pool/sort/NegNegtop_pool/Reshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool/sort/Nego
top_pool/sort/ShapeShapetop_pool/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool/sort/Shape�
!top_pool/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2#
!top_pool/sort/strided_slice/stack�
#top_pool/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool/sort/strided_slice/stack_1�
#top_pool/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool/sort/strided_slice/stack_2�
top_pool/sort/strided_sliceStridedSlicetop_pool/sort/Shape:output:0*top_pool/sort/strided_slice/stack:output:0,top_pool/sort/strided_slice/stack_1:output:0,top_pool/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/sort/strided_slicej
top_pool/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/sort/Rank�
top_pool/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool/sort/transposition�
top_pool/sort/transpose	Transposetop_pool/sort/Neg:y:0$top_pool/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
top_pool/sort/transpose�
top_pool/sort/TopKV2TopKV2top_pool/sort/transpose:y:0$top_pool/sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
top_pool/sort/TopKV2�
top_pool/sort/transpose_1	Transposetop_pool/sort/TopKV2:values:0$top_pool/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
top_pool/sort/transpose_1�
top_pool/sort/Neg_1Negtop_pool/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2
top_pool/sort/Neg_1�
top_pool/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2 
top_pool/strided_slice_7/stack�
 top_pool/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool/strided_slice_7/stack_1�
 top_pool/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2"
 top_pool/strided_slice_7/stack_2�
top_pool/strided_slice_7StridedSlicetop_pool/sort/Neg_1:y:0'top_pool/strided_slice_7/stack:output:0)top_pool/strided_slice_7/stack_1:output:0)top_pool/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool/strided_slice_7�
top_pool/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2 
top_pool/strided_slice_8/stack�
 top_pool/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool/strided_slice_8/stack_1�
 top_pool/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2"
 top_pool/strided_slice_8/stack_2�
top_pool/strided_slice_8StridedSlicetop_pool/sort/Neg_1:y:0'top_pool/strided_slice_8/stack:output:0)top_pool/strided_slice_8/stack_1:output:0)top_pool/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool/strided_slice_8�
top_pool/subSub!top_pool/strided_slice_8:output:0!top_pool/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
top_pool/subi
top_pool/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
top_pool/Floor/xe
top_pool/FloorFloortop_pool/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool/Floorj
top_pool/CastCasttop_pool/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool/Castm
top_pool/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  HA2
top_pool/Floor_1/xk
top_pool/Floor_1Floortop_pool/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool/Floor_1p
top_pool/Cast_1Casttop_pool/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool/Cast_1d
top_pool/Shape_5Shapetop_pool/sub:z:0*
T0*
_output_shapes
:2
top_pool/Shape_5k
top_pool/Shape_6Shapeconv2d/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool/Shape_6�
top_pool/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool/strided_slice_9/stack�
 top_pool/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_9/stack_1�
 top_pool/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool/strided_slice_9/stack_2�
top_pool/strided_slice_9StridedSlicetop_pool/Shape_6:output:0'top_pool/strided_slice_9/stack:output:0)top_pool/strided_slice_9/stack_1:output:0)top_pool/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool/strided_slice_9z
top_pool/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_3/shape/3z
top_pool/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool/Reshape_3/shape/4�
top_pool/Reshape_3/shapePack!top_pool/strided_slice_9:output:0top_pool/Cast:y:0top_pool/Cast_1:y:0#top_pool/Reshape_3/shape/3:output:0#top_pool/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool/Reshape_3/shape�
top_pool/Reshape_3Reshapetop_pool/sub:z:0!top_pool/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
top_pool/Reshape_3�
"top_pool/ExpandDims/ReadVariableOpReadVariableOp+top_pool_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02$
"top_pool/ExpandDims/ReadVariableOpt
top_pool/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool/ExpandDims/dim�
top_pool/ExpandDims
ExpandDims*top_pool/ExpandDims/ReadVariableOp:value:0 top_pool/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool/ExpandDimsx
top_pool/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/ExpandDims_1/dim�
top_pool/ExpandDims_1
ExpandDimstop_pool/ExpandDims:output:0"top_pool/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool/ExpandDims_1x
top_pool/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool/ExpandDims_2/dim�
top_pool/ExpandDims_2
ExpandDimstop_pool/ExpandDims_1:output:0"top_pool/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool/ExpandDims_2�
top_pool/mul_2Multop_pool/Reshape_3:output:0top_pool/ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
top_pool/mul_2�
top_pool/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2 
top_pool/Sum/reduction_indices�
top_pool/SumSumtop_pool/mul_2:z:0'top_pool/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
top_pool/Sum�
top_pool/add/ReadVariableOpReadVariableOp$top_pool_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool/add/ReadVariableOp�
top_pool/addAddV2top_pool/Sum:output:0#top_pool/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
top_pool/add�
conv2d_1/Conv2D/ReadVariableOpReadVariableOp'conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02 
conv2d_1/Conv2D/ReadVariableOp�
conv2d_1/Conv2DConv2Dtop_pool/add:z:0&conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d_1/Conv2D�
conv2d_1/BiasAdd/ReadVariableOpReadVariableOp(conv2d_1_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
conv2d_1/BiasAdd/ReadVariableOp�
conv2d_1/BiasAddBiasAddconv2d_1/Conv2D:output:0'conv2d_1/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
conv2d_1/BiasAdd{
conv2d_1/ReluReluconv2d_1/BiasAdd:output:0*
T0*/
_output_shapes
:���������2
conv2d_1/Reluo
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"����   2
flatten/Const�
flatten/ReshapeReshapeconv2d_1/Relu:activations:0flatten/Const:output:0*
T0*(
_output_shapes
:����������2
flatten/Reshape�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulflatten/Reshape:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense/MatMul�
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
dense/BiasAdd/ReadVariableOp�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense/BiasAdd�
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
dense_1/MatMul/ReadVariableOp�
dense_1/MatMulMatMuldense/BiasAdd:output:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_1/MatMul�
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02 
dense_1/BiasAdd/ReadVariableOp�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_1/BiasAdd�
IdentityIdentitydense_1/BiasAdd:output:0^conv2d/BiasAdd/ReadVariableOp^conv2d/Conv2D/ReadVariableOp ^conv2d_1/BiasAdd/ReadVariableOp^conv2d_1/Conv2D/ReadVariableOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^dense_1/BiasAdd/ReadVariableOp^dense_1/MatMul/ReadVariableOp#^top_pool/ExpandDims/ReadVariableOp^top_pool/add/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:���������  ::::::::::2>
conv2d/BiasAdd/ReadVariableOpconv2d/BiasAdd/ReadVariableOp2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp2B
conv2d_1/BiasAdd/ReadVariableOpconv2d_1/BiasAdd/ReadVariableOp2@
conv2d_1/Conv2D/ReadVariableOpconv2d_1/Conv2D/ReadVariableOp2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp2@
dense_1/BiasAdd/ReadVariableOpdense_1/BiasAdd/ReadVariableOp2>
dense_1/MatMul/ReadVariableOpdense_1/MatMul/ReadVariableOp2H
"top_pool/ExpandDims/ReadVariableOp"top_pool/ExpandDims/ReadVariableOp2:
top_pool/add/ReadVariableOptop_pool/add/ReadVariableOp:& "
 
_user_specified_nameinputs
�I
�
 __inference__traced_save_1735224
file_prefix,
(savev2_conv2d_kernel_read_readvariableop*
&savev2_conv2d_bias_read_readvariableop)
%savev2_top_pool_w_read_readvariableop)
%savev2_top_pool_b_read_readvariableop.
*savev2_conv2d_1_kernel_read_readvariableop,
(savev2_conv2d_1_bias_read_readvariableop+
'savev2_dense_kernel_read_readvariableop)
%savev2_dense_bias_read_readvariableop-
)savev2_dense_1_kernel_read_readvariableop+
'savev2_dense_1_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop3
/savev2_adam_conv2d_kernel_m_read_readvariableop1
-savev2_adam_conv2d_bias_m_read_readvariableop0
,savev2_adam_top_pool_w_m_read_readvariableop0
,savev2_adam_top_pool_b_m_read_readvariableop5
1savev2_adam_conv2d_1_kernel_m_read_readvariableop3
/savev2_adam_conv2d_1_bias_m_read_readvariableop2
.savev2_adam_dense_kernel_m_read_readvariableop0
,savev2_adam_dense_bias_m_read_readvariableop4
0savev2_adam_dense_1_kernel_m_read_readvariableop2
.savev2_adam_dense_1_bias_m_read_readvariableop3
/savev2_adam_conv2d_kernel_v_read_readvariableop1
-savev2_adam_conv2d_bias_v_read_readvariableop0
,savev2_adam_top_pool_w_v_read_readvariableop0
,savev2_adam_top_pool_b_v_read_readvariableop5
1savev2_adam_conv2d_1_kernel_v_read_readvariableop3
/savev2_adam_conv2d_1_bias_v_read_readvariableop2
.savev2_adam_dense_kernel_v_read_readvariableop0
,savev2_adam_dense_bias_v_read_readvariableop4
0savev2_adam_dense_1_kernel_v_read_readvariableop2
.savev2_adam_dense_1_bias_v_read_readvariableop
savev2_1_const

identity_1��MergeV2Checkpoints�SaveV2�SaveV2_1�
StringJoin/inputs_1Const"/device:CPU:0*
_output_shapes
: *
dtype0*<
value3B1 B+_temp_f344ba54149e435997f3495003d69a55/part2
StringJoin/inputs_1�

StringJoin
StringJoinfile_prefixStringJoin/inputs_1:output:0"/device:CPU:0*
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*�
value�B�%B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*]
valueTBR%B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0(savev2_conv2d_kernel_read_readvariableop&savev2_conv2d_bias_read_readvariableop%savev2_top_pool_w_read_readvariableop%savev2_top_pool_b_read_readvariableop*savev2_conv2d_1_kernel_read_readvariableop(savev2_conv2d_1_bias_read_readvariableop'savev2_dense_kernel_read_readvariableop%savev2_dense_bias_read_readvariableop)savev2_dense_1_kernel_read_readvariableop'savev2_dense_1_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop/savev2_adam_conv2d_kernel_m_read_readvariableop-savev2_adam_conv2d_bias_m_read_readvariableop,savev2_adam_top_pool_w_m_read_readvariableop,savev2_adam_top_pool_b_m_read_readvariableop1savev2_adam_conv2d_1_kernel_m_read_readvariableop/savev2_adam_conv2d_1_bias_m_read_readvariableop.savev2_adam_dense_kernel_m_read_readvariableop,savev2_adam_dense_bias_m_read_readvariableop0savev2_adam_dense_1_kernel_m_read_readvariableop.savev2_adam_dense_1_bias_m_read_readvariableop/savev2_adam_conv2d_kernel_v_read_readvariableop-savev2_adam_conv2d_bias_v_read_readvariableop,savev2_adam_top_pool_w_v_read_readvariableop,savev2_adam_top_pool_b_v_read_readvariableop1savev2_adam_conv2d_1_kernel_v_read_readvariableop/savev2_adam_conv2d_1_bias_v_read_readvariableop.savev2_adam_dense_kernel_v_read_readvariableop,savev2_adam_dense_bias_v_read_readvariableop0savev2_adam_dense_1_kernel_v_read_readvariableop.savev2_adam_dense_1_bias_v_read_readvariableop"/device:CPU:0*
_output_shapes
 *3
dtypes)
'2%	2
SaveV2�
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shard�
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1�
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_names�
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slices�
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity�

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :::::::
��:�:	�
:
: : : : : : : :::::::
��:�:	�
:
:::::::
��:�:	�
:
: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:+ '
%
_user_specified_namefile_prefix
�
�
D__inference_dense_1_layer_call_and_return_conditional_losses_1735082

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
�
�
D__inference_dense_1_layer_call_and_return_conditional_losses_1734489

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
C
input_18
serving_default_input_1:0���������  ;
dense_10
StatefulPartitionedCall:0���������
tensorflow/serving/predict:��
�/
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
	optimizer
		variables

regularization_losses
trainable_variables
	keras_api

signatures
q_default_save_signature
r__call__
*s&call_and_return_all_conditional_losses"�+
_tf_keras_sequential�+{"class_name": "Sequential", "name": "sequential", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "sequential", "layers": [{"class_name": "Conv2D", "config": {"name": "conv2d", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "batch_input_shape": [null, 32, 32, 3]}}, {"class_name": "TopPool", "config": {"name": "top_pool", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}, {"class_name": "Conv2D", "config": {"name": "conv2d_1", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Flatten", "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 3}}}, "is_graph_network": true, "keras_version": "2.2.4-tf", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential", "layers": [{"class_name": "Conv2D", "config": {"name": "conv2d", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "batch_input_shape": [null, 32, 32, 3]}}, {"class_name": "TopPool", "config": {"name": "top_pool", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}, {"class_name": "Conv2D", "config": {"name": "conv2d_1", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Flatten", "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}}, "training_config": {"loss": {"class_name": "SparseCategoricalCrossentropy", "config": {"reduction": "auto", "name": "sparse_categorical_crossentropy", "from_logits": true}}, "metrics": ["accuracy"], "weighted_metrics": null, "sample_weight_mode": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0003000000142492354, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
�"�
_tf_keras_input_layer�{"class_name": "InputLayer", "name": "input_1", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": [null, 32, 32, 3], "config": {"batch_input_shape": [null, 32, 32, 3], "dtype": "float32", "sparse": false, "ragged": false, "name": "input_1"}}
�

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
t__call__
*u&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Conv2D", "name": "conv2d", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "conv2d", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 3}}}}
�
w
b
	variables
regularization_losses
trainable_variables
	keras_api
v__call__
*w&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "TopPool", "name": "top_pool", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "top_pool", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}
�

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
x__call__
*y&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Conv2D", "name": "conv2d_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "conv2d_1", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 8}}}}
�
 	variables
!regularization_losses
"trainable_variables
#	keras_api
z__call__
*{&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Flatten", "name": "flatten", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 1, "axes": {}}}}
�

$kernel
%bias
&	variables
'regularization_losses
(trainable_variables
)	keras_api
|__call__
*}&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "dense", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 1024}}}}
�

*kernel
+bias
,	variables
-regularization_losses
.trainable_variables
/	keras_api
~__call__
*&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "dense_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_1", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}}
�
0iter

1beta_1

2beta_2
	3decay
4learning_ratem]m^m_m`mamb$mc%md*me+mfvgvhvivjvkvl$vm%vn*vo+vp"
	optimizer
f
0
1
2
3
4
5
$6
%7
*8
+9"
trackable_list_wrapper
 "
trackable_list_wrapper
f
0
1
2
3
4
5
$6
%7
*8
+9"
trackable_list_wrapper
�
5non_trainable_variables
		variables
6metrics

7layers

regularization_losses
trainable_variables
8layer_regularization_losses
r__call__
q_default_save_signature
*s&call_and_return_all_conditional_losses
&s"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
':%2conv2d/kernel
:2conv2d/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
9non_trainable_variables
	variables
:metrics

;layers
regularization_losses
trainable_variables
<layer_regularization_losses
t__call__
*u&call_and_return_all_conditional_losses
&u"call_and_return_conditional_losses"
_generic_user_object
:2
top_pool/w
$:"2
top_pool/b
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
=non_trainable_variables
	variables
>metrics

?layers
regularization_losses
trainable_variables
@layer_regularization_losses
v__call__
*w&call_and_return_all_conditional_losses
&w"call_and_return_conditional_losses"
_generic_user_object
):'2conv2d_1/kernel
:2conv2d_1/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
Anon_trainable_variables
	variables
Bmetrics

Clayers
regularization_losses
trainable_variables
Dlayer_regularization_losses
x__call__
*y&call_and_return_all_conditional_losses
&y"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Enon_trainable_variables
 	variables
Fmetrics

Glayers
!regularization_losses
"trainable_variables
Hlayer_regularization_losses
z__call__
*{&call_and_return_all_conditional_losses
&{"call_and_return_conditional_losses"
_generic_user_object
 :
��2dense/kernel
:�2
dense/bias
.
$0
%1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
$0
%1"
trackable_list_wrapper
�
Inon_trainable_variables
&	variables
Jmetrics

Klayers
'regularization_losses
(trainable_variables
Llayer_regularization_losses
|__call__
*}&call_and_return_all_conditional_losses
&}"call_and_return_conditional_losses"
_generic_user_object
!:	�
2dense_1/kernel
:
2dense_1/bias
.
*0
+1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
*0
+1"
trackable_list_wrapper
�
Mnon_trainable_variables
,	variables
Nmetrics

Olayers
-regularization_losses
.trainable_variables
Player_regularization_losses
~__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
'
Q0"
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
	Rtotal
	Scount
T
_fn_kwargs
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "MeanMetricWrapper", "name": "accuracy", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "accuracy", "dtype": "float32"}}
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Ynon_trainable_variables
U	variables
Zmetrics

[layers
Vregularization_losses
Wtrainable_variables
\layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
,:*2Adam/conv2d/kernel/m
:2Adam/conv2d/bias/m
!:2Adam/top_pool/w/m
):'2Adam/top_pool/b/m
.:,2Adam/conv2d_1/kernel/m
 :2Adam/conv2d_1/bias/m
%:#
��2Adam/dense/kernel/m
:�2Adam/dense/bias/m
&:$	�
2Adam/dense_1/kernel/m
:
2Adam/dense_1/bias/m
,:*2Adam/conv2d/kernel/v
:2Adam/conv2d/bias/v
!:2Adam/top_pool/w/v
):'2Adam/top_pool/b/v
.:,2Adam/conv2d_1/kernel/v
 :2Adam/conv2d_1/bias/v
%:#
��2Adam/dense/kernel/v
:�2Adam/dense/bias/v
&:$	�
2Adam/dense_1/kernel/v
:
2Adam/dense_1/bias/v
�2�
"__inference__wrapped_model_1734268�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *.�+
)�&
input_1���������  
�2�
,__inference_sequential_layer_call_fn_1734910
,__inference_sequential_layer_call_fn_1734558
,__inference_sequential_layer_call_fn_1734593
,__inference_sequential_layer_call_fn_1734925�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
G__inference_sequential_layer_call_and_return_conditional_losses_1734756
G__inference_sequential_layer_call_and_return_conditional_losses_1734522
G__inference_sequential_layer_call_and_return_conditional_losses_1734895
G__inference_sequential_layer_call_and_return_conditional_losses_1734502�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
(__inference_conv2d_layer_call_fn_1734288�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������
�2�
C__inference_conv2d_layer_call_and_return_conditional_losses_1734280�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������
�2�
*__inference_top_pool_layer_call_fn_1735044�
���
FullArgSpec
args�
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_top_pool_layer_call_and_return_conditional_losses_1735037�
���
FullArgSpec
args�
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_conv2d_1_layer_call_fn_1734309�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������
�2�
E__inference_conv2d_1_layer_call_and_return_conditional_losses_1734301�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *7�4
2�/+���������������������������
�2�
)__inference_flatten_layer_call_fn_1735055�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_flatten_layer_call_and_return_conditional_losses_1735050�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dense_layer_call_fn_1735072�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_dense_layer_call_and_return_conditional_losses_1735065�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_dense_1_layer_call_fn_1735089�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_dense_1_layer_call_and_return_conditional_losses_1735082�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
4B2
%__inference_signature_wrapper_1734617input_1
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkwjkwargs
defaults� 

kwonlyargs�

jtraining%
kwonlydefaults�

trainingp 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkwjkwargs
defaults� 

kwonlyargs�

jtraining%
kwonlydefaults�

trainingp 
annotations� *
 �
"__inference__wrapped_model_1734268y
$%*+8�5
.�+
)�&
input_1���������  
� "1�.
,
dense_1!�
dense_1���������
�
E__inference_conv2d_1_layer_call_and_return_conditional_losses_1734301�I�F
?�<
:�7
inputs+���������������������������
� "?�<
5�2
0+���������������������������
� �
*__inference_conv2d_1_layer_call_fn_1734309�I�F
?�<
:�7
inputs+���������������������������
� "2�/+����������������������������
C__inference_conv2d_layer_call_and_return_conditional_losses_1734280�I�F
?�<
:�7
inputs+���������������������������
� "?�<
5�2
0+���������������������������
� �
(__inference_conv2d_layer_call_fn_1734288�I�F
?�<
:�7
inputs+���������������������������
� "2�/+����������������������������
D__inference_dense_1_layer_call_and_return_conditional_losses_1735082]*+0�-
&�#
!�
inputs����������
� "%�"
�
0���������

� }
)__inference_dense_1_layer_call_fn_1735089P*+0�-
&�#
!�
inputs����������
� "����������
�
B__inference_dense_layer_call_and_return_conditional_losses_1735065^$%0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� |
'__inference_dense_layer_call_fn_1735072Q$%0�-
&�#
!�
inputs����������
� "������������
D__inference_flatten_layer_call_and_return_conditional_losses_1735050a7�4
-�*
(�%
inputs���������
� "&�#
�
0����������
� �
)__inference_flatten_layer_call_fn_1735055T7�4
-�*
(�%
inputs���������
� "������������
G__inference_sequential_layer_call_and_return_conditional_losses_1734502u
$%*+@�=
6�3
)�&
input_1���������  
p

 
� "%�"
�
0���������

� �
G__inference_sequential_layer_call_and_return_conditional_losses_1734522u
$%*+@�=
6�3
)�&
input_1���������  
p 

 
� "%�"
�
0���������

� �
G__inference_sequential_layer_call_and_return_conditional_losses_1734756t
$%*+?�<
5�2
(�%
inputs���������  
p

 
� "%�"
�
0���������

� �
G__inference_sequential_layer_call_and_return_conditional_losses_1734895t
$%*+?�<
5�2
(�%
inputs���������  
p 

 
� "%�"
�
0���������

� �
,__inference_sequential_layer_call_fn_1734558h
$%*+@�=
6�3
)�&
input_1���������  
p

 
� "����������
�
,__inference_sequential_layer_call_fn_1734593h
$%*+@�=
6�3
)�&
input_1���������  
p 

 
� "����������
�
,__inference_sequential_layer_call_fn_1734910g
$%*+?�<
5�2
(�%
inputs���������  
p

 
� "����������
�
,__inference_sequential_layer_call_fn_1734925g
$%*+?�<
5�2
(�%
inputs���������  
p 

 
� "����������
�
%__inference_signature_wrapper_1734617�
$%*+C�@
� 
9�6
4
input_1)�&
input_1���������  "1�.
,
dense_1!�
dense_1���������
�
E__inference_top_pool_layer_call_and_return_conditional_losses_1735037h3�0
)�&
$�!
In���������
� "-�*
#� 
0���������
� �
*__inference_top_pool_layer_call_fn_1735044[3�0
)�&
$�!
In���������
� " ����������