import tensorflow as tf
from tensorflow.compat.v1 import extract_image_patches
import numpy as np


def tens_pooling(image, num_channels, ksize, stride, ks, m, n, mx, my, weights,
 				 sorted = False, for_max = False):
	ext_patches_for_max,\
	ext_patches = extract_external_patches(image, num_channels, ksize, stride,
										   mx, my, for_max = for_max)
	int_patches = extract_internal_patches(ext_patches, num_channels, ks, padded = True)
	vectorized_patches = vectorize_internal_patches(int_patches, ks, num_channels)
	persistence = get_persistence(vectorized_patches, m, n)
	mx, my = get_shape(image, ksize, stride)
	vect_persistence = vectorize_persistence(image, mx, my, ksize, num_channels, persistence)
	if sorted:
		vect_persistence = tf.sort(vect_persistence, axis = 3)
	if not for_max:
		return weighted_output(vect_persistence, weights)
	else:
		max_pool = tf.math.reduce_max(ext_patches_for_max, axis = 3,
									  keepdims = True)
		max_pool = tf.cast(max_pool, tf.float32)
		vect_concat = tf.concat([vect_persistence, max_pool], axis = 3)
		return weighted_output(vect_concat, weights)

def tens_pooling_external_padding(image, num_channels, ksize, stride, ks, m, n,
 								  mx, my, weights, sorted = False, for_max = False):
	ext_patches_for_max,\
	ext_patches = extract_external_patches_with_padding(image, num_channels, ksize,
	 													ks, stride, mx, my, for_max = for_max)
	int_patches = extract_internal_patches(ext_patches, num_channels, ks)
	vectorized_patches = vectorize_internal_patches(int_patches, ks, num_channels)
	persistence = get_persistence(vectorized_patches, m, n)
	mx, my = get_shape(image, ksize, stride)
	vect_persistence = vectorize_persistence(image, mx, my, ksize, num_channels, persistence)
	if sorted:
		vect_persistence = tf.sort(vect_persistence, axis = 3)
	if not for_max:
		return weighted_output(vect_persistence, weights)
	else:
		max_pool = tf.math.reduce_max(ext_patches_for_max, axis = 3,
									  keepdims = True)
		max_pool = tf.cast(max_pool, tf.float32)
		vect_concat = tf.concat([vect_persistence, max_pool], axis = 3)
		return weighted_output(vect_concat, weights)


def weighted_pooling(image, num_channels, ksize, stride, mx, my, weights, sorted = 'FALSE'):
	ext_patches = extract_external_patches(image, num_channels, ksize, stride,
										   mx, my, weighted_pool = True)
	return weighted_output(ext_patches, weights)


def extract_external_patches(image, num_channels, ksize, stride, mx, my,
							 padding = 'VALID', for_max = False,
							 weighted_pool = False):
	patches = extract_image_patches(image, ksizes = [1,ksize,ksize,1],
								    strides = [1, stride, stride,1],
									rates = [1,1,1,1], padding = padding)
	image_shape = tf.shape(image)
	patches_shape = tf.shape(patches)
	shape = [image_shape[0] * mx * my, ksize, ksize,
	 		 num_channels]
	if not weighted_pool:
		if not for_max:
			return None, tf.reshape(patches,shape)
		else:
			shape_max = [image_shape[0], mx, my,
					 	 ksize**2, num_channels]
			return tf.reshape(patches, shape_max), tf.reshape(patches,shape)
	else:
		shape = [image_shape[0], mx, my, ksize**2,
				 num_channels]
		return tf.reshape(patches, shape)

def extract_external_patches_with_padding(image, num_channels, ksize, ks,
										  stride, mx, my, padding = 'VALID',
										  for_max = False, weighted_pool = False):
	image = tf.pad(image, [[0,0],[ks,ks],[ks,ks],[0,0]], "CONSTANT")
	patch_size = ksize + 2*ks
	patches = extract_image_patches(image, ksizes = [1, patch_size, patch_size,1],
								    strides = [1, stride, stride,1],
									rates = [1,1,1,1], padding = padding)
	image_shape = tf.shape(image)
	patches_shape = tf.shape(patches)
	shape = [image_shape[0] * mx * my, patch_size, patch_size, num_channels]
	if not weighted_pool:
		if not for_max:
			return None, tf.reshape(patches,shape)
		else:
			out_patch = tf.reshape(patches,shape)
			shape_max = [image_shape[0], mx, my,
					 	 ksize**2, num_channels]
			out_patch_max = tf.slice(out_patch, [0, ks, ks, 0],
			 						 [shape[0], ksize, ksize, num_channels])
			return tf.reshape(out_patch_max, shape_max), out_patch
	else:
		shape = [image_shape[0], mx, my, ksize**2,
				 num_channels]
		return tf.reshape(patches, shape)


def extract_internal_patches(image, num_channels, ks, padding = 'VALID',
 							 padded = False):
	if padded:
		input_image = tf.pad(image, [[0,0],[ks,ks],[ks,ks],[0,0]], "SYMMETRIC")
	else:
		input_image = image
	ksizes = [1, 2 * ks + 1, 2 * ks + 1, 1]
	strides = [1, 1, 1, 1]
	internal_patches = extract_image_patches(input_image, ksizes = ksizes,
											 strides = strides, rates = [1,1,1,1],
											 padding = padding)
	internal_patches_shape = tf.shape(internal_patches)
	shape = [tf.shape(input_image)[0], internal_patches_shape[1],
	 		 internal_patches_shape[2], 2*ks+1, 2*ks+1, num_channels]
	return tf.reshape(internal_patches, shape)


def vectorize_internal_patches(internal_patches, ks, num_channels):
	dims = tf.shape(internal_patches)
	out_dims = [dims[0], dims[1], dims[2], (2*ks+1)**2, num_channels]
	return tf.reshape(internal_patches, out_dims)


def get_persistence(internal_patches, m, n):
	sorted_patches = tf.sort(internal_patches, axis = 3)
	births = sorted_patches[:, :, :, m-1, :]
	deaths = sorted_patches[:, :, :, n-1, :]
	return deaths - births


def vectorize_persistence(image, mx, my, ksize, num_channels, persistence):
	dims = tf.shape(persistence)
	# out_dims = [tf.shape(image)[0], mx,my, dims[1]*dims[2], dims[3]]
	out_dims = [tf.shape(image)[0], mx,my, (ksize)**2, num_channels]
	return tf.reshape(persistence, out_dims)


def weighted_output(vect_persistence, weights):
	weights = tf.expand_dims(tf.expand_dims(tf.expand_dims(weights, 0), 1), 2)
	return tf.math.reduce_sum(vect_persistence * weights, axis = 3)


def get_shape(image, ksize, stride):
	_, h, w, _ = image.shape
	mx = ((h - ksize) / stride) + 1
	my = ((w - ksize) / stride) + 1
	return tf.cast(tf.floor(mx), tf.int32), tf.cast(tf.floor(my), tf.int32)
