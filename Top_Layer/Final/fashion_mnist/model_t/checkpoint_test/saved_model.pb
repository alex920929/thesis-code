��

��
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape�"serve*2.1.02v2.1.0-rc2-17-ge5bf8de8��	
t
top_pool_2/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_nametop_pool_2/w
m
 top_pool_2/w/Read/ReadVariableOpReadVariableOptop_pool_2/w*
_output_shapes

:*
dtype0
|
top_pool_2/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nametop_pool_2/b
u
 top_pool_2/b/Read/ReadVariableOpReadVariableOptop_pool_2/b*&
_output_shapes
:*
dtype0
|
dense_16/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��* 
shared_namedense_16/kernel
u
#dense_16/kernel/Read/ReadVariableOpReadVariableOpdense_16/kernel* 
_output_shapes
:
��*
dtype0
s
dense_16/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_namedense_16/bias
l
!dense_16/bias/Read/ReadVariableOpReadVariableOpdense_16/bias*
_output_shapes	
:�*
dtype0
{
dense_17/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
* 
shared_namedense_17/kernel
t
#dense_17/kernel/Read/ReadVariableOpReadVariableOpdense_17/kernel*
_output_shapes
:	�
*
dtype0
r
dense_17/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namedense_17/bias
k
!dense_17/bias/Read/ReadVariableOpReadVariableOpdense_17/bias*
_output_shapes
:
*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
�
Adam/top_pool_2/w/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/top_pool_2/w/m
{
'Adam/top_pool_2/w/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_2/w/m*
_output_shapes

:*
dtype0
�
Adam/top_pool_2/b/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/top_pool_2/b/m
�
'Adam/top_pool_2/b/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_2/b/m*&
_output_shapes
:*
dtype0
�
Adam/dense_16/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*'
shared_nameAdam/dense_16/kernel/m
�
*Adam/dense_16/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_16/kernel/m* 
_output_shapes
:
��*
dtype0
�
Adam/dense_16/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*%
shared_nameAdam/dense_16/bias/m
z
(Adam/dense_16/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_16/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/dense_17/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*'
shared_nameAdam/dense_17/kernel/m
�
*Adam/dense_17/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_17/kernel/m*
_output_shapes
:	�
*
dtype0
�
Adam/dense_17/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_17/bias/m
y
(Adam/dense_17/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_17/bias/m*
_output_shapes
:
*
dtype0
�
Adam/top_pool_2/w/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/top_pool_2/w/v
{
'Adam/top_pool_2/w/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_2/w/v*
_output_shapes

:*
dtype0
�
Adam/top_pool_2/b/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/top_pool_2/b/v
�
'Adam/top_pool_2/b/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_2/b/v*&
_output_shapes
:*
dtype0
�
Adam/dense_16/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*'
shared_nameAdam/dense_16/kernel/v
�
*Adam/dense_16/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_16/kernel/v* 
_output_shapes
:
��*
dtype0
�
Adam/dense_16/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*%
shared_nameAdam/dense_16/bias/v
z
(Adam/dense_16/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_16/bias/v*
_output_shapes	
:�*
dtype0
�
Adam/dense_17/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*'
shared_nameAdam/dense_17/kernel/v
�
*Adam/dense_17/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_17/kernel/v*
_output_shapes
:	�
*
dtype0
�
Adam/dense_17/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_17/bias/v
y
(Adam/dense_17/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_17/bias/v*
_output_shapes
:
*
dtype0

NoOpNoOp
�&
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�%
value�%B�% B�%
�
layer-0
layer_with_weights-0
layer-1
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
	optimizer
regularization_losses
trainable_variables
		variables

	keras_api

signatures
 
`
w
b
regularization_losses
trainable_variables
	variables
	keras_api
R
regularization_losses
trainable_variables
	variables
	keras_api
h

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
h

kernel
bias
regularization_losses
trainable_variables
 	variables
!	keras_api
�
"iter

#beta_1

$beta_2
	%decay
&learning_ratemGmHmImJmKmLvMvNvOvPvQvR
 
*
0
1
2
3
4
5
*
0
1
2
3
4
5
�
'metrics
regularization_losses
(non_trainable_variables

)layers
*layer_regularization_losses
trainable_variables
		variables
 
SQ
VARIABLE_VALUEtop_pool_2/w1layer_with_weights-0/w/.ATTRIBUTES/VARIABLE_VALUE
SQ
VARIABLE_VALUEtop_pool_2/b1layer_with_weights-0/b/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
�
+metrics
regularization_losses
,non_trainable_variables

-layers
.layer_regularization_losses
trainable_variables
	variables
 
 
 
�
/metrics
regularization_losses
0non_trainable_variables

1layers
2layer_regularization_losses
trainable_variables
	variables
[Y
VARIABLE_VALUEdense_16/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_16/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
�
3metrics
regularization_losses
4non_trainable_variables

5layers
6layer_regularization_losses
trainable_variables
	variables
[Y
VARIABLE_VALUEdense_17/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_17/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
�
7metrics
regularization_losses
8non_trainable_variables

9layers
:layer_regularization_losses
trainable_variables
 	variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

;0
 

0
1
2
3
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
x
	<total
	=count
>
_fn_kwargs
?regularization_losses
@trainable_variables
A	variables
B	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE
 
 
 

<0
=1
�
Cmetrics
?regularization_losses
Dnon_trainable_variables

Elayers
Flayer_regularization_losses
@trainable_variables
A	variables
 

<0
=1
 
 
vt
VARIABLE_VALUEAdam/top_pool_2/w/mMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_2/b/mMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_16/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_16/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_17/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_17/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_2/w/vMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_2/b/vMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_16/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_16/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_17/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_17/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
serving_default_input_9Placeholder*/
_output_shapes
:���������*
dtype0*$
shape:���������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_9top_pool_2/wtop_pool_2/bdense_16/kerneldense_16/biasdense_17/kerneldense_17/bias*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*.
f)R'
%__inference_signature_wrapper_1203673
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�	
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename top_pool_2/w/Read/ReadVariableOp top_pool_2/b/Read/ReadVariableOp#dense_16/kernel/Read/ReadVariableOp!dense_16/bias/Read/ReadVariableOp#dense_17/kernel/Read/ReadVariableOp!dense_17/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp'Adam/top_pool_2/w/m/Read/ReadVariableOp'Adam/top_pool_2/b/m/Read/ReadVariableOp*Adam/dense_16/kernel/m/Read/ReadVariableOp(Adam/dense_16/bias/m/Read/ReadVariableOp*Adam/dense_17/kernel/m/Read/ReadVariableOp(Adam/dense_17/bias/m/Read/ReadVariableOp'Adam/top_pool_2/w/v/Read/ReadVariableOp'Adam/top_pool_2/b/v/Read/ReadVariableOp*Adam/dense_16/kernel/v/Read/ReadVariableOp(Adam/dense_16/bias/v/Read/ReadVariableOp*Adam/dense_17/kernel/v/Read/ReadVariableOp(Adam/dense_17/bias/v/Read/ReadVariableOpConst*&
Tin
2	*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

CPU

GPU 2J 8*)
f$R"
 __inference__traced_save_1204210
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenametop_pool_2/wtop_pool_2/bdense_16/kerneldense_16/biasdense_17/kerneldense_17/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcountAdam/top_pool_2/w/mAdam/top_pool_2/b/mAdam/dense_16/kernel/mAdam/dense_16/bias/mAdam/dense_17/kernel/mAdam/dense_17/bias/mAdam/top_pool_2/w/vAdam/top_pool_2/b/vAdam/dense_16/kernel/vAdam/dense_16/bias/vAdam/dense_17/kernel/vAdam/dense_17/bias/v*%
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

CPU

GPU 2J 8*,
f'R%
#__inference__traced_restore_1204297��
�	
�
.__inference_sequential_8_layer_call_fn_1203947

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*R
fMRK
I__inference_sequential_8_layer_call_and_return_conditional_losses_12036442
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203619

inputs-
)top_pool_2_statefulpartitionedcall_args_1-
)top_pool_2_statefulpartitionedcall_args_2+
'dense_16_statefulpartitionedcall_args_1+
'dense_16_statefulpartitionedcall_args_2+
'dense_17_statefulpartitionedcall_args_1+
'dense_17_statefulpartitionedcall_args_2
identity�� dense_16/StatefulPartitionedCall� dense_17/StatefulPartitionedCall�"top_pool_2/StatefulPartitionedCall�
"top_pool_2/StatefulPartitionedCallStatefulPartitionedCallinputs)top_pool_2_statefulpartitionedcall_args_1)top_pool_2_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_top_pool_2_layer_call_and_return_conditional_losses_12035172$
"top_pool_2/StatefulPartitionedCall�
flatten_8/PartitionedCallPartitionedCall+top_pool_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_flatten_8_layer_call_and_return_conditional_losses_12035352
flatten_8/PartitionedCall�
 dense_16/StatefulPartitionedCallStatefulPartitionedCall"flatten_8/PartitionedCall:output:0'dense_16_statefulpartitionedcall_args_1'dense_16_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_16_layer_call_and_return_conditional_losses_12035532"
 dense_16/StatefulPartitionedCall�
 dense_17/StatefulPartitionedCallStatefulPartitionedCall)dense_16/StatefulPartitionedCall:output:0'dense_17_statefulpartitionedcall_args_1'dense_17_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12035752"
 dense_17/StatefulPartitionedCall�
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_16/StatefulPartitionedCall!^dense_17/StatefulPartitionedCall#^top_pool_2/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::2D
 dense_16/StatefulPartitionedCall dense_16/StatefulPartitionedCall2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2H
"top_pool_2/StatefulPartitionedCall"top_pool_2/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
,__inference_top_pool_2_layer_call_fn_1204066
in"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_top_pool_2_layer_call_and_return_conditional_losses_12035172
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������::22
StatefulPartitionedCallStatefulPartitionedCall:" 

_user_specified_nameIn
�w
�
G__inference_top_pool_2_layer_call_and_return_conditional_losses_1204059
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identity��ExpandDims/ReadVariableOp�add/ReadVariableOp�
ExtractImagePatchesExtractImagePatchesin*
T0*/
_output_shapes
:���������*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3�
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape�
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:���������2	
Reshape�
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddings�
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
	MirrorPad�
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:���������	*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5�
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shape�
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2�
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2�
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2�
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4�
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape�
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack�
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1�
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2�
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/Rank�
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transposition�
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
sort/transpose�
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
sort/TopKV2�
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
sort/transpose_1�

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2

sort/Neg_1�
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack�
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1�
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2�
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7�
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack�
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1�
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2�
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8�
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2�
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4�
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shape�
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
	Reshape_3�
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dim�

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dim�
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dim�
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
Sum�
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
add�
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
�
G
+__inference_flatten_8_layer_call_fn_1204077

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_flatten_8_layer_call_and_return_conditional_losses_12035352
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:& "
 
_user_specified_nameinputs
�
�
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203644

inputs-
)top_pool_2_statefulpartitionedcall_args_1-
)top_pool_2_statefulpartitionedcall_args_2+
'dense_16_statefulpartitionedcall_args_1+
'dense_16_statefulpartitionedcall_args_2+
'dense_17_statefulpartitionedcall_args_1+
'dense_17_statefulpartitionedcall_args_2
identity�� dense_16/StatefulPartitionedCall� dense_17/StatefulPartitionedCall�"top_pool_2/StatefulPartitionedCall�
"top_pool_2/StatefulPartitionedCallStatefulPartitionedCallinputs)top_pool_2_statefulpartitionedcall_args_1)top_pool_2_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_top_pool_2_layer_call_and_return_conditional_losses_12035172$
"top_pool_2/StatefulPartitionedCall�
flatten_8/PartitionedCallPartitionedCall+top_pool_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_flatten_8_layer_call_and_return_conditional_losses_12035352
flatten_8/PartitionedCall�
 dense_16/StatefulPartitionedCallStatefulPartitionedCall"flatten_8/PartitionedCall:output:0'dense_16_statefulpartitionedcall_args_1'dense_16_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_16_layer_call_and_return_conditional_losses_12035532"
 dense_16/StatefulPartitionedCall�
 dense_17/StatefulPartitionedCallStatefulPartitionedCall)dense_16/StatefulPartitionedCall:output:0'dense_17_statefulpartitionedcall_args_1'dense_17_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12035752"
 dense_17/StatefulPartitionedCall�
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_16/StatefulPartitionedCall!^dense_17/StatefulPartitionedCall#^top_pool_2/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::2D
 dense_16/StatefulPartitionedCall dense_16/StatefulPartitionedCall2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2H
"top_pool_2/StatefulPartitionedCall"top_pool_2/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
�w
�
G__inference_top_pool_2_layer_call_and_return_conditional_losses_1203517
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identity��ExpandDims/ReadVariableOp�add/ReadVariableOp�
ExtractImagePatchesExtractImagePatchesin*
T0*/
_output_shapes
:���������*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3�
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape�
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:���������2	
Reshape�
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddings�
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
	MirrorPad�
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:���������	*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2�
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5�
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shape�
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2�
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2�
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2�
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4�
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape�
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack�
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1�
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2�
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/Rank�
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transposition�
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
sort/transpose�
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
sort/TopKV2�
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
sort/transpose_1�

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2

sort/Neg_1�
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack�
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1�
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2�
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7�
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack�
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1�
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2�
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8�
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2�
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4�
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shape�
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
	Reshape_3�
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dim�

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dim�
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dim�
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
Sum�
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
add�
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
��
�
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203799

inputs1
-top_pool_2_expanddims_readvariableop_resource*
&top_pool_2_add_readvariableop_resource+
'dense_16_matmul_readvariableop_resource,
(dense_16_biasadd_readvariableop_resource+
'dense_17_matmul_readvariableop_resource,
(dense_17_biasadd_readvariableop_resource
identity��dense_16/BiasAdd/ReadVariableOp�dense_16/MatMul/ReadVariableOp�dense_17/BiasAdd/ReadVariableOp�dense_17/MatMul/ReadVariableOp�$top_pool_2/ExpandDims/ReadVariableOp�top_pool_2/add/ReadVariableOp�
top_pool_2/ExtractImagePatchesExtractImagePatchesinputs*
T0*/
_output_shapes
:���������*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool_2/ExtractImagePatchesZ
top_pool_2/ShapeShapeinputs*
T0*
_output_shapes
:2
top_pool_2/Shape�
top_pool_2/Shape_1Shape(top_pool_2/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_2/Shape_1�
top_pool_2/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool_2/strided_slice/stack�
 top_pool_2/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice/stack_1�
 top_pool_2/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice/stack_2�
top_pool_2/strided_sliceStridedSlicetop_pool_2/Shape:output:0'top_pool_2/strided_slice/stack:output:0)top_pool_2/strided_slice/stack_1:output:0)top_pool_2/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slicef
top_pool_2/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/mul/y�
top_pool_2/mulMul!top_pool_2/strided_slice:output:0top_pool_2/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_2/mulj
top_pool_2/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/mul_1/y}
top_pool_2/mul_1Multop_pool_2/mul:z:0top_pool_2/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_2/mul_1z
top_pool_2/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape/shape/1z
top_pool_2/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape/shape/2z
top_pool_2/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape/shape/3�
top_pool_2/Reshape/shapePacktop_pool_2/mul_1:z:0#top_pool_2/Reshape/shape/1:output:0#top_pool_2/Reshape/shape/2:output:0#top_pool_2/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape/shape�
top_pool_2/ReshapeReshape(top_pool_2/ExtractImagePatches:patches:0!top_pool_2/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2
top_pool_2/Reshape�
top_pool_2/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool_2/MirrorPad/paddings�
top_pool_2/MirrorPad	MirrorPadtop_pool_2/Reshape:output:0&top_pool_2/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
top_pool_2/MirrorPad�
 top_pool_2/ExtractImagePatches_1ExtractImagePatchestop_pool_2/MirrorPad:output:0*
T0*/
_output_shapes
:���������	*
ksizes
*
paddingVALID*
rates
*
strides
2"
 top_pool_2/ExtractImagePatches_1�
top_pool_2/Shape_2Shape*top_pool_2/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_2/Shape_2u
top_pool_2/Shape_3Shapetop_pool_2/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool_2/Shape_3�
 top_pool_2/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_2/strided_slice_1/stack�
"top_pool_2/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_1/stack_1�
"top_pool_2/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_1/stack_2�
top_pool_2/strided_slice_1StridedSlicetop_pool_2/Shape_3:output:0)top_pool_2/strided_slice_1/stack:output:0+top_pool_2/strided_slice_1/stack_1:output:0+top_pool_2/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_1�
 top_pool_2/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_2/stack�
"top_pool_2/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_2/stack_1�
"top_pool_2/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_2/stack_2�
top_pool_2/strided_slice_2StridedSlicetop_pool_2/Shape_2:output:0)top_pool_2/strided_slice_2/stack:output:0+top_pool_2/strided_slice_2/stack_1:output:0+top_pool_2/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_2�
 top_pool_2/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_3/stack�
"top_pool_2/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_3/stack_1�
"top_pool_2/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_3/stack_2�
top_pool_2/strided_slice_3StridedSlicetop_pool_2/Shape_2:output:0)top_pool_2/strided_slice_3/stack:output:0+top_pool_2/strided_slice_3/stack_1:output:0+top_pool_2/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_3~
top_pool_2/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_1/shape/3~
top_pool_2/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_1/shape/4~
top_pool_2/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_1/shape/5�
top_pool_2/Reshape_1/shapePack#top_pool_2/strided_slice_1:output:0#top_pool_2/strided_slice_2:output:0#top_pool_2/strided_slice_3:output:0%top_pool_2/Reshape_1/shape/3:output:0%top_pool_2/Reshape_1/shape/4:output:0%top_pool_2/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape_1/shape�
top_pool_2/Reshape_1Reshape*top_pool_2/ExtractImagePatches_1:patches:0#top_pool_2/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
top_pool_2/Reshape_1u
top_pool_2/Shape_4Shapetop_pool_2/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool_2/Shape_4�
 top_pool_2/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_2/strided_slice_4/stack�
"top_pool_2/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_4/stack_1�
"top_pool_2/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_4/stack_2�
top_pool_2/strided_slice_4StridedSlicetop_pool_2/Shape_4:output:0)top_pool_2/strided_slice_4/stack:output:0+top_pool_2/strided_slice_4/stack_1:output:0+top_pool_2/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_4�
 top_pool_2/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_5/stack�
"top_pool_2/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_5/stack_1�
"top_pool_2/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_5/stack_2�
top_pool_2/strided_slice_5StridedSlicetop_pool_2/Shape_4:output:0)top_pool_2/strided_slice_5/stack:output:0+top_pool_2/strided_slice_5/stack_1:output:0+top_pool_2/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_5�
 top_pool_2/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_6/stack�
"top_pool_2/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_6/stack_1�
"top_pool_2/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_6/stack_2�
top_pool_2/strided_slice_6StridedSlicetop_pool_2/Shape_4:output:0)top_pool_2/strided_slice_6/stack:output:0+top_pool_2/strided_slice_6/stack_1:output:0+top_pool_2/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_6~
top_pool_2/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool_2/Reshape_2/shape/3~
top_pool_2/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_2/shape/4�
top_pool_2/Reshape_2/shapePack#top_pool_2/strided_slice_4:output:0#top_pool_2/strided_slice_5:output:0#top_pool_2/strided_slice_6:output:0%top_pool_2/Reshape_2/shape/3:output:0%top_pool_2/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape_2/shape�
top_pool_2/Reshape_2Reshapetop_pool_2/Reshape_1:output:0#top_pool_2/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool_2/Reshape_2n
top_pool_2/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/sort/axis�
top_pool_2/sort/NegNegtop_pool_2/Reshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool_2/sort/Negu
top_pool_2/sort/ShapeShapetop_pool_2/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool_2/sort/Shape�
#top_pool_2/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool_2/sort/strided_slice/stack�
%top_pool_2/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_2/sort/strided_slice/stack_1�
%top_pool_2/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_2/sort/strided_slice/stack_2�
top_pool_2/sort/strided_sliceStridedSlicetop_pool_2/sort/Shape:output:0,top_pool_2/sort/strided_slice/stack:output:0.top_pool_2/sort/strided_slice/stack_1:output:0.top_pool_2/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/sort/strided_slicen
top_pool_2/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/sort/Rank�
top_pool_2/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool_2/sort/transposition�
top_pool_2/sort/transpose	Transposetop_pool_2/sort/Neg:y:0&top_pool_2/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
top_pool_2/sort/transpose�
top_pool_2/sort/TopKV2TopKV2top_pool_2/sort/transpose:y:0&top_pool_2/sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
top_pool_2/sort/TopKV2�
top_pool_2/sort/transpose_1	Transposetop_pool_2/sort/TopKV2:values:0&top_pool_2/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
top_pool_2/sort/transpose_1�
top_pool_2/sort/Neg_1Negtop_pool_2/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2
top_pool_2/sort/Neg_1�
 top_pool_2/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_2/strided_slice_7/stack�
"top_pool_2/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_2/strided_slice_7/stack_1�
"top_pool_2/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_2/strided_slice_7/stack_2�
top_pool_2/strided_slice_7StridedSlicetop_pool_2/sort/Neg_1:y:0)top_pool_2/strided_slice_7/stack:output:0+top_pool_2/strided_slice_7/stack_1:output:0+top_pool_2/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_2/strided_slice_7�
 top_pool_2/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_2/strided_slice_8/stack�
"top_pool_2/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_2/strided_slice_8/stack_1�
"top_pool_2/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_2/strided_slice_8/stack_2�
top_pool_2/strided_slice_8StridedSlicetop_pool_2/sort/Neg_1:y:0)top_pool_2/strided_slice_8/stack:output:0+top_pool_2/strided_slice_8/stack_1:output:0+top_pool_2/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_2/strided_slice_8�
top_pool_2/subSub#top_pool_2/strided_slice_8:output:0#top_pool_2/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
top_pool_2/subm
top_pool_2/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2
top_pool_2/Floor/xk
top_pool_2/FloorFloortop_pool_2/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_2/Floorp
top_pool_2/CastCasttop_pool_2/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_2/Castq
top_pool_2/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2
top_pool_2/Floor_1/xq
top_pool_2/Floor_1Floortop_pool_2/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_2/Floor_1v
top_pool_2/Cast_1Casttop_pool_2/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_2/Cast_1j
top_pool_2/Shape_5Shapetop_pool_2/sub:z:0*
T0*
_output_shapes
:2
top_pool_2/Shape_5^
top_pool_2/Shape_6Shapeinputs*
T0*
_output_shapes
:2
top_pool_2/Shape_6�
 top_pool_2/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_2/strided_slice_9/stack�
"top_pool_2/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_9/stack_1�
"top_pool_2/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_9/stack_2�
top_pool_2/strided_slice_9StridedSlicetop_pool_2/Shape_6:output:0)top_pool_2/strided_slice_9/stack:output:0+top_pool_2/strided_slice_9/stack_1:output:0+top_pool_2/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_9~
top_pool_2/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_3/shape/3~
top_pool_2/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_3/shape/4�
top_pool_2/Reshape_3/shapePack#top_pool_2/strided_slice_9:output:0top_pool_2/Cast:y:0top_pool_2/Cast_1:y:0%top_pool_2/Reshape_3/shape/3:output:0%top_pool_2/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape_3/shape�
top_pool_2/Reshape_3Reshapetop_pool_2/sub:z:0#top_pool_2/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
top_pool_2/Reshape_3�
$top_pool_2/ExpandDims/ReadVariableOpReadVariableOp-top_pool_2_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02&
$top_pool_2/ExpandDims/ReadVariableOpx
top_pool_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool_2/ExpandDims/dim�
top_pool_2/ExpandDims
ExpandDims,top_pool_2/ExpandDims/ReadVariableOp:value:0"top_pool_2/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool_2/ExpandDims|
top_pool_2/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/ExpandDims_1/dim�
top_pool_2/ExpandDims_1
ExpandDimstop_pool_2/ExpandDims:output:0$top_pool_2/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool_2/ExpandDims_1|
top_pool_2/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/ExpandDims_2/dim�
top_pool_2/ExpandDims_2
ExpandDims top_pool_2/ExpandDims_1:output:0$top_pool_2/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool_2/ExpandDims_2�
top_pool_2/mul_2Multop_pool_2/Reshape_3:output:0 top_pool_2/ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
top_pool_2/mul_2�
 top_pool_2/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_2/Sum/reduction_indices�
top_pool_2/SumSumtop_pool_2/mul_2:z:0)top_pool_2/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
top_pool_2/Sum�
top_pool_2/add/ReadVariableOpReadVariableOp&top_pool_2_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool_2/add/ReadVariableOp�
top_pool_2/addAddV2top_pool_2/Sum:output:0%top_pool_2/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
top_pool_2/adds
flatten_8/ConstConst*
_output_shapes
:*
dtype0*
valueB"����@  2
flatten_8/Const�
flatten_8/ReshapeReshapetop_pool_2/add:z:0flatten_8/Const:output:0*
T0*(
_output_shapes
:����������2
flatten_8/Reshape�
dense_16/MatMul/ReadVariableOpReadVariableOp'dense_16_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02 
dense_16/MatMul/ReadVariableOp�
dense_16/MatMulMatMulflatten_8/Reshape:output:0&dense_16/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_16/MatMul�
dense_16/BiasAdd/ReadVariableOpReadVariableOp(dense_16_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02!
dense_16/BiasAdd/ReadVariableOp�
dense_16/BiasAddBiasAdddense_16/MatMul:product:0'dense_16/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_16/BiasAdd�
dense_17/MatMul/ReadVariableOpReadVariableOp'dense_17_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02 
dense_17/MatMul/ReadVariableOp�
dense_17/MatMulMatMuldense_16/BiasAdd:output:0&dense_17/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_17/MatMul�
dense_17/BiasAdd/ReadVariableOpReadVariableOp(dense_17_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_17/BiasAdd/ReadVariableOp�
dense_17/BiasAddBiasAdddense_17/MatMul:product:0'dense_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_17/BiasAdd�
IdentityIdentitydense_17/BiasAdd:output:0 ^dense_16/BiasAdd/ReadVariableOp^dense_16/MatMul/ReadVariableOp ^dense_17/BiasAdd/ReadVariableOp^dense_17/MatMul/ReadVariableOp%^top_pool_2/ExpandDims/ReadVariableOp^top_pool_2/add/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::2B
dense_16/BiasAdd/ReadVariableOpdense_16/BiasAdd/ReadVariableOp2@
dense_16/MatMul/ReadVariableOpdense_16/MatMul/ReadVariableOp2B
dense_17/BiasAdd/ReadVariableOpdense_17/BiasAdd/ReadVariableOp2@
dense_17/MatMul/ReadVariableOpdense_17/MatMul/ReadVariableOp2L
$top_pool_2/ExpandDims/ReadVariableOp$top_pool_2/ExpandDims/ReadVariableOp2>
top_pool_2/add/ReadVariableOptop_pool_2/add/ReadVariableOp:& "
 
_user_specified_nameinputs
�
�
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203588
input_9-
)top_pool_2_statefulpartitionedcall_args_1-
)top_pool_2_statefulpartitionedcall_args_2+
'dense_16_statefulpartitionedcall_args_1+
'dense_16_statefulpartitionedcall_args_2+
'dense_17_statefulpartitionedcall_args_1+
'dense_17_statefulpartitionedcall_args_2
identity�� dense_16/StatefulPartitionedCall� dense_17/StatefulPartitionedCall�"top_pool_2/StatefulPartitionedCall�
"top_pool_2/StatefulPartitionedCallStatefulPartitionedCallinput_9)top_pool_2_statefulpartitionedcall_args_1)top_pool_2_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_top_pool_2_layer_call_and_return_conditional_losses_12035172$
"top_pool_2/StatefulPartitionedCall�
flatten_8/PartitionedCallPartitionedCall+top_pool_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_flatten_8_layer_call_and_return_conditional_losses_12035352
flatten_8/PartitionedCall�
 dense_16/StatefulPartitionedCallStatefulPartitionedCall"flatten_8/PartitionedCall:output:0'dense_16_statefulpartitionedcall_args_1'dense_16_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_16_layer_call_and_return_conditional_losses_12035532"
 dense_16/StatefulPartitionedCall�
 dense_17/StatefulPartitionedCallStatefulPartitionedCall)dense_16/StatefulPartitionedCall:output:0'dense_17_statefulpartitionedcall_args_1'dense_17_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12035752"
 dense_17/StatefulPartitionedCall�
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_16/StatefulPartitionedCall!^dense_17/StatefulPartitionedCall#^top_pool_2/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::2D
 dense_16/StatefulPartitionedCall dense_16/StatefulPartitionedCall2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2H
"top_pool_2/StatefulPartitionedCall"top_pool_2/StatefulPartitionedCall:' #
!
_user_specified_name	input_9
�
�
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203602
input_9-
)top_pool_2_statefulpartitionedcall_args_1-
)top_pool_2_statefulpartitionedcall_args_2+
'dense_16_statefulpartitionedcall_args_1+
'dense_16_statefulpartitionedcall_args_2+
'dense_17_statefulpartitionedcall_args_1+
'dense_17_statefulpartitionedcall_args_2
identity�� dense_16/StatefulPartitionedCall� dense_17/StatefulPartitionedCall�"top_pool_2/StatefulPartitionedCall�
"top_pool_2/StatefulPartitionedCallStatefulPartitionedCallinput_9)top_pool_2_statefulpartitionedcall_args_1)top_pool_2_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:���������**
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_top_pool_2_layer_call_and_return_conditional_losses_12035172$
"top_pool_2/StatefulPartitionedCall�
flatten_8/PartitionedCallPartitionedCall+top_pool_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_flatten_8_layer_call_and_return_conditional_losses_12035352
flatten_8/PartitionedCall�
 dense_16/StatefulPartitionedCallStatefulPartitionedCall"flatten_8/PartitionedCall:output:0'dense_16_statefulpartitionedcall_args_1'dense_16_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_16_layer_call_and_return_conditional_losses_12035532"
 dense_16/StatefulPartitionedCall�
 dense_17/StatefulPartitionedCallStatefulPartitionedCall)dense_16/StatefulPartitionedCall:output:0'dense_17_statefulpartitionedcall_args_1'dense_17_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12035752"
 dense_17/StatefulPartitionedCall�
IdentityIdentity)dense_17/StatefulPartitionedCall:output:0!^dense_16/StatefulPartitionedCall!^dense_17/StatefulPartitionedCall#^top_pool_2/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::2D
 dense_16/StatefulPartitionedCall dense_16/StatefulPartitionedCall2D
 dense_17/StatefulPartitionedCall dense_17/StatefulPartitionedCall2H
"top_pool_2/StatefulPartitionedCall"top_pool_2/StatefulPartitionedCall:' #
!
_user_specified_name	input_9
�
�
E__inference_dense_16_layer_call_and_return_conditional_losses_1203553

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
�
�
E__inference_dense_16_layer_call_and_return_conditional_losses_1204087

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
�
b
F__inference_flatten_8_layer_call_and_return_conditional_losses_1204072

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"����@  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:����������2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:& "
 
_user_specified_nameinputs
�
�
E__inference_dense_17_layer_call_and_return_conditional_losses_1204104

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
�
b
F__inference_flatten_8_layer_call_and_return_conditional_losses_1203535

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"����@  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:����������2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:& "
 
_user_specified_nameinputs
�
�
%__inference_signature_wrapper_1203673
input_9"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_9statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*+
f&R$
"__inference__wrapped_model_12034012
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_9
��
�
"__inference__wrapped_model_1203401
input_9>
:sequential_8_top_pool_2_expanddims_readvariableop_resource7
3sequential_8_top_pool_2_add_readvariableop_resource8
4sequential_8_dense_16_matmul_readvariableop_resource9
5sequential_8_dense_16_biasadd_readvariableop_resource8
4sequential_8_dense_17_matmul_readvariableop_resource9
5sequential_8_dense_17_biasadd_readvariableop_resource
identity��,sequential_8/dense_16/BiasAdd/ReadVariableOp�+sequential_8/dense_16/MatMul/ReadVariableOp�,sequential_8/dense_17/BiasAdd/ReadVariableOp�+sequential_8/dense_17/MatMul/ReadVariableOp�1sequential_8/top_pool_2/ExpandDims/ReadVariableOp�*sequential_8/top_pool_2/add/ReadVariableOp�
+sequential_8/top_pool_2/ExtractImagePatchesExtractImagePatchesinput_9*
T0*/
_output_shapes
:���������*
ksizes
*
paddingVALID*
rates
*
strides
2-
+sequential_8/top_pool_2/ExtractImagePatchesu
sequential_8/top_pool_2/ShapeShapeinput_9*
T0*
_output_shapes
:2
sequential_8/top_pool_2/Shape�
sequential_8/top_pool_2/Shape_1Shape5sequential_8/top_pool_2/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2!
sequential_8/top_pool_2/Shape_1�
+sequential_8/top_pool_2/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2-
+sequential_8/top_pool_2/strided_slice/stack�
-sequential_8/top_pool_2/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_8/top_pool_2/strided_slice/stack_1�
-sequential_8/top_pool_2/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_8/top_pool_2/strided_slice/stack_2�
%sequential_8/top_pool_2/strided_sliceStridedSlice&sequential_8/top_pool_2/Shape:output:04sequential_8/top_pool_2/strided_slice/stack:output:06sequential_8/top_pool_2/strided_slice/stack_1:output:06sequential_8/top_pool_2/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2'
%sequential_8/top_pool_2/strided_slice�
sequential_8/top_pool_2/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
sequential_8/top_pool_2/mul/y�
sequential_8/top_pool_2/mulMul.sequential_8/top_pool_2/strided_slice:output:0&sequential_8/top_pool_2/mul/y:output:0*
T0*
_output_shapes
: 2
sequential_8/top_pool_2/mul�
sequential_8/top_pool_2/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2!
sequential_8/top_pool_2/mul_1/y�
sequential_8/top_pool_2/mul_1Mulsequential_8/top_pool_2/mul:z:0(sequential_8/top_pool_2/mul_1/y:output:0*
T0*
_output_shapes
: 2
sequential_8/top_pool_2/mul_1�
'sequential_8/top_pool_2/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2)
'sequential_8/top_pool_2/Reshape/shape/1�
'sequential_8/top_pool_2/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2)
'sequential_8/top_pool_2/Reshape/shape/2�
'sequential_8/top_pool_2/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2)
'sequential_8/top_pool_2/Reshape/shape/3�
%sequential_8/top_pool_2/Reshape/shapePack!sequential_8/top_pool_2/mul_1:z:00sequential_8/top_pool_2/Reshape/shape/1:output:00sequential_8/top_pool_2/Reshape/shape/2:output:00sequential_8/top_pool_2/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2'
%sequential_8/top_pool_2/Reshape/shape�
sequential_8/top_pool_2/ReshapeReshape5sequential_8/top_pool_2/ExtractImagePatches:patches:0.sequential_8/top_pool_2/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2!
sequential_8/top_pool_2/Reshape�
*sequential_8/top_pool_2/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2,
*sequential_8/top_pool_2/MirrorPad/paddings�
!sequential_8/top_pool_2/MirrorPad	MirrorPad(sequential_8/top_pool_2/Reshape:output:03sequential_8/top_pool_2/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2#
!sequential_8/top_pool_2/MirrorPad�
-sequential_8/top_pool_2/ExtractImagePatches_1ExtractImagePatches*sequential_8/top_pool_2/MirrorPad:output:0*
T0*/
_output_shapes
:���������	*
ksizes
*
paddingVALID*
rates
*
strides
2/
-sequential_8/top_pool_2/ExtractImagePatches_1�
sequential_8/top_pool_2/Shape_2Shape7sequential_8/top_pool_2/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2!
sequential_8/top_pool_2/Shape_2�
sequential_8/top_pool_2/Shape_3Shape*sequential_8/top_pool_2/MirrorPad:output:0*
T0*
_output_shapes
:2!
sequential_8/top_pool_2/Shape_3�
-sequential_8/top_pool_2/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-sequential_8/top_pool_2/strided_slice_1/stack�
/sequential_8/top_pool_2/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_1/stack_1�
/sequential_8/top_pool_2/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_1/stack_2�
'sequential_8/top_pool_2/strided_slice_1StridedSlice(sequential_8/top_pool_2/Shape_3:output:06sequential_8/top_pool_2/strided_slice_1/stack:output:08sequential_8/top_pool_2/strided_slice_1/stack_1:output:08sequential_8/top_pool_2/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_1�
-sequential_8/top_pool_2/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_8/top_pool_2/strided_slice_2/stack�
/sequential_8/top_pool_2/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_2/stack_1�
/sequential_8/top_pool_2/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_2/stack_2�
'sequential_8/top_pool_2/strided_slice_2StridedSlice(sequential_8/top_pool_2/Shape_2:output:06sequential_8/top_pool_2/strided_slice_2/stack:output:08sequential_8/top_pool_2/strided_slice_2/stack_1:output:08sequential_8/top_pool_2/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_2�
-sequential_8/top_pool_2/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_8/top_pool_2/strided_slice_3/stack�
/sequential_8/top_pool_2/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_3/stack_1�
/sequential_8/top_pool_2/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_3/stack_2�
'sequential_8/top_pool_2/strided_slice_3StridedSlice(sequential_8/top_pool_2/Shape_2:output:06sequential_8/top_pool_2/strided_slice_3/stack:output:08sequential_8/top_pool_2/strided_slice_3/stack_1:output:08sequential_8/top_pool_2/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_3�
)sequential_8/top_pool_2/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_8/top_pool_2/Reshape_1/shape/3�
)sequential_8/top_pool_2/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_8/top_pool_2/Reshape_1/shape/4�
)sequential_8/top_pool_2/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_8/top_pool_2/Reshape_1/shape/5�
'sequential_8/top_pool_2/Reshape_1/shapePack0sequential_8/top_pool_2/strided_slice_1:output:00sequential_8/top_pool_2/strided_slice_2:output:00sequential_8/top_pool_2/strided_slice_3:output:02sequential_8/top_pool_2/Reshape_1/shape/3:output:02sequential_8/top_pool_2/Reshape_1/shape/4:output:02sequential_8/top_pool_2/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2)
'sequential_8/top_pool_2/Reshape_1/shape�
!sequential_8/top_pool_2/Reshape_1Reshape7sequential_8/top_pool_2/ExtractImagePatches_1:patches:00sequential_8/top_pool_2/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2#
!sequential_8/top_pool_2/Reshape_1�
sequential_8/top_pool_2/Shape_4Shape*sequential_8/top_pool_2/Reshape_1:output:0*
T0*
_output_shapes
:2!
sequential_8/top_pool_2/Shape_4�
-sequential_8/top_pool_2/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-sequential_8/top_pool_2/strided_slice_4/stack�
/sequential_8/top_pool_2/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_4/stack_1�
/sequential_8/top_pool_2/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_4/stack_2�
'sequential_8/top_pool_2/strided_slice_4StridedSlice(sequential_8/top_pool_2/Shape_4:output:06sequential_8/top_pool_2/strided_slice_4/stack:output:08sequential_8/top_pool_2/strided_slice_4/stack_1:output:08sequential_8/top_pool_2/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_4�
-sequential_8/top_pool_2/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_8/top_pool_2/strided_slice_5/stack�
/sequential_8/top_pool_2/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_5/stack_1�
/sequential_8/top_pool_2/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_5/stack_2�
'sequential_8/top_pool_2/strided_slice_5StridedSlice(sequential_8/top_pool_2/Shape_4:output:06sequential_8/top_pool_2/strided_slice_5/stack:output:08sequential_8/top_pool_2/strided_slice_5/stack_1:output:08sequential_8/top_pool_2/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_5�
-sequential_8/top_pool_2/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_8/top_pool_2/strided_slice_6/stack�
/sequential_8/top_pool_2/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_6/stack_1�
/sequential_8/top_pool_2/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_6/stack_2�
'sequential_8/top_pool_2/strided_slice_6StridedSlice(sequential_8/top_pool_2/Shape_4:output:06sequential_8/top_pool_2/strided_slice_6/stack:output:08sequential_8/top_pool_2/strided_slice_6/stack_1:output:08sequential_8/top_pool_2/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_6�
)sequential_8/top_pool_2/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2+
)sequential_8/top_pool_2/Reshape_2/shape/3�
)sequential_8/top_pool_2/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_8/top_pool_2/Reshape_2/shape/4�
'sequential_8/top_pool_2/Reshape_2/shapePack0sequential_8/top_pool_2/strided_slice_4:output:00sequential_8/top_pool_2/strided_slice_5:output:00sequential_8/top_pool_2/strided_slice_6:output:02sequential_8/top_pool_2/Reshape_2/shape/3:output:02sequential_8/top_pool_2/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2)
'sequential_8/top_pool_2/Reshape_2/shape�
!sequential_8/top_pool_2/Reshape_2Reshape*sequential_8/top_pool_2/Reshape_1:output:00sequential_8/top_pool_2/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2#
!sequential_8/top_pool_2/Reshape_2�
!sequential_8/top_pool_2/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2#
!sequential_8/top_pool_2/sort/axis�
 sequential_8/top_pool_2/sort/NegNeg*sequential_8/top_pool_2/Reshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2"
 sequential_8/top_pool_2/sort/Neg�
"sequential_8/top_pool_2/sort/ShapeShape$sequential_8/top_pool_2/sort/Neg:y:0*
T0*
_output_shapes
:2$
"sequential_8/top_pool_2/sort/Shape�
0sequential_8/top_pool_2/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:22
0sequential_8/top_pool_2/sort/strided_slice/stack�
2sequential_8/top_pool_2/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:24
2sequential_8/top_pool_2/sort/strided_slice/stack_1�
2sequential_8/top_pool_2/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:24
2sequential_8/top_pool_2/sort/strided_slice/stack_2�
*sequential_8/top_pool_2/sort/strided_sliceStridedSlice+sequential_8/top_pool_2/sort/Shape:output:09sequential_8/top_pool_2/sort/strided_slice/stack:output:0;sequential_8/top_pool_2/sort/strided_slice/stack_1:output:0;sequential_8/top_pool_2/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2,
*sequential_8/top_pool_2/sort/strided_slice�
!sequential_8/top_pool_2/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2#
!sequential_8/top_pool_2/sort/Rank�
*sequential_8/top_pool_2/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2,
*sequential_8/top_pool_2/sort/transposition�
&sequential_8/top_pool_2/sort/transpose	Transpose$sequential_8/top_pool_2/sort/Neg:y:03sequential_8/top_pool_2/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2(
&sequential_8/top_pool_2/sort/transpose�
#sequential_8/top_pool_2/sort/TopKV2TopKV2*sequential_8/top_pool_2/sort/transpose:y:03sequential_8/top_pool_2/sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2%
#sequential_8/top_pool_2/sort/TopKV2�
(sequential_8/top_pool_2/sort/transpose_1	Transpose,sequential_8/top_pool_2/sort/TopKV2:values:03sequential_8/top_pool_2/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2*
(sequential_8/top_pool_2/sort/transpose_1�
"sequential_8/top_pool_2/sort/Neg_1Neg,sequential_8/top_pool_2/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2$
"sequential_8/top_pool_2/sort/Neg_1�
-sequential_8/top_pool_2/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2/
-sequential_8/top_pool_2/strided_slice_7/stack�
/sequential_8/top_pool_2/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   21
/sequential_8/top_pool_2/strided_slice_7/stack_1�
/sequential_8/top_pool_2/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               21
/sequential_8/top_pool_2/strided_slice_7/stack_2�
'sequential_8/top_pool_2/strided_slice_7StridedSlice&sequential_8/top_pool_2/sort/Neg_1:y:06sequential_8/top_pool_2/strided_slice_7/stack:output:08sequential_8/top_pool_2/strided_slice_7/stack_1:output:08sequential_8/top_pool_2/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_7�
-sequential_8/top_pool_2/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2/
-sequential_8/top_pool_2/strided_slice_8/stack�
/sequential_8/top_pool_2/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   21
/sequential_8/top_pool_2/strided_slice_8/stack_1�
/sequential_8/top_pool_2/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               21
/sequential_8/top_pool_2/strided_slice_8/stack_2�
'sequential_8/top_pool_2/strided_slice_8StridedSlice&sequential_8/top_pool_2/sort/Neg_1:y:06sequential_8/top_pool_2/strided_slice_8/stack:output:08sequential_8/top_pool_2/strided_slice_8/stack_1:output:08sequential_8/top_pool_2/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_8�
sequential_8/top_pool_2/subSub0sequential_8/top_pool_2/strided_slice_8:output:00sequential_8/top_pool_2/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
sequential_8/top_pool_2/sub�
sequential_8/top_pool_2/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2!
sequential_8/top_pool_2/Floor/x�
sequential_8/top_pool_2/FloorFloor(sequential_8/top_pool_2/Floor/x:output:0*
T0*
_output_shapes
: 2
sequential_8/top_pool_2/Floor�
sequential_8/top_pool_2/CastCast!sequential_8/top_pool_2/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
sequential_8/top_pool_2/Cast�
!sequential_8/top_pool_2/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2#
!sequential_8/top_pool_2/Floor_1/x�
sequential_8/top_pool_2/Floor_1Floor*sequential_8/top_pool_2/Floor_1/x:output:0*
T0*
_output_shapes
: 2!
sequential_8/top_pool_2/Floor_1�
sequential_8/top_pool_2/Cast_1Cast#sequential_8/top_pool_2/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2 
sequential_8/top_pool_2/Cast_1�
sequential_8/top_pool_2/Shape_5Shapesequential_8/top_pool_2/sub:z:0*
T0*
_output_shapes
:2!
sequential_8/top_pool_2/Shape_5y
sequential_8/top_pool_2/Shape_6Shapeinput_9*
T0*
_output_shapes
:2!
sequential_8/top_pool_2/Shape_6�
-sequential_8/top_pool_2/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-sequential_8/top_pool_2/strided_slice_9/stack�
/sequential_8/top_pool_2/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_9/stack_1�
/sequential_8/top_pool_2/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_8/top_pool_2/strided_slice_9/stack_2�
'sequential_8/top_pool_2/strided_slice_9StridedSlice(sequential_8/top_pool_2/Shape_6:output:06sequential_8/top_pool_2/strided_slice_9/stack:output:08sequential_8/top_pool_2/strided_slice_9/stack_1:output:08sequential_8/top_pool_2/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_8/top_pool_2/strided_slice_9�
)sequential_8/top_pool_2/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_8/top_pool_2/Reshape_3/shape/3�
)sequential_8/top_pool_2/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_8/top_pool_2/Reshape_3/shape/4�
'sequential_8/top_pool_2/Reshape_3/shapePack0sequential_8/top_pool_2/strided_slice_9:output:0 sequential_8/top_pool_2/Cast:y:0"sequential_8/top_pool_2/Cast_1:y:02sequential_8/top_pool_2/Reshape_3/shape/3:output:02sequential_8/top_pool_2/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2)
'sequential_8/top_pool_2/Reshape_3/shape�
!sequential_8/top_pool_2/Reshape_3Reshapesequential_8/top_pool_2/sub:z:00sequential_8/top_pool_2/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2#
!sequential_8/top_pool_2/Reshape_3�
1sequential_8/top_pool_2/ExpandDims/ReadVariableOpReadVariableOp:sequential_8_top_pool_2_expanddims_readvariableop_resource*
_output_shapes

:*
dtype023
1sequential_8/top_pool_2/ExpandDims/ReadVariableOp�
&sequential_8/top_pool_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2(
&sequential_8/top_pool_2/ExpandDims/dim�
"sequential_8/top_pool_2/ExpandDims
ExpandDims9sequential_8/top_pool_2/ExpandDims/ReadVariableOp:value:0/sequential_8/top_pool_2/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2$
"sequential_8/top_pool_2/ExpandDims�
(sequential_8/top_pool_2/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2*
(sequential_8/top_pool_2/ExpandDims_1/dim�
$sequential_8/top_pool_2/ExpandDims_1
ExpandDims+sequential_8/top_pool_2/ExpandDims:output:01sequential_8/top_pool_2/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2&
$sequential_8/top_pool_2/ExpandDims_1�
(sequential_8/top_pool_2/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2*
(sequential_8/top_pool_2/ExpandDims_2/dim�
$sequential_8/top_pool_2/ExpandDims_2
ExpandDims-sequential_8/top_pool_2/ExpandDims_1:output:01sequential_8/top_pool_2/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2&
$sequential_8/top_pool_2/ExpandDims_2�
sequential_8/top_pool_2/mul_2Mul*sequential_8/top_pool_2/Reshape_3:output:0-sequential_8/top_pool_2/ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
sequential_8/top_pool_2/mul_2�
-sequential_8/top_pool_2/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2/
-sequential_8/top_pool_2/Sum/reduction_indices�
sequential_8/top_pool_2/SumSum!sequential_8/top_pool_2/mul_2:z:06sequential_8/top_pool_2/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
sequential_8/top_pool_2/Sum�
*sequential_8/top_pool_2/add/ReadVariableOpReadVariableOp3sequential_8_top_pool_2_add_readvariableop_resource*&
_output_shapes
:*
dtype02,
*sequential_8/top_pool_2/add/ReadVariableOp�
sequential_8/top_pool_2/addAddV2$sequential_8/top_pool_2/Sum:output:02sequential_8/top_pool_2/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
sequential_8/top_pool_2/add�
sequential_8/flatten_8/ConstConst*
_output_shapes
:*
dtype0*
valueB"����@  2
sequential_8/flatten_8/Const�
sequential_8/flatten_8/ReshapeReshapesequential_8/top_pool_2/add:z:0%sequential_8/flatten_8/Const:output:0*
T0*(
_output_shapes
:����������2 
sequential_8/flatten_8/Reshape�
+sequential_8/dense_16/MatMul/ReadVariableOpReadVariableOp4sequential_8_dense_16_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02-
+sequential_8/dense_16/MatMul/ReadVariableOp�
sequential_8/dense_16/MatMulMatMul'sequential_8/flatten_8/Reshape:output:03sequential_8/dense_16/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
sequential_8/dense_16/MatMul�
,sequential_8/dense_16/BiasAdd/ReadVariableOpReadVariableOp5sequential_8_dense_16_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,sequential_8/dense_16/BiasAdd/ReadVariableOp�
sequential_8/dense_16/BiasAddBiasAdd&sequential_8/dense_16/MatMul:product:04sequential_8/dense_16/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
sequential_8/dense_16/BiasAdd�
+sequential_8/dense_17/MatMul/ReadVariableOpReadVariableOp4sequential_8_dense_17_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02-
+sequential_8/dense_17/MatMul/ReadVariableOp�
sequential_8/dense_17/MatMulMatMul&sequential_8/dense_16/BiasAdd:output:03sequential_8/dense_17/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
sequential_8/dense_17/MatMul�
,sequential_8/dense_17/BiasAdd/ReadVariableOpReadVariableOp5sequential_8_dense_17_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02.
,sequential_8/dense_17/BiasAdd/ReadVariableOp�
sequential_8/dense_17/BiasAddBiasAdd&sequential_8/dense_17/MatMul:product:04sequential_8/dense_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
sequential_8/dense_17/BiasAdd�
IdentityIdentity&sequential_8/dense_17/BiasAdd:output:0-^sequential_8/dense_16/BiasAdd/ReadVariableOp,^sequential_8/dense_16/MatMul/ReadVariableOp-^sequential_8/dense_17/BiasAdd/ReadVariableOp,^sequential_8/dense_17/MatMul/ReadVariableOp2^sequential_8/top_pool_2/ExpandDims/ReadVariableOp+^sequential_8/top_pool_2/add/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::2\
,sequential_8/dense_16/BiasAdd/ReadVariableOp,sequential_8/dense_16/BiasAdd/ReadVariableOp2Z
+sequential_8/dense_16/MatMul/ReadVariableOp+sequential_8/dense_16/MatMul/ReadVariableOp2\
,sequential_8/dense_17/BiasAdd/ReadVariableOp,sequential_8/dense_17/BiasAdd/ReadVariableOp2Z
+sequential_8/dense_17/MatMul/ReadVariableOp+sequential_8/dense_17/MatMul/ReadVariableOp2f
1sequential_8/top_pool_2/ExpandDims/ReadVariableOp1sequential_8/top_pool_2/ExpandDims/ReadVariableOp2X
*sequential_8/top_pool_2/add/ReadVariableOp*sequential_8/top_pool_2/add/ReadVariableOp:' #
!
_user_specified_name	input_9
�	
�
.__inference_sequential_8_layer_call_fn_1203653
input_9"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_9statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*R
fMRK
I__inference_sequential_8_layer_call_and_return_conditional_losses_12036442
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_9
�	
�
.__inference_sequential_8_layer_call_fn_1203628
input_9"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_9statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*R
fMRK
I__inference_sequential_8_layer_call_and_return_conditional_losses_12036192
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_9
�i
�
#__inference__traced_restore_1204297
file_prefix!
assignvariableop_top_pool_2_w#
assignvariableop_1_top_pool_2_b&
"assignvariableop_2_dense_16_kernel$
 assignvariableop_3_dense_16_bias&
"assignvariableop_4_dense_17_kernel$
 assignvariableop_5_dense_17_bias 
assignvariableop_6_adam_iter"
assignvariableop_7_adam_beta_1"
assignvariableop_8_adam_beta_2!
assignvariableop_9_adam_decay*
&assignvariableop_10_adam_learning_rate
assignvariableop_11_total
assignvariableop_12_count+
'assignvariableop_13_adam_top_pool_2_w_m+
'assignvariableop_14_adam_top_pool_2_b_m.
*assignvariableop_15_adam_dense_16_kernel_m,
(assignvariableop_16_adam_dense_16_bias_m.
*assignvariableop_17_adam_dense_17_kernel_m,
(assignvariableop_18_adam_dense_17_bias_m+
'assignvariableop_19_adam_top_pool_2_w_v+
'assignvariableop_20_adam_top_pool_2_b_v.
*assignvariableop_21_adam_dense_16_kernel_v,
(assignvariableop_22_adam_dense_16_bias_v.
*assignvariableop_23_adam_dense_17_kernel_v,
(assignvariableop_24_adam_dense_17_bias_v
identity_26��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�	RestoreV2�RestoreV2_1�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B1layer_with_weights-0/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-0/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*x
_output_shapesf
d:::::::::::::::::::::::::*'
dtypes
2	2
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOpassignvariableop_top_pool_2_wIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOpassignvariableop_1_top_pool_2_bIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp"assignvariableop_2_dense_16_kernelIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp assignvariableop_3_dense_16_biasIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp"assignvariableop_4_dense_17_kernelIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp assignvariableop_5_dense_17_biasIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0	*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_iterIdentity_6:output:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOpassignvariableop_7_adam_beta_1Identity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOpassignvariableop_8_adam_beta_2Identity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOpassignvariableop_9_adam_decayIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp&assignvariableop_10_adam_learning_rateIdentity_10:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpassignvariableop_11_totalIdentity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOpassignvariableop_12_countIdentity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp'assignvariableop_13_adam_top_pool_2_w_mIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp'assignvariableop_14_adam_top_pool_2_b_mIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp*assignvariableop_15_adam_dense_16_kernel_mIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp(assignvariableop_16_adam_dense_16_bias_mIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp*assignvariableop_17_adam_dense_17_kernel_mIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp(assignvariableop_18_adam_dense_17_bias_mIdentity_18:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp'assignvariableop_19_adam_top_pool_2_w_vIdentity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp'assignvariableop_20_adam_top_pool_2_b_vIdentity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp*assignvariableop_21_adam_dense_16_kernel_vIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21_
Identity_22IdentityRestoreV2:tensors:22*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp(assignvariableop_22_adam_dense_16_bias_vIdentity_22:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_22_
Identity_23IdentityRestoreV2:tensors:23*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp*assignvariableop_23_adam_dense_17_kernel_vIdentity_23:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_23_
Identity_24IdentityRestoreV2:tensors:24*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp(assignvariableop_24_adam_dense_17_bias_vIdentity_24:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_24�
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_names�
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slices�
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_25Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_25�
Identity_26IdentityIdentity_25:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_26"#
identity_26Identity_26:output:0*y
_input_shapesh
f: :::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:+ '
%
_user_specified_namefile_prefix
��
�
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203925

inputs1
-top_pool_2_expanddims_readvariableop_resource*
&top_pool_2_add_readvariableop_resource+
'dense_16_matmul_readvariableop_resource,
(dense_16_biasadd_readvariableop_resource+
'dense_17_matmul_readvariableop_resource,
(dense_17_biasadd_readvariableop_resource
identity��dense_16/BiasAdd/ReadVariableOp�dense_16/MatMul/ReadVariableOp�dense_17/BiasAdd/ReadVariableOp�dense_17/MatMul/ReadVariableOp�$top_pool_2/ExpandDims/ReadVariableOp�top_pool_2/add/ReadVariableOp�
top_pool_2/ExtractImagePatchesExtractImagePatchesinputs*
T0*/
_output_shapes
:���������*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool_2/ExtractImagePatchesZ
top_pool_2/ShapeShapeinputs*
T0*
_output_shapes
:2
top_pool_2/Shape�
top_pool_2/Shape_1Shape(top_pool_2/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_2/Shape_1�
top_pool_2/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool_2/strided_slice/stack�
 top_pool_2/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice/stack_1�
 top_pool_2/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice/stack_2�
top_pool_2/strided_sliceStridedSlicetop_pool_2/Shape:output:0'top_pool_2/strided_slice/stack:output:0)top_pool_2/strided_slice/stack_1:output:0)top_pool_2/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slicef
top_pool_2/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/mul/y�
top_pool_2/mulMul!top_pool_2/strided_slice:output:0top_pool_2/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_2/mulj
top_pool_2/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/mul_1/y}
top_pool_2/mul_1Multop_pool_2/mul:z:0top_pool_2/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_2/mul_1z
top_pool_2/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape/shape/1z
top_pool_2/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape/shape/2z
top_pool_2/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape/shape/3�
top_pool_2/Reshape/shapePacktop_pool_2/mul_1:z:0#top_pool_2/Reshape/shape/1:output:0#top_pool_2/Reshape/shape/2:output:0#top_pool_2/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape/shape�
top_pool_2/ReshapeReshape(top_pool_2/ExtractImagePatches:patches:0!top_pool_2/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2
top_pool_2/Reshape�
top_pool_2/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool_2/MirrorPad/paddings�
top_pool_2/MirrorPad	MirrorPadtop_pool_2/Reshape:output:0&top_pool_2/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:���������*
mode	SYMMETRIC2
top_pool_2/MirrorPad�
 top_pool_2/ExtractImagePatches_1ExtractImagePatchestop_pool_2/MirrorPad:output:0*
T0*/
_output_shapes
:���������	*
ksizes
*
paddingVALID*
rates
*
strides
2"
 top_pool_2/ExtractImagePatches_1�
top_pool_2/Shape_2Shape*top_pool_2/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_2/Shape_2u
top_pool_2/Shape_3Shapetop_pool_2/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool_2/Shape_3�
 top_pool_2/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_2/strided_slice_1/stack�
"top_pool_2/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_1/stack_1�
"top_pool_2/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_1/stack_2�
top_pool_2/strided_slice_1StridedSlicetop_pool_2/Shape_3:output:0)top_pool_2/strided_slice_1/stack:output:0+top_pool_2/strided_slice_1/stack_1:output:0+top_pool_2/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_1�
 top_pool_2/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_2/stack�
"top_pool_2/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_2/stack_1�
"top_pool_2/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_2/stack_2�
top_pool_2/strided_slice_2StridedSlicetop_pool_2/Shape_2:output:0)top_pool_2/strided_slice_2/stack:output:0+top_pool_2/strided_slice_2/stack_1:output:0+top_pool_2/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_2�
 top_pool_2/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_3/stack�
"top_pool_2/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_3/stack_1�
"top_pool_2/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_3/stack_2�
top_pool_2/strided_slice_3StridedSlicetop_pool_2/Shape_2:output:0)top_pool_2/strided_slice_3/stack:output:0+top_pool_2/strided_slice_3/stack_1:output:0+top_pool_2/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_3~
top_pool_2/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_1/shape/3~
top_pool_2/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_1/shape/4~
top_pool_2/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_1/shape/5�
top_pool_2/Reshape_1/shapePack#top_pool_2/strided_slice_1:output:0#top_pool_2/strided_slice_2:output:0#top_pool_2/strided_slice_3:output:0%top_pool_2/Reshape_1/shape/3:output:0%top_pool_2/Reshape_1/shape/4:output:0%top_pool_2/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape_1/shape�
top_pool_2/Reshape_1Reshape*top_pool_2/ExtractImagePatches_1:patches:0#top_pool_2/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3���������������������������2
top_pool_2/Reshape_1u
top_pool_2/Shape_4Shapetop_pool_2/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool_2/Shape_4�
 top_pool_2/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_2/strided_slice_4/stack�
"top_pool_2/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_4/stack_1�
"top_pool_2/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_4/stack_2�
top_pool_2/strided_slice_4StridedSlicetop_pool_2/Shape_4:output:0)top_pool_2/strided_slice_4/stack:output:0+top_pool_2/strided_slice_4/stack_1:output:0+top_pool_2/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_4�
 top_pool_2/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_5/stack�
"top_pool_2/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_5/stack_1�
"top_pool_2/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_5/stack_2�
top_pool_2/strided_slice_5StridedSlicetop_pool_2/Shape_4:output:0)top_pool_2/strided_slice_5/stack:output:0+top_pool_2/strided_slice_5/stack_1:output:0+top_pool_2/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_5�
 top_pool_2/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_2/strided_slice_6/stack�
"top_pool_2/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_6/stack_1�
"top_pool_2/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_6/stack_2�
top_pool_2/strided_slice_6StridedSlicetop_pool_2/Shape_4:output:0)top_pool_2/strided_slice_6/stack:output:0+top_pool_2/strided_slice_6/stack_1:output:0+top_pool_2/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_6~
top_pool_2/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool_2/Reshape_2/shape/3~
top_pool_2/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_2/shape/4�
top_pool_2/Reshape_2/shapePack#top_pool_2/strided_slice_4:output:0#top_pool_2/strided_slice_5:output:0#top_pool_2/strided_slice_6:output:0%top_pool_2/Reshape_2/shape/3:output:0%top_pool_2/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape_2/shape�
top_pool_2/Reshape_2Reshapetop_pool_2/Reshape_1:output:0#top_pool_2/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool_2/Reshape_2n
top_pool_2/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/sort/axis�
top_pool_2/sort/NegNegtop_pool_2/Reshape_2:output:0*
T0*E
_output_shapes3
1:/���������������������������	2
top_pool_2/sort/Negu
top_pool_2/sort/ShapeShapetop_pool_2/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool_2/sort/Shape�
#top_pool_2/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool_2/sort/strided_slice/stack�
%top_pool_2/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_2/sort/strided_slice/stack_1�
%top_pool_2/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_2/sort/strided_slice/stack_2�
top_pool_2/sort/strided_sliceStridedSlicetop_pool_2/sort/Shape:output:0,top_pool_2/sort/strided_slice/stack:output:0.top_pool_2/sort/strided_slice/stack_1:output:0.top_pool_2/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/sort/strided_slicen
top_pool_2/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/sort/Rank�
top_pool_2/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool_2/sort/transposition�
top_pool_2/sort/transpose	Transposetop_pool_2/sort/Neg:y:0&top_pool_2/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/���������������������������	2
top_pool_2/sort/transpose�
top_pool_2/sort/TopKV2TopKV2top_pool_2/sort/transpose:y:0&top_pool_2/sort/strided_slice:output:0*
T0*�
_output_shapesv
t:8������������������������������������:8������������������������������������2
top_pool_2/sort/TopKV2�
top_pool_2/sort/transpose_1	Transposetop_pool_2/sort/TopKV2:values:0&top_pool_2/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8������������������������������������2
top_pool_2/sort/transpose_1�
top_pool_2/sort/Neg_1Negtop_pool_2/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8������������������������������������2
top_pool_2/sort/Neg_1�
 top_pool_2/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_2/strided_slice_7/stack�
"top_pool_2/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_2/strided_slice_7/stack_1�
"top_pool_2/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_2/strided_slice_7/stack_2�
top_pool_2/strided_slice_7StridedSlicetop_pool_2/sort/Neg_1:y:0)top_pool_2/strided_slice_7/stack:output:0+top_pool_2/strided_slice_7/stack_1:output:0+top_pool_2/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_2/strided_slice_7�
 top_pool_2/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_2/strided_slice_8/stack�
"top_pool_2/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_2/strided_slice_8/stack_1�
"top_pool_2/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_2/strided_slice_8/stack_2�
top_pool_2/strided_slice_8StridedSlicetop_pool_2/sort/Neg_1:y:0)top_pool_2/strided_slice_8/stack:output:0+top_pool_2/strided_slice_8/stack_1:output:0+top_pool_2/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+���������������������������*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_2/strided_slice_8�
top_pool_2/subSub#top_pool_2/strided_slice_8:output:0#top_pool_2/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+���������������������������2
top_pool_2/subm
top_pool_2/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2
top_pool_2/Floor/xk
top_pool_2/FloorFloortop_pool_2/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_2/Floorp
top_pool_2/CastCasttop_pool_2/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_2/Castq
top_pool_2/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �A2
top_pool_2/Floor_1/xq
top_pool_2/Floor_1Floortop_pool_2/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_2/Floor_1v
top_pool_2/Cast_1Casttop_pool_2/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_2/Cast_1j
top_pool_2/Shape_5Shapetop_pool_2/sub:z:0*
T0*
_output_shapes
:2
top_pool_2/Shape_5^
top_pool_2/Shape_6Shapeinputs*
T0*
_output_shapes
:2
top_pool_2/Shape_6�
 top_pool_2/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_2/strided_slice_9/stack�
"top_pool_2/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_9/stack_1�
"top_pool_2/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_2/strided_slice_9/stack_2�
top_pool_2/strided_slice_9StridedSlicetop_pool_2/Shape_6:output:0)top_pool_2/strided_slice_9/stack:output:0+top_pool_2/strided_slice_9/stack_1:output:0+top_pool_2/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_2/strided_slice_9~
top_pool_2/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_3/shape/3~
top_pool_2/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/Reshape_3/shape/4�
top_pool_2/Reshape_3/shapePack#top_pool_2/strided_slice_9:output:0top_pool_2/Cast:y:0top_pool_2/Cast_1:y:0%top_pool_2/Reshape_3/shape/3:output:0%top_pool_2/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_2/Reshape_3/shape�
top_pool_2/Reshape_3Reshapetop_pool_2/sub:z:0#top_pool_2/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:���������2
top_pool_2/Reshape_3�
$top_pool_2/ExpandDims/ReadVariableOpReadVariableOp-top_pool_2_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02&
$top_pool_2/ExpandDims/ReadVariableOpx
top_pool_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool_2/ExpandDims/dim�
top_pool_2/ExpandDims
ExpandDims,top_pool_2/ExpandDims/ReadVariableOp:value:0"top_pool_2/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool_2/ExpandDims|
top_pool_2/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/ExpandDims_1/dim�
top_pool_2/ExpandDims_1
ExpandDimstop_pool_2/ExpandDims:output:0$top_pool_2/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool_2/ExpandDims_1|
top_pool_2/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_2/ExpandDims_2/dim�
top_pool_2/ExpandDims_2
ExpandDims top_pool_2/ExpandDims_1:output:0$top_pool_2/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool_2/ExpandDims_2�
top_pool_2/mul_2Multop_pool_2/Reshape_3:output:0 top_pool_2/ExpandDims_2:output:0*
T0*3
_output_shapes!
:���������2
top_pool_2/mul_2�
 top_pool_2/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_2/Sum/reduction_indices�
top_pool_2/SumSumtop_pool_2/mul_2:z:0)top_pool_2/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������2
top_pool_2/Sum�
top_pool_2/add/ReadVariableOpReadVariableOp&top_pool_2_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool_2/add/ReadVariableOp�
top_pool_2/addAddV2top_pool_2/Sum:output:0%top_pool_2/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������2
top_pool_2/adds
flatten_8/ConstConst*
_output_shapes
:*
dtype0*
valueB"����@  2
flatten_8/Const�
flatten_8/ReshapeReshapetop_pool_2/add:z:0flatten_8/Const:output:0*
T0*(
_output_shapes
:����������2
flatten_8/Reshape�
dense_16/MatMul/ReadVariableOpReadVariableOp'dense_16_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02 
dense_16/MatMul/ReadVariableOp�
dense_16/MatMulMatMulflatten_8/Reshape:output:0&dense_16/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_16/MatMul�
dense_16/BiasAdd/ReadVariableOpReadVariableOp(dense_16_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02!
dense_16/BiasAdd/ReadVariableOp�
dense_16/BiasAddBiasAdddense_16/MatMul:product:0'dense_16/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_16/BiasAdd�
dense_17/MatMul/ReadVariableOpReadVariableOp'dense_17_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02 
dense_17/MatMul/ReadVariableOp�
dense_17/MatMulMatMuldense_16/BiasAdd:output:0&dense_17/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_17/MatMul�
dense_17/BiasAdd/ReadVariableOpReadVariableOp(dense_17_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_17/BiasAdd/ReadVariableOp�
dense_17/BiasAddBiasAdddense_17/MatMul:product:0'dense_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_17/BiasAdd�
IdentityIdentitydense_17/BiasAdd:output:0 ^dense_16/BiasAdd/ReadVariableOp^dense_16/MatMul/ReadVariableOp ^dense_17/BiasAdd/ReadVariableOp^dense_17/MatMul/ReadVariableOp%^top_pool_2/ExpandDims/ReadVariableOp^top_pool_2/add/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::2B
dense_16/BiasAdd/ReadVariableOpdense_16/BiasAdd/ReadVariableOp2@
dense_16/MatMul/ReadVariableOpdense_16/MatMul/ReadVariableOp2B
dense_17/BiasAdd/ReadVariableOpdense_17/BiasAdd/ReadVariableOp2@
dense_17/MatMul/ReadVariableOpdense_17/MatMul/ReadVariableOp2L
$top_pool_2/ExpandDims/ReadVariableOp$top_pool_2/ExpandDims/ReadVariableOp2>
top_pool_2/add/ReadVariableOptop_pool_2/add/ReadVariableOp:& "
 
_user_specified_nameinputs
�	
�
.__inference_sequential_8_layer_call_fn_1203936

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6*
Tin
	2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*R
fMRK
I__inference_sequential_8_layer_call_and_return_conditional_losses_12036192
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*F
_input_shapes5
3:���������::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
*__inference_dense_16_layer_call_fn_1204094

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:����������**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_16_layer_call_and_return_conditional_losses_12035532
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
*__inference_dense_17_layer_call_fn_1204111

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:���������
**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_dense_17_layer_call_and_return_conditional_losses_12035752
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
�
�
E__inference_dense_17_layer_call_and_return_conditional_losses_1203575

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2	
BiasAdd�
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*/
_input_shapes
:����������::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
�8
�

 __inference__traced_save_1204210
file_prefix+
'savev2_top_pool_2_w_read_readvariableop+
'savev2_top_pool_2_b_read_readvariableop.
*savev2_dense_16_kernel_read_readvariableop,
(savev2_dense_16_bias_read_readvariableop.
*savev2_dense_17_kernel_read_readvariableop,
(savev2_dense_17_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop2
.savev2_adam_top_pool_2_w_m_read_readvariableop2
.savev2_adam_top_pool_2_b_m_read_readvariableop5
1savev2_adam_dense_16_kernel_m_read_readvariableop3
/savev2_adam_dense_16_bias_m_read_readvariableop5
1savev2_adam_dense_17_kernel_m_read_readvariableop3
/savev2_adam_dense_17_bias_m_read_readvariableop2
.savev2_adam_top_pool_2_w_v_read_readvariableop2
.savev2_adam_top_pool_2_b_v_read_readvariableop5
1savev2_adam_dense_16_kernel_v_read_readvariableop3
/savev2_adam_dense_16_bias_v_read_readvariableop5
1savev2_adam_dense_17_kernel_v_read_readvariableop3
/savev2_adam_dense_17_bias_v_read_readvariableop
savev2_1_const

identity_1��MergeV2Checkpoints�SaveV2�SaveV2_1�
StringJoin/inputs_1Const"/device:CPU:0*
_output_shapes
: *
dtype0*<
value3B1 B+_temp_aecca76c609d4d22a28ae515d1634d81/part2
StringJoin/inputs_1�

StringJoin
StringJoinfile_prefixStringJoin/inputs_1:output:0"/device:CPU:0*
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B1layer_with_weights-0/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-0/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-0/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�

SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0'savev2_top_pool_2_w_read_readvariableop'savev2_top_pool_2_b_read_readvariableop*savev2_dense_16_kernel_read_readvariableop(savev2_dense_16_bias_read_readvariableop*savev2_dense_17_kernel_read_readvariableop(savev2_dense_17_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop.savev2_adam_top_pool_2_w_m_read_readvariableop.savev2_adam_top_pool_2_b_m_read_readvariableop1savev2_adam_dense_16_kernel_m_read_readvariableop/savev2_adam_dense_16_bias_m_read_readvariableop1savev2_adam_dense_17_kernel_m_read_readvariableop/savev2_adam_dense_17_bias_m_read_readvariableop.savev2_adam_top_pool_2_w_v_read_readvariableop.savev2_adam_top_pool_2_b_v_read_readvariableop1savev2_adam_dense_16_kernel_v_read_readvariableop/savev2_adam_dense_16_bias_v_read_readvariableop1savev2_adam_dense_17_kernel_v_read_readvariableop/savev2_adam_dense_17_bias_v_read_readvariableop"/device:CPU:0*
_output_shapes
 *'
dtypes
2	2
SaveV2�
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shard�
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1�
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_names�
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slices�
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity�

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :::
��:�:	�
:
: : : : : : : :::
��:�:	�
:
:::
��:�:	�
:
: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:+ '
%
_user_specified_namefile_prefix"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
C
input_98
serving_default_input_9:0���������<
dense_170
StatefulPartitionedCall:0���������
tensorflow/serving/predict:�
�
layer-0
layer_with_weights-0
layer-1
layer-2
layer_with_weights-1
layer-3
layer_with_weights-2
layer-4
	optimizer
regularization_losses
trainable_variables
		variables

	keras_api

signatures
S_default_save_signature
T__call__
*U&call_and_return_all_conditional_losses"�
_tf_keras_sequential�{"class_name": "Sequential", "name": "sequential_8", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "sequential_8", "layers": [{"class_name": "TopPool", "config": {"name": "top_pool_2", "trainable": true, "dtype": "float32", "num_channels": 1, "ksize": 5, "stride": 1, "ks": 1, "m": 2, "n": 7, "batch_input_shape": [null, 28, 28, 1]}}, {"class_name": "Flatten", "config": {"name": "flatten_8", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_16", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_17", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}, "is_graph_network": true, "keras_version": "2.2.4-tf", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential_8", "layers": [{"class_name": "TopPool", "config": {"name": "top_pool_2", "trainable": true, "dtype": "float32", "num_channels": 1, "ksize": 5, "stride": 1, "ks": 1, "m": 2, "n": 7, "batch_input_shape": [null, 28, 28, 1]}}, {"class_name": "Flatten", "config": {"name": "flatten_8", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_16", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_17", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}}, "training_config": {"loss": {"class_name": "SparseCategoricalCrossentropy", "config": {"reduction": "auto", "name": "sparse_categorical_crossentropy", "from_logits": true}}, "metrics": ["accuracy"], "weighted_metrics": null, "sample_weight_mode": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0003000000142492354, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
�"�
_tf_keras_input_layer�{"class_name": "InputLayer", "name": "input_9", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": [null, 28, 28, 1], "config": {"batch_input_shape": [null, 28, 28, 1], "dtype": "float32", "sparse": false, "ragged": false, "name": "input_9"}}
�
w
b
regularization_losses
trainable_variables
	variables
	keras_api
V__call__
*W&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "TopPool", "name": "top_pool_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "top_pool_2", "trainable": true, "dtype": "float32", "num_channels": 1, "ksize": 5, "stride": 1, "ks": 1, "m": 2, "n": 7}}
�
regularization_losses
trainable_variables
	variables
	keras_api
X__call__
*Y&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Flatten", "name": "flatten_8", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "flatten_8", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 1, "axes": {}}}}
�

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
Z__call__
*[&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "dense_16", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_16", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 576}}}}
�

kernel
bias
regularization_losses
trainable_variables
 	variables
!	keras_api
\__call__
*]&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "dense_17", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_17", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}}
�
"iter

#beta_1

$beta_2
	%decay
&learning_ratemGmHmImJmKmLvMvNvOvPvQvR"
	optimizer
 "
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
�
'metrics
regularization_losses
(non_trainable_variables

)layers
*layer_regularization_losses
trainable_variables
		variables
T__call__
S_default_save_signature
*U&call_and_return_all_conditional_losses
&U"call_and_return_conditional_losses"
_generic_user_object
,
^serving_default"
signature_map
:2top_pool_2/w
&:$2top_pool_2/b
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
+metrics
regularization_losses
,non_trainable_variables

-layers
.layer_regularization_losses
trainable_variables
	variables
V__call__
*W&call_and_return_all_conditional_losses
&W"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
/metrics
regularization_losses
0non_trainable_variables

1layers
2layer_regularization_losses
trainable_variables
	variables
X__call__
*Y&call_and_return_all_conditional_losses
&Y"call_and_return_conditional_losses"
_generic_user_object
#:!
��2dense_16/kernel
:�2dense_16/bias
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
3metrics
regularization_losses
4non_trainable_variables

5layers
6layer_regularization_losses
trainable_variables
	variables
Z__call__
*[&call_and_return_all_conditional_losses
&["call_and_return_conditional_losses"
_generic_user_object
": 	�
2dense_17/kernel
:
2dense_17/bias
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
7metrics
regularization_losses
8non_trainable_variables

9layers
:layer_regularization_losses
trainable_variables
 	variables
\__call__
*]&call_and_return_all_conditional_losses
&]"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
'
;0"
trackable_list_wrapper
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
	<total
	=count
>
_fn_kwargs
?regularization_losses
@trainable_variables
A	variables
B	keras_api
___call__
*`&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "MeanMetricWrapper", "name": "accuracy", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "accuracy", "dtype": "float32"}}
:  (2total
:  (2count
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
<0
=1"
trackable_list_wrapper
�
Cmetrics
?regularization_losses
Dnon_trainable_variables

Elayers
Flayer_regularization_losses
@trainable_variables
A	variables
___call__
*`&call_and_return_all_conditional_losses
&`"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
.
<0
=1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
#:!2Adam/top_pool_2/w/m
+:)2Adam/top_pool_2/b/m
(:&
��2Adam/dense_16/kernel/m
!:�2Adam/dense_16/bias/m
':%	�
2Adam/dense_17/kernel/m
 :
2Adam/dense_17/bias/m
#:!2Adam/top_pool_2/w/v
+:)2Adam/top_pool_2/b/v
(:&
��2Adam/dense_16/kernel/v
!:�2Adam/dense_16/bias/v
':%	�
2Adam/dense_17/kernel/v
 :
2Adam/dense_17/bias/v
�2�
"__inference__wrapped_model_1203401�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *.�+
)�&
input_9���������
�2�
.__inference_sequential_8_layer_call_fn_1203653
.__inference_sequential_8_layer_call_fn_1203628
.__inference_sequential_8_layer_call_fn_1203947
.__inference_sequential_8_layer_call_fn_1203936�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203799
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203602
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203925
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203588�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
,__inference_top_pool_2_layer_call_fn_1204066�
���
FullArgSpec
args�
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
G__inference_top_pool_2_layer_call_and_return_conditional_losses_1204059�
���
FullArgSpec
args�
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
+__inference_flatten_8_layer_call_fn_1204077�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
F__inference_flatten_8_layer_call_and_return_conditional_losses_1204072�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_dense_16_layer_call_fn_1204094�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_dense_16_layer_call_and_return_conditional_losses_1204087�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_dense_17_layer_call_fn_1204111�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_dense_17_layer_call_and_return_conditional_losses_1204104�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
4B2
%__inference_signature_wrapper_1203673input_9
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkwjkwargs
defaults� 

kwonlyargs�

jtraining%
kwonlydefaults�

trainingp 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkwjkwargs
defaults� 

kwonlyargs�

jtraining%
kwonlydefaults�

trainingp 
annotations� *
 �
"__inference__wrapped_model_1203401w8�5
.�+
)�&
input_9���������
� "3�0
.
dense_17"�
dense_17���������
�
E__inference_dense_16_layer_call_and_return_conditional_losses_1204087^0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� 
*__inference_dense_16_layer_call_fn_1204094Q0�-
&�#
!�
inputs����������
� "������������
E__inference_dense_17_layer_call_and_return_conditional_losses_1204104]0�-
&�#
!�
inputs����������
� "%�"
�
0���������

� ~
*__inference_dense_17_layer_call_fn_1204111P0�-
&�#
!�
inputs����������
� "����������
�
F__inference_flatten_8_layer_call_and_return_conditional_losses_1204072a7�4
-�*
(�%
inputs���������
� "&�#
�
0����������
� �
+__inference_flatten_8_layer_call_fn_1204077T7�4
-�*
(�%
inputs���������
� "������������
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203588q@�=
6�3
)�&
input_9���������
p

 
� "%�"
�
0���������

� �
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203602q@�=
6�3
)�&
input_9���������
p 

 
� "%�"
�
0���������

� �
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203799p?�<
5�2
(�%
inputs���������
p

 
� "%�"
�
0���������

� �
I__inference_sequential_8_layer_call_and_return_conditional_losses_1203925p?�<
5�2
(�%
inputs���������
p 

 
� "%�"
�
0���������

� �
.__inference_sequential_8_layer_call_fn_1203628d@�=
6�3
)�&
input_9���������
p

 
� "����������
�
.__inference_sequential_8_layer_call_fn_1203653d@�=
6�3
)�&
input_9���������
p 

 
� "����������
�
.__inference_sequential_8_layer_call_fn_1203936c?�<
5�2
(�%
inputs���������
p

 
� "����������
�
.__inference_sequential_8_layer_call_fn_1203947c?�<
5�2
(�%
inputs���������
p 

 
� "����������
�
%__inference_signature_wrapper_1203673�C�@
� 
9�6
4
input_9)�&
input_9���������"3�0
.
dense_17"�
dense_17���������
�
G__inference_top_pool_2_layer_call_and_return_conditional_losses_1204059h3�0
)�&
$�!
In���������
� "-�*
#� 
0���������
� �
,__inference_top_pool_2_layer_call_fn_1204066[3�0
)�&
$�!
In���������
� " ����������