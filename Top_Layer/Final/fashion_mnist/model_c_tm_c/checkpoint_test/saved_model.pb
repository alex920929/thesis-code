ха
е§
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeѕ
Й
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ѕ
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshapeѕ"serve*2.1.02v2.1.0-rc2-17-ge5bf8de8У╦
ё
conv2d_10/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nameconv2d_10/kernel
}
$conv2d_10/kernel/Read/ReadVariableOpReadVariableOpconv2d_10/kernel*&
_output_shapes
:*
dtype0
t
conv2d_10/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d_10/bias
m
"conv2d_10/bias/Read/ReadVariableOpReadVariableOpconv2d_10/bias*
_output_shapes
:*
dtype0
є
top_pool_with_max_1/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*&
shared_nametop_pool_with_max_1/w

)top_pool_with_max_1/w/Read/ReadVariableOpReadVariableOptop_pool_with_max_1/w*
_output_shapes

:*
dtype0
ј
top_pool_with_max_1/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nametop_pool_with_max_1/b
Є
)top_pool_with_max_1/b/Read/ReadVariableOpReadVariableOptop_pool_with_max_1/b*&
_output_shapes
:*
dtype0
ё
conv2d_11/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nameconv2d_11/kernel
}
$conv2d_11/kernel/Read/ReadVariableOpReadVariableOpconv2d_11/kernel*&
_output_shapes
:*
dtype0
t
conv2d_11/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d_11/bias
m
"conv2d_11/bias/Read/ReadVariableOpReadVariableOpconv2d_11/bias*
_output_shapes
:*
dtype0
|
dense_10/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
└ђ* 
shared_namedense_10/kernel
u
#dense_10/kernel/Read/ReadVariableOpReadVariableOpdense_10/kernel* 
_output_shapes
:
└ђ*
dtype0
s
dense_10/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*
shared_namedense_10/bias
l
!dense_10/bias/Read/ReadVariableOpReadVariableOpdense_10/bias*
_output_shapes	
:ђ*
dtype0
{
dense_11/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	ђ
* 
shared_namedense_11/kernel
t
#dense_11/kernel/Read/ReadVariableOpReadVariableOpdense_11/kernel*
_output_shapes
:	ђ
*
dtype0
r
dense_11/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namedense_11/bias
k
!dense_11/bias/Read/ReadVariableOpReadVariableOpdense_11/bias*
_output_shapes
:
*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
њ
Adam/conv2d_10/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/conv2d_10/kernel/m
І
+Adam/conv2d_10/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_10/kernel/m*&
_output_shapes
:*
dtype0
ѓ
Adam/conv2d_10/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/conv2d_10/bias/m
{
)Adam/conv2d_10/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_10/bias/m*
_output_shapes
:*
dtype0
ћ
Adam/top_pool_with_max_1/w/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*-
shared_nameAdam/top_pool_with_max_1/w/m
Ї
0Adam/top_pool_with_max_1/w/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_with_max_1/w/m*
_output_shapes

:*
dtype0
ю
Adam/top_pool_with_max_1/b/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_nameAdam/top_pool_with_max_1/b/m
Ћ
0Adam/top_pool_with_max_1/b/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_with_max_1/b/m*&
_output_shapes
:*
dtype0
њ
Adam/conv2d_11/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/conv2d_11/kernel/m
І
+Adam/conv2d_11/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_11/kernel/m*&
_output_shapes
:*
dtype0
ѓ
Adam/conv2d_11/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/conv2d_11/bias/m
{
)Adam/conv2d_11/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_11/bias/m*
_output_shapes
:*
dtype0
і
Adam/dense_10/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
└ђ*'
shared_nameAdam/dense_10/kernel/m
Ѓ
*Adam/dense_10/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_10/kernel/m* 
_output_shapes
:
└ђ*
dtype0
Ђ
Adam/dense_10/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*%
shared_nameAdam/dense_10/bias/m
z
(Adam/dense_10/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_10/bias/m*
_output_shapes	
:ђ*
dtype0
Ѕ
Adam/dense_11/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	ђ
*'
shared_nameAdam/dense_11/kernel/m
ѓ
*Adam/dense_11/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_11/kernel/m*
_output_shapes
:	ђ
*
dtype0
ђ
Adam/dense_11/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_11/bias/m
y
(Adam/dense_11/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_11/bias/m*
_output_shapes
:
*
dtype0
њ
Adam/conv2d_10/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/conv2d_10/kernel/v
І
+Adam/conv2d_10/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_10/kernel/v*&
_output_shapes
:*
dtype0
ѓ
Adam/conv2d_10/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/conv2d_10/bias/v
{
)Adam/conv2d_10/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_10/bias/v*
_output_shapes
:*
dtype0
ћ
Adam/top_pool_with_max_1/w/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*-
shared_nameAdam/top_pool_with_max_1/w/v
Ї
0Adam/top_pool_with_max_1/w/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_with_max_1/w/v*
_output_shapes

:*
dtype0
ю
Adam/top_pool_with_max_1/b/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*-
shared_nameAdam/top_pool_with_max_1/b/v
Ћ
0Adam/top_pool_with_max_1/b/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_with_max_1/b/v*&
_output_shapes
:*
dtype0
њ
Adam/conv2d_11/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/conv2d_11/kernel/v
І
+Adam/conv2d_11/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_11/kernel/v*&
_output_shapes
:*
dtype0
ѓ
Adam/conv2d_11/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/conv2d_11/bias/v
{
)Adam/conv2d_11/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_11/bias/v*
_output_shapes
:*
dtype0
і
Adam/dense_10/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
└ђ*'
shared_nameAdam/dense_10/kernel/v
Ѓ
*Adam/dense_10/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_10/kernel/v* 
_output_shapes
:
└ђ*
dtype0
Ђ
Adam/dense_10/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*%
shared_nameAdam/dense_10/bias/v
z
(Adam/dense_10/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_10/bias/v*
_output_shapes	
:ђ*
dtype0
Ѕ
Adam/dense_11/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	ђ
*'
shared_nameAdam/dense_11/kernel/v
ѓ
*Adam/dense_11/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_11/kernel/v*
_output_shapes
:	ђ
*
dtype0
ђ
Adam/dense_11/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_11/bias/v
y
(Adam/dense_11/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_11/bias/v*
_output_shapes
:
*
dtype0

NoOpNoOp
­7
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*Ф7
valueА7Bъ7 BЌ7
╬
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
	optimizer
	regularization_losses

trainable_variables
	variables
	keras_api

signatures
 
h

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
`
w
b
regularization_losses
trainable_variables
	variables
	keras_api
h

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
R
 regularization_losses
!trainable_variables
"	variables
#	keras_api
h

$kernel
%bias
&regularization_losses
'trainable_variables
(	variables
)	keras_api
h

*kernel
+bias
,regularization_losses
-trainable_variables
.	variables
/	keras_api
З
0iter

1beta_1

2beta_2
	3decay
4learning_ratem]m^m_m`mamb$mc%md*me+mfvgvhvivjvkvl$vm%vn*vo+vp
 
F
0
1
2
3
4
5
$6
%7
*8
+9
F
0
1
2
3
4
5
$6
%7
*8
+9
џ
5metrics
	regularization_losses
6non_trainable_variables

7layers
8layer_regularization_losses

trainable_variables
	variables
 
\Z
VARIABLE_VALUEconv2d_10/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEconv2d_10/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
џ
9metrics
regularization_losses
:non_trainable_variables

;layers
<layer_regularization_losses
trainable_variables
	variables
\Z
VARIABLE_VALUEtop_pool_with_max_1/w1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEtop_pool_with_max_1/b1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
џ
=metrics
regularization_losses
>non_trainable_variables

?layers
@layer_regularization_losses
trainable_variables
	variables
\Z
VARIABLE_VALUEconv2d_11/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEconv2d_11/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

0
1
џ
Ametrics
regularization_losses
Bnon_trainable_variables

Clayers
Dlayer_regularization_losses
trainable_variables
	variables
 
 
 
џ
Emetrics
 regularization_losses
Fnon_trainable_variables

Glayers
Hlayer_regularization_losses
!trainable_variables
"	variables
[Y
VARIABLE_VALUEdense_10/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_10/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE
 

$0
%1

$0
%1
џ
Imetrics
&regularization_losses
Jnon_trainable_variables

Klayers
Llayer_regularization_losses
'trainable_variables
(	variables
[Y
VARIABLE_VALUEdense_11/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_11/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE
 

*0
+1

*0
+1
џ
Mmetrics
,regularization_losses
Nnon_trainable_variables

Olayers
Player_regularization_losses
-trainable_variables
.	variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

Q0
 
*
0
1
2
3
4
5
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
x
	Rtotal
	Scount
T
_fn_kwargs
Uregularization_losses
Vtrainable_variables
W	variables
X	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE
 
 
 

R0
S1
џ
Ymetrics
Uregularization_losses
Znon_trainable_variables

[layers
\layer_regularization_losses
Vtrainable_variables
W	variables
 

R0
S1
 
 
}
VARIABLE_VALUEAdam/conv2d_10/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_10/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/top_pool_with_max_1/w/mMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/top_pool_with_max_1/b/mMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_11/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_11/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_10/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_10/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_11/kernel/mRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_11/bias/mPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_10/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_10/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/top_pool_with_max_1/w/vMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/top_pool_with_max_1/b/vMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_11/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_11/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_10/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_10/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_11/kernel/vRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_11/bias/vPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
і
serving_default_input_6Placeholder*/
_output_shapes
:         *
dtype0*$
shape:         
о
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_6conv2d_10/kernelconv2d_10/biastop_pool_with_max_1/wtop_pool_with_max_1/bconv2d_11/kernelconv2d_11/biasdense_10/kerneldense_10/biasdense_11/kerneldense_11/bias*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*-
f(R&
$__inference_signature_wrapper_507333
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
Ѓ
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename$conv2d_10/kernel/Read/ReadVariableOp"conv2d_10/bias/Read/ReadVariableOp)top_pool_with_max_1/w/Read/ReadVariableOp)top_pool_with_max_1/b/Read/ReadVariableOp$conv2d_11/kernel/Read/ReadVariableOp"conv2d_11/bias/Read/ReadVariableOp#dense_10/kernel/Read/ReadVariableOp!dense_10/bias/Read/ReadVariableOp#dense_11/kernel/Read/ReadVariableOp!dense_11/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp+Adam/conv2d_10/kernel/m/Read/ReadVariableOp)Adam/conv2d_10/bias/m/Read/ReadVariableOp0Adam/top_pool_with_max_1/w/m/Read/ReadVariableOp0Adam/top_pool_with_max_1/b/m/Read/ReadVariableOp+Adam/conv2d_11/kernel/m/Read/ReadVariableOp)Adam/conv2d_11/bias/m/Read/ReadVariableOp*Adam/dense_10/kernel/m/Read/ReadVariableOp(Adam/dense_10/bias/m/Read/ReadVariableOp*Adam/dense_11/kernel/m/Read/ReadVariableOp(Adam/dense_11/bias/m/Read/ReadVariableOp+Adam/conv2d_10/kernel/v/Read/ReadVariableOp)Adam/conv2d_10/bias/v/Read/ReadVariableOp0Adam/top_pool_with_max_1/w/v/Read/ReadVariableOp0Adam/top_pool_with_max_1/b/v/Read/ReadVariableOp+Adam/conv2d_11/kernel/v/Read/ReadVariableOp)Adam/conv2d_11/bias/v/Read/ReadVariableOp*Adam/dense_10/kernel/v/Read/ReadVariableOp(Adam/dense_10/bias/v/Read/ReadVariableOp*Adam/dense_11/kernel/v/Read/ReadVariableOp(Adam/dense_11/bias/v/Read/ReadVariableOpConst*2
Tin+
)2'	*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

CPU

GPU 2J 8*(
f#R!
__inference__traced_save_508000
џ
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameconv2d_10/kernelconv2d_10/biastop_pool_with_max_1/wtop_pool_with_max_1/bconv2d_11/kernelconv2d_11/biasdense_10/kerneldense_10/biasdense_11/kerneldense_11/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcountAdam/conv2d_10/kernel/mAdam/conv2d_10/bias/mAdam/top_pool_with_max_1/w/mAdam/top_pool_with_max_1/b/mAdam/conv2d_11/kernel/mAdam/conv2d_11/bias/mAdam/dense_10/kernel/mAdam/dense_10/bias/mAdam/dense_11/kernel/mAdam/dense_11/bias/mAdam/conv2d_10/kernel/vAdam/conv2d_10/bias/vAdam/top_pool_with_max_1/w/vAdam/top_pool_with_max_1/b/vAdam/conv2d_11/kernel/vAdam/conv2d_11/bias/vAdam/dense_10/kernel/vAdam/dense_10/bias/vAdam/dense_11/kernel/vAdam/dense_11/bias/v*1
Tin*
(2&*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

CPU

GPU 2J 8*+
f&R$
"__inference__traced_restore_508123░«
¤
л
-__inference_sequential_5_layer_call_fn_507274
input_6"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityѕбStatefulPartitionedCallћ
StatefulPartitionedCallStatefulPartitionedCallinput_6statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*Q
fLRJ
H__inference_sequential_5_layer_call_and_return_conditional_losses_5072612
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_6
і"
 
H__inference_sequential_5_layer_call_and_return_conditional_losses_507296

inputs,
(conv2d_10_statefulpartitionedcall_args_1,
(conv2d_10_statefulpartitionedcall_args_26
2top_pool_with_max_1_statefulpartitionedcall_args_16
2top_pool_with_max_1_statefulpartitionedcall_args_2,
(conv2d_11_statefulpartitionedcall_args_1,
(conv2d_11_statefulpartitionedcall_args_2+
'dense_10_statefulpartitionedcall_args_1+
'dense_10_statefulpartitionedcall_args_2+
'dense_11_statefulpartitionedcall_args_1+
'dense_11_statefulpartitionedcall_args_2
identityѕб!conv2d_10/StatefulPartitionedCallб!conv2d_11/StatefulPartitionedCallб dense_10/StatefulPartitionedCallб dense_11/StatefulPartitionedCallб+top_pool_with_max_1/StatefulPartitionedCallи
!conv2d_10/StatefulPartitionedCallStatefulPartitionedCallinputs(conv2d_10_statefulpartitionedcall_args_1(conv2d_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_10_layer_call_and_return_conditional_losses_5069762#
!conv2d_10/StatefulPartitionedCallЇ
+top_pool_with_max_1/StatefulPartitionedCallStatefulPartitionedCall*conv2d_10/StatefulPartitionedCall:output:02top_pool_with_max_1_statefulpartitionedcall_args_12top_pool_with_max_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         

**
config_proto

CPU

GPU 2J 8*X
fSRQ
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_5071442-
+top_pool_with_max_1/StatefulPartitionedCallт
!conv2d_11/StatefulPartitionedCallStatefulPartitionedCall4top_pool_with_max_1/StatefulPartitionedCall:output:0(conv2d_11_statefulpartitionedcall_args_1(conv2d_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_11_layer_call_and_return_conditional_losses_5069972#
!conv2d_11/StatefulPartitionedCallТ
flatten_5/PartitionedCallPartitionedCall*conv2d_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         └**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_flatten_5_layer_call_and_return_conditional_losses_5071652
flatten_5/PartitionedCallК
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_5/PartitionedCall:output:0'dense_10_statefulpartitionedcall_args_1'dense_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         ђ**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_5071832"
 dense_10/StatefulPartitionedCall═
 dense_11/StatefulPartitionedCallStatefulPartitionedCall)dense_10/StatefulPartitionedCall:output:0'dense_11_statefulpartitionedcall_args_1'dense_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_5072052"
 dense_11/StatefulPartitionedCall╣
IdentityIdentity)dense_11/StatefulPartitionedCall:output:0"^conv2d_10/StatefulPartitionedCall"^conv2d_11/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall,^top_pool_with_max_1/StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::2F
!conv2d_10/StatefulPartitionedCall!conv2d_10/StatefulPartitionedCall2F
!conv2d_11/StatefulPartitionedCall!conv2d_11/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall2Z
+top_pool_with_max_1/StatefulPartitionedCall+top_pool_with_max_1/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
в
П
D__inference_dense_11_layer_call_and_return_conditional_losses_507205

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpј
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	ђ
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2	
BiasAddЋ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
фЎ
Т
"__inference__traced_restore_508123
file_prefix%
!assignvariableop_conv2d_10_kernel%
!assignvariableop_1_conv2d_10_bias,
(assignvariableop_2_top_pool_with_max_1_w,
(assignvariableop_3_top_pool_with_max_1_b'
#assignvariableop_4_conv2d_11_kernel%
!assignvariableop_5_conv2d_11_bias&
"assignvariableop_6_dense_10_kernel$
 assignvariableop_7_dense_10_bias&
"assignvariableop_8_dense_11_kernel$
 assignvariableop_9_dense_11_bias!
assignvariableop_10_adam_iter#
assignvariableop_11_adam_beta_1#
assignvariableop_12_adam_beta_2"
assignvariableop_13_adam_decay*
&assignvariableop_14_adam_learning_rate
assignvariableop_15_total
assignvariableop_16_count/
+assignvariableop_17_adam_conv2d_10_kernel_m-
)assignvariableop_18_adam_conv2d_10_bias_m4
0assignvariableop_19_adam_top_pool_with_max_1_w_m4
0assignvariableop_20_adam_top_pool_with_max_1_b_m/
+assignvariableop_21_adam_conv2d_11_kernel_m-
)assignvariableop_22_adam_conv2d_11_bias_m.
*assignvariableop_23_adam_dense_10_kernel_m,
(assignvariableop_24_adam_dense_10_bias_m.
*assignvariableop_25_adam_dense_11_kernel_m,
(assignvariableop_26_adam_dense_11_bias_m/
+assignvariableop_27_adam_conv2d_10_kernel_v-
)assignvariableop_28_adam_conv2d_10_bias_v4
0assignvariableop_29_adam_top_pool_with_max_1_w_v4
0assignvariableop_30_adam_top_pool_with_max_1_b_v/
+assignvariableop_31_adam_conv2d_11_kernel_v-
)assignvariableop_32_adam_conv2d_11_bias_v.
*assignvariableop_33_adam_dense_10_kernel_v,
(assignvariableop_34_adam_dense_10_bias_v.
*assignvariableop_35_adam_dense_11_kernel_v,
(assignvariableop_36_adam_dense_11_bias_v
identity_38ѕбAssignVariableOpбAssignVariableOp_1бAssignVariableOp_10бAssignVariableOp_11бAssignVariableOp_12бAssignVariableOp_13бAssignVariableOp_14бAssignVariableOp_15бAssignVariableOp_16бAssignVariableOp_17бAssignVariableOp_18бAssignVariableOp_19бAssignVariableOp_2бAssignVariableOp_20бAssignVariableOp_21бAssignVariableOp_22бAssignVariableOp_23бAssignVariableOp_24бAssignVariableOp_25бAssignVariableOp_26бAssignVariableOp_27бAssignVariableOp_28бAssignVariableOp_29бAssignVariableOp_3бAssignVariableOp_30бAssignVariableOp_31бAssignVariableOp_32бAssignVariableOp_33бAssignVariableOp_34бAssignVariableOp_35бAssignVariableOp_36бAssignVariableOp_4бAssignVariableOp_5бAssignVariableOp_6бAssignVariableOp_7бAssignVariableOp_8бAssignVariableOp_9б	RestoreV2бRestoreV2_1С
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*­
valueТBс%B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_namesп
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*]
valueTBR%B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesу
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*ф
_output_shapesЌ
ћ:::::::::::::::::::::::::::::::::::::*3
dtypes)
'2%	2
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

IdentityЉ
AssignVariableOpAssignVariableOp!assignvariableop_conv2d_10_kernelIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1Ќ
AssignVariableOp_1AssignVariableOp!assignvariableop_1_conv2d_10_biasIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2ъ
AssignVariableOp_2AssignVariableOp(assignvariableop_2_top_pool_with_max_1_wIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3ъ
AssignVariableOp_3AssignVariableOp(assignvariableop_3_top_pool_with_max_1_bIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4Ў
AssignVariableOp_4AssignVariableOp#assignvariableop_4_conv2d_11_kernelIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5Ќ
AssignVariableOp_5AssignVariableOp!assignvariableop_5_conv2d_11_biasIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0*
_output_shapes
:2

Identity_6ў
AssignVariableOp_6AssignVariableOp"assignvariableop_6_dense_10_kernelIdentity_6:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7ќ
AssignVariableOp_7AssignVariableOp assignvariableop_7_dense_10_biasIdentity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8ў
AssignVariableOp_8AssignVariableOp"assignvariableop_8_dense_11_kernelIdentity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9ќ
AssignVariableOp_9AssignVariableOp assignvariableop_9_dense_11_biasIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0	*
_output_shapes
:2
Identity_10ќ
AssignVariableOp_10AssignVariableOpassignvariableop_10_adam_iterIdentity_10:output:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11ў
AssignVariableOp_11AssignVariableOpassignvariableop_11_adam_beta_1Identity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12ў
AssignVariableOp_12AssignVariableOpassignvariableop_12_adam_beta_2Identity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13Ќ
AssignVariableOp_13AssignVariableOpassignvariableop_13_adam_decayIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14Ъ
AssignVariableOp_14AssignVariableOp&assignvariableop_14_adam_learning_rateIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15њ
AssignVariableOp_15AssignVariableOpassignvariableop_15_totalIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16њ
AssignVariableOp_16AssignVariableOpassignvariableop_16_countIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17ц
AssignVariableOp_17AssignVariableOp+assignvariableop_17_adam_conv2d_10_kernel_mIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0*
_output_shapes
:2
Identity_18б
AssignVariableOp_18AssignVariableOp)assignvariableop_18_adam_conv2d_10_bias_mIdentity_18:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19Е
AssignVariableOp_19AssignVariableOp0assignvariableop_19_adam_top_pool_with_max_1_w_mIdentity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20Е
AssignVariableOp_20AssignVariableOp0assignvariableop_20_adam_top_pool_with_max_1_b_mIdentity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21ц
AssignVariableOp_21AssignVariableOp+assignvariableop_21_adam_conv2d_11_kernel_mIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21_
Identity_22IdentityRestoreV2:tensors:22*
T0*
_output_shapes
:2
Identity_22б
AssignVariableOp_22AssignVariableOp)assignvariableop_22_adam_conv2d_11_bias_mIdentity_22:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_22_
Identity_23IdentityRestoreV2:tensors:23*
T0*
_output_shapes
:2
Identity_23Б
AssignVariableOp_23AssignVariableOp*assignvariableop_23_adam_dense_10_kernel_mIdentity_23:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_23_
Identity_24IdentityRestoreV2:tensors:24*
T0*
_output_shapes
:2
Identity_24А
AssignVariableOp_24AssignVariableOp(assignvariableop_24_adam_dense_10_bias_mIdentity_24:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_24_
Identity_25IdentityRestoreV2:tensors:25*
T0*
_output_shapes
:2
Identity_25Б
AssignVariableOp_25AssignVariableOp*assignvariableop_25_adam_dense_11_kernel_mIdentity_25:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_25_
Identity_26IdentityRestoreV2:tensors:26*
T0*
_output_shapes
:2
Identity_26А
AssignVariableOp_26AssignVariableOp(assignvariableop_26_adam_dense_11_bias_mIdentity_26:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_26_
Identity_27IdentityRestoreV2:tensors:27*
T0*
_output_shapes
:2
Identity_27ц
AssignVariableOp_27AssignVariableOp+assignvariableop_27_adam_conv2d_10_kernel_vIdentity_27:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_27_
Identity_28IdentityRestoreV2:tensors:28*
T0*
_output_shapes
:2
Identity_28б
AssignVariableOp_28AssignVariableOp)assignvariableop_28_adam_conv2d_10_bias_vIdentity_28:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_28_
Identity_29IdentityRestoreV2:tensors:29*
T0*
_output_shapes
:2
Identity_29Е
AssignVariableOp_29AssignVariableOp0assignvariableop_29_adam_top_pool_with_max_1_w_vIdentity_29:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_29_
Identity_30IdentityRestoreV2:tensors:30*
T0*
_output_shapes
:2
Identity_30Е
AssignVariableOp_30AssignVariableOp0assignvariableop_30_adam_top_pool_with_max_1_b_vIdentity_30:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_30_
Identity_31IdentityRestoreV2:tensors:31*
T0*
_output_shapes
:2
Identity_31ц
AssignVariableOp_31AssignVariableOp+assignvariableop_31_adam_conv2d_11_kernel_vIdentity_31:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_31_
Identity_32IdentityRestoreV2:tensors:32*
T0*
_output_shapes
:2
Identity_32б
AssignVariableOp_32AssignVariableOp)assignvariableop_32_adam_conv2d_11_bias_vIdentity_32:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_32_
Identity_33IdentityRestoreV2:tensors:33*
T0*
_output_shapes
:2
Identity_33Б
AssignVariableOp_33AssignVariableOp*assignvariableop_33_adam_dense_10_kernel_vIdentity_33:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_33_
Identity_34IdentityRestoreV2:tensors:34*
T0*
_output_shapes
:2
Identity_34А
AssignVariableOp_34AssignVariableOp(assignvariableop_34_adam_dense_10_bias_vIdentity_34:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_34_
Identity_35IdentityRestoreV2:tensors:35*
T0*
_output_shapes
:2
Identity_35Б
AssignVariableOp_35AssignVariableOp*assignvariableop_35_adam_dense_11_kernel_vIdentity_35:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_35_
Identity_36IdentityRestoreV2:tensors:36*
T0*
_output_shapes
:2
Identity_36А
AssignVariableOp_36AssignVariableOp(assignvariableop_36_adam_dense_11_bias_vIdentity_36:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_36е
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_namesћ
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slices─
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpї
Identity_37Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_37Ў
Identity_38IdentityIdentity_37:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_38"#
identity_38Identity_38:output:0*Ф
_input_shapesЎ
ќ: :::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:+ '
%
_user_specified_namefile_prefix
Б­
▒
H__inference_sequential_5_layer_call_and_return_conditional_losses_507651

inputs,
(conv2d_10_conv2d_readvariableop_resource-
)conv2d_10_biasadd_readvariableop_resource:
6top_pool_with_max_1_expanddims_readvariableop_resource3
/top_pool_with_max_1_add_readvariableop_resource,
(conv2d_11_conv2d_readvariableop_resource-
)conv2d_11_biasadd_readvariableop_resource+
'dense_10_matmul_readvariableop_resource,
(dense_10_biasadd_readvariableop_resource+
'dense_11_matmul_readvariableop_resource,
(dense_11_biasadd_readvariableop_resource
identityѕб conv2d_10/BiasAdd/ReadVariableOpбconv2d_10/Conv2D/ReadVariableOpб conv2d_11/BiasAdd/ReadVariableOpбconv2d_11/Conv2D/ReadVariableOpбdense_10/BiasAdd/ReadVariableOpбdense_10/MatMul/ReadVariableOpбdense_11/BiasAdd/ReadVariableOpбdense_11/MatMul/ReadVariableOpб-top_pool_with_max_1/ExpandDims/ReadVariableOpб&top_pool_with_max_1/add/ReadVariableOp│
conv2d_10/Conv2D/ReadVariableOpReadVariableOp(conv2d_10_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02!
conv2d_10/Conv2D/ReadVariableOp┬
conv2d_10/Conv2DConv2Dinputs'conv2d_10/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:         *
paddingVALID*
strides
2
conv2d_10/Conv2Dф
 conv2d_10/BiasAdd/ReadVariableOpReadVariableOp)conv2d_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02"
 conv2d_10/BiasAdd/ReadVariableOp░
conv2d_10/BiasAddBiasAddconv2d_10/Conv2D:output:0(conv2d_10/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:         2
conv2d_10/BiasAddх
 top_pool_with_max_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2"
 top_pool_with_max_1/Pad/paddings║
top_pool_with_max_1/PadPadconv2d_10/BiasAdd:output:0)top_pool_with_max_1/Pad/paddings:output:0*
T0*/
_output_shapes
:         2
top_pool_with_max_1/Padќ
'top_pool_with_max_1/ExtractImagePatchesExtractImagePatches top_pool_with_max_1/Pad:output:0*
T0*0
_output_shapes
:         

ѕ*
ksizes
*
paddingVALID*
rates
*
strides
2)
'top_pool_with_max_1/ExtractImagePatchesє
top_pool_with_max_1/ShapeShape top_pool_with_max_1/Pad:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/ShapeЏ
top_pool_with_max_1/Shape_1Shape1top_pool_with_max_1/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_1ю
'top_pool_with_max_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2)
'top_pool_with_max_1/strided_slice/stackа
)top_pool_with_max_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice/stack_1а
)top_pool_with_max_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice/stack_2┌
!top_pool_with_max_1/strided_sliceStridedSlice"top_pool_with_max_1/Shape:output:00top_pool_with_max_1/strided_slice/stack:output:02top_pool_with_max_1/strided_slice/stack_1:output:02top_pool_with_max_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2#
!top_pool_with_max_1/strided_slicex
top_pool_with_max_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_with_max_1/mul/yф
top_pool_with_max_1/mulMul*top_pool_with_max_1/strided_slice:output:0"top_pool_with_max_1/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/mul|
top_pool_with_max_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_with_max_1/mul_1/yА
top_pool_with_max_1/mul_1Multop_pool_with_max_1/mul:z:0$top_pool_with_max_1/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/mul_1ї
#top_pool_with_max_1/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2%
#top_pool_with_max_1/Reshape/shape/1ї
#top_pool_with_max_1/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2%
#top_pool_with_max_1/Reshape/shape/2ї
#top_pool_with_max_1/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2%
#top_pool_with_max_1/Reshape/shape/3Ц
!top_pool_with_max_1/Reshape/shapePacktop_pool_with_max_1/mul_1:z:0,top_pool_with_max_1/Reshape/shape/1:output:0,top_pool_with_max_1/Reshape/shape/2:output:0,top_pool_with_max_1/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2#
!top_pool_with_max_1/Reshape/shapeя
top_pool_with_max_1/ReshapeReshape1top_pool_with_max_1/ExtractImagePatches:patches:0*top_pool_with_max_1/Reshape/shape:output:0*
T0*/
_output_shapes
:         2
top_pool_with_max_1/Reshapeа
)top_pool_with_max_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)top_pool_with_max_1/strided_slice_1/stackц
+top_pool_with_max_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_1/stack_1ц
+top_pool_with_max_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_1/stack_2С
#top_pool_with_max_1/strided_slice_1StridedSlice"top_pool_with_max_1/Shape:output:02top_pool_with_max_1/strided_slice_1/stack:output:04top_pool_with_max_1/strided_slice_1/stack_1:output:04top_pool_with_max_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_1Џ
top_pool_with_max_1/Slice/beginConst*
_output_shapes
:*
dtype0*%
valueB"              2!
top_pool_with_max_1/Slice/beginє
 top_pool_with_max_1/Slice/size/1Const*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_with_max_1/Slice/size/1є
 top_pool_with_max_1/Slice/size/2Const*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_with_max_1/Slice/size/2є
 top_pool_with_max_1/Slice/size/3Const*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_with_max_1/Slice/size/3ќ
top_pool_with_max_1/Slice/sizePacktop_pool_with_max_1/mul_1:z:0)top_pool_with_max_1/Slice/size/1:output:0)top_pool_with_max_1/Slice/size/2:output:0)top_pool_with_max_1/Slice/size/3:output:0*
N*
T0*
_output_shapes
:2 
top_pool_with_max_1/Slice/size 
top_pool_with_max_1/SliceSlice$top_pool_with_max_1/Reshape:output:0(top_pool_with_max_1/Slice/begin:output:0'top_pool_with_max_1/Slice/size:output:0*
Index0*
T0*/
_output_shapes
:         2
top_pool_with_max_1/Sliceљ
%top_pool_with_max_1/Reshape_1/shape/1Const*
_output_shapes
: *
dtype0*
value	B :
2'
%top_pool_with_max_1/Reshape_1/shape/1љ
%top_pool_with_max_1/Reshape_1/shape/2Const*
_output_shapes
: *
dtype0*
value	B :
2'
%top_pool_with_max_1/Reshape_1/shape/2љ
%top_pool_with_max_1/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_1/shape/3љ
%top_pool_with_max_1/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_1/shape/4Ь
#top_pool_with_max_1/Reshape_1/shapePack,top_pool_with_max_1/strided_slice_1:output:0.top_pool_with_max_1/Reshape_1/shape/1:output:0.top_pool_with_max_1/Reshape_1/shape/2:output:0.top_pool_with_max_1/Reshape_1/shape/3:output:0.top_pool_with_max_1/Reshape_1/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_1/shape┘
top_pool_with_max_1/Reshape_1Reshape"top_pool_with_max_1/Slice:output:0,top_pool_with_max_1/Reshape_1/shape:output:0*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/Reshape_1Ю
)top_pool_with_max_1/ExtractImagePatches_1ExtractImagePatches$top_pool_with_max_1/Reshape:output:0*
T0*/
_output_shapes
:         H*
ksizes
*
paddingVALID*
rates
*
strides
2+
)top_pool_with_max_1/ExtractImagePatches_1Ю
top_pool_with_max_1/Shape_2Shape3top_pool_with_max_1/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_2ј
top_pool_with_max_1/Shape_3Shape$top_pool_with_max_1/Reshape:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_3а
)top_pool_with_max_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)top_pool_with_max_1/strided_slice_2/stackц
+top_pool_with_max_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_2/stack_1ц
+top_pool_with_max_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_2/stack_2Т
#top_pool_with_max_1/strided_slice_2StridedSlice$top_pool_with_max_1/Shape_3:output:02top_pool_with_max_1/strided_slice_2/stack:output:04top_pool_with_max_1/strided_slice_2/stack_1:output:04top_pool_with_max_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_2а
)top_pool_with_max_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_3/stackц
+top_pool_with_max_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_3/stack_1ц
+top_pool_with_max_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_3/stack_2Т
#top_pool_with_max_1/strided_slice_3StridedSlice$top_pool_with_max_1/Shape_2:output:02top_pool_with_max_1/strided_slice_3/stack:output:04top_pool_with_max_1/strided_slice_3/stack_1:output:04top_pool_with_max_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_3а
)top_pool_with_max_1/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_4/stackц
+top_pool_with_max_1/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_4/stack_1ц
+top_pool_with_max_1/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_4/stack_2Т
#top_pool_with_max_1/strided_slice_4StridedSlice$top_pool_with_max_1/Shape_2:output:02top_pool_with_max_1/strided_slice_4/stack:output:04top_pool_with_max_1/strided_slice_4/stack_1:output:04top_pool_with_max_1/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_4љ
%top_pool_with_max_1/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_2/shape/3љ
%top_pool_with_max_1/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_2/shape/4љ
%top_pool_with_max_1/Reshape_2/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_2/shape/5џ
#top_pool_with_max_1/Reshape_2/shapePack,top_pool_with_max_1/strided_slice_2:output:0,top_pool_with_max_1/strided_slice_3:output:0,top_pool_with_max_1/strided_slice_4:output:0.top_pool_with_max_1/Reshape_2/shape/3:output:0.top_pool_with_max_1/Reshape_2/shape/4:output:0.top_pool_with_max_1/Reshape_2/shape/5:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_2/shapeђ
top_pool_with_max_1/Reshape_2Reshape3top_pool_with_max_1/ExtractImagePatches_1:patches:0,top_pool_with_max_1/Reshape_2/shape:output:0*
T0*I
_output_shapes7
5:3                           2
top_pool_with_max_1/Reshape_2љ
top_pool_with_max_1/Shape_4Shape&top_pool_with_max_1/Reshape_2:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_4а
)top_pool_with_max_1/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)top_pool_with_max_1/strided_slice_5/stackц
+top_pool_with_max_1/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_5/stack_1ц
+top_pool_with_max_1/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_5/stack_2Т
#top_pool_with_max_1/strided_slice_5StridedSlice$top_pool_with_max_1/Shape_4:output:02top_pool_with_max_1/strided_slice_5/stack:output:04top_pool_with_max_1/strided_slice_5/stack_1:output:04top_pool_with_max_1/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_5а
)top_pool_with_max_1/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_6/stackц
+top_pool_with_max_1/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_6/stack_1ц
+top_pool_with_max_1/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_6/stack_2Т
#top_pool_with_max_1/strided_slice_6StridedSlice$top_pool_with_max_1/Shape_4:output:02top_pool_with_max_1/strided_slice_6/stack:output:04top_pool_with_max_1/strided_slice_6/stack_1:output:04top_pool_with_max_1/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_6а
)top_pool_with_max_1/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_7/stackц
+top_pool_with_max_1/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_7/stack_1ц
+top_pool_with_max_1/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_7/stack_2Т
#top_pool_with_max_1/strided_slice_7StridedSlice$top_pool_with_max_1/Shape_4:output:02top_pool_with_max_1/strided_slice_7/stack:output:04top_pool_with_max_1/strided_slice_7/stack_1:output:04top_pool_with_max_1/strided_slice_7/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_7љ
%top_pool_with_max_1/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2'
%top_pool_with_max_1/Reshape_3/shape/3љ
%top_pool_with_max_1/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_3/shape/4Ж
#top_pool_with_max_1/Reshape_3/shapePack,top_pool_with_max_1/strided_slice_5:output:0,top_pool_with_max_1/strided_slice_6:output:0,top_pool_with_max_1/strided_slice_7:output:0.top_pool_with_max_1/Reshape_3/shape/3:output:0.top_pool_with_max_1/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_3/shape№
top_pool_with_max_1/Reshape_3Reshape&top_pool_with_max_1/Reshape_2:output:0,top_pool_with_max_1/Reshape_3/shape:output:0*
T0*E
_output_shapes3
1:/                           	2
top_pool_with_max_1/Reshape_3ђ
top_pool_with_max_1/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_with_max_1/sort/axis╗
top_pool_with_max_1/sort/NegNeg&top_pool_with_max_1/Reshape_3:output:0*
T0*E
_output_shapes3
1:/                           	2
top_pool_with_max_1/sort/Negљ
top_pool_with_max_1/sort/ShapeShape top_pool_with_max_1/sort/Neg:y:0*
T0*
_output_shapes
:2 
top_pool_with_max_1/sort/Shapeд
,top_pool_with_max_1/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2.
,top_pool_with_max_1/sort/strided_slice/stackф
.top_pool_with_max_1/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.top_pool_with_max_1/sort/strided_slice/stack_1ф
.top_pool_with_max_1/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.top_pool_with_max_1/sort/strided_slice/stack_2Э
&top_pool_with_max_1/sort/strided_sliceStridedSlice'top_pool_with_max_1/sort/Shape:output:05top_pool_with_max_1/sort/strided_slice/stack:output:07top_pool_with_max_1/sort/strided_slice/stack_1:output:07top_pool_with_max_1/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&top_pool_with_max_1/sort/strided_sliceђ
top_pool_with_max_1/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_with_max_1/sort/Rank┴
&top_pool_with_max_1/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2(
&top_pool_with_max_1/sort/transpositionЁ
"top_pool_with_max_1/sort/transpose	Transpose top_pool_with_max_1/sort/Neg:y:0/top_pool_with_max_1/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/                           	2$
"top_pool_with_max_1/sort/transpose╣
top_pool_with_max_1/sort/TopKV2TopKV2&top_pool_with_max_1/sort/transpose:y:0/top_pool_with_max_1/sort/strided_slice:output:0*
T0*ѕ
_output_shapesv
t:8                                    :8                                    2!
top_pool_with_max_1/sort/TopKV2џ
$top_pool_with_max_1/sort/transpose_1	Transpose(top_pool_with_max_1/sort/TopKV2:values:0/top_pool_with_max_1/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8                                    2&
$top_pool_with_max_1/sort/transpose_1╩
top_pool_with_max_1/sort/Neg_1Neg(top_pool_with_max_1/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8                                    2 
top_pool_with_max_1/sort/Neg_1│
)top_pool_with_max_1/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2+
)top_pool_with_max_1/strided_slice_8/stackи
+top_pool_with_max_1/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2-
+top_pool_with_max_1/strided_slice_8/stack_1и
+top_pool_with_max_1/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2-
+top_pool_with_max_1/strided_slice_8/stack_2▒
#top_pool_with_max_1/strided_slice_8StridedSlice"top_pool_with_max_1/sort/Neg_1:y:02top_pool_with_max_1/strided_slice_8/stack:output:04top_pool_with_max_1/strided_slice_8/stack_1:output:04top_pool_with_max_1/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_8│
)top_pool_with_max_1/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2+
)top_pool_with_max_1/strided_slice_9/stackи
+top_pool_with_max_1/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2-
+top_pool_with_max_1/strided_slice_9/stack_1и
+top_pool_with_max_1/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2-
+top_pool_with_max_1/strided_slice_9/stack_2▒
#top_pool_with_max_1/strided_slice_9StridedSlice"top_pool_with_max_1/sort/Neg_1:y:02top_pool_with_max_1/strided_slice_9/stack:output:04top_pool_with_max_1/strided_slice_9/stack_1:output:04top_pool_with_max_1/strided_slice_9/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_9р
top_pool_with_max_1/subSub,top_pool_with_max_1/strided_slice_9:output:0,top_pool_with_max_1/strided_slice_8:output:0*
T0*A
_output_shapes/
-:+                           2
top_pool_with_max_1/sub
top_pool_with_max_1/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_with_max_1/Floor/xє
top_pool_with_max_1/FloorFloor$top_pool_with_max_1/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/FloorІ
top_pool_with_max_1/CastCasttop_pool_with_max_1/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_with_max_1/CastЃ
top_pool_with_max_1/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_with_max_1/Floor_1/xї
top_pool_with_max_1/Floor_1Floor&top_pool_with_max_1/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/Floor_1Љ
top_pool_with_max_1/Cast_1Casttop_pool_with_max_1/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_with_max_1/Cast_1Ё
top_pool_with_max_1/Shape_5Shapetop_pool_with_max_1/sub:z:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_5ё
top_pool_with_max_1/Shape_6Shapeconv2d_10/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_6б
*top_pool_with_max_1/strided_slice_10/stackConst*
_output_shapes
:*
dtype0*
valueB: 2,
*top_pool_with_max_1/strided_slice_10/stackд
,top_pool_with_max_1/strided_slice_10/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2.
,top_pool_with_max_1/strided_slice_10/stack_1д
,top_pool_with_max_1/strided_slice_10/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2.
,top_pool_with_max_1/strided_slice_10/stack_2в
$top_pool_with_max_1/strided_slice_10StridedSlice$top_pool_with_max_1/Shape_6:output:03top_pool_with_max_1/strided_slice_10/stack:output:05top_pool_with_max_1/strided_slice_10/stack_1:output:05top_pool_with_max_1/strided_slice_10/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2&
$top_pool_with_max_1/strided_slice_10љ
%top_pool_with_max_1/Reshape_4/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_4/shape/3љ
%top_pool_with_max_1/Reshape_4/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_4/shape/4═
#top_pool_with_max_1/Reshape_4/shapePack-top_pool_with_max_1/strided_slice_10:output:0top_pool_with_max_1/Cast:y:0top_pool_with_max_1/Cast_1:y:0.top_pool_with_max_1/Reshape_4/shape/3:output:0.top_pool_with_max_1/Reshape_4/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_4/shapeм
top_pool_with_max_1/Reshape_4Reshapetop_pool_with_max_1/sub:z:0,top_pool_with_max_1/Reshape_4/shape:output:0*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/Reshape_4ў
)top_pool_with_max_1/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2+
)top_pool_with_max_1/Max/reduction_indicesС
top_pool_with_max_1/MaxMax&top_pool_with_max_1/Reshape_1:output:02top_pool_with_max_1/Max/reduction_indices:output:0*
T0*3
_output_shapes!
:         

*
	keep_dims(2
top_pool_with_max_1/Maxё
top_pool_with_max_1/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2!
top_pool_with_max_1/concat/axis 
top_pool_with_max_1/concatConcatV2&top_pool_with_max_1/Reshape_4:output:0 top_pool_with_max_1/Max:output:0(top_pool_with_max_1/concat/axis:output:0*
N*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/concatН
-top_pool_with_max_1/ExpandDims/ReadVariableOpReadVariableOp6top_pool_with_max_1_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02/
-top_pool_with_max_1/ExpandDims/ReadVariableOpі
"top_pool_with_max_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2$
"top_pool_with_max_1/ExpandDims/dim▀
top_pool_with_max_1/ExpandDims
ExpandDims5top_pool_with_max_1/ExpandDims/ReadVariableOp:value:0+top_pool_with_max_1/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2 
top_pool_with_max_1/ExpandDimsј
$top_pool_with_max_1/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$top_pool_with_max_1/ExpandDims_1/dim█
 top_pool_with_max_1/ExpandDims_1
ExpandDims'top_pool_with_max_1/ExpandDims:output:0-top_pool_with_max_1/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2"
 top_pool_with_max_1/ExpandDims_1ј
$top_pool_with_max_1/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$top_pool_with_max_1/ExpandDims_2/dimр
 top_pool_with_max_1/ExpandDims_2
ExpandDims)top_pool_with_max_1/ExpandDims_1:output:0-top_pool_with_max_1/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2"
 top_pool_with_max_1/ExpandDims_2╦
top_pool_with_max_1/mul_2Mul#top_pool_with_max_1/concat:output:0)top_pool_with_max_1/ExpandDims_2:output:0*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/mul_2ў
)top_pool_with_max_1/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2+
)top_pool_with_max_1/Sum/reduction_indicesк
top_pool_with_max_1/SumSumtop_pool_with_max_1/mul_2:z:02top_pool_with_max_1/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         

2
top_pool_with_max_1/Sum╚
&top_pool_with_max_1/add/ReadVariableOpReadVariableOp/top_pool_with_max_1_add_readvariableop_resource*&
_output_shapes
:*
dtype02(
&top_pool_with_max_1/add/ReadVariableOpК
top_pool_with_max_1/addAddV2 top_pool_with_max_1/Sum:output:0.top_pool_with_max_1/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:         

2
top_pool_with_max_1/add│
conv2d_11/Conv2D/ReadVariableOpReadVariableOp(conv2d_11_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02!
conv2d_11/Conv2D/ReadVariableOpО
conv2d_11/Conv2DConv2Dtop_pool_with_max_1/add:z:0'conv2d_11/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:         *
paddingVALID*
strides
2
conv2d_11/Conv2Dф
 conv2d_11/BiasAdd/ReadVariableOpReadVariableOp)conv2d_11_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02"
 conv2d_11/BiasAdd/ReadVariableOp░
conv2d_11/BiasAddBiasAddconv2d_11/Conv2D:output:0(conv2d_11/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:         2
conv2d_11/BiasAdd~
conv2d_11/ReluReluconv2d_11/BiasAdd:output:0*
T0*/
_output_shapes
:         2
conv2d_11/Relus
flatten_5/ConstConst*
_output_shapes
:*
dtype0*
valueB"    @  2
flatten_5/Constю
flatten_5/ReshapeReshapeconv2d_11/Relu:activations:0flatten_5/Const:output:0*
T0*(
_output_shapes
:         └2
flatten_5/Reshapeф
dense_10/MatMul/ReadVariableOpReadVariableOp'dense_10_matmul_readvariableop_resource* 
_output_shapes
:
└ђ*
dtype02 
dense_10/MatMul/ReadVariableOpБ
dense_10/MatMulMatMulflatten_5/Reshape:output:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_10/MatMulе
dense_10/BiasAdd/ReadVariableOpReadVariableOp(dense_10_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02!
dense_10/BiasAdd/ReadVariableOpд
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_10/BiasAddЕ
dense_11/MatMul/ReadVariableOpReadVariableOp'dense_11_matmul_readvariableop_resource*
_output_shapes
:	ђ
*
dtype02 
dense_11/MatMul/ReadVariableOpА
dense_11/MatMulMatMuldense_10/BiasAdd:output:0&dense_11/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
dense_11/MatMulД
dense_11/BiasAdd/ReadVariableOpReadVariableOp(dense_11_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_11/BiasAdd/ReadVariableOpЦ
dense_11/BiasAddBiasAdddense_11/MatMul:product:0'dense_11/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
dense_11/BiasAddо
IdentityIdentitydense_11/BiasAdd:output:0!^conv2d_10/BiasAdd/ReadVariableOp ^conv2d_10/Conv2D/ReadVariableOp!^conv2d_11/BiasAdd/ReadVariableOp ^conv2d_11/Conv2D/ReadVariableOp ^dense_10/BiasAdd/ReadVariableOp^dense_10/MatMul/ReadVariableOp ^dense_11/BiasAdd/ReadVariableOp^dense_11/MatMul/ReadVariableOp.^top_pool_with_max_1/ExpandDims/ReadVariableOp'^top_pool_with_max_1/add/ReadVariableOp*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::2D
 conv2d_10/BiasAdd/ReadVariableOp conv2d_10/BiasAdd/ReadVariableOp2B
conv2d_10/Conv2D/ReadVariableOpconv2d_10/Conv2D/ReadVariableOp2D
 conv2d_11/BiasAdd/ReadVariableOp conv2d_11/BiasAdd/ReadVariableOp2B
conv2d_11/Conv2D/ReadVariableOpconv2d_11/Conv2D/ReadVariableOp2B
dense_10/BiasAdd/ReadVariableOpdense_10/BiasAdd/ReadVariableOp2@
dense_10/MatMul/ReadVariableOpdense_10/MatMul/ReadVariableOp2B
dense_11/BiasAdd/ReadVariableOpdense_11/BiasAdd/ReadVariableOp2@
dense_11/MatMul/ReadVariableOpdense_11/MatMul/ReadVariableOp2^
-top_pool_with_max_1/ExpandDims/ReadVariableOp-top_pool_with_max_1/ExpandDims/ReadVariableOp2P
&top_pool_with_max_1/add/ReadVariableOp&top_pool_with_max_1/add/ReadVariableOp:& "
 
_user_specified_nameinputs
юБ
Ј	
!__inference__wrapped_model_506964
input_69
5sequential_5_conv2d_10_conv2d_readvariableop_resource:
6sequential_5_conv2d_10_biasadd_readvariableop_resourceG
Csequential_5_top_pool_with_max_1_expanddims_readvariableop_resource@
<sequential_5_top_pool_with_max_1_add_readvariableop_resource9
5sequential_5_conv2d_11_conv2d_readvariableop_resource:
6sequential_5_conv2d_11_biasadd_readvariableop_resource8
4sequential_5_dense_10_matmul_readvariableop_resource9
5sequential_5_dense_10_biasadd_readvariableop_resource8
4sequential_5_dense_11_matmul_readvariableop_resource9
5sequential_5_dense_11_biasadd_readvariableop_resource
identityѕб-sequential_5/conv2d_10/BiasAdd/ReadVariableOpб,sequential_5/conv2d_10/Conv2D/ReadVariableOpб-sequential_5/conv2d_11/BiasAdd/ReadVariableOpб,sequential_5/conv2d_11/Conv2D/ReadVariableOpб,sequential_5/dense_10/BiasAdd/ReadVariableOpб+sequential_5/dense_10/MatMul/ReadVariableOpб,sequential_5/dense_11/BiasAdd/ReadVariableOpб+sequential_5/dense_11/MatMul/ReadVariableOpб:sequential_5/top_pool_with_max_1/ExpandDims/ReadVariableOpб3sequential_5/top_pool_with_max_1/add/ReadVariableOp┌
,sequential_5/conv2d_10/Conv2D/ReadVariableOpReadVariableOp5sequential_5_conv2d_10_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02.
,sequential_5/conv2d_10/Conv2D/ReadVariableOpЖ
sequential_5/conv2d_10/Conv2DConv2Dinput_64sequential_5/conv2d_10/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:         *
paddingVALID*
strides
2
sequential_5/conv2d_10/Conv2DЛ
-sequential_5/conv2d_10/BiasAdd/ReadVariableOpReadVariableOp6sequential_5_conv2d_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-sequential_5/conv2d_10/BiasAdd/ReadVariableOpС
sequential_5/conv2d_10/BiasAddBiasAdd&sequential_5/conv2d_10/Conv2D:output:05sequential_5/conv2d_10/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:         2 
sequential_5/conv2d_10/BiasAdd¤
-sequential_5/top_pool_with_max_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2/
-sequential_5/top_pool_with_max_1/Pad/paddingsЬ
$sequential_5/top_pool_with_max_1/PadPad'sequential_5/conv2d_10/BiasAdd:output:06sequential_5/top_pool_with_max_1/Pad/paddings:output:0*
T0*/
_output_shapes
:         2&
$sequential_5/top_pool_with_max_1/Padй
4sequential_5/top_pool_with_max_1/ExtractImagePatchesExtractImagePatches-sequential_5/top_pool_with_max_1/Pad:output:0*
T0*0
_output_shapes
:         

ѕ*
ksizes
*
paddingVALID*
rates
*
strides
26
4sequential_5/top_pool_with_max_1/ExtractImagePatchesГ
&sequential_5/top_pool_with_max_1/ShapeShape-sequential_5/top_pool_with_max_1/Pad:output:0*
T0*
_output_shapes
:2(
&sequential_5/top_pool_with_max_1/Shape┬
(sequential_5/top_pool_with_max_1/Shape_1Shape>sequential_5/top_pool_with_max_1/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2*
(sequential_5/top_pool_with_max_1/Shape_1Х
4sequential_5/top_pool_with_max_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 26
4sequential_5/top_pool_with_max_1/strided_slice/stack║
6sequential_5/top_pool_with_max_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:28
6sequential_5/top_pool_with_max_1/strided_slice/stack_1║
6sequential_5/top_pool_with_max_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:28
6sequential_5/top_pool_with_max_1/strided_slice/stack_2е
.sequential_5/top_pool_with_max_1/strided_sliceStridedSlice/sequential_5/top_pool_with_max_1/Shape:output:0=sequential_5/top_pool_with_max_1/strided_slice/stack:output:0?sequential_5/top_pool_with_max_1/strided_slice/stack_1:output:0?sequential_5/top_pool_with_max_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask20
.sequential_5/top_pool_with_max_1/strided_sliceњ
&sequential_5/top_pool_with_max_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2(
&sequential_5/top_pool_with_max_1/mul/yя
$sequential_5/top_pool_with_max_1/mulMul7sequential_5/top_pool_with_max_1/strided_slice:output:0/sequential_5/top_pool_with_max_1/mul/y:output:0*
T0*
_output_shapes
: 2&
$sequential_5/top_pool_with_max_1/mulќ
(sequential_5/top_pool_with_max_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2*
(sequential_5/top_pool_with_max_1/mul_1/yН
&sequential_5/top_pool_with_max_1/mul_1Mul(sequential_5/top_pool_with_max_1/mul:z:01sequential_5/top_pool_with_max_1/mul_1/y:output:0*
T0*
_output_shapes
: 2(
&sequential_5/top_pool_with_max_1/mul_1д
0sequential_5/top_pool_with_max_1/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :22
0sequential_5/top_pool_with_max_1/Reshape/shape/1д
0sequential_5/top_pool_with_max_1/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :22
0sequential_5/top_pool_with_max_1/Reshape/shape/2д
0sequential_5/top_pool_with_max_1/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :22
0sequential_5/top_pool_with_max_1/Reshape/shape/3з
.sequential_5/top_pool_with_max_1/Reshape/shapePack*sequential_5/top_pool_with_max_1/mul_1:z:09sequential_5/top_pool_with_max_1/Reshape/shape/1:output:09sequential_5/top_pool_with_max_1/Reshape/shape/2:output:09sequential_5/top_pool_with_max_1/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:20
.sequential_5/top_pool_with_max_1/Reshape/shapeњ
(sequential_5/top_pool_with_max_1/ReshapeReshape>sequential_5/top_pool_with_max_1/ExtractImagePatches:patches:07sequential_5/top_pool_with_max_1/Reshape/shape:output:0*
T0*/
_output_shapes
:         2*
(sequential_5/top_pool_with_max_1/Reshape║
6sequential_5/top_pool_with_max_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 28
6sequential_5/top_pool_with_max_1/strided_slice_1/stackЙ
8sequential_5/top_pool_with_max_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_1/stack_1Й
8sequential_5/top_pool_with_max_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_1/stack_2▓
0sequential_5/top_pool_with_max_1/strided_slice_1StridedSlice/sequential_5/top_pool_with_max_1/Shape:output:0?sequential_5/top_pool_with_max_1/strided_slice_1/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_1/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_1х
,sequential_5/top_pool_with_max_1/Slice/beginConst*
_output_shapes
:*
dtype0*%
valueB"              2.
,sequential_5/top_pool_with_max_1/Slice/beginа
-sequential_5/top_pool_with_max_1/Slice/size/1Const*
_output_shapes
: *
dtype0*
value	B :2/
-sequential_5/top_pool_with_max_1/Slice/size/1а
-sequential_5/top_pool_with_max_1/Slice/size/2Const*
_output_shapes
: *
dtype0*
value	B :2/
-sequential_5/top_pool_with_max_1/Slice/size/2а
-sequential_5/top_pool_with_max_1/Slice/size/3Const*
_output_shapes
: *
dtype0*
value	B :2/
-sequential_5/top_pool_with_max_1/Slice/size/3С
+sequential_5/top_pool_with_max_1/Slice/sizePack*sequential_5/top_pool_with_max_1/mul_1:z:06sequential_5/top_pool_with_max_1/Slice/size/1:output:06sequential_5/top_pool_with_max_1/Slice/size/2:output:06sequential_5/top_pool_with_max_1/Slice/size/3:output:0*
N*
T0*
_output_shapes
:2-
+sequential_5/top_pool_with_max_1/Slice/size└
&sequential_5/top_pool_with_max_1/SliceSlice1sequential_5/top_pool_with_max_1/Reshape:output:05sequential_5/top_pool_with_max_1/Slice/begin:output:04sequential_5/top_pool_with_max_1/Slice/size:output:0*
Index0*
T0*/
_output_shapes
:         2(
&sequential_5/top_pool_with_max_1/Sliceф
2sequential_5/top_pool_with_max_1/Reshape_1/shape/1Const*
_output_shapes
: *
dtype0*
value	B :
24
2sequential_5/top_pool_with_max_1/Reshape_1/shape/1ф
2sequential_5/top_pool_with_max_1/Reshape_1/shape/2Const*
_output_shapes
: *
dtype0*
value	B :
24
2sequential_5/top_pool_with_max_1/Reshape_1/shape/2ф
2sequential_5/top_pool_with_max_1/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_1/shape/3ф
2sequential_5/top_pool_with_max_1/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_1/shape/4╔
0sequential_5/top_pool_with_max_1/Reshape_1/shapePack9sequential_5/top_pool_with_max_1/strided_slice_1:output:0;sequential_5/top_pool_with_max_1/Reshape_1/shape/1:output:0;sequential_5/top_pool_with_max_1/Reshape_1/shape/2:output:0;sequential_5/top_pool_with_max_1/Reshape_1/shape/3:output:0;sequential_5/top_pool_with_max_1/Reshape_1/shape/4:output:0*
N*
T0*
_output_shapes
:22
0sequential_5/top_pool_with_max_1/Reshape_1/shapeЇ
*sequential_5/top_pool_with_max_1/Reshape_1Reshape/sequential_5/top_pool_with_max_1/Slice:output:09sequential_5/top_pool_with_max_1/Reshape_1/shape:output:0*
T0*3
_output_shapes!
:         

2,
*sequential_5/top_pool_with_max_1/Reshape_1─
6sequential_5/top_pool_with_max_1/ExtractImagePatches_1ExtractImagePatches1sequential_5/top_pool_with_max_1/Reshape:output:0*
T0*/
_output_shapes
:         H*
ksizes
*
paddingVALID*
rates
*
strides
28
6sequential_5/top_pool_with_max_1/ExtractImagePatches_1─
(sequential_5/top_pool_with_max_1/Shape_2Shape@sequential_5/top_pool_with_max_1/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2*
(sequential_5/top_pool_with_max_1/Shape_2х
(sequential_5/top_pool_with_max_1/Shape_3Shape1sequential_5/top_pool_with_max_1/Reshape:output:0*
T0*
_output_shapes
:2*
(sequential_5/top_pool_with_max_1/Shape_3║
6sequential_5/top_pool_with_max_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 28
6sequential_5/top_pool_with_max_1/strided_slice_2/stackЙ
8sequential_5/top_pool_with_max_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_2/stack_1Й
8sequential_5/top_pool_with_max_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_2/stack_2┤
0sequential_5/top_pool_with_max_1/strided_slice_2StridedSlice1sequential_5/top_pool_with_max_1/Shape_3:output:0?sequential_5/top_pool_with_max_1/strided_slice_2/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_2/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_2║
6sequential_5/top_pool_with_max_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:28
6sequential_5/top_pool_with_max_1/strided_slice_3/stackЙ
8sequential_5/top_pool_with_max_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_3/stack_1Й
8sequential_5/top_pool_with_max_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_3/stack_2┤
0sequential_5/top_pool_with_max_1/strided_slice_3StridedSlice1sequential_5/top_pool_with_max_1/Shape_2:output:0?sequential_5/top_pool_with_max_1/strided_slice_3/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_3/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_3║
6sequential_5/top_pool_with_max_1/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB:28
6sequential_5/top_pool_with_max_1/strided_slice_4/stackЙ
8sequential_5/top_pool_with_max_1/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_4/stack_1Й
8sequential_5/top_pool_with_max_1/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_4/stack_2┤
0sequential_5/top_pool_with_max_1/strided_slice_4StridedSlice1sequential_5/top_pool_with_max_1/Shape_2:output:0?sequential_5/top_pool_with_max_1/strided_slice_4/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_4/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_4ф
2sequential_5/top_pool_with_max_1/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_2/shape/3ф
2sequential_5/top_pool_with_max_1/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_2/shape/4ф
2sequential_5/top_pool_with_max_1/Reshape_2/shape/5Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_2/shape/5ѓ
0sequential_5/top_pool_with_max_1/Reshape_2/shapePack9sequential_5/top_pool_with_max_1/strided_slice_2:output:09sequential_5/top_pool_with_max_1/strided_slice_3:output:09sequential_5/top_pool_with_max_1/strided_slice_4:output:0;sequential_5/top_pool_with_max_1/Reshape_2/shape/3:output:0;sequential_5/top_pool_with_max_1/Reshape_2/shape/4:output:0;sequential_5/top_pool_with_max_1/Reshape_2/shape/5:output:0*
N*
T0*
_output_shapes
:22
0sequential_5/top_pool_with_max_1/Reshape_2/shape┤
*sequential_5/top_pool_with_max_1/Reshape_2Reshape@sequential_5/top_pool_with_max_1/ExtractImagePatches_1:patches:09sequential_5/top_pool_with_max_1/Reshape_2/shape:output:0*
T0*I
_output_shapes7
5:3                           2,
*sequential_5/top_pool_with_max_1/Reshape_2и
(sequential_5/top_pool_with_max_1/Shape_4Shape3sequential_5/top_pool_with_max_1/Reshape_2:output:0*
T0*
_output_shapes
:2*
(sequential_5/top_pool_with_max_1/Shape_4║
6sequential_5/top_pool_with_max_1/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB: 28
6sequential_5/top_pool_with_max_1/strided_slice_5/stackЙ
8sequential_5/top_pool_with_max_1/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_5/stack_1Й
8sequential_5/top_pool_with_max_1/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_5/stack_2┤
0sequential_5/top_pool_with_max_1/strided_slice_5StridedSlice1sequential_5/top_pool_with_max_1/Shape_4:output:0?sequential_5/top_pool_with_max_1/strided_slice_5/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_5/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_5║
6sequential_5/top_pool_with_max_1/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:28
6sequential_5/top_pool_with_max_1/strided_slice_6/stackЙ
8sequential_5/top_pool_with_max_1/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_6/stack_1Й
8sequential_5/top_pool_with_max_1/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_6/stack_2┤
0sequential_5/top_pool_with_max_1/strided_slice_6StridedSlice1sequential_5/top_pool_with_max_1/Shape_4:output:0?sequential_5/top_pool_with_max_1/strided_slice_6/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_6/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_6║
6sequential_5/top_pool_with_max_1/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*
valueB:28
6sequential_5/top_pool_with_max_1/strided_slice_7/stackЙ
8sequential_5/top_pool_with_max_1/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_7/stack_1Й
8sequential_5/top_pool_with_max_1/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8sequential_5/top_pool_with_max_1/strided_slice_7/stack_2┤
0sequential_5/top_pool_with_max_1/strided_slice_7StridedSlice1sequential_5/top_pool_with_max_1/Shape_4:output:0?sequential_5/top_pool_with_max_1/strided_slice_7/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_7/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_7/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_7ф
2sequential_5/top_pool_with_max_1/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	24
2sequential_5/top_pool_with_max_1/Reshape_3/shape/3ф
2sequential_5/top_pool_with_max_1/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_3/shape/4┼
0sequential_5/top_pool_with_max_1/Reshape_3/shapePack9sequential_5/top_pool_with_max_1/strided_slice_5:output:09sequential_5/top_pool_with_max_1/strided_slice_6:output:09sequential_5/top_pool_with_max_1/strided_slice_7:output:0;sequential_5/top_pool_with_max_1/Reshape_3/shape/3:output:0;sequential_5/top_pool_with_max_1/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:22
0sequential_5/top_pool_with_max_1/Reshape_3/shapeБ
*sequential_5/top_pool_with_max_1/Reshape_3Reshape3sequential_5/top_pool_with_max_1/Reshape_2:output:09sequential_5/top_pool_with_max_1/Reshape_3/shape:output:0*
T0*E
_output_shapes3
1:/                           	2,
*sequential_5/top_pool_with_max_1/Reshape_3џ
*sequential_5/top_pool_with_max_1/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_5/top_pool_with_max_1/sort/axisР
)sequential_5/top_pool_with_max_1/sort/NegNeg3sequential_5/top_pool_with_max_1/Reshape_3:output:0*
T0*E
_output_shapes3
1:/                           	2+
)sequential_5/top_pool_with_max_1/sort/Negи
+sequential_5/top_pool_with_max_1/sort/ShapeShape-sequential_5/top_pool_with_max_1/sort/Neg:y:0*
T0*
_output_shapes
:2-
+sequential_5/top_pool_with_max_1/sort/Shape└
9sequential_5/top_pool_with_max_1/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2;
9sequential_5/top_pool_with_max_1/sort/strided_slice/stack─
;sequential_5/top_pool_with_max_1/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2=
;sequential_5/top_pool_with_max_1/sort/strided_slice/stack_1─
;sequential_5/top_pool_with_max_1/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2=
;sequential_5/top_pool_with_max_1/sort/strided_slice/stack_2к
3sequential_5/top_pool_with_max_1/sort/strided_sliceStridedSlice4sequential_5/top_pool_with_max_1/sort/Shape:output:0Bsequential_5/top_pool_with_max_1/sort/strided_slice/stack:output:0Dsequential_5/top_pool_with_max_1/sort/strided_slice/stack_1:output:0Dsequential_5/top_pool_with_max_1/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask25
3sequential_5/top_pool_with_max_1/sort/strided_sliceџ
*sequential_5/top_pool_with_max_1/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2,
*sequential_5/top_pool_with_max_1/sort/Rank█
3sequential_5/top_pool_with_max_1/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    25
3sequential_5/top_pool_with_max_1/sort/transposition╣
/sequential_5/top_pool_with_max_1/sort/transpose	Transpose-sequential_5/top_pool_with_max_1/sort/Neg:y:0<sequential_5/top_pool_with_max_1/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/                           	21
/sequential_5/top_pool_with_max_1/sort/transposeь
,sequential_5/top_pool_with_max_1/sort/TopKV2TopKV23sequential_5/top_pool_with_max_1/sort/transpose:y:0<sequential_5/top_pool_with_max_1/sort/strided_slice:output:0*
T0*ѕ
_output_shapesv
t:8                                    :8                                    2.
,sequential_5/top_pool_with_max_1/sort/TopKV2╬
1sequential_5/top_pool_with_max_1/sort/transpose_1	Transpose5sequential_5/top_pool_with_max_1/sort/TopKV2:values:0<sequential_5/top_pool_with_max_1/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8                                    23
1sequential_5/top_pool_with_max_1/sort/transpose_1ы
+sequential_5/top_pool_with_max_1/sort/Neg_1Neg5sequential_5/top_pool_with_max_1/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8                                    2-
+sequential_5/top_pool_with_max_1/sort/Neg_1═
6sequential_5/top_pool_with_max_1/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   28
6sequential_5/top_pool_with_max_1/strided_slice_8/stackЛ
8sequential_5/top_pool_with_max_1/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2:
8sequential_5/top_pool_with_max_1/strided_slice_8/stack_1Л
8sequential_5/top_pool_with_max_1/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2:
8sequential_5/top_pool_with_max_1/strided_slice_8/stack_2 
0sequential_5/top_pool_with_max_1/strided_slice_8StridedSlice/sequential_5/top_pool_with_max_1/sort/Neg_1:y:0?sequential_5/top_pool_with_max_1/strided_slice_8/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_8/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_8═
6sequential_5/top_pool_with_max_1/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*)
value B"                   28
6sequential_5/top_pool_with_max_1/strided_slice_9/stackЛ
8sequential_5/top_pool_with_max_1/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2:
8sequential_5/top_pool_with_max_1/strided_slice_9/stack_1Л
8sequential_5/top_pool_with_max_1/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2:
8sequential_5/top_pool_with_max_1/strided_slice_9/stack_2 
0sequential_5/top_pool_with_max_1/strided_slice_9StridedSlice/sequential_5/top_pool_with_max_1/sort/Neg_1:y:0?sequential_5/top_pool_with_max_1/strided_slice_9/stack:output:0Asequential_5/top_pool_with_max_1/strided_slice_9/stack_1:output:0Asequential_5/top_pool_with_max_1/strided_slice_9/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask22
0sequential_5/top_pool_with_max_1/strided_slice_9Ћ
$sequential_5/top_pool_with_max_1/subSub9sequential_5/top_pool_with_max_1/strided_slice_9:output:09sequential_5/top_pool_with_max_1/strided_slice_8:output:0*
T0*A
_output_shapes/
-:+                           2&
$sequential_5/top_pool_with_max_1/subЎ
(sequential_5/top_pool_with_max_1/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2*
(sequential_5/top_pool_with_max_1/Floor/xГ
&sequential_5/top_pool_with_max_1/FloorFloor1sequential_5/top_pool_with_max_1/Floor/x:output:0*
T0*
_output_shapes
: 2(
&sequential_5/top_pool_with_max_1/Floor▓
%sequential_5/top_pool_with_max_1/CastCast*sequential_5/top_pool_with_max_1/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2'
%sequential_5/top_pool_with_max_1/CastЮ
*sequential_5/top_pool_with_max_1/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2,
*sequential_5/top_pool_with_max_1/Floor_1/x│
(sequential_5/top_pool_with_max_1/Floor_1Floor3sequential_5/top_pool_with_max_1/Floor_1/x:output:0*
T0*
_output_shapes
: 2*
(sequential_5/top_pool_with_max_1/Floor_1И
'sequential_5/top_pool_with_max_1/Cast_1Cast,sequential_5/top_pool_with_max_1/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2)
'sequential_5/top_pool_with_max_1/Cast_1г
(sequential_5/top_pool_with_max_1/Shape_5Shape(sequential_5/top_pool_with_max_1/sub:z:0*
T0*
_output_shapes
:2*
(sequential_5/top_pool_with_max_1/Shape_5Ф
(sequential_5/top_pool_with_max_1/Shape_6Shape'sequential_5/conv2d_10/BiasAdd:output:0*
T0*
_output_shapes
:2*
(sequential_5/top_pool_with_max_1/Shape_6╝
7sequential_5/top_pool_with_max_1/strided_slice_10/stackConst*
_output_shapes
:*
dtype0*
valueB: 29
7sequential_5/top_pool_with_max_1/strided_slice_10/stack└
9sequential_5/top_pool_with_max_1/strided_slice_10/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2;
9sequential_5/top_pool_with_max_1/strided_slice_10/stack_1└
9sequential_5/top_pool_with_max_1/strided_slice_10/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2;
9sequential_5/top_pool_with_max_1/strided_slice_10/stack_2╣
1sequential_5/top_pool_with_max_1/strided_slice_10StridedSlice1sequential_5/top_pool_with_max_1/Shape_6:output:0@sequential_5/top_pool_with_max_1/strided_slice_10/stack:output:0Bsequential_5/top_pool_with_max_1/strided_slice_10/stack_1:output:0Bsequential_5/top_pool_with_max_1/strided_slice_10/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask23
1sequential_5/top_pool_with_max_1/strided_slice_10ф
2sequential_5/top_pool_with_max_1/Reshape_4/shape/3Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_4/shape/3ф
2sequential_5/top_pool_with_max_1/Reshape_4/shape/4Const*
_output_shapes
: *
dtype0*
value	B :24
2sequential_5/top_pool_with_max_1/Reshape_4/shape/4е
0sequential_5/top_pool_with_max_1/Reshape_4/shapePack:sequential_5/top_pool_with_max_1/strided_slice_10:output:0)sequential_5/top_pool_with_max_1/Cast:y:0+sequential_5/top_pool_with_max_1/Cast_1:y:0;sequential_5/top_pool_with_max_1/Reshape_4/shape/3:output:0;sequential_5/top_pool_with_max_1/Reshape_4/shape/4:output:0*
N*
T0*
_output_shapes
:22
0sequential_5/top_pool_with_max_1/Reshape_4/shapeє
*sequential_5/top_pool_with_max_1/Reshape_4Reshape(sequential_5/top_pool_with_max_1/sub:z:09sequential_5/top_pool_with_max_1/Reshape_4/shape:output:0*
T0*3
_output_shapes!
:         

2,
*sequential_5/top_pool_with_max_1/Reshape_4▓
6sequential_5/top_pool_with_max_1/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :28
6sequential_5/top_pool_with_max_1/Max/reduction_indicesў
$sequential_5/top_pool_with_max_1/MaxMax3sequential_5/top_pool_with_max_1/Reshape_1:output:0?sequential_5/top_pool_with_max_1/Max/reduction_indices:output:0*
T0*3
_output_shapes!
:         

*
	keep_dims(2&
$sequential_5/top_pool_with_max_1/Maxъ
,sequential_5/top_pool_with_max_1/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2.
,sequential_5/top_pool_with_max_1/concat/axis└
'sequential_5/top_pool_with_max_1/concatConcatV23sequential_5/top_pool_with_max_1/Reshape_4:output:0-sequential_5/top_pool_with_max_1/Max:output:05sequential_5/top_pool_with_max_1/concat/axis:output:0*
N*
T0*3
_output_shapes!
:         

2)
'sequential_5/top_pool_with_max_1/concatЧ
:sequential_5/top_pool_with_max_1/ExpandDims/ReadVariableOpReadVariableOpCsequential_5_top_pool_with_max_1_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02<
:sequential_5/top_pool_with_max_1/ExpandDims/ReadVariableOpц
/sequential_5/top_pool_with_max_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 21
/sequential_5/top_pool_with_max_1/ExpandDims/dimЊ
+sequential_5/top_pool_with_max_1/ExpandDims
ExpandDimsBsequential_5/top_pool_with_max_1/ExpandDims/ReadVariableOp:value:08sequential_5/top_pool_with_max_1/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2-
+sequential_5/top_pool_with_max_1/ExpandDimsе
1sequential_5/top_pool_with_max_1/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :23
1sequential_5/top_pool_with_max_1/ExpandDims_1/dimЈ
-sequential_5/top_pool_with_max_1/ExpandDims_1
ExpandDims4sequential_5/top_pool_with_max_1/ExpandDims:output:0:sequential_5/top_pool_with_max_1/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2/
-sequential_5/top_pool_with_max_1/ExpandDims_1е
1sequential_5/top_pool_with_max_1/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :23
1sequential_5/top_pool_with_max_1/ExpandDims_2/dimЋ
-sequential_5/top_pool_with_max_1/ExpandDims_2
ExpandDims6sequential_5/top_pool_with_max_1/ExpandDims_1:output:0:sequential_5/top_pool_with_max_1/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2/
-sequential_5/top_pool_with_max_1/ExpandDims_2 
&sequential_5/top_pool_with_max_1/mul_2Mul0sequential_5/top_pool_with_max_1/concat:output:06sequential_5/top_pool_with_max_1/ExpandDims_2:output:0*
T0*3
_output_shapes!
:         

2(
&sequential_5/top_pool_with_max_1/mul_2▓
6sequential_5/top_pool_with_max_1/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :28
6sequential_5/top_pool_with_max_1/Sum/reduction_indicesЩ
$sequential_5/top_pool_with_max_1/SumSum*sequential_5/top_pool_with_max_1/mul_2:z:0?sequential_5/top_pool_with_max_1/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         

2&
$sequential_5/top_pool_with_max_1/Sum№
3sequential_5/top_pool_with_max_1/add/ReadVariableOpReadVariableOp<sequential_5_top_pool_with_max_1_add_readvariableop_resource*&
_output_shapes
:*
dtype025
3sequential_5/top_pool_with_max_1/add/ReadVariableOpч
$sequential_5/top_pool_with_max_1/addAddV2-sequential_5/top_pool_with_max_1/Sum:output:0;sequential_5/top_pool_with_max_1/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:         

2&
$sequential_5/top_pool_with_max_1/add┌
,sequential_5/conv2d_11/Conv2D/ReadVariableOpReadVariableOp5sequential_5_conv2d_11_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02.
,sequential_5/conv2d_11/Conv2D/ReadVariableOpІ
sequential_5/conv2d_11/Conv2DConv2D(sequential_5/top_pool_with_max_1/add:z:04sequential_5/conv2d_11/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:         *
paddingVALID*
strides
2
sequential_5/conv2d_11/Conv2DЛ
-sequential_5/conv2d_11/BiasAdd/ReadVariableOpReadVariableOp6sequential_5_conv2d_11_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02/
-sequential_5/conv2d_11/BiasAdd/ReadVariableOpС
sequential_5/conv2d_11/BiasAddBiasAdd&sequential_5/conv2d_11/Conv2D:output:05sequential_5/conv2d_11/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:         2 
sequential_5/conv2d_11/BiasAddЦ
sequential_5/conv2d_11/ReluRelu'sequential_5/conv2d_11/BiasAdd:output:0*
T0*/
_output_shapes
:         2
sequential_5/conv2d_11/ReluЇ
sequential_5/flatten_5/ConstConst*
_output_shapes
:*
dtype0*
valueB"    @  2
sequential_5/flatten_5/Constл
sequential_5/flatten_5/ReshapeReshape)sequential_5/conv2d_11/Relu:activations:0%sequential_5/flatten_5/Const:output:0*
T0*(
_output_shapes
:         └2 
sequential_5/flatten_5/ReshapeЛ
+sequential_5/dense_10/MatMul/ReadVariableOpReadVariableOp4sequential_5_dense_10_matmul_readvariableop_resource* 
_output_shapes
:
└ђ*
dtype02-
+sequential_5/dense_10/MatMul/ReadVariableOpО
sequential_5/dense_10/MatMulMatMul'sequential_5/flatten_5/Reshape:output:03sequential_5/dense_10/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
sequential_5/dense_10/MatMul¤
,sequential_5/dense_10/BiasAdd/ReadVariableOpReadVariableOp5sequential_5_dense_10_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02.
,sequential_5/dense_10/BiasAdd/ReadVariableOp┌
sequential_5/dense_10/BiasAddBiasAdd&sequential_5/dense_10/MatMul:product:04sequential_5/dense_10/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
sequential_5/dense_10/BiasAddл
+sequential_5/dense_11/MatMul/ReadVariableOpReadVariableOp4sequential_5_dense_11_matmul_readvariableop_resource*
_output_shapes
:	ђ
*
dtype02-
+sequential_5/dense_11/MatMul/ReadVariableOpН
sequential_5/dense_11/MatMulMatMul&sequential_5/dense_10/BiasAdd:output:03sequential_5/dense_11/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
sequential_5/dense_11/MatMul╬
,sequential_5/dense_11/BiasAdd/ReadVariableOpReadVariableOp5sequential_5_dense_11_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02.
,sequential_5/dense_11/BiasAdd/ReadVariableOp┘
sequential_5/dense_11/BiasAddBiasAdd&sequential_5/dense_11/MatMul:product:04sequential_5/dense_11/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
sequential_5/dense_11/BiasAddт
IdentityIdentity&sequential_5/dense_11/BiasAdd:output:0.^sequential_5/conv2d_10/BiasAdd/ReadVariableOp-^sequential_5/conv2d_10/Conv2D/ReadVariableOp.^sequential_5/conv2d_11/BiasAdd/ReadVariableOp-^sequential_5/conv2d_11/Conv2D/ReadVariableOp-^sequential_5/dense_10/BiasAdd/ReadVariableOp,^sequential_5/dense_10/MatMul/ReadVariableOp-^sequential_5/dense_11/BiasAdd/ReadVariableOp,^sequential_5/dense_11/MatMul/ReadVariableOp;^sequential_5/top_pool_with_max_1/ExpandDims/ReadVariableOp4^sequential_5/top_pool_with_max_1/add/ReadVariableOp*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::2^
-sequential_5/conv2d_10/BiasAdd/ReadVariableOp-sequential_5/conv2d_10/BiasAdd/ReadVariableOp2\
,sequential_5/conv2d_10/Conv2D/ReadVariableOp,sequential_5/conv2d_10/Conv2D/ReadVariableOp2^
-sequential_5/conv2d_11/BiasAdd/ReadVariableOp-sequential_5/conv2d_11/BiasAdd/ReadVariableOp2\
,sequential_5/conv2d_11/Conv2D/ReadVariableOp,sequential_5/conv2d_11/Conv2D/ReadVariableOp2\
,sequential_5/dense_10/BiasAdd/ReadVariableOp,sequential_5/dense_10/BiasAdd/ReadVariableOp2Z
+sequential_5/dense_10/MatMul/ReadVariableOp+sequential_5/dense_10/MatMul/ReadVariableOp2\
,sequential_5/dense_11/BiasAdd/ReadVariableOp,sequential_5/dense_11/BiasAdd/ReadVariableOp2Z
+sequential_5/dense_11/MatMul/ReadVariableOp+sequential_5/dense_11/MatMul/ReadVariableOp2x
:sequential_5/top_pool_with_max_1/ExpandDims/ReadVariableOp:sequential_5/top_pool_with_max_1/ExpandDims/ReadVariableOp2j
3sequential_5/top_pool_with_max_1/add/ReadVariableOp3sequential_5/top_pool_with_max_1/add/ReadVariableOp:' #
!
_user_specified_name	input_6
і"
 
H__inference_sequential_5_layer_call_and_return_conditional_losses_507261

inputs,
(conv2d_10_statefulpartitionedcall_args_1,
(conv2d_10_statefulpartitionedcall_args_26
2top_pool_with_max_1_statefulpartitionedcall_args_16
2top_pool_with_max_1_statefulpartitionedcall_args_2,
(conv2d_11_statefulpartitionedcall_args_1,
(conv2d_11_statefulpartitionedcall_args_2+
'dense_10_statefulpartitionedcall_args_1+
'dense_10_statefulpartitionedcall_args_2+
'dense_11_statefulpartitionedcall_args_1+
'dense_11_statefulpartitionedcall_args_2
identityѕб!conv2d_10/StatefulPartitionedCallб!conv2d_11/StatefulPartitionedCallб dense_10/StatefulPartitionedCallб dense_11/StatefulPartitionedCallб+top_pool_with_max_1/StatefulPartitionedCallи
!conv2d_10/StatefulPartitionedCallStatefulPartitionedCallinputs(conv2d_10_statefulpartitionedcall_args_1(conv2d_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_10_layer_call_and_return_conditional_losses_5069762#
!conv2d_10/StatefulPartitionedCallЇ
+top_pool_with_max_1/StatefulPartitionedCallStatefulPartitionedCall*conv2d_10/StatefulPartitionedCall:output:02top_pool_with_max_1_statefulpartitionedcall_args_12top_pool_with_max_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         

**
config_proto

CPU

GPU 2J 8*X
fSRQ
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_5071442-
+top_pool_with_max_1/StatefulPartitionedCallт
!conv2d_11/StatefulPartitionedCallStatefulPartitionedCall4top_pool_with_max_1/StatefulPartitionedCall:output:0(conv2d_11_statefulpartitionedcall_args_1(conv2d_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_11_layer_call_and_return_conditional_losses_5069972#
!conv2d_11/StatefulPartitionedCallТ
flatten_5/PartitionedCallPartitionedCall*conv2d_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         └**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_flatten_5_layer_call_and_return_conditional_losses_5071652
flatten_5/PartitionedCallК
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_5/PartitionedCall:output:0'dense_10_statefulpartitionedcall_args_1'dense_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         ђ**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_5071832"
 dense_10/StatefulPartitionedCall═
 dense_11/StatefulPartitionedCallStatefulPartitionedCall)dense_10/StatefulPartitionedCall:output:0'dense_11_statefulpartitionedcall_args_1'dense_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_5072052"
 dense_11/StatefulPartitionedCall╣
IdentityIdentity)dense_11/StatefulPartitionedCall:output:0"^conv2d_10/StatefulPartitionedCall"^conv2d_11/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall,^top_pool_with_max_1/StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::2F
!conv2d_10/StatefulPartitionedCall!conv2d_10/StatefulPartitionedCall2F
!conv2d_11/StatefulPartitionedCall!conv2d_11/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall2Z
+top_pool_with_max_1/StatefulPartitionedCall+top_pool_with_max_1/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
Ж
я
E__inference_conv2d_11_layer_call_and_return_conditional_losses_506997

inputs"
conv2d_readvariableop_resource#
biasadd_readvariableop_resource
identityѕбBiasAdd/ReadVariableOpбConv2D/ReadVariableOpo
dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      2
dilation_rateЋ
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
Conv2D/ReadVariableOpХ
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+                           *
paddingVALID*
strides
2
Conv2Dї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpџ
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+                           2	
BiasAddr
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+                           2
Relu▒
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*A
_output_shapes/
-:+                           2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+                           ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:& "
 
_user_specified_nameinputs
┬
Ф
*__inference_conv2d_10_layer_call_fn_506984

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityѕбStatefulPartitionedCallА
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*A
_output_shapes/
-:+                           **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_10_layer_call_and_return_conditional_losses_5069762
StatefulPartitionedCallе
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+                           2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+                           ::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
╠
¤
-__inference_sequential_5_layer_call_fn_507681

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityѕбStatefulPartitionedCallЊ
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*Q
fLRJ
H__inference_sequential_5_layer_call_and_return_conditional_losses_5072962
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
╠
¤
-__inference_sequential_5_layer_call_fn_507666

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityѕбStatefulPartitionedCallЊ
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*Q
fLRJ
H__inference_sequential_5_layer_call_and_return_conditional_losses_5072612
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
ш
ф
)__inference_dense_10_layer_call_fn_507848

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityѕбStatefulPartitionedCallЄ
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         ђ**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_5071832
StatefulPartitionedCallЈ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         └::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
Б­
▒
H__inference_sequential_5_layer_call_and_return_conditional_losses_507492

inputs,
(conv2d_10_conv2d_readvariableop_resource-
)conv2d_10_biasadd_readvariableop_resource:
6top_pool_with_max_1_expanddims_readvariableop_resource3
/top_pool_with_max_1_add_readvariableop_resource,
(conv2d_11_conv2d_readvariableop_resource-
)conv2d_11_biasadd_readvariableop_resource+
'dense_10_matmul_readvariableop_resource,
(dense_10_biasadd_readvariableop_resource+
'dense_11_matmul_readvariableop_resource,
(dense_11_biasadd_readvariableop_resource
identityѕб conv2d_10/BiasAdd/ReadVariableOpбconv2d_10/Conv2D/ReadVariableOpб conv2d_11/BiasAdd/ReadVariableOpбconv2d_11/Conv2D/ReadVariableOpбdense_10/BiasAdd/ReadVariableOpбdense_10/MatMul/ReadVariableOpбdense_11/BiasAdd/ReadVariableOpбdense_11/MatMul/ReadVariableOpб-top_pool_with_max_1/ExpandDims/ReadVariableOpб&top_pool_with_max_1/add/ReadVariableOp│
conv2d_10/Conv2D/ReadVariableOpReadVariableOp(conv2d_10_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02!
conv2d_10/Conv2D/ReadVariableOp┬
conv2d_10/Conv2DConv2Dinputs'conv2d_10/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:         *
paddingVALID*
strides
2
conv2d_10/Conv2Dф
 conv2d_10/BiasAdd/ReadVariableOpReadVariableOp)conv2d_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02"
 conv2d_10/BiasAdd/ReadVariableOp░
conv2d_10/BiasAddBiasAddconv2d_10/Conv2D:output:0(conv2d_10/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:         2
conv2d_10/BiasAddх
 top_pool_with_max_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2"
 top_pool_with_max_1/Pad/paddings║
top_pool_with_max_1/PadPadconv2d_10/BiasAdd:output:0)top_pool_with_max_1/Pad/paddings:output:0*
T0*/
_output_shapes
:         2
top_pool_with_max_1/Padќ
'top_pool_with_max_1/ExtractImagePatchesExtractImagePatches top_pool_with_max_1/Pad:output:0*
T0*0
_output_shapes
:         

ѕ*
ksizes
*
paddingVALID*
rates
*
strides
2)
'top_pool_with_max_1/ExtractImagePatchesє
top_pool_with_max_1/ShapeShape top_pool_with_max_1/Pad:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/ShapeЏ
top_pool_with_max_1/Shape_1Shape1top_pool_with_max_1/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_1ю
'top_pool_with_max_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2)
'top_pool_with_max_1/strided_slice/stackа
)top_pool_with_max_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice/stack_1а
)top_pool_with_max_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice/stack_2┌
!top_pool_with_max_1/strided_sliceStridedSlice"top_pool_with_max_1/Shape:output:00top_pool_with_max_1/strided_slice/stack:output:02top_pool_with_max_1/strided_slice/stack_1:output:02top_pool_with_max_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2#
!top_pool_with_max_1/strided_slicex
top_pool_with_max_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_with_max_1/mul/yф
top_pool_with_max_1/mulMul*top_pool_with_max_1/strided_slice:output:0"top_pool_with_max_1/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/mul|
top_pool_with_max_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_with_max_1/mul_1/yА
top_pool_with_max_1/mul_1Multop_pool_with_max_1/mul:z:0$top_pool_with_max_1/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/mul_1ї
#top_pool_with_max_1/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2%
#top_pool_with_max_1/Reshape/shape/1ї
#top_pool_with_max_1/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2%
#top_pool_with_max_1/Reshape/shape/2ї
#top_pool_with_max_1/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2%
#top_pool_with_max_1/Reshape/shape/3Ц
!top_pool_with_max_1/Reshape/shapePacktop_pool_with_max_1/mul_1:z:0,top_pool_with_max_1/Reshape/shape/1:output:0,top_pool_with_max_1/Reshape/shape/2:output:0,top_pool_with_max_1/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2#
!top_pool_with_max_1/Reshape/shapeя
top_pool_with_max_1/ReshapeReshape1top_pool_with_max_1/ExtractImagePatches:patches:0*top_pool_with_max_1/Reshape/shape:output:0*
T0*/
_output_shapes
:         2
top_pool_with_max_1/Reshapeа
)top_pool_with_max_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)top_pool_with_max_1/strided_slice_1/stackц
+top_pool_with_max_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_1/stack_1ц
+top_pool_with_max_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_1/stack_2С
#top_pool_with_max_1/strided_slice_1StridedSlice"top_pool_with_max_1/Shape:output:02top_pool_with_max_1/strided_slice_1/stack:output:04top_pool_with_max_1/strided_slice_1/stack_1:output:04top_pool_with_max_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_1Џ
top_pool_with_max_1/Slice/beginConst*
_output_shapes
:*
dtype0*%
valueB"              2!
top_pool_with_max_1/Slice/beginє
 top_pool_with_max_1/Slice/size/1Const*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_with_max_1/Slice/size/1є
 top_pool_with_max_1/Slice/size/2Const*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_with_max_1/Slice/size/2є
 top_pool_with_max_1/Slice/size/3Const*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_with_max_1/Slice/size/3ќ
top_pool_with_max_1/Slice/sizePacktop_pool_with_max_1/mul_1:z:0)top_pool_with_max_1/Slice/size/1:output:0)top_pool_with_max_1/Slice/size/2:output:0)top_pool_with_max_1/Slice/size/3:output:0*
N*
T0*
_output_shapes
:2 
top_pool_with_max_1/Slice/size 
top_pool_with_max_1/SliceSlice$top_pool_with_max_1/Reshape:output:0(top_pool_with_max_1/Slice/begin:output:0'top_pool_with_max_1/Slice/size:output:0*
Index0*
T0*/
_output_shapes
:         2
top_pool_with_max_1/Sliceљ
%top_pool_with_max_1/Reshape_1/shape/1Const*
_output_shapes
: *
dtype0*
value	B :
2'
%top_pool_with_max_1/Reshape_1/shape/1љ
%top_pool_with_max_1/Reshape_1/shape/2Const*
_output_shapes
: *
dtype0*
value	B :
2'
%top_pool_with_max_1/Reshape_1/shape/2љ
%top_pool_with_max_1/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_1/shape/3љ
%top_pool_with_max_1/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_1/shape/4Ь
#top_pool_with_max_1/Reshape_1/shapePack,top_pool_with_max_1/strided_slice_1:output:0.top_pool_with_max_1/Reshape_1/shape/1:output:0.top_pool_with_max_1/Reshape_1/shape/2:output:0.top_pool_with_max_1/Reshape_1/shape/3:output:0.top_pool_with_max_1/Reshape_1/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_1/shape┘
top_pool_with_max_1/Reshape_1Reshape"top_pool_with_max_1/Slice:output:0,top_pool_with_max_1/Reshape_1/shape:output:0*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/Reshape_1Ю
)top_pool_with_max_1/ExtractImagePatches_1ExtractImagePatches$top_pool_with_max_1/Reshape:output:0*
T0*/
_output_shapes
:         H*
ksizes
*
paddingVALID*
rates
*
strides
2+
)top_pool_with_max_1/ExtractImagePatches_1Ю
top_pool_with_max_1/Shape_2Shape3top_pool_with_max_1/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_2ј
top_pool_with_max_1/Shape_3Shape$top_pool_with_max_1/Reshape:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_3а
)top_pool_with_max_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)top_pool_with_max_1/strided_slice_2/stackц
+top_pool_with_max_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_2/stack_1ц
+top_pool_with_max_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_2/stack_2Т
#top_pool_with_max_1/strided_slice_2StridedSlice$top_pool_with_max_1/Shape_3:output:02top_pool_with_max_1/strided_slice_2/stack:output:04top_pool_with_max_1/strided_slice_2/stack_1:output:04top_pool_with_max_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_2а
)top_pool_with_max_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_3/stackц
+top_pool_with_max_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_3/stack_1ц
+top_pool_with_max_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_3/stack_2Т
#top_pool_with_max_1/strided_slice_3StridedSlice$top_pool_with_max_1/Shape_2:output:02top_pool_with_max_1/strided_slice_3/stack:output:04top_pool_with_max_1/strided_slice_3/stack_1:output:04top_pool_with_max_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_3а
)top_pool_with_max_1/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_4/stackц
+top_pool_with_max_1/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_4/stack_1ц
+top_pool_with_max_1/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_4/stack_2Т
#top_pool_with_max_1/strided_slice_4StridedSlice$top_pool_with_max_1/Shape_2:output:02top_pool_with_max_1/strided_slice_4/stack:output:04top_pool_with_max_1/strided_slice_4/stack_1:output:04top_pool_with_max_1/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_4љ
%top_pool_with_max_1/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_2/shape/3љ
%top_pool_with_max_1/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_2/shape/4љ
%top_pool_with_max_1/Reshape_2/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_2/shape/5џ
#top_pool_with_max_1/Reshape_2/shapePack,top_pool_with_max_1/strided_slice_2:output:0,top_pool_with_max_1/strided_slice_3:output:0,top_pool_with_max_1/strided_slice_4:output:0.top_pool_with_max_1/Reshape_2/shape/3:output:0.top_pool_with_max_1/Reshape_2/shape/4:output:0.top_pool_with_max_1/Reshape_2/shape/5:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_2/shapeђ
top_pool_with_max_1/Reshape_2Reshape3top_pool_with_max_1/ExtractImagePatches_1:patches:0,top_pool_with_max_1/Reshape_2/shape:output:0*
T0*I
_output_shapes7
5:3                           2
top_pool_with_max_1/Reshape_2љ
top_pool_with_max_1/Shape_4Shape&top_pool_with_max_1/Reshape_2:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_4а
)top_pool_with_max_1/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB: 2+
)top_pool_with_max_1/strided_slice_5/stackц
+top_pool_with_max_1/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_5/stack_1ц
+top_pool_with_max_1/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_5/stack_2Т
#top_pool_with_max_1/strided_slice_5StridedSlice$top_pool_with_max_1/Shape_4:output:02top_pool_with_max_1/strided_slice_5/stack:output:04top_pool_with_max_1/strided_slice_5/stack_1:output:04top_pool_with_max_1/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_5а
)top_pool_with_max_1/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_6/stackц
+top_pool_with_max_1/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_6/stack_1ц
+top_pool_with_max_1/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_6/stack_2Т
#top_pool_with_max_1/strided_slice_6StridedSlice$top_pool_with_max_1/Shape_4:output:02top_pool_with_max_1/strided_slice_6/stack:output:04top_pool_with_max_1/strided_slice_6/stack_1:output:04top_pool_with_max_1/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_6а
)top_pool_with_max_1/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*
valueB:2+
)top_pool_with_max_1/strided_slice_7/stackц
+top_pool_with_max_1/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_7/stack_1ц
+top_pool_with_max_1/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2-
+top_pool_with_max_1/strided_slice_7/stack_2Т
#top_pool_with_max_1/strided_slice_7StridedSlice$top_pool_with_max_1/Shape_4:output:02top_pool_with_max_1/strided_slice_7/stack:output:04top_pool_with_max_1/strided_slice_7/stack_1:output:04top_pool_with_max_1/strided_slice_7/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_7љ
%top_pool_with_max_1/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2'
%top_pool_with_max_1/Reshape_3/shape/3љ
%top_pool_with_max_1/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_3/shape/4Ж
#top_pool_with_max_1/Reshape_3/shapePack,top_pool_with_max_1/strided_slice_5:output:0,top_pool_with_max_1/strided_slice_6:output:0,top_pool_with_max_1/strided_slice_7:output:0.top_pool_with_max_1/Reshape_3/shape/3:output:0.top_pool_with_max_1/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_3/shape№
top_pool_with_max_1/Reshape_3Reshape&top_pool_with_max_1/Reshape_2:output:0,top_pool_with_max_1/Reshape_3/shape:output:0*
T0*E
_output_shapes3
1:/                           	2
top_pool_with_max_1/Reshape_3ђ
top_pool_with_max_1/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_with_max_1/sort/axis╗
top_pool_with_max_1/sort/NegNeg&top_pool_with_max_1/Reshape_3:output:0*
T0*E
_output_shapes3
1:/                           	2
top_pool_with_max_1/sort/Negљ
top_pool_with_max_1/sort/ShapeShape top_pool_with_max_1/sort/Neg:y:0*
T0*
_output_shapes
:2 
top_pool_with_max_1/sort/Shapeд
,top_pool_with_max_1/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2.
,top_pool_with_max_1/sort/strided_slice/stackф
.top_pool_with_max_1/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:20
.top_pool_with_max_1/sort/strided_slice/stack_1ф
.top_pool_with_max_1/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:20
.top_pool_with_max_1/sort/strided_slice/stack_2Э
&top_pool_with_max_1/sort/strided_sliceStridedSlice'top_pool_with_max_1/sort/Shape:output:05top_pool_with_max_1/sort/strided_slice/stack:output:07top_pool_with_max_1/sort/strided_slice/stack_1:output:07top_pool_with_max_1/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2(
&top_pool_with_max_1/sort/strided_sliceђ
top_pool_with_max_1/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_with_max_1/sort/Rank┴
&top_pool_with_max_1/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2(
&top_pool_with_max_1/sort/transpositionЁ
"top_pool_with_max_1/sort/transpose	Transpose top_pool_with_max_1/sort/Neg:y:0/top_pool_with_max_1/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/                           	2$
"top_pool_with_max_1/sort/transpose╣
top_pool_with_max_1/sort/TopKV2TopKV2&top_pool_with_max_1/sort/transpose:y:0/top_pool_with_max_1/sort/strided_slice:output:0*
T0*ѕ
_output_shapesv
t:8                                    :8                                    2!
top_pool_with_max_1/sort/TopKV2џ
$top_pool_with_max_1/sort/transpose_1	Transpose(top_pool_with_max_1/sort/TopKV2:values:0/top_pool_with_max_1/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8                                    2&
$top_pool_with_max_1/sort/transpose_1╩
top_pool_with_max_1/sort/Neg_1Neg(top_pool_with_max_1/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8                                    2 
top_pool_with_max_1/sort/Neg_1│
)top_pool_with_max_1/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2+
)top_pool_with_max_1/strided_slice_8/stackи
+top_pool_with_max_1/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2-
+top_pool_with_max_1/strided_slice_8/stack_1и
+top_pool_with_max_1/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2-
+top_pool_with_max_1/strided_slice_8/stack_2▒
#top_pool_with_max_1/strided_slice_8StridedSlice"top_pool_with_max_1/sort/Neg_1:y:02top_pool_with_max_1/strided_slice_8/stack:output:04top_pool_with_max_1/strided_slice_8/stack_1:output:04top_pool_with_max_1/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_8│
)top_pool_with_max_1/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2+
)top_pool_with_max_1/strided_slice_9/stackи
+top_pool_with_max_1/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2-
+top_pool_with_max_1/strided_slice_9/stack_1и
+top_pool_with_max_1/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2-
+top_pool_with_max_1/strided_slice_9/stack_2▒
#top_pool_with_max_1/strided_slice_9StridedSlice"top_pool_with_max_1/sort/Neg_1:y:02top_pool_with_max_1/strided_slice_9/stack:output:04top_pool_with_max_1/strided_slice_9/stack_1:output:04top_pool_with_max_1/strided_slice_9/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2%
#top_pool_with_max_1/strided_slice_9р
top_pool_with_max_1/subSub,top_pool_with_max_1/strided_slice_9:output:0,top_pool_with_max_1/strided_slice_8:output:0*
T0*A
_output_shapes/
-:+                           2
top_pool_with_max_1/sub
top_pool_with_max_1/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_with_max_1/Floor/xє
top_pool_with_max_1/FloorFloor$top_pool_with_max_1/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/FloorІ
top_pool_with_max_1/CastCasttop_pool_with_max_1/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_with_max_1/CastЃ
top_pool_with_max_1/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_with_max_1/Floor_1/xї
top_pool_with_max_1/Floor_1Floor&top_pool_with_max_1/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_with_max_1/Floor_1Љ
top_pool_with_max_1/Cast_1Casttop_pool_with_max_1/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_with_max_1/Cast_1Ё
top_pool_with_max_1/Shape_5Shapetop_pool_with_max_1/sub:z:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_5ё
top_pool_with_max_1/Shape_6Shapeconv2d_10/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool_with_max_1/Shape_6б
*top_pool_with_max_1/strided_slice_10/stackConst*
_output_shapes
:*
dtype0*
valueB: 2,
*top_pool_with_max_1/strided_slice_10/stackд
,top_pool_with_max_1/strided_slice_10/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2.
,top_pool_with_max_1/strided_slice_10/stack_1д
,top_pool_with_max_1/strided_slice_10/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2.
,top_pool_with_max_1/strided_slice_10/stack_2в
$top_pool_with_max_1/strided_slice_10StridedSlice$top_pool_with_max_1/Shape_6:output:03top_pool_with_max_1/strided_slice_10/stack:output:05top_pool_with_max_1/strided_slice_10/stack_1:output:05top_pool_with_max_1/strided_slice_10/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2&
$top_pool_with_max_1/strided_slice_10љ
%top_pool_with_max_1/Reshape_4/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_4/shape/3љ
%top_pool_with_max_1/Reshape_4/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2'
%top_pool_with_max_1/Reshape_4/shape/4═
#top_pool_with_max_1/Reshape_4/shapePack-top_pool_with_max_1/strided_slice_10:output:0top_pool_with_max_1/Cast:y:0top_pool_with_max_1/Cast_1:y:0.top_pool_with_max_1/Reshape_4/shape/3:output:0.top_pool_with_max_1/Reshape_4/shape/4:output:0*
N*
T0*
_output_shapes
:2%
#top_pool_with_max_1/Reshape_4/shapeм
top_pool_with_max_1/Reshape_4Reshapetop_pool_with_max_1/sub:z:0,top_pool_with_max_1/Reshape_4/shape:output:0*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/Reshape_4ў
)top_pool_with_max_1/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2+
)top_pool_with_max_1/Max/reduction_indicesС
top_pool_with_max_1/MaxMax&top_pool_with_max_1/Reshape_1:output:02top_pool_with_max_1/Max/reduction_indices:output:0*
T0*3
_output_shapes!
:         

*
	keep_dims(2
top_pool_with_max_1/Maxё
top_pool_with_max_1/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2!
top_pool_with_max_1/concat/axis 
top_pool_with_max_1/concatConcatV2&top_pool_with_max_1/Reshape_4:output:0 top_pool_with_max_1/Max:output:0(top_pool_with_max_1/concat/axis:output:0*
N*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/concatН
-top_pool_with_max_1/ExpandDims/ReadVariableOpReadVariableOp6top_pool_with_max_1_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02/
-top_pool_with_max_1/ExpandDims/ReadVariableOpі
"top_pool_with_max_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2$
"top_pool_with_max_1/ExpandDims/dim▀
top_pool_with_max_1/ExpandDims
ExpandDims5top_pool_with_max_1/ExpandDims/ReadVariableOp:value:0+top_pool_with_max_1/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2 
top_pool_with_max_1/ExpandDimsј
$top_pool_with_max_1/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$top_pool_with_max_1/ExpandDims_1/dim█
 top_pool_with_max_1/ExpandDims_1
ExpandDims'top_pool_with_max_1/ExpandDims:output:0-top_pool_with_max_1/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2"
 top_pool_with_max_1/ExpandDims_1ј
$top_pool_with_max_1/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2&
$top_pool_with_max_1/ExpandDims_2/dimр
 top_pool_with_max_1/ExpandDims_2
ExpandDims)top_pool_with_max_1/ExpandDims_1:output:0-top_pool_with_max_1/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2"
 top_pool_with_max_1/ExpandDims_2╦
top_pool_with_max_1/mul_2Mul#top_pool_with_max_1/concat:output:0)top_pool_with_max_1/ExpandDims_2:output:0*
T0*3
_output_shapes!
:         

2
top_pool_with_max_1/mul_2ў
)top_pool_with_max_1/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2+
)top_pool_with_max_1/Sum/reduction_indicesк
top_pool_with_max_1/SumSumtop_pool_with_max_1/mul_2:z:02top_pool_with_max_1/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         

2
top_pool_with_max_1/Sum╚
&top_pool_with_max_1/add/ReadVariableOpReadVariableOp/top_pool_with_max_1_add_readvariableop_resource*&
_output_shapes
:*
dtype02(
&top_pool_with_max_1/add/ReadVariableOpК
top_pool_with_max_1/addAddV2 top_pool_with_max_1/Sum:output:0.top_pool_with_max_1/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:         

2
top_pool_with_max_1/add│
conv2d_11/Conv2D/ReadVariableOpReadVariableOp(conv2d_11_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02!
conv2d_11/Conv2D/ReadVariableOpО
conv2d_11/Conv2DConv2Dtop_pool_with_max_1/add:z:0'conv2d_11/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:         *
paddingVALID*
strides
2
conv2d_11/Conv2Dф
 conv2d_11/BiasAdd/ReadVariableOpReadVariableOp)conv2d_11_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02"
 conv2d_11/BiasAdd/ReadVariableOp░
conv2d_11/BiasAddBiasAddconv2d_11/Conv2D:output:0(conv2d_11/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:         2
conv2d_11/BiasAdd~
conv2d_11/ReluReluconv2d_11/BiasAdd:output:0*
T0*/
_output_shapes
:         2
conv2d_11/Relus
flatten_5/ConstConst*
_output_shapes
:*
dtype0*
valueB"    @  2
flatten_5/Constю
flatten_5/ReshapeReshapeconv2d_11/Relu:activations:0flatten_5/Const:output:0*
T0*(
_output_shapes
:         └2
flatten_5/Reshapeф
dense_10/MatMul/ReadVariableOpReadVariableOp'dense_10_matmul_readvariableop_resource* 
_output_shapes
:
└ђ*
dtype02 
dense_10/MatMul/ReadVariableOpБ
dense_10/MatMulMatMulflatten_5/Reshape:output:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_10/MatMulе
dense_10/BiasAdd/ReadVariableOpReadVariableOp(dense_10_biasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02!
dense_10/BiasAdd/ReadVariableOpд
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
dense_10/BiasAddЕ
dense_11/MatMul/ReadVariableOpReadVariableOp'dense_11_matmul_readvariableop_resource*
_output_shapes
:	ђ
*
dtype02 
dense_11/MatMul/ReadVariableOpА
dense_11/MatMulMatMuldense_10/BiasAdd:output:0&dense_11/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
dense_11/MatMulД
dense_11/BiasAdd/ReadVariableOpReadVariableOp(dense_11_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_11/BiasAdd/ReadVariableOpЦ
dense_11/BiasAddBiasAdddense_11/MatMul:product:0'dense_11/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
dense_11/BiasAddо
IdentityIdentitydense_11/BiasAdd:output:0!^conv2d_10/BiasAdd/ReadVariableOp ^conv2d_10/Conv2D/ReadVariableOp!^conv2d_11/BiasAdd/ReadVariableOp ^conv2d_11/Conv2D/ReadVariableOp ^dense_10/BiasAdd/ReadVariableOp^dense_10/MatMul/ReadVariableOp ^dense_11/BiasAdd/ReadVariableOp^dense_11/MatMul/ReadVariableOp.^top_pool_with_max_1/ExpandDims/ReadVariableOp'^top_pool_with_max_1/add/ReadVariableOp*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::2D
 conv2d_10/BiasAdd/ReadVariableOp conv2d_10/BiasAdd/ReadVariableOp2B
conv2d_10/Conv2D/ReadVariableOpconv2d_10/Conv2D/ReadVariableOp2D
 conv2d_11/BiasAdd/ReadVariableOp conv2d_11/BiasAdd/ReadVariableOp2B
conv2d_11/Conv2D/ReadVariableOpconv2d_11/Conv2D/ReadVariableOp2B
dense_10/BiasAdd/ReadVariableOpdense_10/BiasAdd/ReadVariableOp2@
dense_10/MatMul/ReadVariableOpdense_10/MatMul/ReadVariableOp2B
dense_11/BiasAdd/ReadVariableOpdense_11/BiasAdd/ReadVariableOp2@
dense_11/MatMul/ReadVariableOpdense_11/MatMul/ReadVariableOp2^
-top_pool_with_max_1/ExpandDims/ReadVariableOp-top_pool_with_max_1/ExpandDims/ReadVariableOp2P
&top_pool_with_max_1/add/ReadVariableOp&top_pool_with_max_1/add/ReadVariableOp:& "
 
_user_specified_nameinputs
ї
a
E__inference_flatten_5_layer_call_and_return_conditional_losses_507826

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"    @  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:         └2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:         └2

Identity"
identityIdentity:output:0*.
_input_shapes
:         :& "
 
_user_specified_nameinputs
З

я
E__inference_conv2d_10_layer_call_and_return_conditional_losses_506976

inputs"
conv2d_readvariableop_resource#
biasadd_readvariableop_resource
identityѕбBiasAdd/ReadVariableOpбConv2D/ReadVariableOpo
dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      2
dilation_rateЋ
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
Conv2D/ReadVariableOpХ
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+                           *
paddingVALID*
strides
2
Conv2Dї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpџ
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+                           2	
BiasAdd»
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*A
_output_shapes/
-:+                           2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+                           ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:& "
 
_user_specified_nameinputs
Ї"
ђ
H__inference_sequential_5_layer_call_and_return_conditional_losses_507238
input_6,
(conv2d_10_statefulpartitionedcall_args_1,
(conv2d_10_statefulpartitionedcall_args_26
2top_pool_with_max_1_statefulpartitionedcall_args_16
2top_pool_with_max_1_statefulpartitionedcall_args_2,
(conv2d_11_statefulpartitionedcall_args_1,
(conv2d_11_statefulpartitionedcall_args_2+
'dense_10_statefulpartitionedcall_args_1+
'dense_10_statefulpartitionedcall_args_2+
'dense_11_statefulpartitionedcall_args_1+
'dense_11_statefulpartitionedcall_args_2
identityѕб!conv2d_10/StatefulPartitionedCallб!conv2d_11/StatefulPartitionedCallб dense_10/StatefulPartitionedCallб dense_11/StatefulPartitionedCallб+top_pool_with_max_1/StatefulPartitionedCallИ
!conv2d_10/StatefulPartitionedCallStatefulPartitionedCallinput_6(conv2d_10_statefulpartitionedcall_args_1(conv2d_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_10_layer_call_and_return_conditional_losses_5069762#
!conv2d_10/StatefulPartitionedCallЇ
+top_pool_with_max_1/StatefulPartitionedCallStatefulPartitionedCall*conv2d_10/StatefulPartitionedCall:output:02top_pool_with_max_1_statefulpartitionedcall_args_12top_pool_with_max_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         

**
config_proto

CPU

GPU 2J 8*X
fSRQ
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_5071442-
+top_pool_with_max_1/StatefulPartitionedCallт
!conv2d_11/StatefulPartitionedCallStatefulPartitionedCall4top_pool_with_max_1/StatefulPartitionedCall:output:0(conv2d_11_statefulpartitionedcall_args_1(conv2d_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_11_layer_call_and_return_conditional_losses_5069972#
!conv2d_11/StatefulPartitionedCallТ
flatten_5/PartitionedCallPartitionedCall*conv2d_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         └**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_flatten_5_layer_call_and_return_conditional_losses_5071652
flatten_5/PartitionedCallК
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_5/PartitionedCall:output:0'dense_10_statefulpartitionedcall_args_1'dense_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         ђ**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_5071832"
 dense_10/StatefulPartitionedCall═
 dense_11/StatefulPartitionedCallStatefulPartitionedCall)dense_10/StatefulPartitionedCall:output:0'dense_11_statefulpartitionedcall_args_1'dense_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_5072052"
 dense_11/StatefulPartitionedCall╣
IdentityIdentity)dense_11/StatefulPartitionedCall:output:0"^conv2d_10/StatefulPartitionedCall"^conv2d_11/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall,^top_pool_with_max_1/StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::2F
!conv2d_10/StatefulPartitionedCall!conv2d_10/StatefulPartitionedCall2F
!conv2d_11/StatefulPartitionedCall!conv2d_11/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall2Z
+top_pool_with_max_1/StatefulPartitionedCall+top_pool_with_max_1/StatefulPartitionedCall:' #
!
_user_specified_name	input_6
Ъ
К
$__inference_signature_wrapper_507333
input_6"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityѕбStatefulPartitionedCallь
StatefulPartitionedCallStatefulPartitionedCallinput_6statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8**
f%R#
!__inference__wrapped_model_5069642
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_6
▀
F
*__inference_flatten_5_layer_call_fn_507831

inputs
identity«
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         └**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_flatten_5_layer_call_and_return_conditional_losses_5071652
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:         └2

Identity"
identityIdentity:output:0*.
_input_shapes
:         :& "
 
_user_specified_nameinputs
бї
С
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_507144
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identityѕбExpandDims/ReadVariableOpбadd/ReadVariableOpЇ
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddingsf
PadPadinPad/paddings:output:0*
T0*/
_output_shapes
:         2
Pad┌
ExtractImagePatchesExtractImagePatchesPad:output:0*
T0*0
_output_shapes
:         

ѕ*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatchesJ
ShapeShapePad:output:0*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2Р
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3Г
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapeј
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:         2	
Reshapex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2В
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1s
Slice/beginConst*
_output_shapes
:*
dtype0*%
valueB"              2
Slice/begin^
Slice/size/1Const*
_output_shapes
: *
dtype0*
value	B :2
Slice/size/1^
Slice/size/2Const*
_output_shapes
: *
dtype0*
value	B :2
Slice/size/2^
Slice/size/3Const*
_output_shapes
: *
dtype0*
value	B :2
Slice/size/3ъ

Slice/sizePack	mul_1:z:0Slice/size/1:output:0Slice/size/2:output:0Slice/size/3:output:0*
N*
T0*
_output_shapes
:2

Slice/sizeЏ
SliceSliceReshape:output:0Slice/begin:output:0Slice/size:output:0*
Index0*
T0*/
_output_shapes
:         2
Sliceh
Reshape_1/shape/1Const*
_output_shapes
: *
dtype0*
value	B :
2
Reshape_1/shape/1h
Reshape_1/shape/2Const*
_output_shapes
: *
dtype0*
value	B :
2
Reshape_1/shape/2h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4Р
Reshape_1/shapePackstrided_slice_1:output:0Reshape_1/shape/1:output:0Reshape_1/shape/2:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shapeЅ
	Reshape_1ReshapeSlice:output:0Reshape_1/shape:output:0*
T0*3
_output_shapes!
:         

2
	Reshape_1р
ExtractImagePatches_1ExtractImagePatchesReshape:output:0*
T0*/
_output_shapes
:         H*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2R
Shape_3ShapeReshape:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2Ь
strided_slice_2StridedSliceShape_3:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2Ь
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2Ь
strided_slice_4StridedSliceShape_2:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4h
Reshape_2/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/5Щ
Reshape_2/shapePackstrided_slice_2:output:0strided_slice_3:output:0strided_slice_4:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0Reshape_2/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape░
	Reshape_2ReshapeExtractImagePatches_1:patches:0Reshape_2/shape:output:0*
T0*I
_output_shapes7
5:3                           2
	Reshape_2T
Shape_4ShapeReshape_2:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2Ь
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2Ь
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6x
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_7/stack|
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_7/stack_1|
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_7/stack_2Ь
strided_slice_7StridedSliceShape_4:output:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_7h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4я
Reshape_3/shapePackstrided_slice_5:output:0strided_slice_6:output:0strided_slice_7:output:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shapeЪ
	Reshape_3ReshapeReshape_2:output:0Reshape_3/shape:output:0*
T0*E
_output_shapes3
1:/                           	2
	Reshape_3X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_3:output:0*
T0*E
_output_shapes3
1:/                           	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stackѓ
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1ѓ
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2ђ
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/RankЎ
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transpositionх
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/                           	2
sort/transposeж
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*ѕ
_output_shapesv
t:8                                    :8                                    2
sort/TopKV2╩
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8                                    2
sort/transpose_1ј

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8                                    2

sort/Neg_1І
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stackЈ
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1Ј
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2╣
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8І
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_9/stackЈ
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_9/stack_1Ј
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_9/stack_2╣
strided_slice_9StridedSlicesort/Neg_1:y:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_9Љ
subSubstrided_slice_9:output:0strided_slice_8:output:0*
T0*A
_output_shapes/
-:+                           2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6z
strided_slice_10/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_10/stack~
strided_slice_10/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_10/stack_1~
strided_slice_10/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_10/stack_2з
strided_slice_10StridedSliceShape_6:output:0strided_slice_10/stack:output:0!strided_slice_10/stack_1:output:0!strided_slice_10/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_10h
Reshape_4/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_4/shape/3h
Reshape_4/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_4/shape/4┴
Reshape_4/shapePackstrided_slice_10:output:0Cast:y:0
Cast_1:y:0Reshape_4/shape/3:output:0Reshape_4/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_4/shapeѓ
	Reshape_4Reshapesub:z:0Reshape_4/shape:output:0*
T0*3
_output_shapes!
:         

2
	Reshape_4p
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Max/reduction_indicesћ
MaxMaxReshape_1:output:0Max/reduction_indices:output:0*
T0*3
_output_shapes!
:         

*
	keep_dims(2
Max\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axisЏ
concatConcatV2Reshape_4:output:0Max:output:0concat/axis:output:0*
N*
T0*3
_output_shapes!
:         

2
concatЎ
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dimЈ

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dimІ
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dimЉ
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2{
mul_2Mulconcat:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:         

2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         

2
Sumї
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:         

2
addћ
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:         

2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:         ::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
¤
л
-__inference_sequential_5_layer_call_fn_507309
input_6"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityѕбStatefulPartitionedCallћ
StatefulPartitionedCallStatefulPartitionedCallinput_6statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*Q
fLRJ
H__inference_sequential_5_layer_call_and_return_conditional_losses_5072962
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_6
бї
С
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_507813
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identityѕбExpandDims/ReadVariableOpбadd/ReadVariableOpЇ
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddingsf
PadPadinPad/paddings:output:0*
T0*/
_output_shapes
:         2
Pad┌
ExtractImagePatchesExtractImagePatchesPad:output:0*
T0*0
_output_shapes
:         

ѕ*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatchesJ
ShapeShapePad:output:0*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2Р
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3Г
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapeј
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:         2	
Reshapex
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2В
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1s
Slice/beginConst*
_output_shapes
:*
dtype0*%
valueB"              2
Slice/begin^
Slice/size/1Const*
_output_shapes
: *
dtype0*
value	B :2
Slice/size/1^
Slice/size/2Const*
_output_shapes
: *
dtype0*
value	B :2
Slice/size/2^
Slice/size/3Const*
_output_shapes
: *
dtype0*
value	B :2
Slice/size/3ъ

Slice/sizePack	mul_1:z:0Slice/size/1:output:0Slice/size/2:output:0Slice/size/3:output:0*
N*
T0*
_output_shapes
:2

Slice/sizeЏ
SliceSliceReshape:output:0Slice/begin:output:0Slice/size:output:0*
Index0*
T0*/
_output_shapes
:         2
Sliceh
Reshape_1/shape/1Const*
_output_shapes
: *
dtype0*
value	B :
2
Reshape_1/shape/1h
Reshape_1/shape/2Const*
_output_shapes
: *
dtype0*
value	B :
2
Reshape_1/shape/2h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4Р
Reshape_1/shapePackstrided_slice_1:output:0Reshape_1/shape/1:output:0Reshape_1/shape/2:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shapeЅ
	Reshape_1ReshapeSlice:output:0Reshape_1/shape:output:0*
T0*3
_output_shapes!
:         

2
	Reshape_1р
ExtractImagePatches_1ExtractImagePatchesReshape:output:0*
T0*/
_output_shapes
:         H*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2R
Shape_3ShapeReshape:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2Ь
strided_slice_2StridedSliceShape_3:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2Ь
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2Ь
strided_slice_4StridedSliceShape_2:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4h
Reshape_2/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/5Щ
Reshape_2/shapePackstrided_slice_2:output:0strided_slice_3:output:0strided_slice_4:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0Reshape_2/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape░
	Reshape_2ReshapeExtractImagePatches_1:patches:0Reshape_2/shape:output:0*
T0*I
_output_shapes7
5:3                           2
	Reshape_2T
Shape_4ShapeReshape_2:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2Ь
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2Ь
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6x
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_7/stack|
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_7/stack_1|
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_7/stack_2Ь
strided_slice_7StridedSliceShape_4:output:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_7h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4я
Reshape_3/shapePackstrided_slice_5:output:0strided_slice_6:output:0strided_slice_7:output:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shapeЪ
	Reshape_3ReshapeReshape_2:output:0Reshape_3/shape:output:0*
T0*E
_output_shapes3
1:/                           	2
	Reshape_3X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_3:output:0*
T0*E
_output_shapes3
1:/                           	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stackѓ
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1ѓ
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2ђ
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/RankЎ
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transpositionх
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/                           	2
sort/transposeж
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*ѕ
_output_shapesv
t:8                                    :8                                    2
sort/TopKV2╩
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8                                    2
sort/transpose_1ј

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8                                    2

sort/Neg_1І
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stackЈ
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1Ј
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2╣
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8І
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_9/stackЈ
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_9/stack_1Ј
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_9/stack_2╣
strided_slice_9StridedSlicesort/Neg_1:y:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+                           *

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_9Љ
subSubstrided_slice_9:output:0strided_slice_8:output:0*
T0*A
_output_shapes/
-:+                           2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6z
strided_slice_10/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_10/stack~
strided_slice_10/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_10/stack_1~
strided_slice_10/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_10/stack_2з
strided_slice_10StridedSliceShape_6:output:0strided_slice_10/stack:output:0!strided_slice_10/stack_1:output:0!strided_slice_10/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_10h
Reshape_4/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_4/shape/3h
Reshape_4/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_4/shape/4┴
Reshape_4/shapePackstrided_slice_10:output:0Cast:y:0
Cast_1:y:0Reshape_4/shape/3:output:0Reshape_4/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_4/shapeѓ
	Reshape_4Reshapesub:z:0Reshape_4/shape:output:0*
T0*3
_output_shapes!
:         

2
	Reshape_4p
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Max/reduction_indicesћ
MaxMaxReshape_1:output:0Max/reduction_indices:output:0*
T0*3
_output_shapes!
:         

*
	keep_dims(2
Max\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axisЏ
concatConcatV2Reshape_4:output:0Max:output:0concat/axis:output:0*
N*
T0*3
_output_shapes!
:         

2
concatЎ
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dimЈ

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dimІ
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dimЉ
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2{
mul_2Mulconcat:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:         

2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         

2
Sumї
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:         

2
addћ
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:         

2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:         ::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
ћ
▒
4__inference_top_pool_with_max_1_layer_call_fn_507820
in"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityѕбStatefulPartitionedCallЋ
StatefulPartitionedCallStatefulPartitionedCallinstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         

**
config_proto

CPU

GPU 2J 8*X
fSRQ
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_5071442
StatefulPartitionedCallќ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:         

2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:         ::22
StatefulPartitionedCallStatefulPartitionedCall:" 

_user_specified_nameIn
з
ф
)__inference_dense_11_layer_call_fn_507865

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityѕбStatefulPartitionedCallє
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_5072052
StatefulPartitionedCallј
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
в
П
D__inference_dense_11_layer_call_and_return_conditional_losses_507858

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpј
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	ђ
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         
2	
BiasAddЋ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*/
_input_shapes
:         ђ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
┬
Ф
*__inference_conv2d_11_layer_call_fn_507005

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityѕбStatefulPartitionedCallА
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*A
_output_shapes/
-:+                           **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_11_layer_call_and_return_conditional_losses_5069972
StatefulPartitionedCallе
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+                           2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+                           ::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
Ї"
ђ
H__inference_sequential_5_layer_call_and_return_conditional_losses_507218
input_6,
(conv2d_10_statefulpartitionedcall_args_1,
(conv2d_10_statefulpartitionedcall_args_26
2top_pool_with_max_1_statefulpartitionedcall_args_16
2top_pool_with_max_1_statefulpartitionedcall_args_2,
(conv2d_11_statefulpartitionedcall_args_1,
(conv2d_11_statefulpartitionedcall_args_2+
'dense_10_statefulpartitionedcall_args_1+
'dense_10_statefulpartitionedcall_args_2+
'dense_11_statefulpartitionedcall_args_1+
'dense_11_statefulpartitionedcall_args_2
identityѕб!conv2d_10/StatefulPartitionedCallб!conv2d_11/StatefulPartitionedCallб dense_10/StatefulPartitionedCallб dense_11/StatefulPartitionedCallб+top_pool_with_max_1/StatefulPartitionedCallИ
!conv2d_10/StatefulPartitionedCallStatefulPartitionedCallinput_6(conv2d_10_statefulpartitionedcall_args_1(conv2d_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_10_layer_call_and_return_conditional_losses_5069762#
!conv2d_10/StatefulPartitionedCallЇ
+top_pool_with_max_1/StatefulPartitionedCallStatefulPartitionedCall*conv2d_10/StatefulPartitionedCall:output:02top_pool_with_max_1_statefulpartitionedcall_args_12top_pool_with_max_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         

**
config_proto

CPU

GPU 2J 8*X
fSRQ
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_5071442-
+top_pool_with_max_1/StatefulPartitionedCallт
!conv2d_11/StatefulPartitionedCallStatefulPartitionedCall4top_pool_with_max_1/StatefulPartitionedCall:output:0(conv2d_11_statefulpartitionedcall_args_1(conv2d_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:         **
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_conv2d_11_layer_call_and_return_conditional_losses_5069972#
!conv2d_11/StatefulPartitionedCallТ
flatten_5/PartitionedCallPartitionedCall*conv2d_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         └**
config_proto

CPU

GPU 2J 8*N
fIRG
E__inference_flatten_5_layer_call_and_return_conditional_losses_5071652
flatten_5/PartitionedCallК
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_5/PartitionedCall:output:0'dense_10_statefulpartitionedcall_args_1'dense_10_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:         ђ**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_5071832"
 dense_10/StatefulPartitionedCall═
 dense_11/StatefulPartitionedCallStatefulPartitionedCall)dense_10/StatefulPartitionedCall:output:0'dense_11_statefulpartitionedcall_args_1'dense_11_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:         
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_5072052"
 dense_11/StatefulPartitionedCall╣
IdentityIdentity)dense_11/StatefulPartitionedCall:output:0"^conv2d_10/StatefulPartitionedCall"^conv2d_11/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall,^top_pool_with_max_1/StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:         ::::::::::2F
!conv2d_10/StatefulPartitionedCall!conv2d_10/StatefulPartitionedCall2F
!conv2d_11/StatefulPartitionedCall!conv2d_11/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall2Z
+top_pool_with_max_1/StatefulPartitionedCall+top_pool_with_max_1/StatefulPartitionedCall:' #
!
_user_specified_name	input_6
ї
a
E__inference_flatten_5_layer_call_and_return_conditional_losses_507165

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"    @  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:         └2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:         └2

Identity"
identityIdentity:output:0*.
_input_shapes
:         :& "
 
_user_specified_nameinputs
ЫJ
Ы
__inference__traced_save_508000
file_prefix/
+savev2_conv2d_10_kernel_read_readvariableop-
)savev2_conv2d_10_bias_read_readvariableop4
0savev2_top_pool_with_max_1_w_read_readvariableop4
0savev2_top_pool_with_max_1_b_read_readvariableop/
+savev2_conv2d_11_kernel_read_readvariableop-
)savev2_conv2d_11_bias_read_readvariableop.
*savev2_dense_10_kernel_read_readvariableop,
(savev2_dense_10_bias_read_readvariableop.
*savev2_dense_11_kernel_read_readvariableop,
(savev2_dense_11_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop6
2savev2_adam_conv2d_10_kernel_m_read_readvariableop4
0savev2_adam_conv2d_10_bias_m_read_readvariableop;
7savev2_adam_top_pool_with_max_1_w_m_read_readvariableop;
7savev2_adam_top_pool_with_max_1_b_m_read_readvariableop6
2savev2_adam_conv2d_11_kernel_m_read_readvariableop4
0savev2_adam_conv2d_11_bias_m_read_readvariableop5
1savev2_adam_dense_10_kernel_m_read_readvariableop3
/savev2_adam_dense_10_bias_m_read_readvariableop5
1savev2_adam_dense_11_kernel_m_read_readvariableop3
/savev2_adam_dense_11_bias_m_read_readvariableop6
2savev2_adam_conv2d_10_kernel_v_read_readvariableop4
0savev2_adam_conv2d_10_bias_v_read_readvariableop;
7savev2_adam_top_pool_with_max_1_w_v_read_readvariableop;
7savev2_adam_top_pool_with_max_1_b_v_read_readvariableop6
2savev2_adam_conv2d_11_kernel_v_read_readvariableop4
0savev2_adam_conv2d_11_bias_v_read_readvariableop5
1savev2_adam_dense_10_kernel_v_read_readvariableop3
/savev2_adam_dense_10_bias_v_read_readvariableop5
1savev2_adam_dense_11_kernel_v_read_readvariableop3
/savev2_adam_dense_11_bias_v_read_readvariableop
savev2_1_const

identity_1ѕбMergeV2CheckpointsбSaveV2бSaveV2_1Ц
StringJoin/inputs_1Const"/device:CPU:0*
_output_shapes
: *
dtype0*<
value3B1 B+_temp_407e36ea02bc4e7aad4797ca32cdfa25/part2
StringJoin/inputs_1Ђ

StringJoin
StringJoinfile_prefixStringJoin/inputs_1:output:0"/device:CPU:0*
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardд
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameя
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*­
valueТBс%B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_namesм
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*]
valueTBR%B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesг
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0+savev2_conv2d_10_kernel_read_readvariableop)savev2_conv2d_10_bias_read_readvariableop0savev2_top_pool_with_max_1_w_read_readvariableop0savev2_top_pool_with_max_1_b_read_readvariableop+savev2_conv2d_11_kernel_read_readvariableop)savev2_conv2d_11_bias_read_readvariableop*savev2_dense_10_kernel_read_readvariableop(savev2_dense_10_bias_read_readvariableop*savev2_dense_11_kernel_read_readvariableop(savev2_dense_11_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop2savev2_adam_conv2d_10_kernel_m_read_readvariableop0savev2_adam_conv2d_10_bias_m_read_readvariableop7savev2_adam_top_pool_with_max_1_w_m_read_readvariableop7savev2_adam_top_pool_with_max_1_b_m_read_readvariableop2savev2_adam_conv2d_11_kernel_m_read_readvariableop0savev2_adam_conv2d_11_bias_m_read_readvariableop1savev2_adam_dense_10_kernel_m_read_readvariableop/savev2_adam_dense_10_bias_m_read_readvariableop1savev2_adam_dense_11_kernel_m_read_readvariableop/savev2_adam_dense_11_bias_m_read_readvariableop2savev2_adam_conv2d_10_kernel_v_read_readvariableop0savev2_adam_conv2d_10_bias_v_read_readvariableop7savev2_adam_top_pool_with_max_1_w_v_read_readvariableop7savev2_adam_top_pool_with_max_1_b_v_read_readvariableop2savev2_adam_conv2d_11_kernel_v_read_readvariableop0savev2_adam_conv2d_11_bias_v_read_readvariableop1savev2_adam_dense_10_kernel_v_read_readvariableop/savev2_adam_dense_10_bias_v_read_readvariableop1savev2_adam_dense_11_kernel_v_read_readvariableop/savev2_adam_dense_11_bias_v_read_readvariableop"/device:CPU:0*
_output_shapes
 *3
dtypes)
'2%	2
SaveV2Ѓ
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shardг
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1б
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_namesј
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slices¤
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1с
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesг
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

IdentityЂ

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*э
_input_shapesт
Р: :::::::
└ђ:ђ:	ђ
:
: : : : : : : :::::::
└ђ:ђ:	ђ
:
:::::::
└ђ:ђ:	ђ
:
: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:+ '
%
_user_specified_namefile_prefix
­
П
D__inference_dense_10_layer_call_and_return_conditional_losses_507183

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpЈ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
└ђ*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
MatMulЇ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
BiasAdd/ReadVariableOpѓ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2	
BiasAddќ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         └::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
­
П
D__inference_dense_10_layer_call_and_return_conditional_losses_507841

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpЈ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
└ђ*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2
MatMulЇ
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:ђ*
dtype02
BiasAdd/ReadVariableOpѓ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:         ђ2	
BiasAddќ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:         ђ2

Identity"
identityIdentity:output:0*/
_input_shapes
:         └::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs"»L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*│
serving_defaultЪ
C
input_68
serving_default_input_6:0         <
dense_110
StatefulPartitionedCall:0         
tensorflow/serving/predict:х═
╩/
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
	optimizer
	regularization_losses

trainable_variables
	variables
	keras_api

signatures
q_default_save_signature
r__call__
*s&call_and_return_all_conditional_losses"б,
_tf_keras_sequentialЃ,{"class_name": "Sequential", "name": "sequential_5", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "sequential_5", "layers": [{"class_name": "Conv2D", "config": {"name": "conv2d_10", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "batch_input_shape": [null, 28, 28, 1]}}, {"class_name": "TopPoolWithMax", "config": {"name": "top_pool_with_max_1", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}, {"class_name": "Conv2D", "config": {"name": "conv2d_11", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Flatten", "config": {"name": "flatten_5", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_10", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_11", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 1}}}, "is_graph_network": true, "keras_version": "2.2.4-tf", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential_5", "layers": [{"class_name": "Conv2D", "config": {"name": "conv2d_10", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "batch_input_shape": [null, 28, 28, 1]}}, {"class_name": "TopPoolWithMax", "config": {"name": "top_pool_with_max_1", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}, {"class_name": "Conv2D", "config": {"name": "conv2d_11", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Flatten", "config": {"name": "flatten_5", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_10", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_11", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}}, "training_config": {"loss": {"class_name": "SparseCategoricalCrossentropy", "config": {"reduction": "auto", "name": "sparse_categorical_crossentropy", "from_logits": true}}, "metrics": ["accuracy"], "weighted_metrics": null, "sample_weight_mode": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0003000000142492354, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
Г"ф
_tf_keras_input_layerі{"class_name": "InputLayer", "name": "input_6", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": [null, 28, 28, 1], "config": {"batch_input_shape": [null, 28, 28, 1], "dtype": "float32", "sparse": false, "ragged": false, "name": "input_6"}}
№

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
t__call__
*u&call_and_return_all_conditional_losses"╩
_tf_keras_layer░{"class_name": "Conv2D", "name": "conv2d_10", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "conv2d_10", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 1}}}}
Ь
w
b
regularization_losses
trainable_variables
	variables
	keras_api
v__call__
*w&call_and_return_all_conditional_losses"Л
_tf_keras_layerи{"class_name": "TopPoolWithMax", "name": "top_pool_with_max_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "top_pool_with_max_1", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}
Ь

kernel
bias
regularization_losses
trainable_variables
	variables
	keras_api
x__call__
*y&call_and_return_all_conditional_losses"╔
_tf_keras_layer»{"class_name": "Conv2D", "name": "conv2d_11", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "conv2d_11", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 8}}}}
░
 regularization_losses
!trainable_variables
"	variables
#	keras_api
z__call__
*{&call_and_return_all_conditional_losses"А
_tf_keras_layerЄ{"class_name": "Flatten", "name": "flatten_5", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "flatten_5", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 1, "axes": {}}}}
э

$kernel
%bias
&regularization_losses
'trainable_variables
(	variables
)	keras_api
|__call__
*}&call_and_return_all_conditional_losses"м
_tf_keras_layerИ{"class_name": "Dense", "name": "dense_10", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_10", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 576}}}}
Ш

*kernel
+bias
,regularization_losses
-trainable_variables
.	variables
/	keras_api
~__call__
*&call_and_return_all_conditional_losses"Л
_tf_keras_layerи{"class_name": "Dense", "name": "dense_11", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_11", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}}
Є
0iter

1beta_1

2beta_2
	3decay
4learning_ratem]m^m_m`mamb$mc%md*me+mfvgvhvivjvkvl$vm%vn*vo+vp"
	optimizer
 "
trackable_list_wrapper
f
0
1
2
3
4
5
$6
%7
*8
+9"
trackable_list_wrapper
f
0
1
2
3
4
5
$6
%7
*8
+9"
trackable_list_wrapper
и
5metrics
	regularization_losses
6non_trainable_variables

7layers
8layer_regularization_losses

trainable_variables
	variables
r__call__
q_default_save_signature
*s&call_and_return_all_conditional_losses
&s"call_and_return_conditional_losses"
_generic_user_object
-
ђserving_default"
signature_map
*:(2conv2d_10/kernel
:2conv2d_10/bias
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
џ
9metrics
regularization_losses
:non_trainable_variables

;layers
<layer_regularization_losses
trainable_variables
	variables
t__call__
*u&call_and_return_all_conditional_losses
&u"call_and_return_conditional_losses"
_generic_user_object
':%2top_pool_with_max_1/w
/:-2top_pool_with_max_1/b
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
џ
=metrics
regularization_losses
>non_trainable_variables

?layers
@layer_regularization_losses
trainable_variables
	variables
v__call__
*w&call_and_return_all_conditional_losses
&w"call_and_return_conditional_losses"
_generic_user_object
*:(2conv2d_11/kernel
:2conv2d_11/bias
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
џ
Ametrics
regularization_losses
Bnon_trainable_variables

Clayers
Dlayer_regularization_losses
trainable_variables
	variables
x__call__
*y&call_and_return_all_conditional_losses
&y"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
џ
Emetrics
 regularization_losses
Fnon_trainable_variables

Glayers
Hlayer_regularization_losses
!trainable_variables
"	variables
z__call__
*{&call_and_return_all_conditional_losses
&{"call_and_return_conditional_losses"
_generic_user_object
#:!
└ђ2dense_10/kernel
:ђ2dense_10/bias
 "
trackable_list_wrapper
.
$0
%1"
trackable_list_wrapper
.
$0
%1"
trackable_list_wrapper
џ
Imetrics
&regularization_losses
Jnon_trainable_variables

Klayers
Llayer_regularization_losses
'trainable_variables
(	variables
|__call__
*}&call_and_return_all_conditional_losses
&}"call_and_return_conditional_losses"
_generic_user_object
": 	ђ
2dense_11/kernel
:
2dense_11/bias
 "
trackable_list_wrapper
.
*0
+1"
trackable_list_wrapper
.
*0
+1"
trackable_list_wrapper
џ
Mmetrics
,regularization_losses
Nnon_trainable_variables

Olayers
Player_regularization_losses
-trainable_variables
.	variables
~__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
'
Q0"
trackable_list_wrapper
 "
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
ю
	Rtotal
	Scount
T
_fn_kwargs
Uregularization_losses
Vtrainable_variables
W	variables
X	keras_api
Ђ__call__
+ѓ&call_and_return_all_conditional_losses"т
_tf_keras_layer╦{"class_name": "MeanMetricWrapper", "name": "accuracy", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "accuracy", "dtype": "float32"}}
:  (2total
:  (2count
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
R0
S1"
trackable_list_wrapper
Ю
Ymetrics
Uregularization_losses
Znon_trainable_variables

[layers
\layer_regularization_losses
Vtrainable_variables
W	variables
Ђ__call__
+ѓ&call_and_return_all_conditional_losses
'ѓ"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
/:-2Adam/conv2d_10/kernel/m
!:2Adam/conv2d_10/bias/m
,:*2Adam/top_pool_with_max_1/w/m
4:22Adam/top_pool_with_max_1/b/m
/:-2Adam/conv2d_11/kernel/m
!:2Adam/conv2d_11/bias/m
(:&
└ђ2Adam/dense_10/kernel/m
!:ђ2Adam/dense_10/bias/m
':%	ђ
2Adam/dense_11/kernel/m
 :
2Adam/dense_11/bias/m
/:-2Adam/conv2d_10/kernel/v
!:2Adam/conv2d_10/bias/v
,:*2Adam/top_pool_with_max_1/w/v
4:22Adam/top_pool_with_max_1/b/v
/:-2Adam/conv2d_11/kernel/v
!:2Adam/conv2d_11/bias/v
(:&
└ђ2Adam/dense_10/kernel/v
!:ђ2Adam/dense_10/bias/v
':%	ђ
2Adam/dense_11/kernel/v
 :
2Adam/dense_11/bias/v
у2С
!__inference__wrapped_model_506964Й
І▓Є
FullArgSpec
argsџ 
varargsjargs
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *.б+
)і&
input_6         
ѓ2 
-__inference_sequential_5_layer_call_fn_507681
-__inference_sequential_5_layer_call_fn_507274
-__inference_sequential_5_layer_call_fn_507666
-__inference_sequential_5_layer_call_fn_507309└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
Ь2в
H__inference_sequential_5_layer_call_and_return_conditional_losses_507492
H__inference_sequential_5_layer_call_and_return_conditional_losses_507218
H__inference_sequential_5_layer_call_and_return_conditional_losses_507651
H__inference_sequential_5_layer_call_and_return_conditional_losses_507238└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
Ѕ2є
*__inference_conv2d_10_layer_call_fn_506984О
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *7б4
2і/+                           
ц2А
E__inference_conv2d_10_layer_call_and_return_conditional_losses_506976О
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *7б4
2і/+                           
┌2О
4__inference_top_pool_with_max_1_layer_call_fn_507820ъ
Ћ▓Љ
FullArgSpec
argsџ
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
ш2Ы
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_507813ъ
Ћ▓Љ
FullArgSpec
argsџ
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
Ѕ2є
*__inference_conv2d_11_layer_call_fn_507005О
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *7б4
2і/+                           
ц2А
E__inference_conv2d_11_layer_call_and_return_conditional_losses_506997О
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *7б4
2і/+                           
н2Л
*__inference_flatten_5_layer_call_fn_507831б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
№2В
E__inference_flatten_5_layer_call_and_return_conditional_losses_507826б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
М2л
)__inference_dense_10_layer_call_fn_507848б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
Ь2в
D__inference_dense_10_layer_call_and_return_conditional_losses_507841б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
М2л
)__inference_dense_11_layer_call_fn_507865б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
Ь2в
D__inference_dense_11_layer_call_and_return_conditional_losses_507858б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
3B1
$__inference_signature_wrapper_507333input_6
╠2╔к
й▓╣
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkwjkwargs
defaultsџ 

kwonlyargsџ

jtraining%
kwonlydefaultsф

trainingp 
annotationsф *
 
╠2╔к
й▓╣
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkwjkwargs
defaultsџ 

kwonlyargsџ

jtraining%
kwonlydefaultsф

trainingp 
annotationsф *
 а
!__inference__wrapped_model_506964{
$%*+8б5
.б+
)і&
input_6         
ф "3ф0
.
dense_11"і
dense_11         
┌
E__inference_conv2d_10_layer_call_and_return_conditional_losses_506976љIбF
?б<
:і7
inputs+                           
ф "?б<
5і2
0+                           
џ ▓
*__inference_conv2d_10_layer_call_fn_506984ЃIбF
?б<
:і7
inputs+                           
ф "2і/+                           ┌
E__inference_conv2d_11_layer_call_and_return_conditional_losses_506997љIбF
?б<
:і7
inputs+                           
ф "?б<
5і2
0+                           
џ ▓
*__inference_conv2d_11_layer_call_fn_507005ЃIбF
?б<
:і7
inputs+                           
ф "2і/+                           д
D__inference_dense_10_layer_call_and_return_conditional_losses_507841^$%0б-
&б#
!і
inputs         └
ф "&б#
і
0         ђ
џ ~
)__inference_dense_10_layer_call_fn_507848Q$%0б-
&б#
!і
inputs         └
ф "і         ђЦ
D__inference_dense_11_layer_call_and_return_conditional_losses_507858]*+0б-
&б#
!і
inputs         ђ
ф "%б"
і
0         

џ }
)__inference_dense_11_layer_call_fn_507865P*+0б-
&б#
!і
inputs         ђ
ф "і         
ф
E__inference_flatten_5_layer_call_and_return_conditional_losses_507826a7б4
-б*
(і%
inputs         
ф "&б#
і
0         └
џ ѓ
*__inference_flatten_5_layer_call_fn_507831T7б4
-б*
(і%
inputs         
ф "і         └┴
H__inference_sequential_5_layer_call_and_return_conditional_losses_507218u
$%*+@б=
6б3
)і&
input_6         
p

 
ф "%б"
і
0         

џ ┴
H__inference_sequential_5_layer_call_and_return_conditional_losses_507238u
$%*+@б=
6б3
)і&
input_6         
p 

 
ф "%б"
і
0         

џ └
H__inference_sequential_5_layer_call_and_return_conditional_losses_507492t
$%*+?б<
5б2
(і%
inputs         
p

 
ф "%б"
і
0         

џ └
H__inference_sequential_5_layer_call_and_return_conditional_losses_507651t
$%*+?б<
5б2
(і%
inputs         
p 

 
ф "%б"
і
0         

џ Ў
-__inference_sequential_5_layer_call_fn_507274h
$%*+@б=
6б3
)і&
input_6         
p

 
ф "і         
Ў
-__inference_sequential_5_layer_call_fn_507309h
$%*+@б=
6б3
)і&
input_6         
p 

 
ф "і         
ў
-__inference_sequential_5_layer_call_fn_507666g
$%*+?б<
5б2
(і%
inputs         
p

 
ф "і         
ў
-__inference_sequential_5_layer_call_fn_507681g
$%*+?б<
5б2
(і%
inputs         
p 

 
ф "і         
»
$__inference_signature_wrapper_507333є
$%*+Cб@
б 
9ф6
4
input_6)і&
input_6         "3ф0
.
dense_11"і
dense_11         
╗
O__inference_top_pool_with_max_1_layer_call_and_return_conditional_losses_507813h3б0
)б&
$і!
In         
ф "-б*
#і 
0         


џ Њ
4__inference_top_pool_with_max_1_layer_call_fn_507820[3б0
)б&
$і!
In         
ф " і         

