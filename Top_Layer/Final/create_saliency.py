import os
from os import path
import sys
sys.path.append('../')
from models import models
from classes import classes
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from main import check_path

datasets = ["mnist", "cifar10", "fashion_mnist"]

for name_dataset in datasets:
    data_classes = classes[name_dataset]
    num_classes = len(data_classes)
    for name_model in models:
        name_model = name_model.replace('_cifar10', '')
        name_model = name_model.replace('_fashion_mnist', '')
        path = './'+name_dataset+'/'+name_model+'/saliency'
        x = np.load(path+'/dictionary.npy', allow_pickle = True)
        x = x.tolist()
        fig, ax = plt.subplots(num_classes,3,figsize=(6, 9))
        for i in range(num_classes):
            class_i = data_classes[i]
            for j in range(3):
                index = str(i)+str(j)
                imgs = x[index]
                ax[i,j].imshow(imgs[0])
                im = ax[i,j].imshow(imgs[1], cmap='jet', alpha=0.5)
                ax[i,j].axis('off')
        cbar_ax = fig.add_axes([0.87, 0.18, 0.03, 0.4])
        fig.colorbar(im, cax=cbar_ax)
        fig.suptitle(name_model)
        plt.savefig(path+'/maps', format = 'pdf')
        plt.close(fig)
