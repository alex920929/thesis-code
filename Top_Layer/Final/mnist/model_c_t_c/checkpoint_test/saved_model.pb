ШП
Ј§
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype
О
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"serve*2.1.02v2.1.0-rc2-17-ge5bf8de8Ыь

conv2d_8/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_nameconv2d_8/kernel
{
#conv2d_8/kernel/Read/ReadVariableOpReadVariableOpconv2d_8/kernel*&
_output_shapes
:*
dtype0
r
conv2d_8/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d_8/bias
k
!conv2d_8/bias/Read/ReadVariableOpReadVariableOpconv2d_8/bias*
_output_shapes
:*
dtype0
t
top_pool_1/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_nametop_pool_1/w
m
 top_pool_1/w/Read/ReadVariableOpReadVariableOptop_pool_1/w*
_output_shapes

:*
dtype0
|
top_pool_1/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nametop_pool_1/b
u
 top_pool_1/b/Read/ReadVariableOpReadVariableOptop_pool_1/b*&
_output_shapes
:*
dtype0

conv2d_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_nameconv2d_9/kernel
{
#conv2d_9/kernel/Read/ReadVariableOpReadVariableOpconv2d_9/kernel*&
_output_shapes
:*
dtype0
r
conv2d_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d_9/bias
k
!conv2d_9/bias/Read/ReadVariableOpReadVariableOpconv2d_9/bias*
_output_shapes
:*
dtype0
z
dense_8/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
Р*
shared_namedense_8/kernel
s
"dense_8/kernel/Read/ReadVariableOpReadVariableOpdense_8/kernel* 
_output_shapes
:
Р*
dtype0
q
dense_8/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_8/bias
j
 dense_8/bias/Read/ReadVariableOpReadVariableOpdense_8/bias*
_output_shapes	
:*
dtype0
y
dense_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	
*
shared_namedense_9/kernel
r
"dense_9/kernel/Read/ReadVariableOpReadVariableOpdense_9/kernel*
_output_shapes
:	
*
dtype0
p
dense_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namedense_9/bias
i
 dense_9/bias/Read/ReadVariableOpReadVariableOpdense_9/bias*
_output_shapes
:
*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0

Adam/conv2d_8/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*'
shared_nameAdam/conv2d_8/kernel/m

*Adam/conv2d_8/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_8/kernel/m*&
_output_shapes
:*
dtype0

Adam/conv2d_8/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d_8/bias/m
y
(Adam/conv2d_8/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_8/bias/m*
_output_shapes
:*
dtype0

Adam/top_pool_1/w/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/top_pool_1/w/m
{
'Adam/top_pool_1/w/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_1/w/m*
_output_shapes

:*
dtype0

Adam/top_pool_1/b/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/top_pool_1/b/m

'Adam/top_pool_1/b/m/Read/ReadVariableOpReadVariableOpAdam/top_pool_1/b/m*&
_output_shapes
:*
dtype0

Adam/conv2d_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*'
shared_nameAdam/conv2d_9/kernel/m

*Adam/conv2d_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_9/kernel/m*&
_output_shapes
:*
dtype0

Adam/conv2d_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d_9/bias/m
y
(Adam/conv2d_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_9/bias/m*
_output_shapes
:*
dtype0

Adam/dense_8/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
Р*&
shared_nameAdam/dense_8/kernel/m

)Adam/dense_8/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/m* 
_output_shapes
:
Р*
dtype0

Adam/dense_8/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_8/bias/m
x
'Adam/dense_8/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/m*
_output_shapes	
:*
dtype0

Adam/dense_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	
*&
shared_nameAdam/dense_9/kernel/m

)Adam/dense_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_9/kernel/m*
_output_shapes
:	
*
dtype0
~
Adam/dense_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*$
shared_nameAdam/dense_9/bias/m
w
'Adam/dense_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_9/bias/m*
_output_shapes
:
*
dtype0

Adam/conv2d_8/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*'
shared_nameAdam/conv2d_8/kernel/v

*Adam/conv2d_8/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_8/kernel/v*&
_output_shapes
:*
dtype0

Adam/conv2d_8/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d_8/bias/v
y
(Adam/conv2d_8/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_8/bias/v*
_output_shapes
:*
dtype0

Adam/top_pool_1/w/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/top_pool_1/w/v
{
'Adam/top_pool_1/w/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_1/w/v*
_output_shapes

:*
dtype0

Adam/top_pool_1/b/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/top_pool_1/b/v

'Adam/top_pool_1/b/v/Read/ReadVariableOpReadVariableOpAdam/top_pool_1/b/v*&
_output_shapes
:*
dtype0

Adam/conv2d_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*'
shared_nameAdam/conv2d_9/kernel/v

*Adam/conv2d_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_9/kernel/v*&
_output_shapes
:*
dtype0

Adam/conv2d_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/conv2d_9/bias/v
y
(Adam/conv2d_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_9/bias/v*
_output_shapes
:*
dtype0

Adam/dense_8/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
Р*&
shared_nameAdam/dense_8/kernel/v

)Adam/dense_8/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/kernel/v* 
_output_shapes
:
Р*
dtype0

Adam/dense_8/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_8/bias/v
x
'Adam/dense_8/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_8/bias/v*
_output_shapes	
:*
dtype0

Adam/dense_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	
*&
shared_nameAdam/dense_9/kernel/v

)Adam/dense_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_9/kernel/v*
_output_shapes
:	
*
dtype0
~
Adam/dense_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*$
shared_nameAdam/dense_9/bias/v
w
'Adam/dense_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_9/bias/v*
_output_shapes
:
*
dtype0

NoOpNoOp
Ђ7
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*н6
valueг6Bа6 BЩ6
Ю
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
	optimizer
		variables

regularization_losses
trainable_variables
	keras_api

signatures
 
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
`
w
b
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
R
 	variables
!regularization_losses
"trainable_variables
#	keras_api
h

$kernel
%bias
&	variables
'regularization_losses
(trainable_variables
)	keras_api
h

*kernel
+bias
,	variables
-regularization_losses
.trainable_variables
/	keras_api
є
0iter

1beta_1

2beta_2
	3decay
4learning_ratem]m^m_m`mamb$mc%md*me+mfvgvhvivjvkvl$vm%vn*vo+vp
F
0
1
2
3
4
5
$6
%7
*8
+9
 
F
0
1
2
3
4
5
$6
%7
*8
+9

5non_trainable_variables
		variables
6metrics

7layers

regularization_losses
trainable_variables
8layer_regularization_losses
 
[Y
VARIABLE_VALUEconv2d_8/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEconv2d_8/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1

9non_trainable_variables
	variables
:metrics

;layers
regularization_losses
trainable_variables
<layer_regularization_losses
SQ
VARIABLE_VALUEtop_pool_1/w1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUE
SQ
VARIABLE_VALUEtop_pool_1/b1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1

=non_trainable_variables
	variables
>metrics

?layers
regularization_losses
trainable_variables
@layer_regularization_losses
[Y
VARIABLE_VALUEconv2d_9/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEconv2d_9/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1

Anon_trainable_variables
	variables
Bmetrics

Clayers
regularization_losses
trainable_variables
Dlayer_regularization_losses
 
 
 

Enon_trainable_variables
 	variables
Fmetrics

Glayers
!regularization_losses
"trainable_variables
Hlayer_regularization_losses
ZX
VARIABLE_VALUEdense_8/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_8/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

$0
%1
 

$0
%1

Inon_trainable_variables
&	variables
Jmetrics

Klayers
'regularization_losses
(trainable_variables
Llayer_regularization_losses
ZX
VARIABLE_VALUEdense_9/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_9/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE

*0
+1
 

*0
+1

Mnon_trainable_variables
,	variables
Nmetrics

Olayers
-regularization_losses
.trainable_variables
Player_regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
 

Q0
*
0
1
2
3
4
5
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
x
	Rtotal
	Scount
T
_fn_kwargs
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE
 

R0
S1
 
 

Ynon_trainable_variables
U	variables
Zmetrics

[layers
Vregularization_losses
Wtrainable_variables
\layer_regularization_losses

R0
S1
 
 
 
~|
VARIABLE_VALUEAdam/conv2d_8/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/conv2d_8/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_1/w/mMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_1/b/mMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/conv2d_9/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/conv2d_9/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_8/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_8/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_9/kernel/mRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_9/bias/mPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/conv2d_8/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/conv2d_8/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_1/w/vMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
vt
VARIABLE_VALUEAdam/top_pool_1/b/vMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/conv2d_9/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/conv2d_9/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_8/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_8/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_9/kernel/vRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_9/bias/vPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE

serving_default_input_5Placeholder*/
_output_shapes
:џџџџџџџџџ*
dtype0*$
shape:џџџџџџџџџ
М
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_5conv2d_8/kernelconv2d_8/biastop_pool_1/wtop_pool_1/bconv2d_9/kernelconv2d_9/biasdense_8/kerneldense_8/biasdense_9/kerneldense_9/bias*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*-
f(R&
$__inference_signature_wrapper_156036
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
Е
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename#conv2d_8/kernel/Read/ReadVariableOp!conv2d_8/bias/Read/ReadVariableOp top_pool_1/w/Read/ReadVariableOp top_pool_1/b/Read/ReadVariableOp#conv2d_9/kernel/Read/ReadVariableOp!conv2d_9/bias/Read/ReadVariableOp"dense_8/kernel/Read/ReadVariableOp dense_8/bias/Read/ReadVariableOp"dense_9/kernel/Read/ReadVariableOp dense_9/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp*Adam/conv2d_8/kernel/m/Read/ReadVariableOp(Adam/conv2d_8/bias/m/Read/ReadVariableOp'Adam/top_pool_1/w/m/Read/ReadVariableOp'Adam/top_pool_1/b/m/Read/ReadVariableOp*Adam/conv2d_9/kernel/m/Read/ReadVariableOp(Adam/conv2d_9/bias/m/Read/ReadVariableOp)Adam/dense_8/kernel/m/Read/ReadVariableOp'Adam/dense_8/bias/m/Read/ReadVariableOp)Adam/dense_9/kernel/m/Read/ReadVariableOp'Adam/dense_9/bias/m/Read/ReadVariableOp*Adam/conv2d_8/kernel/v/Read/ReadVariableOp(Adam/conv2d_8/bias/v/Read/ReadVariableOp'Adam/top_pool_1/w/v/Read/ReadVariableOp'Adam/top_pool_1/b/v/Read/ReadVariableOp*Adam/conv2d_9/kernel/v/Read/ReadVariableOp(Adam/conv2d_9/bias/v/Read/ReadVariableOp)Adam/dense_8/kernel/v/Read/ReadVariableOp'Adam/dense_8/bias/v/Read/ReadVariableOp)Adam/dense_9/kernel/v/Read/ReadVariableOp'Adam/dense_9/bias/v/Read/ReadVariableOpConst*2
Tin+
)2'	*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

GPU 

CPU2J 8*(
f#R!
__inference__traced_save_156643
Ь
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameconv2d_8/kernelconv2d_8/biastop_pool_1/wtop_pool_1/bconv2d_9/kernelconv2d_9/biasdense_8/kerneldense_8/biasdense_9/kerneldense_9/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcountAdam/conv2d_8/kernel/mAdam/conv2d_8/bias/mAdam/top_pool_1/w/mAdam/top_pool_1/b/mAdam/conv2d_9/kernel/mAdam/conv2d_9/bias/mAdam/dense_8/kernel/mAdam/dense_8/bias/mAdam/dense_9/kernel/mAdam/dense_9/bias/mAdam/conv2d_8/kernel/vAdam/conv2d_8/bias/vAdam/top_pool_1/w/vAdam/top_pool_1/b/vAdam/conv2d_9/kernel/vAdam/conv2d_9/bias/vAdam/dense_8/kernel/vAdam/dense_8/bias/vAdam/dense_9/kernel/vAdam/dense_9/bias/v*1
Tin*
(2&*
Tout
2*,
_gradient_op_typePartitionedCallUnused*
_output_shapes
: **
config_proto

GPU 

CPU2J 8*+
f&R$
"__inference__traced_restore_156766иг



"__inference__traced_restore_156766
file_prefix$
 assignvariableop_conv2d_8_kernel$
 assignvariableop_1_conv2d_8_bias#
assignvariableop_2_top_pool_1_w#
assignvariableop_3_top_pool_1_b&
"assignvariableop_4_conv2d_9_kernel$
 assignvariableop_5_conv2d_9_bias%
!assignvariableop_6_dense_8_kernel#
assignvariableop_7_dense_8_bias%
!assignvariableop_8_dense_9_kernel#
assignvariableop_9_dense_9_bias!
assignvariableop_10_adam_iter#
assignvariableop_11_adam_beta_1#
assignvariableop_12_adam_beta_2"
assignvariableop_13_adam_decay*
&assignvariableop_14_adam_learning_rate
assignvariableop_15_total
assignvariableop_16_count.
*assignvariableop_17_adam_conv2d_8_kernel_m,
(assignvariableop_18_adam_conv2d_8_bias_m+
'assignvariableop_19_adam_top_pool_1_w_m+
'assignvariableop_20_adam_top_pool_1_b_m.
*assignvariableop_21_adam_conv2d_9_kernel_m,
(assignvariableop_22_adam_conv2d_9_bias_m-
)assignvariableop_23_adam_dense_8_kernel_m+
'assignvariableop_24_adam_dense_8_bias_m-
)assignvariableop_25_adam_dense_9_kernel_m+
'assignvariableop_26_adam_dense_9_bias_m.
*assignvariableop_27_adam_conv2d_8_kernel_v,
(assignvariableop_28_adam_conv2d_8_bias_v+
'assignvariableop_29_adam_top_pool_1_w_v+
'assignvariableop_30_adam_top_pool_1_b_v.
*assignvariableop_31_adam_conv2d_9_kernel_v,
(assignvariableop_32_adam_conv2d_9_bias_v-
)assignvariableop_33_adam_dense_8_kernel_v+
'assignvariableop_34_adam_dense_8_bias_v-
)assignvariableop_35_adam_dense_9_kernel_v+
'assignvariableop_36_adam_dense_9_bias_v
identity_38ЂAssignVariableOpЂAssignVariableOp_1ЂAssignVariableOp_10ЂAssignVariableOp_11ЂAssignVariableOp_12ЂAssignVariableOp_13ЂAssignVariableOp_14ЂAssignVariableOp_15ЂAssignVariableOp_16ЂAssignVariableOp_17ЂAssignVariableOp_18ЂAssignVariableOp_19ЂAssignVariableOp_2ЂAssignVariableOp_20ЂAssignVariableOp_21ЂAssignVariableOp_22ЂAssignVariableOp_23ЂAssignVariableOp_24ЂAssignVariableOp_25ЂAssignVariableOp_26ЂAssignVariableOp_27ЂAssignVariableOp_28ЂAssignVariableOp_29ЂAssignVariableOp_3ЂAssignVariableOp_30ЂAssignVariableOp_31ЂAssignVariableOp_32ЂAssignVariableOp_33ЂAssignVariableOp_34ЂAssignVariableOp_35ЂAssignVariableOp_36ЂAssignVariableOp_4ЂAssignVariableOp_5ЂAssignVariableOp_6ЂAssignVariableOp_7ЂAssignVariableOp_8ЂAssignVariableOp_9Ђ	RestoreV2ЂRestoreV2_1ф
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*№
valueцBу%B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_namesи
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*]
valueTBR%B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesч
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*Њ
_output_shapes
:::::::::::::::::::::::::::::::::::::*3
dtypes)
'2%	2
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

Identity
AssignVariableOpAssignVariableOp assignvariableop_conv2d_8_kernelIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1
AssignVariableOp_1AssignVariableOp assignvariableop_1_conv2d_8_biasIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2
AssignVariableOp_2AssignVariableOpassignvariableop_2_top_pool_1_wIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3
AssignVariableOp_3AssignVariableOpassignvariableop_3_top_pool_1_bIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4
AssignVariableOp_4AssignVariableOp"assignvariableop_4_conv2d_9_kernelIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5
AssignVariableOp_5AssignVariableOp assignvariableop_5_conv2d_9_biasIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0*
_output_shapes
:2

Identity_6
AssignVariableOp_6AssignVariableOp!assignvariableop_6_dense_8_kernelIdentity_6:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7
AssignVariableOp_7AssignVariableOpassignvariableop_7_dense_8_biasIdentity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8
AssignVariableOp_8AssignVariableOp!assignvariableop_8_dense_9_kernelIdentity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9
AssignVariableOp_9AssignVariableOpassignvariableop_9_dense_9_biasIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0	*
_output_shapes
:2
Identity_10
AssignVariableOp_10AssignVariableOpassignvariableop_10_adam_iterIdentity_10:output:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11
AssignVariableOp_11AssignVariableOpassignvariableop_11_adam_beta_1Identity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12
AssignVariableOp_12AssignVariableOpassignvariableop_12_adam_beta_2Identity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13
AssignVariableOp_13AssignVariableOpassignvariableop_13_adam_decayIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14
AssignVariableOp_14AssignVariableOp&assignvariableop_14_adam_learning_rateIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15
AssignVariableOp_15AssignVariableOpassignvariableop_15_totalIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16
AssignVariableOp_16AssignVariableOpassignvariableop_16_countIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17Ѓ
AssignVariableOp_17AssignVariableOp*assignvariableop_17_adam_conv2d_8_kernel_mIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0*
_output_shapes
:2
Identity_18Ё
AssignVariableOp_18AssignVariableOp(assignvariableop_18_adam_conv2d_8_bias_mIdentity_18:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19 
AssignVariableOp_19AssignVariableOp'assignvariableop_19_adam_top_pool_1_w_mIdentity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20 
AssignVariableOp_20AssignVariableOp'assignvariableop_20_adam_top_pool_1_b_mIdentity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21Ѓ
AssignVariableOp_21AssignVariableOp*assignvariableop_21_adam_conv2d_9_kernel_mIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21_
Identity_22IdentityRestoreV2:tensors:22*
T0*
_output_shapes
:2
Identity_22Ё
AssignVariableOp_22AssignVariableOp(assignvariableop_22_adam_conv2d_9_bias_mIdentity_22:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_22_
Identity_23IdentityRestoreV2:tensors:23*
T0*
_output_shapes
:2
Identity_23Ђ
AssignVariableOp_23AssignVariableOp)assignvariableop_23_adam_dense_8_kernel_mIdentity_23:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_23_
Identity_24IdentityRestoreV2:tensors:24*
T0*
_output_shapes
:2
Identity_24 
AssignVariableOp_24AssignVariableOp'assignvariableop_24_adam_dense_8_bias_mIdentity_24:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_24_
Identity_25IdentityRestoreV2:tensors:25*
T0*
_output_shapes
:2
Identity_25Ђ
AssignVariableOp_25AssignVariableOp)assignvariableop_25_adam_dense_9_kernel_mIdentity_25:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_25_
Identity_26IdentityRestoreV2:tensors:26*
T0*
_output_shapes
:2
Identity_26 
AssignVariableOp_26AssignVariableOp'assignvariableop_26_adam_dense_9_bias_mIdentity_26:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_26_
Identity_27IdentityRestoreV2:tensors:27*
T0*
_output_shapes
:2
Identity_27Ѓ
AssignVariableOp_27AssignVariableOp*assignvariableop_27_adam_conv2d_8_kernel_vIdentity_27:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_27_
Identity_28IdentityRestoreV2:tensors:28*
T0*
_output_shapes
:2
Identity_28Ё
AssignVariableOp_28AssignVariableOp(assignvariableop_28_adam_conv2d_8_bias_vIdentity_28:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_28_
Identity_29IdentityRestoreV2:tensors:29*
T0*
_output_shapes
:2
Identity_29 
AssignVariableOp_29AssignVariableOp'assignvariableop_29_adam_top_pool_1_w_vIdentity_29:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_29_
Identity_30IdentityRestoreV2:tensors:30*
T0*
_output_shapes
:2
Identity_30 
AssignVariableOp_30AssignVariableOp'assignvariableop_30_adam_top_pool_1_b_vIdentity_30:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_30_
Identity_31IdentityRestoreV2:tensors:31*
T0*
_output_shapes
:2
Identity_31Ѓ
AssignVariableOp_31AssignVariableOp*assignvariableop_31_adam_conv2d_9_kernel_vIdentity_31:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_31_
Identity_32IdentityRestoreV2:tensors:32*
T0*
_output_shapes
:2
Identity_32Ё
AssignVariableOp_32AssignVariableOp(assignvariableop_32_adam_conv2d_9_bias_vIdentity_32:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_32_
Identity_33IdentityRestoreV2:tensors:33*
T0*
_output_shapes
:2
Identity_33Ђ
AssignVariableOp_33AssignVariableOp)assignvariableop_33_adam_dense_8_kernel_vIdentity_33:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_33_
Identity_34IdentityRestoreV2:tensors:34*
T0*
_output_shapes
:2
Identity_34 
AssignVariableOp_34AssignVariableOp'assignvariableop_34_adam_dense_8_bias_vIdentity_34:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_34_
Identity_35IdentityRestoreV2:tensors:35*
T0*
_output_shapes
:2
Identity_35Ђ
AssignVariableOp_35AssignVariableOp)assignvariableop_35_adam_dense_9_kernel_vIdentity_35:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_35_
Identity_36IdentityRestoreV2:tensors:36*
T0*
_output_shapes
:2
Identity_36 
AssignVariableOp_36AssignVariableOp'assignvariableop_36_adam_dense_9_bias_vIdentity_36:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_36Ј
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_names
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slicesФ
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp
Identity_37Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_37
Identity_38IdentityIdentity_37:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_38"#
identity_38Identity_38:output:0*Ћ
_input_shapes
: :::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:+ '
%
_user_specified_namefile_prefix
ъ
м
C__inference_dense_9_layer_call_and_return_conditional_losses_155908

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂMatMul/ReadVariableOp
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
MatMul
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*/
_input_shapes
:џџџџџџџџџ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
Я
а
-__inference_sequential_4_layer_call_fn_156012
input_5"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinput_5statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_sequential_4_layer_call_and_return_conditional_losses_1559992
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_5
ю 
и
H__inference_sequential_4_layer_call_and_return_conditional_losses_155964

inputs+
'conv2d_8_statefulpartitionedcall_args_1+
'conv2d_8_statefulpartitionedcall_args_2-
)top_pool_1_statefulpartitionedcall_args_1-
)top_pool_1_statefulpartitionedcall_args_2+
'conv2d_9_statefulpartitionedcall_args_1+
'conv2d_9_statefulpartitionedcall_args_2*
&dense_8_statefulpartitionedcall_args_1*
&dense_8_statefulpartitionedcall_args_2*
&dense_9_statefulpartitionedcall_args_1*
&dense_9_statefulpartitionedcall_args_2
identityЂ conv2d_8/StatefulPartitionedCallЂ conv2d_9/StatefulPartitionedCallЂdense_8/StatefulPartitionedCallЂdense_9/StatefulPartitionedCallЂ"top_pool_1/StatefulPartitionedCallВ
 conv2d_8/StatefulPartitionedCallStatefulPartitionedCallinputs'conv2d_8_statefulpartitionedcall_args_1'conv2d_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_8_layer_call_and_return_conditional_losses_1556992"
 conv2d_8/StatefulPartitionedCallп
"top_pool_1/StatefulPartitionedCallStatefulPartitionedCall)conv2d_8/StatefulPartitionedCall:output:0)top_pool_1_statefulpartitionedcall_args_1)top_pool_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ

**
config_proto

GPU 

CPU2J 8*O
fJRH
F__inference_top_pool_1_layer_call_and_return_conditional_losses_1558472$
"top_pool_1/StatefulPartitionedCallз
 conv2d_9/StatefulPartitionedCallStatefulPartitionedCall+top_pool_1/StatefulPartitionedCall:output:0'conv2d_9_statefulpartitionedcall_args_1'conv2d_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_9_layer_call_and_return_conditional_losses_1557202"
 conv2d_9/StatefulPartitionedCallх
flatten_4/PartitionedCallPartitionedCall)conv2d_9/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџР**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_flatten_4_layer_call_and_return_conditional_losses_1558682
flatten_4/PartitionedCallТ
dense_8/StatefulPartitionedCallStatefulPartitionedCall"flatten_4/PartitionedCall:output:0&dense_8_statefulpartitionedcall_args_1&dense_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_1558862!
dense_8/StatefulPartitionedCallЧ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0&dense_9_statefulpartitionedcall_args_1&dense_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_1559082!
dense_9/StatefulPartitionedCallЋ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0!^conv2d_8/StatefulPartitionedCall!^conv2d_9/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall#^top_pool_1/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::2D
 conv2d_8/StatefulPartitionedCall conv2d_8/StatefulPartitionedCall2D
 conv2d_9/StatefulPartitionedCall conv2d_9/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2H
"top_pool_1/StatefulPartitionedCall"top_pool_1/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
я
м
C__inference_dense_8_layer_call_and_return_conditional_losses_155886

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂMatMul/ReadVariableOp
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
Р*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
MatMul
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*/
_input_shapes
:џџџџџџџџџР::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
ѓ
Љ
(__inference_dense_8_layer_call_fn_156491

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_1558862
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*(
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*/
_input_shapes
:џџџџџџџџџР::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
Ь
Я
-__inference_sequential_4_layer_call_fn_156329

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_sequential_4_layer_call_and_return_conditional_losses_1559642
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
ё 
й
H__inference_sequential_4_layer_call_and_return_conditional_losses_155921
input_5+
'conv2d_8_statefulpartitionedcall_args_1+
'conv2d_8_statefulpartitionedcall_args_2-
)top_pool_1_statefulpartitionedcall_args_1-
)top_pool_1_statefulpartitionedcall_args_2+
'conv2d_9_statefulpartitionedcall_args_1+
'conv2d_9_statefulpartitionedcall_args_2*
&dense_8_statefulpartitionedcall_args_1*
&dense_8_statefulpartitionedcall_args_2*
&dense_9_statefulpartitionedcall_args_1*
&dense_9_statefulpartitionedcall_args_2
identityЂ conv2d_8/StatefulPartitionedCallЂ conv2d_9/StatefulPartitionedCallЂdense_8/StatefulPartitionedCallЂdense_9/StatefulPartitionedCallЂ"top_pool_1/StatefulPartitionedCallГ
 conv2d_8/StatefulPartitionedCallStatefulPartitionedCallinput_5'conv2d_8_statefulpartitionedcall_args_1'conv2d_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_8_layer_call_and_return_conditional_losses_1556992"
 conv2d_8/StatefulPartitionedCallп
"top_pool_1/StatefulPartitionedCallStatefulPartitionedCall)conv2d_8/StatefulPartitionedCall:output:0)top_pool_1_statefulpartitionedcall_args_1)top_pool_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ

**
config_proto

GPU 

CPU2J 8*O
fJRH
F__inference_top_pool_1_layer_call_and_return_conditional_losses_1558472$
"top_pool_1/StatefulPartitionedCallз
 conv2d_9/StatefulPartitionedCallStatefulPartitionedCall+top_pool_1/StatefulPartitionedCall:output:0'conv2d_9_statefulpartitionedcall_args_1'conv2d_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_9_layer_call_and_return_conditional_losses_1557202"
 conv2d_9/StatefulPartitionedCallх
flatten_4/PartitionedCallPartitionedCall)conv2d_9/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџР**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_flatten_4_layer_call_and_return_conditional_losses_1558682
flatten_4/PartitionedCallТ
dense_8/StatefulPartitionedCallStatefulPartitionedCall"flatten_4/PartitionedCall:output:0&dense_8_statefulpartitionedcall_args_1&dense_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_1558862!
dense_8/StatefulPartitionedCallЧ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0&dense_9_statefulpartitionedcall_args_1&dense_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_1559082!
dense_9/StatefulPartitionedCallЋ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0!^conv2d_8/StatefulPartitionedCall!^conv2d_9/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall#^top_pool_1/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::2D
 conv2d_8/StatefulPartitionedCall conv2d_8/StatefulPartitionedCall2D
 conv2d_9/StatefulPartitionedCall conv2d_9/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2H
"top_pool_1/StatefulPartitionedCall"top_pool_1/StatefulPartitionedCall:' #
!
_user_specified_name	input_5
Р
Њ
)__inference_conv2d_8_layer_call_fn_155707

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityЂStatefulPartitionedCall 
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_8_layer_call_and_return_conditional_losses_1556992
StatefulPartitionedCallЈ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
ё
Љ
(__inference_dense_9_layer_call_fn_156508

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_1559082
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*/
_input_shapes
:џџџџџџџџџ::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
Ь
Я
-__inference_sequential_4_layer_call_fn_156344

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_sequential_4_layer_call_and_return_conditional_losses_1559992
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
Я
а
-__inference_sequential_4_layer_call_fn_155977
input_5"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinput_5statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*Q
fLRJ
H__inference_sequential_4_layer_call_and_return_conditional_losses_1559642
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_5
ю 
и
H__inference_sequential_4_layer_call_and_return_conditional_losses_155999

inputs+
'conv2d_8_statefulpartitionedcall_args_1+
'conv2d_8_statefulpartitionedcall_args_2-
)top_pool_1_statefulpartitionedcall_args_1-
)top_pool_1_statefulpartitionedcall_args_2+
'conv2d_9_statefulpartitionedcall_args_1+
'conv2d_9_statefulpartitionedcall_args_2*
&dense_8_statefulpartitionedcall_args_1*
&dense_8_statefulpartitionedcall_args_2*
&dense_9_statefulpartitionedcall_args_1*
&dense_9_statefulpartitionedcall_args_2
identityЂ conv2d_8/StatefulPartitionedCallЂ conv2d_9/StatefulPartitionedCallЂdense_8/StatefulPartitionedCallЂdense_9/StatefulPartitionedCallЂ"top_pool_1/StatefulPartitionedCallВ
 conv2d_8/StatefulPartitionedCallStatefulPartitionedCallinputs'conv2d_8_statefulpartitionedcall_args_1'conv2d_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_8_layer_call_and_return_conditional_losses_1556992"
 conv2d_8/StatefulPartitionedCallп
"top_pool_1/StatefulPartitionedCallStatefulPartitionedCall)conv2d_8/StatefulPartitionedCall:output:0)top_pool_1_statefulpartitionedcall_args_1)top_pool_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ

**
config_proto

GPU 

CPU2J 8*O
fJRH
F__inference_top_pool_1_layer_call_and_return_conditional_losses_1558472$
"top_pool_1/StatefulPartitionedCallз
 conv2d_9/StatefulPartitionedCallStatefulPartitionedCall+top_pool_1/StatefulPartitionedCall:output:0'conv2d_9_statefulpartitionedcall_args_1'conv2d_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_9_layer_call_and_return_conditional_losses_1557202"
 conv2d_9/StatefulPartitionedCallх
flatten_4/PartitionedCallPartitionedCall)conv2d_9/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџР**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_flatten_4_layer_call_and_return_conditional_losses_1558682
flatten_4/PartitionedCallТ
dense_8/StatefulPartitionedCallStatefulPartitionedCall"flatten_4/PartitionedCall:output:0&dense_8_statefulpartitionedcall_args_1&dense_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_1558862!
dense_8/StatefulPartitionedCallЧ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0&dense_9_statefulpartitionedcall_args_1&dense_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_1559082!
dense_9/StatefulPartitionedCallЋ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0!^conv2d_8/StatefulPartitionedCall!^conv2d_9/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall#^top_pool_1/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::2D
 conv2d_8/StatefulPartitionedCall conv2d_8/StatefulPartitionedCall2D
 conv2d_9/StatefulPartitionedCall conv2d_9/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2H
"top_pool_1/StatefulPartitionedCall"top_pool_1/StatefulPartitionedCall:& "
 
_user_specified_nameinputs
жI
Є
__inference__traced_save_156643
file_prefix.
*savev2_conv2d_8_kernel_read_readvariableop,
(savev2_conv2d_8_bias_read_readvariableop+
'savev2_top_pool_1_w_read_readvariableop+
'savev2_top_pool_1_b_read_readvariableop.
*savev2_conv2d_9_kernel_read_readvariableop,
(savev2_conv2d_9_bias_read_readvariableop-
)savev2_dense_8_kernel_read_readvariableop+
'savev2_dense_8_bias_read_readvariableop-
)savev2_dense_9_kernel_read_readvariableop+
'savev2_dense_9_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop5
1savev2_adam_conv2d_8_kernel_m_read_readvariableop3
/savev2_adam_conv2d_8_bias_m_read_readvariableop2
.savev2_adam_top_pool_1_w_m_read_readvariableop2
.savev2_adam_top_pool_1_b_m_read_readvariableop5
1savev2_adam_conv2d_9_kernel_m_read_readvariableop3
/savev2_adam_conv2d_9_bias_m_read_readvariableop4
0savev2_adam_dense_8_kernel_m_read_readvariableop2
.savev2_adam_dense_8_bias_m_read_readvariableop4
0savev2_adam_dense_9_kernel_m_read_readvariableop2
.savev2_adam_dense_9_bias_m_read_readvariableop5
1savev2_adam_conv2d_8_kernel_v_read_readvariableop3
/savev2_adam_conv2d_8_bias_v_read_readvariableop2
.savev2_adam_top_pool_1_w_v_read_readvariableop2
.savev2_adam_top_pool_1_b_v_read_readvariableop5
1savev2_adam_conv2d_9_kernel_v_read_readvariableop3
/savev2_adam_conv2d_9_bias_v_read_readvariableop4
0savev2_adam_dense_8_kernel_v_read_readvariableop2
.savev2_adam_dense_8_bias_v_read_readvariableop4
0savev2_adam_dense_9_kernel_v_read_readvariableop2
.savev2_adam_dense_9_bias_v_read_readvariableop
savev2_1_const

identity_1ЂMergeV2CheckpointsЂSaveV2ЂSaveV2_1Ѕ
StringJoin/inputs_1Const"/device:CPU:0*
_output_shapes
: *
dtype0*<
value3B1 B+_temp_9927c8ae17c441a7acb9ed3acb37b2f1/part2
StringJoin/inputs_1

StringJoin
StringJoinfile_prefixStringJoin/inputs_1:output:0"/device:CPU:0*
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardІ
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameо
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*№
valueцBу%B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/w/.ATTRIBUTES/VARIABLE_VALUEB1layer_with_weights-1/b/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/w/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBMlayer_with_weights-1/b/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_namesв
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:%*
dtype0*]
valueTBR%B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesо
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0*savev2_conv2d_8_kernel_read_readvariableop(savev2_conv2d_8_bias_read_readvariableop'savev2_top_pool_1_w_read_readvariableop'savev2_top_pool_1_b_read_readvariableop*savev2_conv2d_9_kernel_read_readvariableop(savev2_conv2d_9_bias_read_readvariableop)savev2_dense_8_kernel_read_readvariableop'savev2_dense_8_bias_read_readvariableop)savev2_dense_9_kernel_read_readvariableop'savev2_dense_9_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop1savev2_adam_conv2d_8_kernel_m_read_readvariableop/savev2_adam_conv2d_8_bias_m_read_readvariableop.savev2_adam_top_pool_1_w_m_read_readvariableop.savev2_adam_top_pool_1_b_m_read_readvariableop1savev2_adam_conv2d_9_kernel_m_read_readvariableop/savev2_adam_conv2d_9_bias_m_read_readvariableop0savev2_adam_dense_8_kernel_m_read_readvariableop.savev2_adam_dense_8_bias_m_read_readvariableop0savev2_adam_dense_9_kernel_m_read_readvariableop.savev2_adam_dense_9_bias_m_read_readvariableop1savev2_adam_conv2d_8_kernel_v_read_readvariableop/savev2_adam_conv2d_8_bias_v_read_readvariableop.savev2_adam_top_pool_1_w_v_read_readvariableop.savev2_adam_top_pool_1_b_v_read_readvariableop1savev2_adam_conv2d_9_kernel_v_read_readvariableop/savev2_adam_conv2d_9_bias_v_read_readvariableop0savev2_adam_dense_8_kernel_v_read_readvariableop.savev2_adam_dense_8_bias_v_read_readvariableop0savev2_adam_dense_9_kernel_v_read_readvariableop.savev2_adam_dense_9_bias_v_read_readvariableop"/device:CPU:0*
_output_shapes
 *3
dtypes)
'2%	2
SaveV2
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shardЌ
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1Ђ
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_names
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slicesЯ
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1у
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesЌ
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*ї
_input_shapesх
т: :::::::
Р::	
:
: : : : : : : :::::::
Р::	
:
:::::::
Р::	
:
: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:+ '
%
_user_specified_namefile_prefix
ё 
й
H__inference_sequential_4_layer_call_and_return_conditional_losses_155941
input_5+
'conv2d_8_statefulpartitionedcall_args_1+
'conv2d_8_statefulpartitionedcall_args_2-
)top_pool_1_statefulpartitionedcall_args_1-
)top_pool_1_statefulpartitionedcall_args_2+
'conv2d_9_statefulpartitionedcall_args_1+
'conv2d_9_statefulpartitionedcall_args_2*
&dense_8_statefulpartitionedcall_args_1*
&dense_8_statefulpartitionedcall_args_2*
&dense_9_statefulpartitionedcall_args_1*
&dense_9_statefulpartitionedcall_args_2
identityЂ conv2d_8/StatefulPartitionedCallЂ conv2d_9/StatefulPartitionedCallЂdense_8/StatefulPartitionedCallЂdense_9/StatefulPartitionedCallЂ"top_pool_1/StatefulPartitionedCallГ
 conv2d_8/StatefulPartitionedCallStatefulPartitionedCallinput_5'conv2d_8_statefulpartitionedcall_args_1'conv2d_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_8_layer_call_and_return_conditional_losses_1556992"
 conv2d_8/StatefulPartitionedCallп
"top_pool_1/StatefulPartitionedCallStatefulPartitionedCall)conv2d_8/StatefulPartitionedCall:output:0)top_pool_1_statefulpartitionedcall_args_1)top_pool_1_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ

**
config_proto

GPU 

CPU2J 8*O
fJRH
F__inference_top_pool_1_layer_call_and_return_conditional_losses_1558472$
"top_pool_1/StatefulPartitionedCallз
 conv2d_9/StatefulPartitionedCallStatefulPartitionedCall+top_pool_1/StatefulPartitionedCall:output:0'conv2d_9_statefulpartitionedcall_args_1'conv2d_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_9_layer_call_and_return_conditional_losses_1557202"
 conv2d_9/StatefulPartitionedCallх
flatten_4/PartitionedCallPartitionedCall)conv2d_9/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџР**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_flatten_4_layer_call_and_return_conditional_losses_1558682
flatten_4/PartitionedCallТ
dense_8/StatefulPartitionedCallStatefulPartitionedCall"flatten_4/PartitionedCall:output:0&dense_8_statefulpartitionedcall_args_1&dense_8_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџ**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_8_layer_call_and_return_conditional_losses_1558862!
dense_8/StatefulPartitionedCallЧ
dense_9/StatefulPartitionedCallStatefulPartitionedCall(dense_8/StatefulPartitionedCall:output:0&dense_9_statefulpartitionedcall_args_1&dense_9_statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8*L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_1559082!
dense_9/StatefulPartitionedCallЋ
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0!^conv2d_8/StatefulPartitionedCall!^conv2d_9/StatefulPartitionedCall ^dense_8/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall#^top_pool_1/StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::2D
 conv2d_8/StatefulPartitionedCall conv2d_8/StatefulPartitionedCall2D
 conv2d_9/StatefulPartitionedCall conv2d_9/StatefulPartitionedCall2B
dense_8/StatefulPartitionedCalldense_8/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2H
"top_pool_1/StatefulPartitionedCall"top_pool_1/StatefulPartitionedCall:' #
!
_user_specified_name	input_5
Р
Њ
)__inference_conv2d_9_layer_call_fn_155728

inputs"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityЂStatefulPartitionedCall 
StatefulPartitionedCallStatefulPartitionedCallinputsstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ**
config_proto

GPU 

CPU2J 8*M
fHRF
D__inference_conv2d_9_layer_call_and_return_conditional_losses_1557202
StatefulPartitionedCallЈ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ::22
StatefulPartitionedCallStatefulPartitionedCall:& "
 
_user_specified_nameinputs
п
F
*__inference_flatten_4_layer_call_fn_156474

inputs
identityЎ
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*(
_output_shapes
:џџџџџџџџџР**
config_proto

GPU 

CPU2J 8*N
fIRG
E__inference_flatten_4_layer_call_and_return_conditional_losses_1558682
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2

Identity"
identityIdentity:output:0*.
_input_shapes
:џџџџџџџџџ:& "
 
_user_specified_nameinputs
ёw
л
F__inference_top_pool_1_layer_call_and_return_conditional_losses_156456
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identityЂExpandDims/ReadVariableOpЂadd/ReadVariableOpа
ExtractImagePatchesExtractImagePatchesin*
T0*0
_output_shapes
:џџџџџџџџџ

Ш*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3­
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2	
Reshape
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddingsЁ
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:џџџџџџџџџ*
mode	SYMMETRIC2
	MirrorPadу
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:џџџџџџџџџH*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ю
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2ю
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5њ
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shapeА
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2ю
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2ю
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2ю
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4о
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/Rank
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transpositionЕ
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
sort/transposeщ
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*
_output_shapesv
t:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
sort/TopKV2Ъ
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
sort/transpose_1

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2

sort/Neg_1
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2Й
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2Й
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2ю
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4Р
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shape
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
	Reshape_3
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dim

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dim
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dim
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
Sum
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
add
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:џџџџџџџџџ

2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:џџџџџџџџџ::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn
ёw
л
F__inference_top_pool_1_layer_call_and_return_conditional_losses_155847
in&
"expanddims_readvariableop_resource
add_readvariableop_resource
identityЂExpandDims/ReadVariableOpЂadd/ReadVariableOpа
ExtractImagePatchesExtractImagePatchesin*
T0*0
_output_shapes
:џџџџџџџџџ

Ш*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches@
ShapeShapein*
T0*
_output_shapes
:2
Shape_
Shape_1ShapeExtractImagePatches:patches:0*
T0*
_output_shapes
:2	
Shape_1t
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2т
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliceP
mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
mul/yZ
mulMulstrided_slice:output:0mul/y:output:0*
T0*
_output_shapes
: 2
mulT
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2	
mul_1/yQ
mul_1Mulmul:z:0mul_1/y:output:0*
T0*
_output_shapes
: 2
mul_1d
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3­
Reshape/shapePack	mul_1:z:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape
ReshapeReshapeExtractImagePatches:patches:0Reshape/shape:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2	
Reshape
MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
MirrorPad/paddingsЁ
	MirrorPad	MirrorPadReshape:output:0MirrorPad/paddings:output:0*
T0*/
_output_shapes
:џџџџџџџџџ*
mode	SYMMETRIC2
	MirrorPadу
ExtractImagePatches_1ExtractImagePatchesMirrorPad:output:0*
T0*/
_output_shapes
:џџџџџџџџџH*
ksizes
*
paddingVALID*
rates
*
strides
2
ExtractImagePatches_1a
Shape_2ShapeExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2	
Shape_2T
Shape_3ShapeMirrorPad:output:0*
T0*
_output_shapes
:2	
Shape_3x
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_1/stack|
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2ю
strided_slice_1StridedSliceShape_3:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1x
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack|
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2ю
strided_slice_2StridedSliceShape_2:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2x
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack|
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_1|
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_3/stack_2ю
strided_slice_3StridedSliceShape_2:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_3h
Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/3h
Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/4h
Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_1/shape/5њ
Reshape_1/shapePackstrided_slice_1:output:0strided_slice_2:output:0strided_slice_3:output:0Reshape_1/shape/3:output:0Reshape_1/shape/4:output:0Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
Reshape_1/shapeА
	Reshape_1ReshapeExtractImagePatches_1:patches:0Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
	Reshape_1T
Shape_4ShapeReshape_1:output:0*
T0*
_output_shapes
:2	
Shape_4x
strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_4/stack|
strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_1|
strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_4/stack_2ю
strided_slice_4StridedSliceShape_4:output:0strided_slice_4/stack:output:0 strided_slice_4/stack_1:output:0 strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_4x
strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack|
strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_1|
strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_5/stack_2ю
strided_slice_5StridedSliceShape_4:output:0strided_slice_5/stack:output:0 strided_slice_5/stack_1:output:0 strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_5x
strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack|
strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_1|
strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_6/stack_2ю
strided_slice_6StridedSliceShape_4:output:0strided_slice_6/stack:output:0 strided_slice_6/stack_1:output:0 strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_6h
Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
Reshape_2/shape/3h
Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_2/shape/4о
Reshape_2/shapePackstrided_slice_4:output:0strided_slice_5:output:0strided_slice_6:output:0Reshape_2/shape/3:output:0Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_2/shape
	Reshape_2ReshapeReshape_1:output:0Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
	Reshape_2X
	sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/axis
sort/NegNegReshape_2:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2

sort/NegT

sort/ShapeShapesort/Neg:y:0*
T0*
_output_shapes
:2

sort/Shape~
sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack
sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_1
sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
sort/strided_slice/stack_2
sort/strided_sliceStridedSlicesort/Shape:output:0!sort/strided_slice/stack:output:0#sort/strided_slice/stack_1:output:0#sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
sort/strided_sliceX
	sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
	sort/Rank
sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
sort/transpositionЕ
sort/transpose	Transposesort/Neg:y:0sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
sort/transposeщ
sort/TopKV2TopKV2sort/transpose:y:0sort/strided_slice:output:0*
T0*
_output_shapesv
t:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
sort/TopKV2Ъ
sort/transpose_1	Transposesort/TopKV2:values:0sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
sort/transpose_1

sort/Neg_1Negsort/transpose_1:y:0*
T0*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2

sort/Neg_1
strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack
strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_7/stack_1
strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_7/stack_2Й
strided_slice_7StridedSlicesort/Neg_1:y:0strided_slice_7/stack:output:0 strided_slice_7/stack_1:output:0 strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_7
strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack
strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2
strided_slice_8/stack_1
strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2
strided_slice_8/stack_2Й
strided_slice_8StridedSlicesort/Neg_1:y:0strided_slice_8/stack:output:0 strided_slice_8/stack_1:output:0 strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_8
subSubstrided_slice_8:output:0strided_slice_7:output:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
subW
Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2	
Floor/xJ
FloorFloorFloor/x:output:0*
T0*
_output_shapes
: 2
FloorO
CastCast	Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast[
	Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
	Floor_1/xP
Floor_1FloorFloor_1/x:output:0*
T0*
_output_shapes
: 2	
Floor_1U
Cast_1CastFloor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1I
Shape_5Shapesub:z:0*
T0*
_output_shapes
:2	
Shape_5D
Shape_6Shapein*
T0*
_output_shapes
:2	
Shape_6x
strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice_9/stack|
strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_1|
strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_9/stack_2ю
strided_slice_9StridedSliceShape_6:output:0strided_slice_9/stack:output:0 strided_slice_9/stack_1:output:0 strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_9h
Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/3h
Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape_3/shape/4Р
Reshape_3/shapePackstrided_slice_9:output:0Cast:y:0
Cast_1:y:0Reshape_3/shape/3:output:0Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
Reshape_3/shape
	Reshape_3Reshapesub:z:0Reshape_3/shape:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
	Reshape_3
ExpandDims/ReadVariableOpReadVariableOp"expanddims_readvariableop_resource*
_output_shapes

:*
dtype02
ExpandDims/ReadVariableOpb
ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
ExpandDims/dim

ExpandDims
ExpandDims!ExpandDims/ReadVariableOp:value:0ExpandDims/dim:output:0*
T0*"
_output_shapes
:2

ExpandDimsf
ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_1/dim
ExpandDims_1
ExpandDimsExpandDims:output:0ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
ExpandDims_1f
ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
ExpandDims_2/dim
ExpandDims_2
ExpandDimsExpandDims_1:output:0ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
ExpandDims_2~
mul_2MulReshape_3:output:0ExpandDims_2:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
mul_2p
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2
Sum/reduction_indicesv
SumSum	mul_2:z:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
Sum
add/ReadVariableOpReadVariableOpadd_readvariableop_resource*&
_output_shapes
:*
dtype02
add/ReadVariableOpw
addAddV2Sum:output:0add/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
add
IdentityIdentityadd:z:0^ExpandDims/ReadVariableOp^add/ReadVariableOp*
T0*/
_output_shapes
:џџџџџџџџџ

2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:џџџџџџџџџ::26
ExpandDims/ReadVariableOpExpandDims/ReadVariableOp2(
add/ReadVariableOpadd/ReadVariableOp:" 

_user_specified_nameIn

a
E__inference_flatten_4_layer_call_and_return_conditional_losses_155868

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"џџџџ@  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2

Identity"
identityIdentity:output:0*.
_input_shapes
:џџџџџџџџџ:& "
 
_user_specified_nameinputs
ѓ

н
D__inference_conv2d_8_layer_call_and_return_conditional_losses_155699

inputs"
conv2d_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂConv2D/ReadVariableOpo
dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      2
dilation_rate
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
Conv2D/ReadVariableOpЖ
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*
paddingVALID*
strides
2
Conv2D
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2	
BiasAddЏ
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:& "
 
_user_specified_nameinputs

Ј
+__inference_top_pool_1_layer_call_fn_156463
in"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2
identityЂStatefulPartitionedCall
StatefulPartitionedCallStatefulPartitionedCallinstatefulpartitionedcall_args_1statefulpartitionedcall_args_2*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*/
_output_shapes
:џџџџџџџџџ

**
config_proto

GPU 

CPU2J 8*O
fJRH
F__inference_top_pool_1_layer_call_and_return_conditional_losses_1558472
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:џџџџџџџџџ

2

Identity"
identityIdentity:output:0*6
_input_shapes%
#:џџџџџџџџџ::22
StatefulPartitionedCallStatefulPartitionedCall:" 

_user_specified_nameIn
ъ
м
C__inference_dense_9_layer_call_and_return_conditional_losses_156501

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂMatMul/ReadVariableOp
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
MatMul
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*/
_input_shapes
:џџџџџџџџџ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs

a
E__inference_flatten_4_layer_call_and_return_conditional_losses_156469

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"џџџџ@  2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2

Identity"
identityIdentity:output:0*.
_input_shapes
:џџџџџџџџџ:& "
 
_user_specified_nameinputs

Ч
$__inference_signature_wrapper_156036
input_5"
statefulpartitionedcall_args_1"
statefulpartitionedcall_args_2"
statefulpartitionedcall_args_3"
statefulpartitionedcall_args_4"
statefulpartitionedcall_args_5"
statefulpartitionedcall_args_6"
statefulpartitionedcall_args_7"
statefulpartitionedcall_args_8"
statefulpartitionedcall_args_9#
statefulpartitionedcall_args_10
identityЂStatefulPartitionedCallэ
StatefulPartitionedCallStatefulPartitionedCallinput_5statefulpartitionedcall_args_1statefulpartitionedcall_args_2statefulpartitionedcall_args_3statefulpartitionedcall_args_4statefulpartitionedcall_args_5statefulpartitionedcall_args_6statefulpartitionedcall_args_7statefulpartitionedcall_args_8statefulpartitionedcall_args_9statefulpartitionedcall_args_10*
Tin
2*
Tout
2*,
_gradient_op_typePartitionedCallUnused*'
_output_shapes
:џџџџџџџџџ
**
config_proto

GPU 

CPU2J 8**
f%R#
!__inference__wrapped_model_1556872
StatefulPartitionedCall
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:' #
!
_user_specified_name	input_5
щ
н
D__inference_conv2d_9_layer_call_and_return_conditional_losses_155720

inputs"
conv2d_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂConv2D/ReadVariableOpo
dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      2
dilation_rate
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
Conv2D/ReadVariableOpЖ
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*
paddingVALID*
strides
2
Conv2D
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2	
BiasAddr
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
ReluБ
IdentityIdentityRelu:activations:0^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2

Identity"
identityIdentity:output:0*H
_input_shapes7
5:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:& "
 
_user_specified_nameinputs
ЧЙ
§
H__inference_sequential_4_layer_call_and_return_conditional_losses_156314

inputs+
'conv2d_8_conv2d_readvariableop_resource,
(conv2d_8_biasadd_readvariableop_resource1
-top_pool_1_expanddims_readvariableop_resource*
&top_pool_1_add_readvariableop_resource+
'conv2d_9_conv2d_readvariableop_resource,
(conv2d_9_biasadd_readvariableop_resource*
&dense_8_matmul_readvariableop_resource+
'dense_8_biasadd_readvariableop_resource*
&dense_9_matmul_readvariableop_resource+
'dense_9_biasadd_readvariableop_resource
identityЂconv2d_8/BiasAdd/ReadVariableOpЂconv2d_8/Conv2D/ReadVariableOpЂconv2d_9/BiasAdd/ReadVariableOpЂconv2d_9/Conv2D/ReadVariableOpЂdense_8/BiasAdd/ReadVariableOpЂdense_8/MatMul/ReadVariableOpЂdense_9/BiasAdd/ReadVariableOpЂdense_9/MatMul/ReadVariableOpЂ$top_pool_1/ExpandDims/ReadVariableOpЂtop_pool_1/add/ReadVariableOpА
conv2d_8/Conv2D/ReadVariableOpReadVariableOp'conv2d_8_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02 
conv2d_8/Conv2D/ReadVariableOpП
conv2d_8/Conv2DConv2Dinputs&conv2d_8/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ*
paddingVALID*
strides
2
conv2d_8/Conv2DЇ
conv2d_8/BiasAdd/ReadVariableOpReadVariableOp(conv2d_8_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
conv2d_8/BiasAdd/ReadVariableOpЌ
conv2d_8/BiasAddBiasAddconv2d_8/Conv2D:output:0'conv2d_8/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ2
conv2d_8/BiasAdd§
top_pool_1/ExtractImagePatchesExtractImagePatchesconv2d_8/BiasAdd:output:0*
T0*0
_output_shapes
:џџџџџџџџџ

Ш*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool_1/ExtractImagePatchesm
top_pool_1/ShapeShapeconv2d_8/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape
top_pool_1/Shape_1Shape(top_pool_1/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_1/Shape_1
top_pool_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool_1/strided_slice/stack
 top_pool_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice/stack_1
 top_pool_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice/stack_2Є
top_pool_1/strided_sliceStridedSlicetop_pool_1/Shape:output:0'top_pool_1/strided_slice/stack:output:0)top_pool_1/strided_slice/stack_1:output:0)top_pool_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slicef
top_pool_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_1/mul/y
top_pool_1/mulMul!top_pool_1/strided_slice:output:0top_pool_1/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_1/mulj
top_pool_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_1/mul_1/y}
top_pool_1/mul_1Multop_pool_1/mul:z:0top_pool_1/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_1/mul_1z
top_pool_1/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape/shape/1z
top_pool_1/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape/shape/2z
top_pool_1/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape/shape/3я
top_pool_1/Reshape/shapePacktop_pool_1/mul_1:z:0#top_pool_1/Reshape/shape/1:output:0#top_pool_1/Reshape/shape/2:output:0#top_pool_1/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape/shapeК
top_pool_1/ReshapeReshape(top_pool_1/ExtractImagePatches:patches:0!top_pool_1/Reshape/shape:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2
top_pool_1/ReshapeЏ
top_pool_1/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool_1/MirrorPad/paddingsЭ
top_pool_1/MirrorPad	MirrorPadtop_pool_1/Reshape:output:0&top_pool_1/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:џџџџџџџџџ*
mode	SYMMETRIC2
top_pool_1/MirrorPad
 top_pool_1/ExtractImagePatches_1ExtractImagePatchestop_pool_1/MirrorPad:output:0*
T0*/
_output_shapes
:џџџџџџџџџH*
ksizes
*
paddingVALID*
rates
*
strides
2"
 top_pool_1/ExtractImagePatches_1
top_pool_1/Shape_2Shape*top_pool_1/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_1/Shape_2u
top_pool_1/Shape_3Shapetop_pool_1/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape_3
 top_pool_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_1/strided_slice_1/stack
"top_pool_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_1/stack_1
"top_pool_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_1/stack_2А
top_pool_1/strided_slice_1StridedSlicetop_pool_1/Shape_3:output:0)top_pool_1/strided_slice_1/stack:output:0+top_pool_1/strided_slice_1/stack_1:output:0+top_pool_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_1
 top_pool_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_2/stack
"top_pool_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_2/stack_1
"top_pool_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_2/stack_2А
top_pool_1/strided_slice_2StridedSlicetop_pool_1/Shape_2:output:0)top_pool_1/strided_slice_2/stack:output:0+top_pool_1/strided_slice_2/stack_1:output:0+top_pool_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_2
 top_pool_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_3/stack
"top_pool_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_3/stack_1
"top_pool_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_3/stack_2А
top_pool_1/strided_slice_3StridedSlicetop_pool_1/Shape_2:output:0)top_pool_1/strided_slice_3/stack:output:0+top_pool_1/strided_slice_3/stack_1:output:0+top_pool_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_3~
top_pool_1/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_1/shape/3~
top_pool_1/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_1/shape/4~
top_pool_1/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_1/shape/5в
top_pool_1/Reshape_1/shapePack#top_pool_1/strided_slice_1:output:0#top_pool_1/strided_slice_2:output:0#top_pool_1/strided_slice_3:output:0%top_pool_1/Reshape_1/shape/3:output:0%top_pool_1/Reshape_1/shape/4:output:0%top_pool_1/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape_1/shapeм
top_pool_1/Reshape_1Reshape*top_pool_1/ExtractImagePatches_1:patches:0#top_pool_1/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/Reshape_1u
top_pool_1/Shape_4Shapetop_pool_1/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape_4
 top_pool_1/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_1/strided_slice_4/stack
"top_pool_1/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_4/stack_1
"top_pool_1/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_4/stack_2А
top_pool_1/strided_slice_4StridedSlicetop_pool_1/Shape_4:output:0)top_pool_1/strided_slice_4/stack:output:0+top_pool_1/strided_slice_4/stack_1:output:0+top_pool_1/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_4
 top_pool_1/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_5/stack
"top_pool_1/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_5/stack_1
"top_pool_1/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_5/stack_2А
top_pool_1/strided_slice_5StridedSlicetop_pool_1/Shape_4:output:0)top_pool_1/strided_slice_5/stack:output:0+top_pool_1/strided_slice_5/stack_1:output:0+top_pool_1/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_5
 top_pool_1/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_6/stack
"top_pool_1/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_6/stack_1
"top_pool_1/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_6/stack_2А
top_pool_1/strided_slice_6StridedSlicetop_pool_1/Shape_4:output:0)top_pool_1/strided_slice_6/stack:output:0+top_pool_1/strided_slice_6/stack_1:output:0+top_pool_1/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_6~
top_pool_1/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool_1/Reshape_2/shape/3~
top_pool_1/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_2/shape/4Ћ
top_pool_1/Reshape_2/shapePack#top_pool_1/strided_slice_4:output:0#top_pool_1/strided_slice_5:output:0#top_pool_1/strided_slice_6:output:0%top_pool_1/Reshape_2/shape/3:output:0%top_pool_1/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape_2/shapeЫ
top_pool_1/Reshape_2Reshapetop_pool_1/Reshape_1:output:0#top_pool_1/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
top_pool_1/Reshape_2n
top_pool_1/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/sort/axis 
top_pool_1/sort/NegNegtop_pool_1/Reshape_2:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
top_pool_1/sort/Negu
top_pool_1/sort/ShapeShapetop_pool_1/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool_1/sort/Shape
#top_pool_1/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool_1/sort/strided_slice/stack
%top_pool_1/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_1/sort/strided_slice/stack_1
%top_pool_1/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_1/sort/strided_slice/stack_2Т
top_pool_1/sort/strided_sliceStridedSlicetop_pool_1/sort/Shape:output:0,top_pool_1/sort/strided_slice/stack:output:0.top_pool_1/sort/strided_slice/stack_1:output:0.top_pool_1/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/sort/strided_slicen
top_pool_1/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/sort/RankЏ
top_pool_1/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool_1/sort/transpositionс
top_pool_1/sort/transpose	Transposetop_pool_1/sort/Neg:y:0&top_pool_1/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
top_pool_1/sort/transpose
top_pool_1/sort/TopKV2TopKV2top_pool_1/sort/transpose:y:0&top_pool_1/sort/strided_slice:output:0*
T0*
_output_shapesv
t:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/sort/TopKV2і
top_pool_1/sort/transpose_1	Transposetop_pool_1/sort/TopKV2:values:0&top_pool_1/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/sort/transpose_1Џ
top_pool_1/sort/Neg_1Negtop_pool_1/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/sort/Neg_1Ё
 top_pool_1/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_1/strided_slice_7/stackЅ
"top_pool_1/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_1/strided_slice_7/stack_1Ѕ
"top_pool_1/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_1/strided_slice_7/stack_2ћ
top_pool_1/strided_slice_7StridedSlicetop_pool_1/sort/Neg_1:y:0)top_pool_1/strided_slice_7/stack:output:0+top_pool_1/strided_slice_7/stack_1:output:0+top_pool_1/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_1/strided_slice_7Ё
 top_pool_1/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_1/strided_slice_8/stackЅ
"top_pool_1/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_1/strided_slice_8/stack_1Ѕ
"top_pool_1/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_1/strided_slice_8/stack_2ћ
top_pool_1/strided_slice_8StridedSlicetop_pool_1/sort/Neg_1:y:0)top_pool_1/strided_slice_8/stack:output:0+top_pool_1/strided_slice_8/stack_1:output:0+top_pool_1/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_1/strided_slice_8Н
top_pool_1/subSub#top_pool_1/strided_slice_8:output:0#top_pool_1/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/subm
top_pool_1/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_1/Floor/xk
top_pool_1/FloorFloortop_pool_1/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_1/Floorp
top_pool_1/CastCasttop_pool_1/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_1/Castq
top_pool_1/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_1/Floor_1/xq
top_pool_1/Floor_1Floortop_pool_1/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_1/Floor_1v
top_pool_1/Cast_1Casttop_pool_1/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_1/Cast_1j
top_pool_1/Shape_5Shapetop_pool_1/sub:z:0*
T0*
_output_shapes
:2
top_pool_1/Shape_5q
top_pool_1/Shape_6Shapeconv2d_8/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape_6
 top_pool_1/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_1/strided_slice_9/stack
"top_pool_1/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_9/stack_1
"top_pool_1/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_9/stack_2А
top_pool_1/strided_slice_9StridedSlicetop_pool_1/Shape_6:output:0)top_pool_1/strided_slice_9/stack:output:0+top_pool_1/strided_slice_9/stack_1:output:0+top_pool_1/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_9~
top_pool_1/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_3/shape/3~
top_pool_1/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_3/shape/4
top_pool_1/Reshape_3/shapePack#top_pool_1/strided_slice_9:output:0top_pool_1/Cast:y:0top_pool_1/Cast_1:y:0%top_pool_1/Reshape_3/shape/3:output:0%top_pool_1/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape_3/shapeЎ
top_pool_1/Reshape_3Reshapetop_pool_1/sub:z:0#top_pool_1/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
top_pool_1/Reshape_3К
$top_pool_1/ExpandDims/ReadVariableOpReadVariableOp-top_pool_1_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02&
$top_pool_1/ExpandDims/ReadVariableOpx
top_pool_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool_1/ExpandDims/dimЛ
top_pool_1/ExpandDims
ExpandDims,top_pool_1/ExpandDims/ReadVariableOp:value:0"top_pool_1/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool_1/ExpandDims|
top_pool_1/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/ExpandDims_1/dimЗ
top_pool_1/ExpandDims_1
ExpandDimstop_pool_1/ExpandDims:output:0$top_pool_1/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool_1/ExpandDims_1|
top_pool_1/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/ExpandDims_2/dimН
top_pool_1/ExpandDims_2
ExpandDims top_pool_1/ExpandDims_1:output:0$top_pool_1/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool_1/ExpandDims_2Њ
top_pool_1/mul_2Multop_pool_1/Reshape_3:output:0 top_pool_1/ExpandDims_2:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
top_pool_1/mul_2
 top_pool_1/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_1/Sum/reduction_indicesЂ
top_pool_1/SumSumtop_pool_1/mul_2:z:0)top_pool_1/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
top_pool_1/Sum­
top_pool_1/add/ReadVariableOpReadVariableOp&top_pool_1_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool_1/add/ReadVariableOpЃ
top_pool_1/addAddV2top_pool_1/Sum:output:0%top_pool_1/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
top_pool_1/addА
conv2d_9/Conv2D/ReadVariableOpReadVariableOp'conv2d_9_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02 
conv2d_9/Conv2D/ReadVariableOpЫ
conv2d_9/Conv2DConv2Dtop_pool_1/add:z:0&conv2d_9/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ*
paddingVALID*
strides
2
conv2d_9/Conv2DЇ
conv2d_9/BiasAdd/ReadVariableOpReadVariableOp(conv2d_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
conv2d_9/BiasAdd/ReadVariableOpЌ
conv2d_9/BiasAddBiasAddconv2d_9/Conv2D:output:0'conv2d_9/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ2
conv2d_9/BiasAdd{
conv2d_9/ReluReluconv2d_9/BiasAdd:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2
conv2d_9/Relus
flatten_4/ConstConst*
_output_shapes
:*
dtype0*
valueB"џџџџ@  2
flatten_4/Const
flatten_4/ReshapeReshapeconv2d_9/Relu:activations:0flatten_4/Const:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2
flatten_4/ReshapeЇ
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource* 
_output_shapes
:
Р*
dtype02
dense_8/MatMul/ReadVariableOp 
dense_8/MatMulMatMulflatten_4/Reshape:output:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
dense_8/MatMulЅ
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes	
:*
dtype02 
dense_8/BiasAdd/ReadVariableOpЂ
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
dense_8/BiasAddІ
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource*
_output_shapes
:	
*
dtype02
dense_9/MatMul/ReadVariableOp
dense_9/MatMulMatMuldense_8/BiasAdd:output:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
dense_9/MatMulЄ
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02 
dense_9/BiasAdd/ReadVariableOpЁ
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
dense_9/BiasAddЛ
IdentityIdentitydense_9/BiasAdd:output:0 ^conv2d_8/BiasAdd/ReadVariableOp^conv2d_8/Conv2D/ReadVariableOp ^conv2d_9/BiasAdd/ReadVariableOp^conv2d_9/Conv2D/ReadVariableOp^dense_8/BiasAdd/ReadVariableOp^dense_8/MatMul/ReadVariableOp^dense_9/BiasAdd/ReadVariableOp^dense_9/MatMul/ReadVariableOp%^top_pool_1/ExpandDims/ReadVariableOp^top_pool_1/add/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::2B
conv2d_8/BiasAdd/ReadVariableOpconv2d_8/BiasAdd/ReadVariableOp2@
conv2d_8/Conv2D/ReadVariableOpconv2d_8/Conv2D/ReadVariableOp2B
conv2d_9/BiasAdd/ReadVariableOpconv2d_9/BiasAdd/ReadVariableOp2@
conv2d_9/Conv2D/ReadVariableOpconv2d_9/Conv2D/ReadVariableOp2@
dense_8/BiasAdd/ReadVariableOpdense_8/BiasAdd/ReadVariableOp2>
dense_8/MatMul/ReadVariableOpdense_8/MatMul/ReadVariableOp2@
dense_9/BiasAdd/ReadVariableOpdense_9/BiasAdd/ReadVariableOp2>
dense_9/MatMul/ReadVariableOpdense_9/MatMul/ReadVariableOp2L
$top_pool_1/ExpandDims/ReadVariableOp$top_pool_1/ExpandDims/ReadVariableOp2>
top_pool_1/add/ReadVariableOptop_pool_1/add/ReadVariableOp:& "
 
_user_specified_nameinputs
Јц
л
!__inference__wrapped_model_155687
input_58
4sequential_4_conv2d_8_conv2d_readvariableop_resource9
5sequential_4_conv2d_8_biasadd_readvariableop_resource>
:sequential_4_top_pool_1_expanddims_readvariableop_resource7
3sequential_4_top_pool_1_add_readvariableop_resource8
4sequential_4_conv2d_9_conv2d_readvariableop_resource9
5sequential_4_conv2d_9_biasadd_readvariableop_resource7
3sequential_4_dense_8_matmul_readvariableop_resource8
4sequential_4_dense_8_biasadd_readvariableop_resource7
3sequential_4_dense_9_matmul_readvariableop_resource8
4sequential_4_dense_9_biasadd_readvariableop_resource
identityЂ,sequential_4/conv2d_8/BiasAdd/ReadVariableOpЂ+sequential_4/conv2d_8/Conv2D/ReadVariableOpЂ,sequential_4/conv2d_9/BiasAdd/ReadVariableOpЂ+sequential_4/conv2d_9/Conv2D/ReadVariableOpЂ+sequential_4/dense_8/BiasAdd/ReadVariableOpЂ*sequential_4/dense_8/MatMul/ReadVariableOpЂ+sequential_4/dense_9/BiasAdd/ReadVariableOpЂ*sequential_4/dense_9/MatMul/ReadVariableOpЂ1sequential_4/top_pool_1/ExpandDims/ReadVariableOpЂ*sequential_4/top_pool_1/add/ReadVariableOpз
+sequential_4/conv2d_8/Conv2D/ReadVariableOpReadVariableOp4sequential_4_conv2d_8_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02-
+sequential_4/conv2d_8/Conv2D/ReadVariableOpч
sequential_4/conv2d_8/Conv2DConv2Dinput_53sequential_4/conv2d_8/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ*
paddingVALID*
strides
2
sequential_4/conv2d_8/Conv2DЮ
,sequential_4/conv2d_8/BiasAdd/ReadVariableOpReadVariableOp5sequential_4_conv2d_8_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02.
,sequential_4/conv2d_8/BiasAdd/ReadVariableOpр
sequential_4/conv2d_8/BiasAddBiasAdd%sequential_4/conv2d_8/Conv2D:output:04sequential_4/conv2d_8/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ2
sequential_4/conv2d_8/BiasAddЄ
+sequential_4/top_pool_1/ExtractImagePatchesExtractImagePatches&sequential_4/conv2d_8/BiasAdd:output:0*
T0*0
_output_shapes
:џџџџџџџџџ

Ш*
ksizes
*
paddingVALID*
rates
*
strides
2-
+sequential_4/top_pool_1/ExtractImagePatches
sequential_4/top_pool_1/ShapeShape&sequential_4/conv2d_8/BiasAdd:output:0*
T0*
_output_shapes
:2
sequential_4/top_pool_1/ShapeЇ
sequential_4/top_pool_1/Shape_1Shape5sequential_4/top_pool_1/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2!
sequential_4/top_pool_1/Shape_1Є
+sequential_4/top_pool_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2-
+sequential_4/top_pool_1/strided_slice/stackЈ
-sequential_4/top_pool_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_4/top_pool_1/strided_slice/stack_1Ј
-sequential_4/top_pool_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_4/top_pool_1/strided_slice/stack_2ђ
%sequential_4/top_pool_1/strided_sliceStridedSlice&sequential_4/top_pool_1/Shape:output:04sequential_4/top_pool_1/strided_slice/stack:output:06sequential_4/top_pool_1/strided_slice/stack_1:output:06sequential_4/top_pool_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2'
%sequential_4/top_pool_1/strided_slice
sequential_4/top_pool_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
sequential_4/top_pool_1/mul/yК
sequential_4/top_pool_1/mulMul.sequential_4/top_pool_1/strided_slice:output:0&sequential_4/top_pool_1/mul/y:output:0*
T0*
_output_shapes
: 2
sequential_4/top_pool_1/mul
sequential_4/top_pool_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2!
sequential_4/top_pool_1/mul_1/yБ
sequential_4/top_pool_1/mul_1Mulsequential_4/top_pool_1/mul:z:0(sequential_4/top_pool_1/mul_1/y:output:0*
T0*
_output_shapes
: 2
sequential_4/top_pool_1/mul_1
'sequential_4/top_pool_1/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2)
'sequential_4/top_pool_1/Reshape/shape/1
'sequential_4/top_pool_1/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2)
'sequential_4/top_pool_1/Reshape/shape/2
'sequential_4/top_pool_1/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2)
'sequential_4/top_pool_1/Reshape/shape/3Н
%sequential_4/top_pool_1/Reshape/shapePack!sequential_4/top_pool_1/mul_1:z:00sequential_4/top_pool_1/Reshape/shape/1:output:00sequential_4/top_pool_1/Reshape/shape/2:output:00sequential_4/top_pool_1/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2'
%sequential_4/top_pool_1/Reshape/shapeю
sequential_4/top_pool_1/ReshapeReshape5sequential_4/top_pool_1/ExtractImagePatches:patches:0.sequential_4/top_pool_1/Reshape/shape:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2!
sequential_4/top_pool_1/ReshapeЩ
*sequential_4/top_pool_1/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2,
*sequential_4/top_pool_1/MirrorPad/paddings
!sequential_4/top_pool_1/MirrorPad	MirrorPad(sequential_4/top_pool_1/Reshape:output:03sequential_4/top_pool_1/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:џџџџџџџџџ*
mode	SYMMETRIC2#
!sequential_4/top_pool_1/MirrorPadЋ
-sequential_4/top_pool_1/ExtractImagePatches_1ExtractImagePatches*sequential_4/top_pool_1/MirrorPad:output:0*
T0*/
_output_shapes
:џџџџџџџџџH*
ksizes
*
paddingVALID*
rates
*
strides
2/
-sequential_4/top_pool_1/ExtractImagePatches_1Љ
sequential_4/top_pool_1/Shape_2Shape7sequential_4/top_pool_1/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2!
sequential_4/top_pool_1/Shape_2
sequential_4/top_pool_1/Shape_3Shape*sequential_4/top_pool_1/MirrorPad:output:0*
T0*
_output_shapes
:2!
sequential_4/top_pool_1/Shape_3Ј
-sequential_4/top_pool_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-sequential_4/top_pool_1/strided_slice_1/stackЌ
/sequential_4/top_pool_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_1/stack_1Ќ
/sequential_4/top_pool_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_1/stack_2ў
'sequential_4/top_pool_1/strided_slice_1StridedSlice(sequential_4/top_pool_1/Shape_3:output:06sequential_4/top_pool_1/strided_slice_1/stack:output:08sequential_4/top_pool_1/strided_slice_1/stack_1:output:08sequential_4/top_pool_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_1Ј
-sequential_4/top_pool_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_4/top_pool_1/strided_slice_2/stackЌ
/sequential_4/top_pool_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_2/stack_1Ќ
/sequential_4/top_pool_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_2/stack_2ў
'sequential_4/top_pool_1/strided_slice_2StridedSlice(sequential_4/top_pool_1/Shape_2:output:06sequential_4/top_pool_1/strided_slice_2/stack:output:08sequential_4/top_pool_1/strided_slice_2/stack_1:output:08sequential_4/top_pool_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_2Ј
-sequential_4/top_pool_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_4/top_pool_1/strided_slice_3/stackЌ
/sequential_4/top_pool_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_3/stack_1Ќ
/sequential_4/top_pool_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_3/stack_2ў
'sequential_4/top_pool_1/strided_slice_3StridedSlice(sequential_4/top_pool_1/Shape_2:output:06sequential_4/top_pool_1/strided_slice_3/stack:output:08sequential_4/top_pool_1/strided_slice_3/stack_1:output:08sequential_4/top_pool_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_3
)sequential_4/top_pool_1/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_4/top_pool_1/Reshape_1/shape/3
)sequential_4/top_pool_1/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_4/top_pool_1/Reshape_1/shape/4
)sequential_4/top_pool_1/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_4/top_pool_1/Reshape_1/shape/5К
'sequential_4/top_pool_1/Reshape_1/shapePack0sequential_4/top_pool_1/strided_slice_1:output:00sequential_4/top_pool_1/strided_slice_2:output:00sequential_4/top_pool_1/strided_slice_3:output:02sequential_4/top_pool_1/Reshape_1/shape/3:output:02sequential_4/top_pool_1/Reshape_1/shape/4:output:02sequential_4/top_pool_1/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2)
'sequential_4/top_pool_1/Reshape_1/shape
!sequential_4/top_pool_1/Reshape_1Reshape7sequential_4/top_pool_1/ExtractImagePatches_1:patches:00sequential_4/top_pool_1/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3џџџџџџџџџџџџџџџџџџџџџџџџџџџ2#
!sequential_4/top_pool_1/Reshape_1
sequential_4/top_pool_1/Shape_4Shape*sequential_4/top_pool_1/Reshape_1:output:0*
T0*
_output_shapes
:2!
sequential_4/top_pool_1/Shape_4Ј
-sequential_4/top_pool_1/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-sequential_4/top_pool_1/strided_slice_4/stackЌ
/sequential_4/top_pool_1/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_4/stack_1Ќ
/sequential_4/top_pool_1/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_4/stack_2ў
'sequential_4/top_pool_1/strided_slice_4StridedSlice(sequential_4/top_pool_1/Shape_4:output:06sequential_4/top_pool_1/strided_slice_4/stack:output:08sequential_4/top_pool_1/strided_slice_4/stack_1:output:08sequential_4/top_pool_1/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_4Ј
-sequential_4/top_pool_1/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_4/top_pool_1/strided_slice_5/stackЌ
/sequential_4/top_pool_1/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_5/stack_1Ќ
/sequential_4/top_pool_1/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_5/stack_2ў
'sequential_4/top_pool_1/strided_slice_5StridedSlice(sequential_4/top_pool_1/Shape_4:output:06sequential_4/top_pool_1/strided_slice_5/stack:output:08sequential_4/top_pool_1/strided_slice_5/stack_1:output:08sequential_4/top_pool_1/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_5Ј
-sequential_4/top_pool_1/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2/
-sequential_4/top_pool_1/strided_slice_6/stackЌ
/sequential_4/top_pool_1/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_6/stack_1Ќ
/sequential_4/top_pool_1/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_6/stack_2ў
'sequential_4/top_pool_1/strided_slice_6StridedSlice(sequential_4/top_pool_1/Shape_4:output:06sequential_4/top_pool_1/strided_slice_6/stack:output:08sequential_4/top_pool_1/strided_slice_6/stack_1:output:08sequential_4/top_pool_1/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_6
)sequential_4/top_pool_1/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2+
)sequential_4/top_pool_1/Reshape_2/shape/3
)sequential_4/top_pool_1/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_4/top_pool_1/Reshape_2/shape/4
'sequential_4/top_pool_1/Reshape_2/shapePack0sequential_4/top_pool_1/strided_slice_4:output:00sequential_4/top_pool_1/strided_slice_5:output:00sequential_4/top_pool_1/strided_slice_6:output:02sequential_4/top_pool_1/Reshape_2/shape/3:output:02sequential_4/top_pool_1/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2)
'sequential_4/top_pool_1/Reshape_2/shapeџ
!sequential_4/top_pool_1/Reshape_2Reshape*sequential_4/top_pool_1/Reshape_1:output:00sequential_4/top_pool_1/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2#
!sequential_4/top_pool_1/Reshape_2
!sequential_4/top_pool_1/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2#
!sequential_4/top_pool_1/sort/axisЧ
 sequential_4/top_pool_1/sort/NegNeg*sequential_4/top_pool_1/Reshape_2:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2"
 sequential_4/top_pool_1/sort/Neg
"sequential_4/top_pool_1/sort/ShapeShape$sequential_4/top_pool_1/sort/Neg:y:0*
T0*
_output_shapes
:2$
"sequential_4/top_pool_1/sort/ShapeЎ
0sequential_4/top_pool_1/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:22
0sequential_4/top_pool_1/sort/strided_slice/stackВ
2sequential_4/top_pool_1/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:24
2sequential_4/top_pool_1/sort/strided_slice/stack_1В
2sequential_4/top_pool_1/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:24
2sequential_4/top_pool_1/sort/strided_slice/stack_2
*sequential_4/top_pool_1/sort/strided_sliceStridedSlice+sequential_4/top_pool_1/sort/Shape:output:09sequential_4/top_pool_1/sort/strided_slice/stack:output:0;sequential_4/top_pool_1/sort/strided_slice/stack_1:output:0;sequential_4/top_pool_1/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2,
*sequential_4/top_pool_1/sort/strided_slice
!sequential_4/top_pool_1/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2#
!sequential_4/top_pool_1/sort/RankЩ
*sequential_4/top_pool_1/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2,
*sequential_4/top_pool_1/sort/transposition
&sequential_4/top_pool_1/sort/transpose	Transpose$sequential_4/top_pool_1/sort/Neg:y:03sequential_4/top_pool_1/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2(
&sequential_4/top_pool_1/sort/transposeЩ
#sequential_4/top_pool_1/sort/TopKV2TopKV2*sequential_4/top_pool_1/sort/transpose:y:03sequential_4/top_pool_1/sort/strided_slice:output:0*
T0*
_output_shapesv
t:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2%
#sequential_4/top_pool_1/sort/TopKV2Њ
(sequential_4/top_pool_1/sort/transpose_1	Transpose,sequential_4/top_pool_1/sort/TopKV2:values:03sequential_4/top_pool_1/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2*
(sequential_4/top_pool_1/sort/transpose_1ж
"sequential_4/top_pool_1/sort/Neg_1Neg,sequential_4/top_pool_1/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2$
"sequential_4/top_pool_1/sort/Neg_1Л
-sequential_4/top_pool_1/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2/
-sequential_4/top_pool_1/strided_slice_7/stackП
/sequential_4/top_pool_1/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   21
/sequential_4/top_pool_1/strided_slice_7/stack_1П
/sequential_4/top_pool_1/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               21
/sequential_4/top_pool_1/strided_slice_7/stack_2Щ
'sequential_4/top_pool_1/strided_slice_7StridedSlice&sequential_4/top_pool_1/sort/Neg_1:y:06sequential_4/top_pool_1/strided_slice_7/stack:output:08sequential_4/top_pool_1/strided_slice_7/stack_1:output:08sequential_4/top_pool_1/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_7Л
-sequential_4/top_pool_1/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2/
-sequential_4/top_pool_1/strided_slice_8/stackП
/sequential_4/top_pool_1/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   21
/sequential_4/top_pool_1/strided_slice_8/stack_1П
/sequential_4/top_pool_1/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               21
/sequential_4/top_pool_1/strided_slice_8/stack_2Щ
'sequential_4/top_pool_1/strided_slice_8StridedSlice&sequential_4/top_pool_1/sort/Neg_1:y:06sequential_4/top_pool_1/strided_slice_8/stack:output:08sequential_4/top_pool_1/strided_slice_8/stack_1:output:08sequential_4/top_pool_1/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_8ё
sequential_4/top_pool_1/subSub0sequential_4/top_pool_1/strided_slice_8:output:00sequential_4/top_pool_1/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
sequential_4/top_pool_1/sub
sequential_4/top_pool_1/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2!
sequential_4/top_pool_1/Floor/x
sequential_4/top_pool_1/FloorFloor(sequential_4/top_pool_1/Floor/x:output:0*
T0*
_output_shapes
: 2
sequential_4/top_pool_1/Floor
sequential_4/top_pool_1/CastCast!sequential_4/top_pool_1/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
sequential_4/top_pool_1/Cast
!sequential_4/top_pool_1/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2#
!sequential_4/top_pool_1/Floor_1/x
sequential_4/top_pool_1/Floor_1Floor*sequential_4/top_pool_1/Floor_1/x:output:0*
T0*
_output_shapes
: 2!
sequential_4/top_pool_1/Floor_1
sequential_4/top_pool_1/Cast_1Cast#sequential_4/top_pool_1/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2 
sequential_4/top_pool_1/Cast_1
sequential_4/top_pool_1/Shape_5Shapesequential_4/top_pool_1/sub:z:0*
T0*
_output_shapes
:2!
sequential_4/top_pool_1/Shape_5
sequential_4/top_pool_1/Shape_6Shape&sequential_4/conv2d_8/BiasAdd:output:0*
T0*
_output_shapes
:2!
sequential_4/top_pool_1/Shape_6Ј
-sequential_4/top_pool_1/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-sequential_4/top_pool_1/strided_slice_9/stackЌ
/sequential_4/top_pool_1/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_9/stack_1Ќ
/sequential_4/top_pool_1/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/sequential_4/top_pool_1/strided_slice_9/stack_2ў
'sequential_4/top_pool_1/strided_slice_9StridedSlice(sequential_4/top_pool_1/Shape_6:output:06sequential_4/top_pool_1/strided_slice_9/stack:output:08sequential_4/top_pool_1/strided_slice_9/stack_1:output:08sequential_4/top_pool_1/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2)
'sequential_4/top_pool_1/strided_slice_9
)sequential_4/top_pool_1/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_4/top_pool_1/Reshape_3/shape/3
)sequential_4/top_pool_1/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2+
)sequential_4/top_pool_1/Reshape_3/shape/4ш
'sequential_4/top_pool_1/Reshape_3/shapePack0sequential_4/top_pool_1/strided_slice_9:output:0 sequential_4/top_pool_1/Cast:y:0"sequential_4/top_pool_1/Cast_1:y:02sequential_4/top_pool_1/Reshape_3/shape/3:output:02sequential_4/top_pool_1/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2)
'sequential_4/top_pool_1/Reshape_3/shapeт
!sequential_4/top_pool_1/Reshape_3Reshapesequential_4/top_pool_1/sub:z:00sequential_4/top_pool_1/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2#
!sequential_4/top_pool_1/Reshape_3с
1sequential_4/top_pool_1/ExpandDims/ReadVariableOpReadVariableOp:sequential_4_top_pool_1_expanddims_readvariableop_resource*
_output_shapes

:*
dtype023
1sequential_4/top_pool_1/ExpandDims/ReadVariableOp
&sequential_4/top_pool_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2(
&sequential_4/top_pool_1/ExpandDims/dimя
"sequential_4/top_pool_1/ExpandDims
ExpandDims9sequential_4/top_pool_1/ExpandDims/ReadVariableOp:value:0/sequential_4/top_pool_1/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2$
"sequential_4/top_pool_1/ExpandDims
(sequential_4/top_pool_1/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2*
(sequential_4/top_pool_1/ExpandDims_1/dimы
$sequential_4/top_pool_1/ExpandDims_1
ExpandDims+sequential_4/top_pool_1/ExpandDims:output:01sequential_4/top_pool_1/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2&
$sequential_4/top_pool_1/ExpandDims_1
(sequential_4/top_pool_1/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2*
(sequential_4/top_pool_1/ExpandDims_2/dimё
$sequential_4/top_pool_1/ExpandDims_2
ExpandDims-sequential_4/top_pool_1/ExpandDims_1:output:01sequential_4/top_pool_1/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2&
$sequential_4/top_pool_1/ExpandDims_2о
sequential_4/top_pool_1/mul_2Mul*sequential_4/top_pool_1/Reshape_3:output:0-sequential_4/top_pool_1/ExpandDims_2:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
sequential_4/top_pool_1/mul_2 
-sequential_4/top_pool_1/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2/
-sequential_4/top_pool_1/Sum/reduction_indicesж
sequential_4/top_pool_1/SumSum!sequential_4/top_pool_1/mul_2:z:06sequential_4/top_pool_1/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
sequential_4/top_pool_1/Sumд
*sequential_4/top_pool_1/add/ReadVariableOpReadVariableOp3sequential_4_top_pool_1_add_readvariableop_resource*&
_output_shapes
:*
dtype02,
*sequential_4/top_pool_1/add/ReadVariableOpз
sequential_4/top_pool_1/addAddV2$sequential_4/top_pool_1/Sum:output:02sequential_4/top_pool_1/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
sequential_4/top_pool_1/addз
+sequential_4/conv2d_9/Conv2D/ReadVariableOpReadVariableOp4sequential_4_conv2d_9_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02-
+sequential_4/conv2d_9/Conv2D/ReadVariableOpџ
sequential_4/conv2d_9/Conv2DConv2Dsequential_4/top_pool_1/add:z:03sequential_4/conv2d_9/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ*
paddingVALID*
strides
2
sequential_4/conv2d_9/Conv2DЮ
,sequential_4/conv2d_9/BiasAdd/ReadVariableOpReadVariableOp5sequential_4_conv2d_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02.
,sequential_4/conv2d_9/BiasAdd/ReadVariableOpр
sequential_4/conv2d_9/BiasAddBiasAdd%sequential_4/conv2d_9/Conv2D:output:04sequential_4/conv2d_9/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ2
sequential_4/conv2d_9/BiasAddЂ
sequential_4/conv2d_9/ReluRelu&sequential_4/conv2d_9/BiasAdd:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2
sequential_4/conv2d_9/Relu
sequential_4/flatten_4/ConstConst*
_output_shapes
:*
dtype0*
valueB"џџџџ@  2
sequential_4/flatten_4/ConstЯ
sequential_4/flatten_4/ReshapeReshape(sequential_4/conv2d_9/Relu:activations:0%sequential_4/flatten_4/Const:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2 
sequential_4/flatten_4/ReshapeЮ
*sequential_4/dense_8/MatMul/ReadVariableOpReadVariableOp3sequential_4_dense_8_matmul_readvariableop_resource* 
_output_shapes
:
Р*
dtype02,
*sequential_4/dense_8/MatMul/ReadVariableOpд
sequential_4/dense_8/MatMulMatMul'sequential_4/flatten_4/Reshape:output:02sequential_4/dense_8/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
sequential_4/dense_8/MatMulЬ
+sequential_4/dense_8/BiasAdd/ReadVariableOpReadVariableOp4sequential_4_dense_8_biasadd_readvariableop_resource*
_output_shapes	
:*
dtype02-
+sequential_4/dense_8/BiasAdd/ReadVariableOpж
sequential_4/dense_8/BiasAddBiasAdd%sequential_4/dense_8/MatMul:product:03sequential_4/dense_8/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
sequential_4/dense_8/BiasAddЭ
*sequential_4/dense_9/MatMul/ReadVariableOpReadVariableOp3sequential_4_dense_9_matmul_readvariableop_resource*
_output_shapes
:	
*
dtype02,
*sequential_4/dense_9/MatMul/ReadVariableOpб
sequential_4/dense_9/MatMulMatMul%sequential_4/dense_8/BiasAdd:output:02sequential_4/dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
sequential_4/dense_9/MatMulЫ
+sequential_4/dense_9/BiasAdd/ReadVariableOpReadVariableOp4sequential_4_dense_9_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02-
+sequential_4/dense_9/BiasAdd/ReadVariableOpе
sequential_4/dense_9/BiasAddBiasAdd%sequential_4/dense_9/MatMul:product:03sequential_4/dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
sequential_4/dense_9/BiasAddЪ
IdentityIdentity%sequential_4/dense_9/BiasAdd:output:0-^sequential_4/conv2d_8/BiasAdd/ReadVariableOp,^sequential_4/conv2d_8/Conv2D/ReadVariableOp-^sequential_4/conv2d_9/BiasAdd/ReadVariableOp,^sequential_4/conv2d_9/Conv2D/ReadVariableOp,^sequential_4/dense_8/BiasAdd/ReadVariableOp+^sequential_4/dense_8/MatMul/ReadVariableOp,^sequential_4/dense_9/BiasAdd/ReadVariableOp+^sequential_4/dense_9/MatMul/ReadVariableOp2^sequential_4/top_pool_1/ExpandDims/ReadVariableOp+^sequential_4/top_pool_1/add/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::2\
,sequential_4/conv2d_8/BiasAdd/ReadVariableOp,sequential_4/conv2d_8/BiasAdd/ReadVariableOp2Z
+sequential_4/conv2d_8/Conv2D/ReadVariableOp+sequential_4/conv2d_8/Conv2D/ReadVariableOp2\
,sequential_4/conv2d_9/BiasAdd/ReadVariableOp,sequential_4/conv2d_9/BiasAdd/ReadVariableOp2Z
+sequential_4/conv2d_9/Conv2D/ReadVariableOp+sequential_4/conv2d_9/Conv2D/ReadVariableOp2Z
+sequential_4/dense_8/BiasAdd/ReadVariableOp+sequential_4/dense_8/BiasAdd/ReadVariableOp2X
*sequential_4/dense_8/MatMul/ReadVariableOp*sequential_4/dense_8/MatMul/ReadVariableOp2Z
+sequential_4/dense_9/BiasAdd/ReadVariableOp+sequential_4/dense_9/BiasAdd/ReadVariableOp2X
*sequential_4/dense_9/MatMul/ReadVariableOp*sequential_4/dense_9/MatMul/ReadVariableOp2f
1sequential_4/top_pool_1/ExpandDims/ReadVariableOp1sequential_4/top_pool_1/ExpandDims/ReadVariableOp2X
*sequential_4/top_pool_1/add/ReadVariableOp*sequential_4/top_pool_1/add/ReadVariableOp:' #
!
_user_specified_name	input_5
я
м
C__inference_dense_8_layer_call_and_return_conditional_losses_156484

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identityЂBiasAdd/ReadVariableOpЂMatMul/ReadVariableOp
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
Р*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
MatMul
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:*
dtype02
BiasAdd/ReadVariableOp
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2	
BiasAdd
IdentityIdentityBiasAdd:output:0^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*
T0*(
_output_shapes
:џџџџџџџџџ2

Identity"
identityIdentity:output:0*/
_input_shapes
:џџџџџџџџџР::20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:& "
 
_user_specified_nameinputs
ЧЙ
§
H__inference_sequential_4_layer_call_and_return_conditional_losses_156175

inputs+
'conv2d_8_conv2d_readvariableop_resource,
(conv2d_8_biasadd_readvariableop_resource1
-top_pool_1_expanddims_readvariableop_resource*
&top_pool_1_add_readvariableop_resource+
'conv2d_9_conv2d_readvariableop_resource,
(conv2d_9_biasadd_readvariableop_resource*
&dense_8_matmul_readvariableop_resource+
'dense_8_biasadd_readvariableop_resource*
&dense_9_matmul_readvariableop_resource+
'dense_9_biasadd_readvariableop_resource
identityЂconv2d_8/BiasAdd/ReadVariableOpЂconv2d_8/Conv2D/ReadVariableOpЂconv2d_9/BiasAdd/ReadVariableOpЂconv2d_9/Conv2D/ReadVariableOpЂdense_8/BiasAdd/ReadVariableOpЂdense_8/MatMul/ReadVariableOpЂdense_9/BiasAdd/ReadVariableOpЂdense_9/MatMul/ReadVariableOpЂ$top_pool_1/ExpandDims/ReadVariableOpЂtop_pool_1/add/ReadVariableOpА
conv2d_8/Conv2D/ReadVariableOpReadVariableOp'conv2d_8_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02 
conv2d_8/Conv2D/ReadVariableOpП
conv2d_8/Conv2DConv2Dinputs&conv2d_8/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ*
paddingVALID*
strides
2
conv2d_8/Conv2DЇ
conv2d_8/BiasAdd/ReadVariableOpReadVariableOp(conv2d_8_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
conv2d_8/BiasAdd/ReadVariableOpЌ
conv2d_8/BiasAddBiasAddconv2d_8/Conv2D:output:0'conv2d_8/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ2
conv2d_8/BiasAdd§
top_pool_1/ExtractImagePatchesExtractImagePatchesconv2d_8/BiasAdd:output:0*
T0*0
_output_shapes
:џџџџџџџџџ

Ш*
ksizes
*
paddingVALID*
rates
*
strides
2 
top_pool_1/ExtractImagePatchesm
top_pool_1/ShapeShapeconv2d_8/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape
top_pool_1/Shape_1Shape(top_pool_1/ExtractImagePatches:patches:0*
T0*
_output_shapes
:2
top_pool_1/Shape_1
top_pool_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2 
top_pool_1/strided_slice/stack
 top_pool_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice/stack_1
 top_pool_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice/stack_2Є
top_pool_1/strided_sliceStridedSlicetop_pool_1/Shape:output:0'top_pool_1/strided_slice/stack:output:0)top_pool_1/strided_slice/stack_1:output:0)top_pool_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slicef
top_pool_1/mul/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_1/mul/y
top_pool_1/mulMul!top_pool_1/strided_slice:output:0top_pool_1/mul/y:output:0*
T0*
_output_shapes
: 2
top_pool_1/mulj
top_pool_1/mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :
2
top_pool_1/mul_1/y}
top_pool_1/mul_1Multop_pool_1/mul:z:0top_pool_1/mul_1/y:output:0*
T0*
_output_shapes
: 2
top_pool_1/mul_1z
top_pool_1/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape/shape/1z
top_pool_1/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape/shape/2z
top_pool_1/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape/shape/3я
top_pool_1/Reshape/shapePacktop_pool_1/mul_1:z:0#top_pool_1/Reshape/shape/1:output:0#top_pool_1/Reshape/shape/2:output:0#top_pool_1/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape/shapeК
top_pool_1/ReshapeReshape(top_pool_1/ExtractImagePatches:patches:0!top_pool_1/Reshape/shape:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2
top_pool_1/ReshapeЏ
top_pool_1/MirrorPad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
top_pool_1/MirrorPad/paddingsЭ
top_pool_1/MirrorPad	MirrorPadtop_pool_1/Reshape:output:0&top_pool_1/MirrorPad/paddings:output:0*
T0*/
_output_shapes
:џџџџџџџџџ*
mode	SYMMETRIC2
top_pool_1/MirrorPad
 top_pool_1/ExtractImagePatches_1ExtractImagePatchestop_pool_1/MirrorPad:output:0*
T0*/
_output_shapes
:џџџџџџџџџH*
ksizes
*
paddingVALID*
rates
*
strides
2"
 top_pool_1/ExtractImagePatches_1
top_pool_1/Shape_2Shape*top_pool_1/ExtractImagePatches_1:patches:0*
T0*
_output_shapes
:2
top_pool_1/Shape_2u
top_pool_1/Shape_3Shapetop_pool_1/MirrorPad:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape_3
 top_pool_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_1/strided_slice_1/stack
"top_pool_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_1/stack_1
"top_pool_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_1/stack_2А
top_pool_1/strided_slice_1StridedSlicetop_pool_1/Shape_3:output:0)top_pool_1/strided_slice_1/stack:output:0+top_pool_1/strided_slice_1/stack_1:output:0+top_pool_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_1
 top_pool_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_2/stack
"top_pool_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_2/stack_1
"top_pool_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_2/stack_2А
top_pool_1/strided_slice_2StridedSlicetop_pool_1/Shape_2:output:0)top_pool_1/strided_slice_2/stack:output:0+top_pool_1/strided_slice_2/stack_1:output:0+top_pool_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_2
 top_pool_1/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_3/stack
"top_pool_1/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_3/stack_1
"top_pool_1/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_3/stack_2А
top_pool_1/strided_slice_3StridedSlicetop_pool_1/Shape_2:output:0)top_pool_1/strided_slice_3/stack:output:0+top_pool_1/strided_slice_3/stack_1:output:0+top_pool_1/strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_3~
top_pool_1/Reshape_1/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_1/shape/3~
top_pool_1/Reshape_1/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_1/shape/4~
top_pool_1/Reshape_1/shape/5Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_1/shape/5в
top_pool_1/Reshape_1/shapePack#top_pool_1/strided_slice_1:output:0#top_pool_1/strided_slice_2:output:0#top_pool_1/strided_slice_3:output:0%top_pool_1/Reshape_1/shape/3:output:0%top_pool_1/Reshape_1/shape/4:output:0%top_pool_1/Reshape_1/shape/5:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape_1/shapeм
top_pool_1/Reshape_1Reshape*top_pool_1/ExtractImagePatches_1:patches:0#top_pool_1/Reshape_1/shape:output:0*
T0*I
_output_shapes7
5:3џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/Reshape_1u
top_pool_1/Shape_4Shapetop_pool_1/Reshape_1:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape_4
 top_pool_1/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_1/strided_slice_4/stack
"top_pool_1/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_4/stack_1
"top_pool_1/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_4/stack_2А
top_pool_1/strided_slice_4StridedSlicetop_pool_1/Shape_4:output:0)top_pool_1/strided_slice_4/stack:output:0+top_pool_1/strided_slice_4/stack_1:output:0+top_pool_1/strided_slice_4/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_4
 top_pool_1/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_5/stack
"top_pool_1/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_5/stack_1
"top_pool_1/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_5/stack_2А
top_pool_1/strided_slice_5StridedSlicetop_pool_1/Shape_4:output:0)top_pool_1/strided_slice_5/stack:output:0+top_pool_1/strided_slice_5/stack_1:output:0+top_pool_1/strided_slice_5/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_5
 top_pool_1/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB:2"
 top_pool_1/strided_slice_6/stack
"top_pool_1/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_6/stack_1
"top_pool_1/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_6/stack_2А
top_pool_1/strided_slice_6StridedSlicetop_pool_1/Shape_4:output:0)top_pool_1/strided_slice_6/stack:output:0+top_pool_1/strided_slice_6/stack_1:output:0+top_pool_1/strided_slice_6/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_6~
top_pool_1/Reshape_2/shape/3Const*
_output_shapes
: *
dtype0*
value	B :	2
top_pool_1/Reshape_2/shape/3~
top_pool_1/Reshape_2/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_2/shape/4Ћ
top_pool_1/Reshape_2/shapePack#top_pool_1/strided_slice_4:output:0#top_pool_1/strided_slice_5:output:0#top_pool_1/strided_slice_6:output:0%top_pool_1/Reshape_2/shape/3:output:0%top_pool_1/Reshape_2/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape_2/shapeЫ
top_pool_1/Reshape_2Reshapetop_pool_1/Reshape_1:output:0#top_pool_1/Reshape_2/shape:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
top_pool_1/Reshape_2n
top_pool_1/sort/axisConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/sort/axis 
top_pool_1/sort/NegNegtop_pool_1/Reshape_2:output:0*
T0*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
top_pool_1/sort/Negu
top_pool_1/sort/ShapeShapetop_pool_1/sort/Neg:y:0*
T0*
_output_shapes
:2
top_pool_1/sort/Shape
#top_pool_1/sort/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2%
#top_pool_1/sort/strided_slice/stack
%top_pool_1/sort/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_1/sort/strided_slice/stack_1
%top_pool_1/sort/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%top_pool_1/sort/strided_slice/stack_2Т
top_pool_1/sort/strided_sliceStridedSlicetop_pool_1/sort/Shape:output:0,top_pool_1/sort/strided_slice/stack:output:0.top_pool_1/sort/strided_slice/stack_1:output:0.top_pool_1/sort/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/sort/strided_slicen
top_pool_1/sort/RankConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/sort/RankЏ
top_pool_1/sort/transpositionConst*
_output_shapes
:*
dtype0	*=
value4B2	"(                                    2
top_pool_1/sort/transpositionс
top_pool_1/sort/transpose	Transposetop_pool_1/sort/Neg:y:0&top_pool_1/sort/transposition:output:0*
T0*
Tperm0	*E
_output_shapes3
1:/џџџџџџџџџџџџџџџџџџџџџџџџџџџ	2
top_pool_1/sort/transpose
top_pool_1/sort/TopKV2TopKV2top_pool_1/sort/transpose:y:0&top_pool_1/sort/strided_slice:output:0*
T0*
_output_shapesv
t:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ:8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/sort/TopKV2і
top_pool_1/sort/transpose_1	Transposetop_pool_1/sort/TopKV2:values:0&top_pool_1/sort/transposition:output:0*
T0*
Tperm0	*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/sort/transpose_1Џ
top_pool_1/sort/Neg_1Negtop_pool_1/sort/transpose_1:y:0*
T0*N
_output_shapes<
::8џџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/sort/Neg_1Ё
 top_pool_1/strided_slice_7/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_1/strided_slice_7/stackЅ
"top_pool_1/strided_slice_7/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_1/strided_slice_7/stack_1Ѕ
"top_pool_1/strided_slice_7/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_1/strided_slice_7/stack_2ћ
top_pool_1/strided_slice_7StridedSlicetop_pool_1/sort/Neg_1:y:0)top_pool_1/strided_slice_7/stack:output:0+top_pool_1/strided_slice_7/stack_1:output:0+top_pool_1/strided_slice_7/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_1/strided_slice_7Ё
 top_pool_1/strided_slice_8/stackConst*
_output_shapes
:*
dtype0*)
value B"                   2"
 top_pool_1/strided_slice_8/stackЅ
"top_pool_1/strided_slice_8/stack_1Const*
_output_shapes
:*
dtype0*)
value B"                   2$
"top_pool_1/strided_slice_8/stack_1Ѕ
"top_pool_1/strided_slice_8/stack_2Const*
_output_shapes
:*
dtype0*)
value B"               2$
"top_pool_1/strided_slice_8/stack_2ћ
top_pool_1/strided_slice_8StridedSlicetop_pool_1/sort/Neg_1:y:0)top_pool_1/strided_slice_8/stack:output:0+top_pool_1/strided_slice_8/stack_1:output:0+top_pool_1/strided_slice_8/stack_2:output:0*
Index0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ*

begin_mask*
end_mask*
shrink_axis_mask2
top_pool_1/strided_slice_8Н
top_pool_1/subSub#top_pool_1/strided_slice_8:output:0#top_pool_1/strided_slice_7:output:0*
T0*A
_output_shapes/
-:+џџџџџџџџџџџџџџџџџџџџџџџџџџџ2
top_pool_1/subm
top_pool_1/Floor/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_1/Floor/xk
top_pool_1/FloorFloortop_pool_1/Floor/x:output:0*
T0*
_output_shapes
: 2
top_pool_1/Floorp
top_pool_1/CastCasttop_pool_1/Floor:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_1/Castq
top_pool_1/Floor_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *  (A2
top_pool_1/Floor_1/xq
top_pool_1/Floor_1Floortop_pool_1/Floor_1/x:output:0*
T0*
_output_shapes
: 2
top_pool_1/Floor_1v
top_pool_1/Cast_1Casttop_pool_1/Floor_1:y:0*

DstT0*

SrcT0*
_output_shapes
: 2
top_pool_1/Cast_1j
top_pool_1/Shape_5Shapetop_pool_1/sub:z:0*
T0*
_output_shapes
:2
top_pool_1/Shape_5q
top_pool_1/Shape_6Shapeconv2d_8/BiasAdd:output:0*
T0*
_output_shapes
:2
top_pool_1/Shape_6
 top_pool_1/strided_slice_9/stackConst*
_output_shapes
:*
dtype0*
valueB: 2"
 top_pool_1/strided_slice_9/stack
"top_pool_1/strided_slice_9/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_9/stack_1
"top_pool_1/strided_slice_9/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2$
"top_pool_1/strided_slice_9/stack_2А
top_pool_1/strided_slice_9StridedSlicetop_pool_1/Shape_6:output:0)top_pool_1/strided_slice_9/stack:output:0+top_pool_1/strided_slice_9/stack_1:output:0+top_pool_1/strided_slice_9/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
top_pool_1/strided_slice_9~
top_pool_1/Reshape_3/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_3/shape/3~
top_pool_1/Reshape_3/shape/4Const*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/Reshape_3/shape/4
top_pool_1/Reshape_3/shapePack#top_pool_1/strided_slice_9:output:0top_pool_1/Cast:y:0top_pool_1/Cast_1:y:0%top_pool_1/Reshape_3/shape/3:output:0%top_pool_1/Reshape_3/shape/4:output:0*
N*
T0*
_output_shapes
:2
top_pool_1/Reshape_3/shapeЎ
top_pool_1/Reshape_3Reshapetop_pool_1/sub:z:0#top_pool_1/Reshape_3/shape:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
top_pool_1/Reshape_3К
$top_pool_1/ExpandDims/ReadVariableOpReadVariableOp-top_pool_1_expanddims_readvariableop_resource*
_output_shapes

:*
dtype02&
$top_pool_1/ExpandDims/ReadVariableOpx
top_pool_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B : 2
top_pool_1/ExpandDims/dimЛ
top_pool_1/ExpandDims
ExpandDims,top_pool_1/ExpandDims/ReadVariableOp:value:0"top_pool_1/ExpandDims/dim:output:0*
T0*"
_output_shapes
:2
top_pool_1/ExpandDims|
top_pool_1/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/ExpandDims_1/dimЗ
top_pool_1/ExpandDims_1
ExpandDimstop_pool_1/ExpandDims:output:0$top_pool_1/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:2
top_pool_1/ExpandDims_1|
top_pool_1/ExpandDims_2/dimConst*
_output_shapes
: *
dtype0*
value	B :2
top_pool_1/ExpandDims_2/dimН
top_pool_1/ExpandDims_2
ExpandDims top_pool_1/ExpandDims_1:output:0$top_pool_1/ExpandDims_2/dim:output:0*
T0**
_output_shapes
:2
top_pool_1/ExpandDims_2Њ
top_pool_1/mul_2Multop_pool_1/Reshape_3:output:0 top_pool_1/ExpandDims_2:output:0*
T0*3
_output_shapes!
:џџџџџџџџџ

2
top_pool_1/mul_2
 top_pool_1/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :2"
 top_pool_1/Sum/reduction_indicesЂ
top_pool_1/SumSumtop_pool_1/mul_2:z:0)top_pool_1/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
top_pool_1/Sum­
top_pool_1/add/ReadVariableOpReadVariableOp&top_pool_1_add_readvariableop_resource*&
_output_shapes
:*
dtype02
top_pool_1/add/ReadVariableOpЃ
top_pool_1/addAddV2top_pool_1/Sum:output:0%top_pool_1/add/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ

2
top_pool_1/addА
conv2d_9/Conv2D/ReadVariableOpReadVariableOp'conv2d_9_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02 
conv2d_9/Conv2D/ReadVariableOpЫ
conv2d_9/Conv2DConv2Dtop_pool_1/add:z:0&conv2d_9/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ*
paddingVALID*
strides
2
conv2d_9/Conv2DЇ
conv2d_9/BiasAdd/ReadVariableOpReadVariableOp(conv2d_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02!
conv2d_9/BiasAdd/ReadVariableOpЌ
conv2d_9/BiasAddBiasAddconv2d_9/Conv2D:output:0'conv2d_9/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:џџџџџџџџџ2
conv2d_9/BiasAdd{
conv2d_9/ReluReluconv2d_9/BiasAdd:output:0*
T0*/
_output_shapes
:џџџџџџџџџ2
conv2d_9/Relus
flatten_4/ConstConst*
_output_shapes
:*
dtype0*
valueB"џџџџ@  2
flatten_4/Const
flatten_4/ReshapeReshapeconv2d_9/Relu:activations:0flatten_4/Const:output:0*
T0*(
_output_shapes
:џџџџџџџџџР2
flatten_4/ReshapeЇ
dense_8/MatMul/ReadVariableOpReadVariableOp&dense_8_matmul_readvariableop_resource* 
_output_shapes
:
Р*
dtype02
dense_8/MatMul/ReadVariableOp 
dense_8/MatMulMatMulflatten_4/Reshape:output:0%dense_8/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
dense_8/MatMulЅ
dense_8/BiasAdd/ReadVariableOpReadVariableOp'dense_8_biasadd_readvariableop_resource*
_output_shapes	
:*
dtype02 
dense_8/BiasAdd/ReadVariableOpЂ
dense_8/BiasAddBiasAdddense_8/MatMul:product:0&dense_8/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:џџџџџџџџџ2
dense_8/BiasAddІ
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource*
_output_shapes
:	
*
dtype02
dense_9/MatMul/ReadVariableOp
dense_9/MatMulMatMuldense_8/BiasAdd:output:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
dense_9/MatMulЄ
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02 
dense_9/BiasAdd/ReadVariableOpЁ
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:џџџџџџџџџ
2
dense_9/BiasAddЛ
IdentityIdentitydense_9/BiasAdd:output:0 ^conv2d_8/BiasAdd/ReadVariableOp^conv2d_8/Conv2D/ReadVariableOp ^conv2d_9/BiasAdd/ReadVariableOp^conv2d_9/Conv2D/ReadVariableOp^dense_8/BiasAdd/ReadVariableOp^dense_8/MatMul/ReadVariableOp^dense_9/BiasAdd/ReadVariableOp^dense_9/MatMul/ReadVariableOp%^top_pool_1/ExpandDims/ReadVariableOp^top_pool_1/add/ReadVariableOp*
T0*'
_output_shapes
:џџџџџџџџџ
2

Identity"
identityIdentity:output:0*V
_input_shapesE
C:џџџџџџџџџ::::::::::2B
conv2d_8/BiasAdd/ReadVariableOpconv2d_8/BiasAdd/ReadVariableOp2@
conv2d_8/Conv2D/ReadVariableOpconv2d_8/Conv2D/ReadVariableOp2B
conv2d_9/BiasAdd/ReadVariableOpconv2d_9/BiasAdd/ReadVariableOp2@
conv2d_9/Conv2D/ReadVariableOpconv2d_9/Conv2D/ReadVariableOp2@
dense_8/BiasAdd/ReadVariableOpdense_8/BiasAdd/ReadVariableOp2>
dense_8/MatMul/ReadVariableOpdense_8/MatMul/ReadVariableOp2@
dense_9/BiasAdd/ReadVariableOpdense_9/BiasAdd/ReadVariableOp2>
dense_9/MatMul/ReadVariableOpdense_9/MatMul/ReadVariableOp2L
$top_pool_1/ExpandDims/ReadVariableOp$top_pool_1/ExpandDims/ReadVariableOp2>
top_pool_1/add/ReadVariableOptop_pool_1/add/ReadVariableOp:& "
 
_user_specified_nameinputs"ЏL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*В
serving_default
C
input_58
serving_default_input_5:0џџџџџџџџџ;
dense_90
StatefulPartitionedCall:0џџџџџџџџџ
tensorflow/serving/predict:цЫ
Ђ/
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer-4
layer_with_weights-3
layer-5
layer_with_weights-4
layer-6
	optimizer
		variables

regularization_losses
trainable_variables
	keras_api

signatures
q_default_save_signature
r__call__
*s&call_and_return_all_conditional_losses"њ+
_tf_keras_sequentialл+{"class_name": "Sequential", "name": "sequential_4", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "sequential_4", "layers": [{"class_name": "Conv2D", "config": {"name": "conv2d_8", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "batch_input_shape": [null, 28, 28, 1]}}, {"class_name": "TopPool", "config": {"name": "top_pool_1", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}, {"class_name": "Conv2D", "config": {"name": "conv2d_9", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Flatten", "config": {"name": "flatten_4", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 1}}}, "is_graph_network": true, "keras_version": "2.2.4-tf", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential_4", "layers": [{"class_name": "Conv2D", "config": {"name": "conv2d_8", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null, "batch_input_shape": [null, 28, 28, 1]}}, {"class_name": "TopPool", "config": {"name": "top_pool_1", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}, {"class_name": "Conv2D", "config": {"name": "conv2d_9", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Flatten", "config": {"name": "flatten_4", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "Dense", "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}]}}, "training_config": {"loss": {"class_name": "SparseCategoricalCrossentropy", "config": {"reduction": "auto", "name": "sparse_categorical_crossentropy", "from_logits": true}}, "metrics": ["accuracy"], "weighted_metrics": null, "sample_weight_mode": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0003000000142492354, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
­"Њ
_tf_keras_input_layer{"class_name": "InputLayer", "name": "input_5", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": [null, 28, 28, 1], "config": {"batch_input_shape": [null, 28, 28, 1], "dtype": "float32", "sparse": false, "ragged": false, "name": "input_5"}}
э

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
t__call__
*u&call_and_return_all_conditional_losses"Ш
_tf_keras_layerЎ{"class_name": "Conv2D", "name": "conv2d_8", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "conv2d_8", "trainable": true, "dtype": "float32", "filters": 8, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 1}}}}
е
w
b
	variables
regularization_losses
trainable_variables
	keras_api
v__call__
*w&call_and_return_all_conditional_losses"И
_tf_keras_layer{"class_name": "TopPool", "name": "top_pool_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "top_pool_1", "trainable": true, "dtype": "float32", "num_channels": 8, "ksize": 5, "stride": 2, "ks": 1, "m": 2, "n": 7}}
ь

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
x__call__
*y&call_and_return_all_conditional_losses"Ч
_tf_keras_layer­{"class_name": "Conv2D", "name": "conv2d_9", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "conv2d_9", "trainable": true, "dtype": "float32", "filters": 16, "kernel_size": [5, 5], "strides": [1, 1], "padding": "valid", "data_format": "channels_last", "dilation_rate": [1, 1], "activation": "relu", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {"-1": 8}}}}
А
 	variables
!regularization_losses
"trainable_variables
#	keras_api
z__call__
*{&call_and_return_all_conditional_losses"Ё
_tf_keras_layer{"class_name": "Flatten", "name": "flatten_4", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "flatten_4", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 1, "axes": {}}}}
ѕ

$kernel
%bias
&	variables
'regularization_losses
(trainable_variables
)	keras_api
|__call__
*}&call_and_return_all_conditional_losses"а
_tf_keras_layerЖ{"class_name": "Dense", "name": "dense_8", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_8", "trainable": true, "dtype": "float32", "units": 128, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 576}}}}
є

*kernel
+bias
,	variables
-regularization_losses
.trainable_variables
/	keras_api
~__call__
*&call_and_return_all_conditional_losses"Я
_tf_keras_layerЕ{"class_name": "Dense", "name": "dense_9", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "config": {"name": "dense_9", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 128}}}}

0iter

1beta_1

2beta_2
	3decay
4learning_ratem]m^m_m`mamb$mc%md*me+mfvgvhvivjvkvl$vm%vn*vo+vp"
	optimizer
f
0
1
2
3
4
5
$6
%7
*8
+9"
trackable_list_wrapper
 "
trackable_list_wrapper
f
0
1
2
3
4
5
$6
%7
*8
+9"
trackable_list_wrapper
З
5non_trainable_variables
		variables
6metrics

7layers

regularization_losses
trainable_variables
8layer_regularization_losses
r__call__
q_default_save_signature
*s&call_and_return_all_conditional_losses
&s"call_and_return_conditional_losses"
_generic_user_object
-
serving_default"
signature_map
):'2conv2d_8/kernel
:2conv2d_8/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper

9non_trainable_variables
	variables
:metrics

;layers
regularization_losses
trainable_variables
<layer_regularization_losses
t__call__
*u&call_and_return_all_conditional_losses
&u"call_and_return_conditional_losses"
_generic_user_object
:2top_pool_1/w
&:$2top_pool_1/b
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper

=non_trainable_variables
	variables
>metrics

?layers
regularization_losses
trainable_variables
@layer_regularization_losses
v__call__
*w&call_and_return_all_conditional_losses
&w"call_and_return_conditional_losses"
_generic_user_object
):'2conv2d_9/kernel
:2conv2d_9/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper

Anon_trainable_variables
	variables
Bmetrics

Clayers
regularization_losses
trainable_variables
Dlayer_regularization_losses
x__call__
*y&call_and_return_all_conditional_losses
&y"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper

Enon_trainable_variables
 	variables
Fmetrics

Glayers
!regularization_losses
"trainable_variables
Hlayer_regularization_losses
z__call__
*{&call_and_return_all_conditional_losses
&{"call_and_return_conditional_losses"
_generic_user_object
": 
Р2dense_8/kernel
:2dense_8/bias
.
$0
%1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
$0
%1"
trackable_list_wrapper

Inon_trainable_variables
&	variables
Jmetrics

Klayers
'regularization_losses
(trainable_variables
Llayer_regularization_losses
|__call__
*}&call_and_return_all_conditional_losses
&}"call_and_return_conditional_losses"
_generic_user_object
!:	
2dense_9/kernel
:
2dense_9/bias
.
*0
+1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
*0
+1"
trackable_list_wrapper

Mnon_trainable_variables
,	variables
Nmetrics

Olayers
-regularization_losses
.trainable_variables
Player_regularization_losses
~__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
'
Q0"
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper

	Rtotal
	Scount
T
_fn_kwargs
U	variables
Vregularization_losses
Wtrainable_variables
X	keras_api
__call__
+&call_and_return_all_conditional_losses"х
_tf_keras_layerЫ{"class_name": "MeanMetricWrapper", "name": "accuracy", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "accuracy", "dtype": "float32"}}
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper

Ynon_trainable_variables
U	variables
Zmetrics

[layers
Vregularization_losses
Wtrainable_variables
\layer_regularization_losses
__call__
+&call_and_return_all_conditional_losses
'"call_and_return_conditional_losses"
_generic_user_object
.
R0
S1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.:,2Adam/conv2d_8/kernel/m
 :2Adam/conv2d_8/bias/m
#:!2Adam/top_pool_1/w/m
+:)2Adam/top_pool_1/b/m
.:,2Adam/conv2d_9/kernel/m
 :2Adam/conv2d_9/bias/m
':%
Р2Adam/dense_8/kernel/m
 :2Adam/dense_8/bias/m
&:$	
2Adam/dense_9/kernel/m
:
2Adam/dense_9/bias/m
.:,2Adam/conv2d_8/kernel/v
 :2Adam/conv2d_8/bias/v
#:!2Adam/top_pool_1/w/v
+:)2Adam/top_pool_1/b/v
.:,2Adam/conv2d_9/kernel/v
 :2Adam/conv2d_9/bias/v
':%
Р2Adam/dense_8/kernel/v
 :2Adam/dense_8/bias/v
&:$	
2Adam/dense_9/kernel/v
:
2Adam/dense_9/bias/v
ч2ф
!__inference__wrapped_model_155687О
В
FullArgSpec
args 
varargsjargs
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *.Ђ+
)&
input_5џџџџџџџџџ
2џ
-__inference_sequential_4_layer_call_fn_155977
-__inference_sequential_4_layer_call_fn_156012
-__inference_sequential_4_layer_call_fn_156344
-__inference_sequential_4_layer_call_fn_156329Р
ЗВГ
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
ю2ы
H__inference_sequential_4_layer_call_and_return_conditional_losses_156175
H__inference_sequential_4_layer_call_and_return_conditional_losses_156314
H__inference_sequential_4_layer_call_and_return_conditional_losses_155921
H__inference_sequential_4_layer_call_and_return_conditional_losses_155941Р
ЗВГ
FullArgSpec1
args)&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults
p 

 

kwonlyargs 
kwonlydefaultsЊ 
annotationsЊ *
 
2
)__inference_conv2d_8_layer_call_fn_155707з
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *7Ђ4
2/+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
Ѓ2 
D__inference_conv2d_8_layer_call_and_return_conditional_losses_155699з
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *7Ђ4
2/+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
б2Ю
+__inference_top_pool_1_layer_call_fn_156463
В
FullArgSpec
args
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
ь2щ
F__inference_top_pool_1_layer_call_and_return_conditional_losses_156456
В
FullArgSpec
args
jself
jIn
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
2
)__inference_conv2d_9_layer_call_fn_155728з
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *7Ђ4
2/+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
Ѓ2 
D__inference_conv2d_9_layer_call_and_return_conditional_losses_155720з
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *7Ђ4
2/+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
д2б
*__inference_flatten_4_layer_call_fn_156474Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
я2ь
E__inference_flatten_4_layer_call_and_return_conditional_losses_156469Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
в2Я
(__inference_dense_8_layer_call_fn_156491Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
э2ъ
C__inference_dense_8_layer_call_and_return_conditional_losses_156484Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
в2Я
(__inference_dense_9_layer_call_fn_156508Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
э2ъ
C__inference_dense_9_layer_call_and_return_conditional_losses_156501Ђ
В
FullArgSpec
args
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs 
kwonlydefaults
 
annotationsЊ *
 
3B1
$__inference_signature_wrapper_156036input_5
Ь2ЩЦ
НВЙ
FullArgSpec
args
jself
jinputs
varargs
 
varkwjkwargs
defaults 

kwonlyargs

jtraining%
kwonlydefaultsЊ

trainingp 
annotationsЊ *
 
Ь2ЩЦ
НВЙ
FullArgSpec
args
jself
jinputs
varargs
 
varkwjkwargs
defaults 

kwonlyargs

jtraining%
kwonlydefaultsЊ

trainingp 
annotationsЊ *
 
!__inference__wrapped_model_155687y
$%*+8Ђ5
.Ђ+
)&
input_5џџџџџџџџџ
Њ "1Њ.
,
dense_9!
dense_9џџџџџџџџџ
й
D__inference_conv2d_8_layer_call_and_return_conditional_losses_155699IЂF
?Ђ<
:7
inputs+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
Њ "?Ђ<
52
0+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 Б
)__inference_conv2d_8_layer_call_fn_155707IЂF
?Ђ<
:7
inputs+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
Њ "2/+џџџџџџџџџџџџџџџџџџџџџџџџџџџй
D__inference_conv2d_9_layer_call_and_return_conditional_losses_155720IЂF
?Ђ<
:7
inputs+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
Њ "?Ђ<
52
0+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
 Б
)__inference_conv2d_9_layer_call_fn_155728IЂF
?Ђ<
:7
inputs+џџџџџџџџџџџџџџџџџџџџџџџџџџџ
Њ "2/+џџџџџџџџџџџџџџџџџџџџџџџџџџџЅ
C__inference_dense_8_layer_call_and_return_conditional_losses_156484^$%0Ђ-
&Ђ#
!
inputsџџџџџџџџџР
Њ "&Ђ#

0џџџџџџџџџ
 }
(__inference_dense_8_layer_call_fn_156491Q$%0Ђ-
&Ђ#
!
inputsџџџџџџџџџР
Њ "џџџџџџџџџЄ
C__inference_dense_9_layer_call_and_return_conditional_losses_156501]*+0Ђ-
&Ђ#
!
inputsџџџџџџџџџ
Њ "%Ђ"

0џџџџџџџџџ

 |
(__inference_dense_9_layer_call_fn_156508P*+0Ђ-
&Ђ#
!
inputsџџџџџџџџџ
Њ "џџџџџџџџџ
Њ
E__inference_flatten_4_layer_call_and_return_conditional_losses_156469a7Ђ4
-Ђ*
(%
inputsџџџџџџџџџ
Њ "&Ђ#

0џџџџџџџџџР
 
*__inference_flatten_4_layer_call_fn_156474T7Ђ4
-Ђ*
(%
inputsџџџџџџџџџ
Њ "џџџџџџџџџРС
H__inference_sequential_4_layer_call_and_return_conditional_losses_155921u
$%*+@Ђ=
6Ђ3
)&
input_5џџџџџџџџџ
p

 
Њ "%Ђ"

0џџџџџџџџџ

 С
H__inference_sequential_4_layer_call_and_return_conditional_losses_155941u
$%*+@Ђ=
6Ђ3
)&
input_5џџџџџџџџџ
p 

 
Њ "%Ђ"

0џџџџџџџџџ

 Р
H__inference_sequential_4_layer_call_and_return_conditional_losses_156175t
$%*+?Ђ<
5Ђ2
(%
inputsџџџџџџџџџ
p

 
Њ "%Ђ"

0џџџџџџџџџ

 Р
H__inference_sequential_4_layer_call_and_return_conditional_losses_156314t
$%*+?Ђ<
5Ђ2
(%
inputsџџџџџџџџџ
p 

 
Њ "%Ђ"

0џџџџџџџџџ

 
-__inference_sequential_4_layer_call_fn_155977h
$%*+@Ђ=
6Ђ3
)&
input_5џџџџџџџџџ
p

 
Њ "џџџџџџџџџ

-__inference_sequential_4_layer_call_fn_156012h
$%*+@Ђ=
6Ђ3
)&
input_5џџџџџџџџџ
p 

 
Њ "џџџџџџџџџ

-__inference_sequential_4_layer_call_fn_156329g
$%*+?Ђ<
5Ђ2
(%
inputsџџџџџџџџџ
p

 
Њ "џџџџџџџџџ

-__inference_sequential_4_layer_call_fn_156344g
$%*+?Ђ<
5Ђ2
(%
inputsџџџџџџџџџ
p 

 
Њ "џџџџџџџџџ
­
$__inference_signature_wrapper_156036
$%*+CЂ@
Ђ 
9Њ6
4
input_5)&
input_5џџџџџџџџџ"1Њ.
,
dense_9!
dense_9џџџџџџџџџ
В
F__inference_top_pool_1_layer_call_and_return_conditional_losses_156456h3Ђ0
)Ђ&
$!
Inџџџџџџџџџ
Њ "-Ђ*
# 
0џџџџџџџџџ


 
+__inference_top_pool_1_layer_call_fn_156463[3Ђ0
)Ђ&
$!
Inџџџџџџџџџ
Њ " џџџџџџџџџ

