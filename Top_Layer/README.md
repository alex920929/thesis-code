# Pooling layer

This folder contains the script to replicate the deep learning experiments reported in the thesis.

## Training

Launching the script

```
python3 main.py
```

you will train the two architectures described in the thesis with the different pooling layers, on three datasets, namely MNIST, FashionMNIST and CIFAR10. The models, together with some information about the training process will be stored in some one folder for each dataset. To access the the information using tensorboard, you can run the following line

```
tensorboard --logdir 'path_to_directory'
```
To create the saliency maps pdf files, you can run the script
```
python3 create_saliency.py
```
